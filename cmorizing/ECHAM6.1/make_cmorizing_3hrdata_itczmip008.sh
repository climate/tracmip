#!/bin/bash

# set variables

expin=itczmip008
expout=Land4xCO2

model=echam6-1

year1=0133
year2=0135

level='100000,92500,85000,70000,60000,50000,40000,30000,25000,20000,15000,10000,7000,5000,3000,2000,1000'


# merge BOT and ATM daily files
cdo -O mergetime BOT_3hr_${expin}_013[345] BOT_3hr_${expin}_${year1}_${year2}
cdo -O mergetime ATM_3hr_${expin}_013[345] ATM_3hr_${expin}_${year1}_${year2}

############## 2d data #####################################

# tas
cdo -r -f nc -t echam6 -setname,tas -selvar,temp2 BOT_3hr_${expin}_${year1}_${year2} \
 	tas_A3hr_${model}_${expout}_r1i1p1_${year1}01-${year2}12.nc
ncatted -O -a long_name,tas,o,c,air_temperature tas_A3hr_${model}_${expout}_r1i1p1_${year1}01-${year2}12.nc

# ts
cdo -r -f nc -t echam6 -setname,ts -selvar,tsurf BOT_3hr_${expin}_${year1}_${year2} \
 	ts_A3hr_${model}_${expout}_r1i1p1_${year1}01-${year2}12.nc
ncatted -O -a long_name,ts,o,c,surface_temperature ts_A3hr_${model}_${expout}_r1i1p1_${year1}01-${year2}12.nc

# tasmin
cdo -r -f nc -t echam6 -setname,tasmin -selvar,t2min BOT_3hr_${expin}_${year1}_${year2} \
 	tasmin_A3hr_${model}_${expout}_r1i1p1_${year1}01-${year2}12.nc
ncatted -O -a long_name,tasmin,o,c,air_temperature tasmin_A3hr_${model}_${expout}_r1i1p1_${year1}01-${year2}12.nc

# tasmax
cdo -r -f nc -t echam6 -setname,tasmax -selvar,t2max BOT_3hr_${expin}_${year1}_${year2} \
 	tasmax_A3hr_${model}_${expout}_r1i1p1_${year1}01-${year2}12.nc
ncatted -O -a long_name,tasmax,o,c,air_temperature tasmax_A3hr_${model}_${expout}_r1i1p1_${year1}01-${year2}12.nc

# psl
cdo -r -f nc -t echam6 -setname,psl -selvar,slp BOT_3hr_${expin}_${year1}_${year2} \
 	psl_A3hr_${model}_${expout}_r1i1p1_${year1}01-${year2}12.nc
ncatted -O -a long_name,psl,o,c,air_pressure_at_sea_level psl_A3hr_${model}_${expout}_r1i1p1_${year1}01-${year2}12.nc

# ps
cdo -r -f nc -t echam6 -setname,ps -selvar,aps BOT_3hr_${expin}_${year1}_${year2} \
 	ps_A3hr_${model}_${expout}_r1i1p1_${year1}01-${year2}12.nc
ncatted -O -a long_name,ps,o,c,surface_air_pressure ps_A3hr_${model}_${expout}_r1i1p1_${year1}01-${year2}12.nc

# uas
cdo -r -f nc -t echam6 -setname,uas -selvar,u10 BOT_3hr_${expin}_${year1}_${year2} \
 	uas_A3hr_${model}_${expout}_r1i1p1_${year1}01-${year2}12.nc
ncatted -O -a long_name,uas,o,c,eastward_wind uas_A3hr_${model}_${expout}_r1i1p1_${year1}01-${year2}12.nc

# vas
cdo -r -f nc -t echam6 -setname,vas -selvar,v10 BOT_3hr_${expin}_${year1}_${year2} \
 	vas_A3hr_${model}_${expout}_r1i1p1_${year1}01-${year2}12.nc
ncatted -O -a long_name,vas,o,c,northward_wind vas_A3hr_${model}_${expout}_r1i1p1_${year1}01-${year2}12.nc

# sfcWind
cdo -r -f nc -t echam6 -setname,sfcWind -selvar,wind10 BOT_3hr_${expin}_${year1}_${year2} \
 	sfcWind_A3hr_${model}_${expout}_r1i1p1_${year1}01-${year2}12.nc
ncatted -O -a long_name,sfcWind,o,c,wind_speed sfcWind_A3hr_${model}_${expout}_r1i1p1_${year1}01-${year2}12.nc

# pr
cdo -r -f nc -t echam6 -setname,pr -selvar,precip BOT_3hr_${expin}_${year1}_${year2} \
 	pr_A3hr_${model}_${expout}_r1i1p1_${year1}01-${year2}12.nc
ncatted -O -a long_name,pr,o,c,precipitation_flux pr_A3hr_${model}_${expout}_r1i1p1_${year1}01-${year2}12.nc

# prsn
cdo -r -f nc -t echam6 -setname,prsn -selvar,aprs BOT_3hr_${expin}_${year1}_${year2} \
 	prsn_A3hr_${model}_${expout}_r1i1p1_${year1}01-${year2}12.nc
ncatted -O -a long_name,prsn,o,c,snowfall_flux prsn_A3hr_${model}_${expout}_r1i1p1_${year1}01-${year2}12.nc

# prc
cdo -r -f nc -t echam6 -setname,prc -selvar,aprc BOT_3hr_${expin}_${year1}_${year2} \
 	prc_A3hr_${model}_${expout}_r1i1p1_${year1}01-${year2}12.nc
ncatted -O -a long_name,prc,o,c,convective_precipitation_flux prc_A3hr_${model}_${expout}_r1i1p1_${year1}01-${year2}12.nc

# evspsbl
cdo -r -f nc -t echam6 -mulc,-1 -setname,evspsbl -selvar,evap BOT_3hr_${expin}_${year1}_${year2} \
 	evspsbl_A3hr_${model}_${expout}_r1i1p1_${year1}01-${year2}12.nc
ncatted -O -a long_name,evspsbl,o,c,water_evaporation_flux evspsbl_A3hr_${model}_${expout}_r1i1p1_${year1}01-${year2}12.nc

# tauu
cdo -r -f nc -t echam6 -setname,tauu -selvar,ustr BOT_3hr_${expin}_${year1}_${year2} \
 	tauu_A3hr_${model}_${expout}_r1i1p1_${year1}01-${year2}12.nc
ncatted -O -a long_name,tauu,o,c,surface_downward_eastward_stress tauu_A3hr_${model}_${expout}_r1i1p1_${year1}01-${year2}12.nc

# tauv
cdo -r -f nc -t echam6 -setname,tauv -selvar,vstr BOT_3hr_${expin}_${year1}_${year2} \
 	tauv_A3hr_${model}_${expout}_r1i1p1_${year1}01-${year2}12.nc
ncatted -O -a long_name,tauv,o,c,surface_downward_northward_stress tauv_A3hr_${model}_${expout}_r1i1p1_${year1}01-${year2}12.nc

# hfls
cdo -r -f nc -t echam6 -mulc,-1 -setname,hfls -selvar,ahfl BOT_3hr_${expin}_${year1}_${year2} \
 	hfls_A3hr_${model}_${expout}_r1i1p1_${year1}01-${year2}12.nc
ncatted -O -a long_name,hfls,o,c,surface_upward_latent_heat_flux hfls_A3hr_${model}_${expout}_r1i1p1_${year1}01-${year2}12.nc

# hfss
cdo -r -f nc -t echam6 -mulc,-1 -setname,hfss -selvar,ahfs BOT_3hr_${expin}_${year1}_${year2} \
 	hfss_A3hr_${model}_${expout}_r1i1p1_${year1}01-${year2}12.nc
ncatted -O -a long_name,hfss,o,c,surface_upward_sensible_heat_flux hfss_A3hr_${model}_${expout}_r1i1p1_${year1}01-${year2}12.nc

#rlds
cdo -r -f nc -t echam6 -setname,rlds -sub -selvar,trads BOT_3hr_${expin}_${year1}_${year2} \
        -selvar,tradsu BOT_3hr_${expin}_${year1}_${year2} rlds_A3hr_${model}_${expout}_r1i1p1_${year1}01-${year2}12.nc
ncatted -O -a long_name,rlds,o,c,surface_downwelling_longwave_flux_in_air rlds_A3hr_${model}_${expout}_r1i1p1_${year1}01-${year2}12.nc

#rlus
cdo -r -f nc -t echam6 -mulc,-1 -setname,rlus -selvar,tradsu BOT_3hr_${expin}_${year1}_${year2} \
        rlus_A3hr_${model}_${expout}_r1i1p1_${year1}01-${year2}12.nc
ncatted -O -a long_name,rlus,o,c,surface_upwelling_longwave_flux_in_air rlus_A3hr_${model}_${expout}_r1i1p1_${year1}01-${year2}12.nc

#rsds
cdo -r -f nc -t echam6 -setname,rsds -sub -selvar,srads BOT_3hr_${expin}_${year1}_${year2} \
        -selvar,sradsu BOT_3hr_${expin}_${year1}_${year2} rsds_A3hr_${model}_${expout}_r1i1p1_${year1}01-${year2}12.nc
ncatted -O -a long_name,rsds,o,c,surface_downwelling_shortwave_flux_in_air rsds_A3hr_${model}_${expout}_r1i1p1_${year1}01-${year2}12.nc

#rsus
cdo -r -f nc -t echam6 -mulc,-1 -setname,rsus -selvar,sradsu BOT_3hr_${expin}_${year1}_${year2} \
        rsus_A3hr_${model}_${expout}_r1i1p1_${year1}01-${year2}12.nc
ncatted -O -a long_name,rsus,o,c,surface_upwelling_shortwave_flux_in_air rsus_A3hr_${model}_${expout}_r1i1p1_${year1}01-${year2}12.nc

########### clear-sky sw surface fluxes (start) 
#need to calcule effective sfc albedo first
rm -f sfcalbedo
cdo div rsus_A3hr_${model}_${expout}_r1i1p1_${year1}01-${year2}12.nc \
        rsds_A3hr_${model}_${expout}_r1i1p1_${year1}01-${year2}12.nc \
        sfcalbedo
#rsdscs
cdo -r -f nc -t echam6 -setmisstoc,0 -setname,rsdscs -div -selvar,srafs BOT_3hr_${expin}_${year1}_${year2} \
        -addc,1 -mulc,-1 sfcalbedo rsdscs_A3hr_${model}_${expout}_r1i1p1_${year1}01-${year2}12.nc 
ncatted -O -a long_name,rsdscs,o,c,surface_downwelling_shortwave_flux_in_air_assuming_clear_sky rsdscs_A3hr_${model}_${expout}_r1i1p1_${year1}01-${year2}12.nc

#rsuscs
cdo -r -f nc -t echam6 -mulc,-1 -setname,rsuscs -sub -selvar,srafs BOT_3hr_${expin}_${year1}_${year2} \
        rsdscs_A3hr_${model}_${expout}_r1i1p1_${year1}01-${year2}12.nc \
        rsuscs_A3hr_${model}_${expout}_r1i1p1_${year1}01-${year2}12.nc
ncatted -O -a long_name,rsuscs,o,c,surface_upwelling_shortwave_flux_in_air_assuming_clear_sky rsuscs_A3hr_${model}_${expout}_r1i1p1_${year1}01-${year2}12.nc

rm -f sfcalbedo
########### clear-sky sw surface fluxes (end)

#rldscs
cdo -r -f nc -t echam6 -setname,rldscs -sub -selvar,trafs BOT_3hr_${expin}_${year1}_${year2} \
        -selvar,tradsu BOT_3hr_${expin}_${year1}_${year2} rldscs_A3hr_${model}_${expout}_r1i1p1_${year1}01-${year2}12.nc
ncatted -O -a long_name,rldscs,o,c,surface_downwelling_longwave_flux_in_air_assuming_clear_sky rldscs_A3hr_${model}_${expout}_r1i1p1_${year1}01-${year2}12.nc

# rsdt 
cdo -r -f nc -t echam6 -setname,rsdt -selvar,srad0d BOT_3hr_${expin}_${year1}_${year2} \
        rsdt_A3hr_${model}_${expout}_r1i1p1_${year1}01-${year2}12.nc
ncatted -O -a long_name,rsdt,o,c,toa_incoming_shortwave_flux rsdt_A3hr_${model}_${expout}_r1i1p1_${year1}01-${year2}12.nc

# rsut 
cdo -r -f nc -t echam6 -mulc,-1 -setname,rsut -selvar,srad0u BOT_3hr_${expin}_${year1}_${year2} \
        rsut_A3hr_${model}_${expout}_r1i1p1_${year1}01-${year2}12.nc
ncatted -O -a long_name,rsut,o,c,toa_outgoing_shortwave_flux rsut_A3hr_${model}_${expout}_r1i1p1_${year1}01-${year2}12.nc

# rlut
cdo -r -f nc -t echam6 -mulc,-1 -setname,rlut -selvar,trad0 BOT_3hr_${expin}_${year1}_${year2} \
        rlut_A3hr_${model}_${expout}_r1i1p1_${year1}01-${year2}12.nc
ncatted -O -a long_name,rlut,o,c,toa_outgoing_longwave_flux rlut_A3hr_${model}_${expout}_r1i1p1_${year1}01-${year2}12.nc

# rlutcs
cdo -r -f nc -t echam6 -mulc,-1 -setname,rlutcs -selvar,traf0 BOT_3hr_${expin}_${year1}_${year2} \
        rlutcs_A3hr_${model}_${expout}_r1i1p1_${year1}01-${year2}12.nc
ncatted -O -a long_name,rlutcs,o,c,toa_outgoing_longwave_flux_assuming_clear_sky rlutcs_A3hr_${model}_${expout}_r1i1p1_${year1}01-${year2}12.nc

# rsutcs
cdo -r -f nc -t echam6 -mulc,-1 -setname,rsutcs -sub -selvar,sraf0 BOT_3hr_${expin}_${year1}_${year2} \
        -selvar,srad0d BOT_3hr_${expin}_${year1}_${year2} rsutcs_A3hr_${model}_${expout}_r1i1p1_${year1}01-${year2}12.nc
ncatted -O -a long_name,rsutcs,o,c,toa_outgoing_shortwave_flux_assuming_clear_sky rsutcs_A3hr_${model}_${expout}_r1i1p1_${year1}01-${year2}12.nc

# prw
cdo -r -f nc -t echam6 -setname,prw -selvar,qvi BOT_3hr_${expin}_${year1}_${year2} \
 	prw_A3hr_${model}_${expout}_r1i1p1_${year1}01-${year2}12.nc
ncatted -O -a long_name,prw,o,c,atmosphere_water_vapor_content prw_A3hr_${model}_${expout}_r1i1p1_${year1}01-${year2}12.nc

# clt
cdo -r -f nc -t echam6 -mulc,100 -setname,clt -selvar,aclcov BOT_3hr_${expin}_${year1}_${year2} \
 	clt_A3hr_${model}_${expout}_r1i1p1_${year1}01-${year2}12.nc
ncatted -O -a long_name,clt,o,c,cloud_area_fraction clt_A3hr_${model}_${expout}_r1i1p1_${year1}01-${year2}12.nc

# clwvi
cdo -r -f nc -t echam6 -setname,clwvi -selvar,xlvi BOT_3hr_${expin}_${year1}_${year2} \
 	clwvi_A3hr_${model}_${expout}_r1i1p1_${year1}01-${year2}12.nc
ncatted -O -a long_name,clwvi,o,c,atmosphere_cloud_condensed_water_content clwvi_A3hr_${model}_${expout}_r1i1p1_${year1}01-${year2}12.nc

# clivi
cdo -r -f nc -t echam6 -setname,clivi -selvar,xivi BOT_3hr_${expin}_${year1}_${year2} \
 	clivi_A3hr_${model}_${expout}_r1i1p1_${year1}01-${year2}12.nc      
ncatted -O -a long_name,clivi,o,c,atmosphere_cloud_ice_content clivi_A3hr_${model}_${expout}_r1i1p1_${year1}01-${year2}12.nc

############## 3d data #####################################

# cl
cdo -r -f nc -t echam6 -mulc,100 -setname,cl -selvar,aclcac -sellevel,${level} ATM_3hr_${expin}_${year1}_${year2} \
        cl_A3hr_${model}_${expout}_r1i1p1_${year1}01-${year2}12.nc
ncatted -O -a long_name,cl,o,c,cloud_area_fraction_in_atmosphere_layer cl_A3hr_${model}_${expout}_r1i1p1_${year1}01-${year2}12.nc
ncrename -d lev,plev -v lev,plev cl_A3hr_${model}_${expout}_r1i1p1_${year1}01-${year2}12.nc

# clw
cdo -r -f nc -t echam6 -mulc,100 -setname,clw -selvar,xl -sellevel,${level} ATM_3hr_${expin}_${year1}_${year2} \
        clw_A3hr_${model}_${expout}_r1i1p1_${year1}01-${year2}12.nc
ncatted -O -a long_name,clw,o,c,mass_fraction_of_cloud_liquid_water_in_air clw_A3hr_${model}_${expout}_r1i1p1_${year1}01-${year2}12.nc
ncrename -d lev,plev -v lev,plev clw_A3hr_${model}_${expout}_r1i1p1_${year1}01-${year2}12.nc

# cli
cdo -r -f nc -t echam6 -mulc,100 -setname,cli -selvar,xi -sellevel,${level} ATM_3hr_${expin}_${year1}_${year2} \
        cli_A3hr_${model}_${expout}_r1i1p1_${year1}01-${year2}12.nc
ncatted -O -a long_name,cli,o,c,mass_fraction_of_cloud_ice_in_air cli_A3hr_${model}_${expout}_r1i1p1_${year1}01-${year2}12.nc
ncrename -d lev,plev -v lev,plev cli_A3hr_${model}_${expout}_r1i1p1_${year1}01-${year2}12.nc

# ta
cdo -r -f nc -t echam6 -setname,ta -selvar,t -sellevel,${level} ATM_3hr_${expin}_${year1}_${year2} \
        ta_A3hr_${model}_${expout}_r1i1p1_${year1}01-${year2}12.nc
ncatted -O -a long_name,ta,o,c,air_temperature ta_A3hr_${model}_${expout}_r1i1p1_${year1}01-${year2}12.nc
ncrename -d lev,plev -v lev,plev ta_A3hr_${model}_${expout}_r1i1p1_${year1}01-${year2}12.nc

# ua
cdo -r -f nc -t echam6 -setname,ua -selvar,u -sellevel,${level} ATM_3hr_${expin}_${year1}_${year2} \
        ua_A3hr_${model}_${expout}_r1i1p1_${year1}01-${year2}12.nc
ncatted -O -a long_name,ua,o,c,eastward_wind ua_A3hr_${model}_${expout}_r1i1p1_${year1}01-${year2}12.nc
ncrename -d lev,plev -v lev,plev ua_A3hr_${model}_${expout}_r1i1p1_${year1}01-${year2}12.nc

# va
cdo -r -f nc -t echam6 -setname,va -selvar,v -sellevel,${level} ATM_3hr_${expin}_${year1}_${year2} \
        va_A3hr_${model}_${expout}_r1i1p1_${year1}01-${year2}12.nc
ncatted -O -a long_name,va,o,c,northward_wind va_A3hr_${model}_${expout}_r1i1p1_${year1}01-${year2}12.nc
ncrename -d lev,plev -v lev,plev va_A3hr_${model}_${expout}_r1i1p1_${year1}01-${year2}12.nc

# hus 
cdo -r -f nc -t echam6 -setname,hus -selvar,q -sellevel,${level} ATM_3hr_${expin}_${year1}_${year2} \
        hus_A3hr_${model}_${expout}_r1i1p1_${year1}01-${year2}12.nc
ncatted -O -a long_name,hus,o,c,specific_humidity hus_A3hr_${model}_${expout}_r1i1p1_${year1}01-${year2}12.nc
ncrename -d lev,plev -v lev,plev hus_A3hr_${model}_${expout}_r1i1p1_${year1}01-${year2}12.nc

# hur 
cdo -r -f nc -t echam6 -mulc,100 -setname,hur -selvar,rhumidity -sellevel,${level} ATM_3hr_${expin}_${year1}_${year2} \
        hur_A3hr_${model}_${expout}_r1i1p1_${year1}01-${year2}12.nc
ncatted -O -a long_name,hur,o,c,relative_humidity hur_A3hr_${model}_${expout}_r1i1p1_${year1}01-${year2}12.nc
ncrename -d lev,plev -v lev,plev hur_A3hr_${model}_${expout}_r1i1p1_${year1}01-${year2}12.nc

# wap 
cdo -r -f nc -t echam6 -setname,wap -selvar,omega -sellevel,${level} ATM_3hr_${expin}_${year1}_${year2} \
        wap_A3hr_${model}_${expout}_r1i1p1_${year1}01-${year2}12.nc
ncatted -O -a long_name,wap,o,c,lagrangian_tendency_or_air_pressure wap_A3hr_${model}_${expout}_r1i1p1_${year1}01-${year2}12.nc
ncrename -d lev,plev -v lev,plev wap_A3hr_${model}_${expout}_r1i1p1_${year1}01-${year2}12.nc

# zg 
cdo -r -f nc -t echam6 -setname,zg -selvar,geopoth -sellevel,${level} ATM_3hr_${expin}_${year1}_${year2} \
        zg_A3hr_${model}_${expout}_r1i1p1_${year1}01-${year2}12.nc
ncatted -O -a long_name,zg,o,c,geopotential_height zg_A3hr_${model}_${expout}_r1i1p1_${year1}01-${year2}12.nc
ncrename -d lev,plev -v lev,plev zg_A3hr_${model}_${expout}_r1i1p1_${year1}01-${year2}12.nc


