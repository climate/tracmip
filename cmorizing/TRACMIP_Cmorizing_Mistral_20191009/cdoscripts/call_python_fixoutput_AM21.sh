#!/bin/bash
# note the following:
# we use python xarray to fix dimensions of AM21 output fields

set -e

# we use the anaconda bleeding edge python version
module unload netcdf_c/4.3.2-gcc48
module load anaconda3/bleeding_edge

rm -f fixdimensions_AM21.py
cat << 'EOF' >> fixdimensions_AM21.py

import xarray as xr
import numpy as np
import sys
import cftime
import os

fpath=sys.argv[1]
fname=sys.argv[2]
freq=sys.argv[3] # for time axis, depends on whether data is monthly, daily, or 8xdaily
print(fpath,fname)

# grid info for AM2 is missing in umiami files and thus is stored here
lat = np.array([-89.49438, -87.97753, -85.95506, -83.93259, -81.91011, -79.88764, \
    -77.86517, -75.8427, -73.82022, -71.79775, -69.77528, -67.75281,                 \
    -65.73034, -63.70787, -61.68539, -59.66292, -57.64045, -55.61798,                \
    -53.5955, -51.57303, -49.55056, -47.52809, -45.50562, -43.48315,                 \
    -41.46067, -39.4382, -37.41573, -35.39326, -33.37078, -31.34831,                 \
    -29.32584, -27.30337, -25.2809, -23.25843, -21.23595, -19.21348,                 \
    -17.19101, -15.16854, -13.14607, -11.1236, -9.101124, -7.078652,                 \
    -5.05618, -3.033708, -1.011236, 1.011236, 3.033708, 5.05618, 7.078652,           \
    9.101124, 11.1236, 13.14607, 15.16854, 17.19101, 19.21348, 21.23595,             \
    23.25843, 25.2809, 27.30337, 29.32584, 31.34831, 33.37078, 35.39326,             \
    37.41573, 39.4382, 41.46067, 43.48315, 45.50562, 47.52809, 49.55056,             \
    51.57303, 53.5955, 55.61798, 57.64045, 59.66292, 61.68539, 63.70787,             \
    65.73034, 67.75281, 69.77528, 71.79775, 73.82022, 75.8427, 77.86517,             \
    79.88764, 81.91011, 83.93259, 85.95506, 87.97753, 89.49438])
lon = np.array([1.25, 3.75, 6.25, 8.75, 11.25, 13.75, 16.25, 18.75, 21.25, 23.75, \
    26.25, 28.75, 31.25, 33.75, 36.25, 38.75, 41.25, 43.75, 46.25, 48.75,            \
    51.25, 53.75, 56.25, 58.75, 61.25, 63.75, 66.25, 68.75, 71.25, 73.75,            \
    76.25, 78.75, 81.25, 83.75, 86.25, 88.75, 91.25, 93.75, 96.25, 98.75,            \
    101.25, 103.75, 106.25, 108.75, 111.25, 113.75, 116.25, 118.75, 121.25,          \
    123.75, 126.25, 128.75, 131.25, 133.75, 136.25, 138.75, 141.25, 143.75,          \
    146.25, 148.75, 151.25, 153.75, 156.25, 158.75, 161.25, 163.75, 166.25,          \
    168.75, 171.25, 173.75, 176.25, 178.75, 181.25, 183.75, 186.25, 188.75,          \
    191.25, 193.75, 196.25, 198.75, 201.25, 203.75, 206.25, 208.75, 211.25,          \
    213.75, 216.25, 218.75, 221.25, 223.75, 226.25, 228.75, 231.25, 233.75,          \
    236.25, 238.75, 241.25, 243.75, 246.25, 248.75, 251.25, 253.75, 256.25,          \
    258.75, 261.25, 263.75, 266.25, 268.75, 271.25, 273.75, 276.25, 278.75,          \
    281.25, 283.75, 286.25, 288.75, 291.25, 293.75, 296.25, 298.75, 301.25,          \
    303.75, 306.25, 308.75, 311.25, 313.75, 316.25, 318.75, 321.25, 323.75,          \
    326.25, 328.75, 331.25, 333.75, 336.25, 338.75, 341.25, 343.75, 346.25,          \
    348.75, 351.25, 353.75, 356.25, 358.75])    
plev = np.array([1000.0, 2000.0, 3000.0, 5000.0, 7000.0, 10000.0, 15000.0, 20000.0, 25000.0, 30000.0, 
          40000.0, 50000.0, 60000.0, 70000.0, 85000.0, 92500.0, 100000.0])

# dictionary of variables' units
dict_units={'tas':'K', 'clivi':'kg m-2', 'clt':'%', 'clwi':'kg m-2', 'evspsbl':'kg m-2 s-1', 'hfls':'W m-2', 'hfss':'W m-2',
            'hurs':'%', 'huss':'kg/kg', 'pr':'kg m-2 s-1', 'prc':'kg m-2 s-1', 'prsn':'kg m-2 s-1','prw':'kg m-2', 'ps':'Pa',
            'psl':'Pa', 'rlds':'W m-2', 'rldscs':'W m-2', 'rlus':'W m-2', 'rlut':'W m-2', 'rlutcs':'W m-2', 'rsds':'W m-2', 
            'rsdscs':'W m-2', 'rsdt':'W m-2', 'rsus':'W m-2', 'rsuscs':'W m-2',
            'rsut':'W m-2', 'rsutcs':'W m-2', 'rtmt':'W m-2', 'sfcWind':'m/s', 'tauu':'Pa', 'tauv':'Pa', 'ts':'K', 
            'uas':'m/s', 'vas':'m/s',
            'hur':'%','hus':'kg/kg', 'ta':'K', 'ua':'m/s', 'va':'m/s', 'wap':'Pa/s', 'zg':'m', 'cl':'1', 'cli':'kg m-2', 'clw':'kg m-2'}

# dictionary of variables' directon
dict_direct={'tas':' ', 'clivi':' ', 'clt':' ', 'clwi':' ', 'evspsbl':' ', 'hfls':'u', 'hfss':'u',
            'hurs':' ', 'huss':' ', 'pr':' ', 'prc':' ', 'prsn':' ','prw':' ', 'ps':' ',
            'psl':' ', 'rlds':'d', 'rldscs':'d', 'rlus':'u', 'rlut':'u', 'rlutcs':'u', 'rsds':'d', 
            'rsdscs':'d', 'rsdt':'d', 'rsus':'u', 'rsuscs':'u',
            'rsut':'u', 'rsutcs':'u', 'rtmt':'d', 'sfcWind':' ', 'tauu':'d', 'tauv':'d', 'ts':' ', 
            'uas':' ', 'vas':' ',
            'hur':' ','hus':' ', 'ta':' ', 'ua':' ', 'va':' ', 'wap':' ', 'zg':' ', 'cl':' ', 'cli':' ', 'clw':' '}

ds = xr.open_dataset(fpath+'/'+fname+'.nc')

varname_list2d=['tas', 'clivi', 'clt', 'clwi', 'evspsbl', 'hfls', 'hfss', 'hurs', 'huss', 'pr', 'prc', 'prsn', 'prw', 'ps', 
         'psl', 'rlds', 'rldscs', 'rlus', 'rlut', 'rlutcs', 'rsds', 'rsdscs', 'rsdt', 'rsus', 'rsuscs',
         'rsut', 'rsutcs', 'rtmt', 'sfcWind', 'tauu', 'tauv', 'ts', 'uas', 'vas']

varname_list3d=['hur', 'hus', 'ta', 'ua', 'va', 'wap', 'zg', 'cl', 'cli', 'clw']

for varname in varname_list2d:

    # check if variable is in data set: only in this case go ahead with the script
    if (varname in ds.keys()):

        #ntim = ds['time'].values.size
        ntim = ds[varname].values.shape[0]
        if freq=='month':
            time = cftime.num2date(30*np.arange(ntim), units='days since 0001-01-16,00:00:00', calendar='360_day')
        elif freq=='daily':
            time = cftime.num2date(np.arange(ntim), units='days since 0001-01-01,12:00:00', calendar='360_day')
        elif freq=='8xdaily':
            time = cftime.num2date(np.arange(ntim), units='hours since 0001-01-01,01:30:00', calendar='360_day')
        
        # create new dataset with variable having fixed dimensions, and save to disk as netcdf
        ds_new = xr.Dataset()
        # convert varint into dataarray
        da_varnew = xr.DataArray(ds[varname].values, name=varname, dims=('time','lat','lon'), 
                      coords={'time': time, 'lat': lat, 'lon': lon}, 
                      attrs={'units': dict_units.get(varname), 'p': dict_direct.get(varname)})
        ds_new[varname]=da_varnew
        ds_new.to_netcdf(path=fpath+'/'+fname+'.'+varname+'.fixed_dimensions.nc')

for varname in varname_list3d:

    # check if variable is in data set: only in this case go ahead with the script
    if (varname in ds.keys()):

        #ntim = ds['time'].values.size
        ntim = ds[varname].values.shape[0]
        if freq=='month':
            time = cftime.num2date(30*np.arange(ntim), units='days since 0001-01-16,00:00:00', calendar='360_day')
        elif freq=='daily':
            time = cftime.num2date(np.arange(ntim), units='days since 0001-01-01,12:00:00', calendar='360_day')
        elif freq=='8xdaily':
            time = cftime.num2date(np.arange(ntim), units='hours since 0001-01-01,01:30:00', calendar='360_day')
        
        # create new dataset with variable having fixed dimensions, and save to disk as netcdf
        ds_new = xr.Dataset()
        # convert varint into dataarray
        da_varnew = xr.DataArray(ds[varname].values, name=varname, dims=('time','plev','lat','lon'), coords={'time': time, 'plev': plev, 'lat': lat, 'lon': lon})
        ds_new[varname]=da_varnew
        ds_new.to_netcdf(path=fpath+'/'+fname+'.'+varname+'.fixed_dimensions.nc')

EOF

# there is no LandOrbit data
for EXPID in AquaControl Aqua4xCO2 LandControl Land4xCO2; do

   python3 fixdimensions_AM21.py "/work/bm0834/b380459/TRACMIP/AM2/${EXPID}" "${EXPID}_month"   "month"
   python3 fixdimensions_AM21.py "/work/bm0834/b380459/TRACMIP/AM2/${EXPID}" "${EXPID}_daily"   "daily"
   python3 fixdimensions_AM21.py "/work/bm0834/b380459/TRACMIP/AM2/${EXPID}" "${EXPID}_8xdaily" "8xdaily"

done

rm -f fixdimensions_AM21.py
