for EXPID in AquaControl Aqua4xCO2 LandControl Land4xCO2 LandOrbit; do
   for STREAM in monthly daily 3hourly; do
      for VAR in CLOUD CLDICE CLDLIQ RELHUM Q T U V OMEGA Z3; do
         sbatch call_plevint_python_CAM4.sh_EXPID_STREAM_VAR_commandline ${EXPID} ${STREAM} ${VAR}
      done
    done
done
