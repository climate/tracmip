#!/bin/bash
#=============================================================================
# btc batch job parameters
#-----------------------------------------------------------------------------
#SBATCH --account=bm0834
#SBATCH --job-name=call_call_make-postvariables_Amon_Aday_A3hr_CAM4.sh
#SBATCH --partition=compute,compute2
#SBATCH --ntasks=2
#SBATCH --cpus-per-task=8      # Specify number of CPUs per task
#SBATCH --output=/pf/b/b380459/TRACMIP_Cmorizing/cdoscripts/call_make-postvariables_Amon_Aday_A3hr_CAM4.sh.%j.o
#SBATCH --error=/pf/b/b380459/TRACMIP_Cmorizing/cdoscripts/call_make-postvariables_Amon_Aday_A3hr_CAM4.sh.%j.o
#SBATCH --exclusive
#SBATCH --time=08:00:00
#=============================================================================

# we manually calculate those variables that are not part in the CAM4 raw output but can be calculated from other variables
# this includes (info from Brian Rose):
# 
# Precipitation (all in kg/m2/s or equivalently in m/s):
# pr = PRECC+PRECL
# prsn = PRECSC+PRECSL
# prc = PRECC
# 
# Surface orography (in meters):
# orog = PHIS / 9.81
# 
# Radiative fluxes (all in W/m2):
# Aiko: probably wrong:rlus = FLNS - FLDS (rlus is positive up)
# must read: rlus = FLNS + FLDS (rlus is positive up)
# rsus = FSDS - FSNS (rsus is positive up)
# rsuscs = FSDSC - FSNSC (rsuscs is positive up)
# rsut = SOLIN - FSNT (rsut is positive up)
# rsutcs = SOLIN - FSNTC (rsutcs in positive up)
# rtmt = FSNT - FLNT (rtmt is positive down)

# Bind your OpenMP threads
export OMP_NUM_THREADS=8
export KMP_AFFINITY=verbose,granularity=thread,compact,1
export KMP_STACKSIZE=64m

set -ex

module load nco

CDO="cdo -P 8" 
CDO="cdo -P 16" 
CDOCMOR="/work/bm0021/cdo_incl_cmor/cdo_11_15_2017_cmor2/src/cdo -P 8" 

MODELUOM=CAM4   # model name at UoMiami server

#for EXPID in AquaControl Aqua4xCO2 LandControl Land4xCO2 LandOrbit; do
for EXPID in Land4xCO2 LandOrbit; do
for STREAM in monthly daily 3hourly; do

   cd /work/bm0162/b380459/TRACMIP/${MODELUOM}/   

   # pr
   ${CDO} -setname,PR -add -selvar,PRECC ${EXPID}.${STREAM}.nc -selvar,PRECL ${EXPID}.${STREAM}.nc ${EXPID}.${STREAM}.PR.nc
   ncatted -O -a longname,PR,o,c,precipitation -a long_name,PR,o,c,precipitation ${EXPID}.${STREAM}.PR.nc
   
   # prsn
   ${CDO} -setname,PRSN -add -selvar,PRECSC ${EXPID}.${STREAM}.nc -selvar,PRECSL ${EXPID}.${STREAM}.nc ${EXPID}.${STREAM}.PRSN.nc
   ncatted -O -a longname,PRSN,o,c,"snow fall (water equivalent)" -a long_name,PRSN,o,c,"snow fall (water equivalent)" ${EXPID}.${STREAM}.PRSN.nc
   
   # prc
   ncks -O -v PRECC ${EXPID}.${STREAM}.nc ${EXPID}.${STREAM}.PRC.nc
   ncrename -O -v PRECC,PRC ${EXPID}.${STREAM}.PRC.nc
   ncatted -O -a longname,PRC,o,c,convprecipitation -a long_name,PRC,o,c,"conv precipitation" ${EXPID}.${STREAM}.PRC.nc

   # rlus
   ${CDO} -setname,RLUS -add -selvar,FLNS ${EXPID}.${STREAM}.nc -selvar,FLDS ${EXPID}.${STREAM}.nc ${EXPID}.${STREAM}.RLUS.nc
   ncatted -O -a longname,RLUS,o,c,surface_upwelling_longwave_flux -a long_name,RLUS,o,c,surface_upwelling_longwave_flux ${EXPID}.${STREAM}.RLUS.nc
   
   # rsus
   ${CDO} -setname,RSUS -add -selvar,FSDS ${EXPID}.${STREAM}.nc -mulc,-1 -selvar,FSNS ${EXPID}.${STREAM}.nc ${EXPID}.${STREAM}.RSUS.nc
   ncatted -O -a longname,RSUS,o,c,surface_upwelling_shortwave_flux -a long_name,RSUS,o,c,surface_upwelling_shortwave_flux ${EXPID}.${STREAM}.RSUS.nc

   # rsuscs
   ${CDO} -setname,RSUSCS -add -selvar,FSDSC ${EXPID}.${STREAM}.nc -mulc,-1 -selvar,FSNSC ${EXPID}.${STREAM}.nc ${EXPID}.${STREAM}.RSUSCS.nc
   ncatted -O -a longname,RSUSCS,o,c,surface_upwelling_shortwave_flux_clearsky -a long_name,RSUSCS,o,c,surface_upwelling_shortwave_flux_clearsky ${EXPID}.${STREAM}.RSUSCS.nc
   
   # rsut
   ${CDO} -setname,RSUT -add -selvar,SOLIN ${EXPID}.${STREAM}.nc -mulc,-1 -selvar,FSNT ${EXPID}.${STREAM}.nc ${EXPID}.${STREAM}.RSUT.nc
   ncatted -O -a longname,RSUT,o,c,top_upwelling_shortwave_flux -a long_name,RSUT,o,c,top_upwelling_shortwave_flux ${EXPID}.${STREAM}.RSUT.nc
   
   # rsutcs
   ${CDO} -setname,RSUTCS -add -selvar,SOLIN ${EXPID}.${STREAM}.nc -mulc,-1 -selvar,FSNTC ${EXPID}.${STREAM}.nc ${EXPID}.${STREAM}.RSUTCS.nc
   ncatted -O -a longname,RSUTCS,o,c,top_upwelling_shortwave_flux_clearsky -a long_name,RSUTCS,o,c,top_upwelling_shortwave_flux_clearsky ${EXPID}.${STREAM}.RSUTCS.nc

   # rtmt
   ${CDO} -setname,RTMT -add -selvar,FSNT ${EXPID}.${STREAM}.nc -mulc,-1 -selvar,FLNT ${EXPID}.${STREAM}.nc ${EXPID}.${STREAM}.RTMT.nc
   ncatted -O -a longname,RTMT,o,c,top_net -a long_name,RTMT,o,c,top_net ${EXPID}.${STREAM}.RTMT.nc

done
done

cd /pf/b/b380459/TRACMIP_Cmorizing/cdoscripts
