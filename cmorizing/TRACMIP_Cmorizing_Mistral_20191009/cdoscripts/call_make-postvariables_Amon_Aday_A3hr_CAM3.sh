#!/bin/bash
#=============================================================================
# btc batch job parameters
#-----------------------------------------------------------------------------
#SBATCH --account=bm0834
#SBATCH --job-name=call_call_make-postvariables_Amon_Aday_A3hr_CAM3.sh
#SBATCH --partition=prepost
#SBATCH --ntasks=2
#SBATCH --cpus-per-task=8      # Specify number of CPUs per task
#SBATCH --output=/pf/b/b380459/TRACMIP_Cmorizing/cdoscripts/call_make-postvariables_Amon_Aday_A3hr_CAM3.sh.%j.o
#SBATCH --error=/pf/b/b380459/TRACMIP_Cmorizing/cdoscripts/call_make-postvariables_Amon_Aday_A3hr_CAM3.sh.%j.o
#SBATCH --time=08:00:00
#=============================================================================

# we manually calculate those variables that are not part in the CAM3 raw output but can be calculated from other variables
# this includes:
# PR = PRECC + PRECL
# PRSN = PRECSC + PRECSL
# RLUS = FLDS + FLNS
# RSUS = FSDS - FSNS
# RSUSCS = FSDSC - FSNSC
# RSUT = SOLIN - FSNT
# RSUTCS = SOLIN - FSNTC

# Bind your OpenMP threads
export OMP_NUM_THREADS=8
export KMP_AFFINITY=verbose,granularity=thread,compact,1
export KMP_STACKSIZE=64m

set -e

module load nco

CDO="cdo -P 8" 
CDOCMOR="/work/bm0021/cdo_incl_cmor/cdo_11_15_2017_cmor2/src/cdo -P 8" 

MODELUOM=CAM3   # model name at UoMiami server

#for EXPID in AquaControl Aqua4xCO2 LandControl Land4xCO2 LandOrbit; do
for EXPID in LandControl Land4xCO2 LandOrbit; do
for STREAM in monthly daily 3hourly; do

   cd /work/bm0162/b380459/TRACMIP/${MODELUOM}/${EXPID}/   

   # PR
   ${CDO} -setname,PR -add -selvar,PRECC ${EXPID}_${STREAM}.nc -selvar,PRECL ${EXPID}_${STREAM}.nc ${EXPID}_${STREAM}_PR.nc
   ncatted -O -a longname,PR,o,c,precipitation -a long_name,PR,o,c,precipitation ${EXPID}_${STREAM}_PR.nc
   
   # PRSN
   ${CDO} -setname,PRSN -add -selvar,PRECSC ${EXPID}_${STREAM}.nc -selvar,PRECSL ${EXPID}_${STREAM}.nc ${EXPID}_${STREAM}_PRSN.nc
   ncatted -O -a longname,PRSN,o,c,"snow fall (water equivalent)" -a long_name,PRSN,o,c," snow fall (water equivalent)" ${EXPID}_${STREAM}_PRSN.nc

   # RLUS
   ${CDO} -setname,RLUS -add -selvar,FLDS ${EXPID}_${STREAM}.nc -selvar,FLNS ${EXPID}_${STREAM}.nc ${EXPID}_${STREAM}_RLUS.nc
   ncatted -O -a longname,RLUS,o,c,surface_upwelling_longwave_flux -a long_name,RLUS,o,c,surface_upwelling_longwave_flux ${EXPID}_${STREAM}_RLUS.nc
   
   # RSUS
   ${CDO} -setname,RSUS -add -selvar,FSDS ${EXPID}_${STREAM}.nc -mulc,-1 -selvar,FSNS ${EXPID}_${STREAM}.nc ${EXPID}_${STREAM}_RSUS.nc
   ncatted -O -a longname,RSUS,o,c,surface_upwelling_shortwave_flux -a long_name,RSUS,o,c,surface_upwelling_shortwave_flux ${EXPID}_${STREAM}_RSUS.nc

   # RSUSCS
   ${CDO} -setname,RSUSCS -add -selvar,FSDSC ${EXPID}_${STREAM}.nc -mulc,-1 -selvar,FSNSC ${EXPID}_${STREAM}.nc ${EXPID}_${STREAM}_RSUSCS.nc
   ncatted -O -a longname,RSUSCS,o,c,surface_upwelling_shortwave_flux_clearsky -a long_name,RSUSCS,o,c,surface_upwelling_shortwave_flux_clearsky ${EXPID}_${STREAM}_RSUSCS.nc
   
   # RSUT
   ${CDO} -setname,RSUT -add -selvar,SOLIN ${EXPID}_${STREAM}.nc -mulc,-1 -selvar,FSNT ${EXPID}_${STREAM}.nc ${EXPID}_${STREAM}_RSUT.nc
   ncatted -O -a longname,RSUT,o,c,top_upwelling_shortwave_flux -a long_name,RSUT,o,c,top_upwelling_shortwave_flux ${EXPID}_${STREAM}_RSUT.nc
   
   # RSUTCS
   ${CDO} -setname,RSUTCS -add -selvar,SOLIN ${EXPID}_${STREAM}.nc -mulc,-1 -selvar,FSNTC ${EXPID}_${STREAM}.nc ${EXPID}_${STREAM}_RSUTCS.nc
   ncatted -O -a longname,RSUTCS,o,c,top_upwelling_shortwave_flux_clearsky -a long_name,RSUTCS,o,c,top_upwelling_shortwave_flux_clearsky ${EXPID}_${STREAM}_RSUTCS.nc

done
done

cd /pf/b/b380459/TRACMIP_Cmorizing/cdoscripts
