#!/bin/bash
#=============================================================================
# btc batch job parameters
#-----------------------------------------------------------------------------
#SBATCH --account=bm0834
#SBATCH --job-name=call_mergetime_Aday_A3hr_MIROC5.sh
#SBATCH --partition=prepost
#SBATCH --ntasks=2
#SBATCH --cpus-per-task=8      # Specify number of CPUs per task
#SBATCH --output=/pf/b/b380459/TRACMIP_Cmorizing/cdoscripts/call_mergetime_Aday_A3hr_MIROC5.sh.%j.o
#SBATCH --error=/pf/b/b380459/TRACMIP_Cmorizing/cdoscripts/call_mergetime_Aday_A3hr_MIROC5.sh.%j.o
#SBATCH --time=08:00:00
#=============================================================================

# note the following:
# 3d cloud fields are on hybrid sigma ("heta40" levels), before cmorizing we interpolate them to pressure levels
# we ignore cmflx here and in the cmorizing as it has 41 heta levels, for which I do not have the hybrid sigma coefficients

# Bind your OpenMP threads
export OMP_NUM_THREADS=8
export KMP_AFFINITY=verbose,granularity=thread,compact,1
export KMP_STACKSIZE=64m

set -e

module load nco

CDO="cdo -P 8" 
CDOCMOR="/work/bm0021/cdo_incl_cmor/cdo_11_15_2017_cmor2/src/cdo -P 8" 

MODELUOM=MIROC5   # model name at UoMiami server

YEARSTART_AquaControl_Amon=1016
YEARSTART_Aqua4xCO2_Amon=1046
YEARSTART_LandControl_Amon=1046
YEARSTART_Land4xCO2_Amon=1086
YEARSTART_LandOrbit_Amon=1086

YEAREND_AquaControl_Amon=1045
YEAREND_Aqua4xCO2_Amon=1085
YEAREND_LandControl_Amon=1085
YEAREND_Land4xCO2_Amon=1125
YEAREND_LandOrbit_Amon=1125

YEARSTART_AquaControl_Aday=1036
YEARSTART_Aqua4xCO2_Aday=1076
YEARSTART_LandControl_Aday=1076
YEARSTART_Land4xCO2_Aday=1116
YEARSTART_LandOrbit_Aday=1116

YEAREND_AquaControl_Aday=1045
YEAREND_Aqua4xCO2_Aday=1085
YEAREND_LandControl_Aday=1085
YEAREND_Land4xCO2_Aday=1125
YEAREND_LandOrbit_Aday=1125

YEARSTART_AquaControl_A3hr=1043
YEARSTART_Aqua4xCO2_A3hr=1083
YEARSTART_LandControl_A3hr=1083
YEARSTART_Land4xCO2_A3hr=1123
YEARSTART_LandOrbit_A3hr=1123

YEAREND_AquaControl_A3hr=1045
YEAREND_Aqua4xCO2_A3hr=1085
YEAREND_LandControl_A3hr=1085
YEAREND_Land4xCO2_A3hr=1125
YEAREND_LandOrbit_A3hr=1125

VARLIST2D="cct clivi clt clwvi evspsbl hfls hfss hurs huss pr prc prsn prw ps psl rlds rldscs rlus rlut rlutcs rsds rsdscs rsdt rsus rsuscs rsut rsutcs sfcWind tas tasmax tasmin tauu tauv ts uas vas"
VARLIST3D="cl cli clw hur hus ta ua va wap zg"

# AquaControl Aday
EXPID1=AquaControl
STREAM=Aday
cd /work/bm0162/b380459/TRACMIP/${MODELUOM}/${EXPID1}/${STREAM}/
for var in ${VARLIST2D} ${VARLIST3D}; do
   echo "Currently merging:" ${MODELUOM} ${EXPID1} ${STREAM} ${var}
   if [ -f  ${var}_${STREAM}_${MODELUOM}_${EXPID1}_r1i1p1_10360101-10361230.nc ]; then
      cdo -O mergetime ${var}_${STREAM}_${MODELUOM}_${EXPID1}_r1i1p1_103[6789]0101-103[6789]1230.nc \
                       ${var}_${STREAM}_${MODELUOM}_${EXPID1}_r1i1p1_104[012345]0101-104[012345]1230.nc \
                       ${var}_${STREAM}_${MODELUOM}_${EXPID1}_r1i1p1_10360101-10451230.nc
   fi
done

# AquaControl A3hr
EXPID1=AquaControl
STREAM=A3hr
cd /work/bm0162/b380459/TRACMIP/${MODELUOM}/${EXPID1}/${STREAM}/
for var in ${VARLIST2D} ${VARLIST3D}; do
   echo "Currently merging:" ${MODELUOM} ${EXPID1} ${STREAM} ${var}
   if [ -f  ${var}_${STREAM}_${MODELUOM}_${EXPID1}_r1i1p1_10430101-10431230.nc ]; then
      cdo -O mergetime ${var}_${STREAM}_${MODELUOM}_${EXPID1}_r1i1p1_104[345]0101-104[345]1230.nc \
                       ${var}_${STREAM}_${MODELUOM}_${EXPID1}_r1i1p1_10430101-10451230.nc
   fi
done

# Aqua4xCO2 Aday
EXPID1=Aqua4xCO2
STREAM=Aday
cd /work/bm0162/b380459/TRACMIP/${MODELUOM}/${EXPID1}/${STREAM}/
for var in ${VARLIST2D} ${VARLIST3D}; do
   echo "Currently merging:" ${MODELUOM} ${EXPID1} ${STREAM} ${var}
   if [ -f  ${var}_${STREAM}_${MODELUOM}_${EXPID1}_r1i1p1_10760101-10761230.nc ]; then
      cdo -O mergetime ${var}_${STREAM}_${MODELUOM}_${EXPID1}_r1i1p1_107[6789]0101-107[6789]1230.nc \
                       ${var}_${STREAM}_${MODELUOM}_${EXPID1}_r1i1p1_108[012345]0101-108[012345]1230.nc \
                       ${var}_${STREAM}_${MODELUOM}_${EXPID1}_r1i1p1_10760101-10851230.nc
   fi
done

# Aqua4xCO2 A3hr
EXPID1=Aqua4xCO2
STREAM=A3hr
cd /work/bm0162/b380459/TRACMIP/${MODELUOM}/${EXPID1}/${STREAM}/
for var in ${VARLIST2D} ${VARLIST3D}; do
   echo "Currently merging:" ${MODELUOM} ${EXPID1} ${STREAM} ${var}
   if [ -f  ${var}_${STREAM}_${MODELUOM}_${EXPID1}_r1i1p1_10830101-10831230.nc ]; then
      cdo -O mergetime ${var}_${STREAM}_${MODELUOM}_${EXPID1}_r1i1p1_108[345]0101-108[345]1230.nc \
                       ${var}_${STREAM}_${MODELUOM}_${EXPID1}_r1i1p1_10830101-10851230.nc
   fi
done

# LandControl Aday
EXPID1=LandControl
STREAM=Aday
cd /work/bm0162/b380459/TRACMIP/${MODELUOM}/${EXPID1}/${STREAM}/
for var in ${VARLIST2D} ${VARLIST3D}; do
   echo "Currently merging:" ${MODELUOM} ${EXPID1} ${STREAM} ${var}
   if [ -f  ${var}_${STREAM}_${MODELUOM}_${EXPID1}_r1i1p1_10760101-10761230.nc ]; then
      cdo -O mergetime ${var}_${STREAM}_${MODELUOM}_${EXPID1}_r1i1p1_107[6789]0101-107[6789]1230.nc \
                       ${var}_${STREAM}_${MODELUOM}_${EXPID1}_r1i1p1_108[012345]0101-108[012345]1230.nc \
                       ${var}_${STREAM}_${MODELUOM}_${EXPID1}_r1i1p1_10760101-10851230.nc
   fi
done

# LandControl A3hr
EXPID1=LandControl
STREAM=A3hr
cd /work/bm0162/b380459/TRACMIP/${MODELUOM}/${EXPID1}/${STREAM}/
for var in ${VARLIST2D} ${VARLIST3D}; do
   echo "Currently merging:" ${MODELUOM} ${EXPID1} ${STREAM} ${var}
   if [ -f  ${var}_${STREAM}_${MODELUOM}_${EXPID1}_r1i1p1_10830101-10831230.nc ]; then
      cdo -O mergetime ${var}_${STREAM}_${MODELUOM}_${EXPID1}_r1i1p1_108[345]0101-108[345]1230.nc \
                       ${var}_${STREAM}_${MODELUOM}_${EXPID1}_r1i1p1_10830101-10851230.nc
   fi
done

# Land4xCO2 Aday
EXPID1=Land4xCO2
STREAM=Aday
cd /work/bm0162/b380459/TRACMIP/${MODELUOM}/${EXPID1}/${STREAM}/
for var in ${VARLIST2D} ${VARLIST3D}; do
   echo "Currently merging:" ${MODELUOM} ${EXPID1} ${STREAM} ${var}
   if [ -f  ${var}_${STREAM}_${MODELUOM}_${EXPID1}_r1i1p1_11160101-11161230.nc ]; then
      cdo -O mergetime ${var}_${STREAM}_${MODELUOM}_${EXPID1}_r1i1p1_111[6789]0101-111[6789]1230.nc \
                       ${var}_${STREAM}_${MODELUOM}_${EXPID1}_r1i1p1_112[012345]0101-112[012345]1230.nc \
                       ${var}_${STREAM}_${MODELUOM}_${EXPID1}_r1i1p1_11160101-11251230.nc
   fi
done

# Land4xCO2 A3hr
EXPID1=Land4xCO2
STREAM=A3hr
cd /work/bm0162/b380459/TRACMIP/${MODELUOM}/${EXPID1}/${STREAM}/
for var in ${VARLIST2D} ${VARLIST3D}; do
   echo "Currently merging:" ${MODELUOM} ${EXPID1} ${STREAM} ${var}
   if [ -f  ${var}_${STREAM}_${MODELUOM}_${EXPID1}_r1i1p1_11230101-11231230.nc ]; then
      cdo -O mergetime ${var}_${STREAM}_${MODELUOM}_${EXPID1}_r1i1p1_112[345]0101-112[345]1230.nc \
                       ${var}_${STREAM}_${MODELUOM}_${EXPID1}_r1i1p1_11230101-11251230.nc
   fi
done

# LandOrbit Aday
EXPID1=LandOrbit
STREAM=Aday
cd /work/bm0162/b380459/TRACMIP/${MODELUOM}/${EXPID1}/${STREAM}/
for var in ${VARLIST2D} ${VARLIST3D}; do
   echo "Currently merging:" ${MODELUOM} ${EXPID1} ${STREAM} ${var}
   if [ -f  ${var}_${STREAM}_${MODELUOM}_${EXPID1}_r1i1p1_11160101-11161230.nc ]; then
      cdo -O mergetime ${var}_${STREAM}_${MODELUOM}_${EXPID1}_r1i1p1_111[6789]0101-111[6789]1230.nc \
                       ${var}_${STREAM}_${MODELUOM}_${EXPID1}_r1i1p1_112[012345]0101-112[012345]1230.nc \
                       ${var}_${STREAM}_${MODELUOM}_${EXPID1}_r1i1p1_11160101-11251230.nc
   fi
done

# LandOrbit A3hr
EXPID1=LandOrbit
STREAM=A3hr
cd /work/bm0162/b380459/TRACMIP/${MODELUOM}/${EXPID1}/${STREAM}/
for var in ${VARLIST2D} ${VARLIST3D}; do
   echo "Currently merging:" ${MODELUOM} ${EXPID1} ${STREAM} ${var}
   if [ -f  ${var}_${STREAM}_${MODELUOM}_${EXPID1}_r1i1p1_11230101-11231230.nc ]; then
      cdo -O mergetime ${var}_${STREAM}_${MODELUOM}_${EXPID1}_r1i1p1_112[345]0101-112[345]1230.nc \
                       ${var}_${STREAM}_${MODELUOM}_${EXPID1}_r1i1p1_11230101-11251230.nc
   fi
done
