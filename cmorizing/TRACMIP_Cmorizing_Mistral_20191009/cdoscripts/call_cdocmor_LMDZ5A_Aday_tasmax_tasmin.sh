#!/bin/bash
#=============================================================================
# btc batch job parameters
#-----------------------------------------------------------------------------
#SBATCH --account=bm0834
#SBATCH --job-name=call_cdocmor_LMDZ5A.sh
#SBATCH --partition=prepost
#SBATCH --ntasks=2
#SBATCH --cpus-per-task=8      # Specify number of CPUs per task
#SBATCH --output=/work/bm0162/b380459/TRACMIP_cmorized/call_cdocmor_LMDZ5A.sh.%j.o
#SBATCH --error=/work/bm0162/b380459/TRACMIP_cmorized/call_cdocmor_LMDZ5A.sh.%j.o
#SBATCH --time=08:00:00
#=============================================================================

# note: no 3hr data available
# clw 3hr missing for AquaControl

# Bind your OpenMP threads
export OMP_NUM_THREADS=8
export KMP_AFFINITY=verbose,granularity=thread,compact,1
export KMP_STACKSIZE=64m

set -ex

module unload nco
module unload cdo

module load cdo/1.9.5-gcc64
module load nco/4.7.5-gcc64

CDO="cdo -P 8" 
CDOCMOR="/work/bm0021/cdo_incl_cmor/cdo_11_15_2017_cmor2/src/cdo -P 8" 

MODELUOM=IPSL-CM5A   # model name at UoMiami server
MODELUOM2=IPSL-CM5A # for some models, there is a second UoMiami name needed for the actual variable file (differs from directory name by, e.g., small or capital letters)
MODELESGF=LMDZ5A  # ESGF model name

MAPPINGTABLE=/pf/b/b380459/TRACMIP_Cmorizing/cdocmor_input/${MODELESGF}/mt_TRACMIP_${MODELESGF}.txt
echo "Using MAPPINGTABLE:" ${MAPPINGTABLE}

EXPID2_AquaControl=aquaControlTRACMIP
EXPID2_Aqua4xCO2=aqua4xCO2TRACMIP
EXPID2_LandControl=landControlTRACMIP
EXPID2_Land4xCO2=land4xCO2TRACMIP
EXPID2_LandOrbit=landOrbitTRACMIP

# move into TRACMIP work directory to do actual work
cd /work/bm0162/b380459/TRACMIP_cmorized
pwd

# create temporary directory for this model for tempfile.nc
rm -rf ${MODELESGF}_tmpdir
mkdir ${MODELESGF}_tmpdir

#############################
# Aday Stream

VARLIST_2D="tasmax tasmin"

make_Aday=true
if [ "x$make_Aday" == "xtrue" ] ; then

for EXPID1 in AquaControl Aqua4xCO2 LandControl Land4xCO2 LandOrbit; do
for STREAM in Aday; do

   EXPID2=EXPID2_${EXPID1}
   MIPTABLE=/pf/b/b380459/TRACMIP_Cmorizing/cdocmor_input/TRACMIP_${STREAM}
   CDOCMORINFO=/pf/b/b380459/TRACMIP_Cmorizing/cdocmor_input/${MODELESGF}/cdocmorinfo_TRACMIP_${MODELESGF}_${!EXPID2}
   for var in ${VARLIST_2D} ${VARLIST_3D}; do
      echo "Currently working on:" ${MODELESGF} ${EXPID1} ${STREAM} ${var}
      rm -f ${MODELESGF}_tmpdir/tempfile.nc
      INFILE=/work/bm0162/b380459/TRACMIP/${MODELUOM}/${var}_${STREAM}_${MODELUOM2}_${EXPID1}_r1i1p1_????01-????12.nc
      echo ${INFILE}
      ${CDOCMOR} -s cmor,${MIPTABLE},info=${CDOCMORINFO},mapping_table=${MAPPINGTABLE} -settaxis,2020-01-01,12:00,1d -setcalendar,360_day -selvar,${var} ${INFILE}
      rm -f ${MODELESGF}_tmpdir/*
    done

done
done


fi
# remove temporary directory
rm -rf ${MODELESGF}_tmpdir 

# move back to cdo script directory
cd /pf/b/b380459/TRACMIP_Cmorizing/cdoscripts


