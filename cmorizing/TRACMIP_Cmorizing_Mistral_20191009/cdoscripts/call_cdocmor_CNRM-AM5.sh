#!/bin/bash
#=============================================================================
# btc batch job parameters
#-----------------------------------------------------------------------------
#SBATCH --account=bm0834
#SBATCH --job-name=call_cdocmor_CNRM-AM5.sh
#SBATCH --partition=prepost
#SBATCH --ntasks=12
#SBATCH --output=/work/bm0162/b380459/TRACMIP_cmorized/call_cdocmor_CNRM-AM5.sh.%j.o
#SBATCH --error=/work/bm0162/b380459/TRACMIP_cmorized/call_cdocmor_CNRM-AM5.sh.%j.o
#SBATCH --time=08:00:00
#=============================================================================

set -e

CDO="cdo -P 8"
CDOCMOR="/work/bm0021/cdo_incl_cmor/cdo_11_15_2017_cmor2/src/cdo -P 8" 

MODELUOM=CNRM-AM6-DIA-v2   # model name at UoMiami server
MODELUOM2=CNRM-AM6-DIA-v2  # for some models, there is a second UoMiami name needed for the actual variable file (differs from directory name by, e.g., small or capital letters)
MODELESGF=CNRM-AM5         # ESGF model name

MAPPINGTABLE=/work/bm0162/b380459/cdocmor_input/${MODELESGF}/mt_TRACMIP_${MODELESGF}.txt
echo "Using MAPPINGTABLE:" ${MAPPINGTABLE}

CALENDAR=proleptic_gregorian

EXPID2_AquaControl=aquaControlTRACMIP
EXPID2_Aqua4xCO2=aqua4xCO2TRACMIP
EXPID2_LandControl=landControlTRACMIP
EXPID2_Land4xCO2=land4xCO2TRACMIP
EXPID2_LandOrbit=landOrbitTRACMIP

YEARSTART_AquaControl_Amon=1979
YEARSTART_Aqua4xCO2_Amon=1969
YEARSTART_LandControl_Amon=1969
YEARSTART_Land4xCO2_Amon=1969
YEARSTART_LandOrbit_Amon=1969

YEAREND_AquaControl_Amon=2008
YEAREND_Aqua4xCO2_Amon=2008
YEAREND_LandControl_Amon=2008
YEAREND_Land4xCO2_Amon=2008
YEAREND_LandOrbit_Amon=2008

YEARSTART_AquaControl_Aday=1999
YEARSTART_Aqua4xCO2_Aday=1999
YEARSTART_LandControl_Aday=1999
YEARSTART_Land4xCO2_Aday=1999
YEARSTART_LandOrbit_Aday=1999

YEAREND_AquaControl_Aday=2008
YEAREND_Aqua4xCO2_Aday=2008
YEAREND_LandControl_Aday=2008
YEAREND_Land4xCO2_Aday=2008
YEAREND_LandOrbit_Aday=2008

YEARSTART_AquaControl_A3hr=2006
YEARSTART_Aqua4xCO2_A3hr=2006
YEARSTART_LandControl_A3hr=2006
YEARSTART_Land4xCO2_A3hr=2006
YEARSTART_LandOrbit_A3hr=2006

YEAREND_AquaControl_A3hr=2009
YEAREND_Aqua4xCO2_A3hr=2009
YEAREND_LandControl_A3hr=2009
YEAREND_Land4xCO2_A3hr=2009
YEAREND_LandOrbit_A3hr=2009

VARLIST="clivi clt clwvi hfls hfss hur hus pr prc prw ps psl rlds rldscs rlus rlut rlutcs rsds rsdscs rsdt rsus rsuscs rsut rsutcs ta tas ts ua va wap zg"

# temporary directory for tempfile.nc
rm -rf ${MODELESGF}_tmpdir
mkdir ${MODELESGF}_tmpdir

#for EXPID1 in AquaControl Aqua4xCO2 LandControl Land4xCO2 LandOrbit; do
for EXPID1 in Land4xCO2 LandOrbit; do
   
   STREAM=Amon  
   EXPID2=EXPID2_${EXPID1}
   MIPTABLE=/work/bm0162/b380459/cdocmor_input/TRACMIP_${STREAM}
   CDOCMORINFO=/work/bm0162/b380459/cdocmor_input/${MODELESGF}/cdocmorinfo_TRACMIP_${MODELESGF}_${!EXPID2}
   for var in ${VARLIST}; do
      rm -f ${MODELESGF}_tmpdir/tempfile.nc
      YEARSTART=YEARSTART_${EXPID1}_${STREAM}
      YEAREND=YEAREND_${EXPID1}_${STREAM}
      INFILE=/work/bm0162/b380459/TRACMIP/${MODELUOM}/${EXPID1}/${STREAM}/${var}_${STREAM}_${MODELUOM2}_${EXPID1}_r1i1p1_${!YEARSTART}01-${!YEAREND}12.nc
      echo "Currently working on:" ${MODELESGF} ${EXPID1} ${STREAM} ${var} ${CDOCMORINFO} ${MIPTABLE} ${INFILE} 
      if [ -f ${INFILE} ]; then 
         ${CDO} -s setcalendar,${CALENDAR} ${INFILE} ${MODELESGF}_tmpdir/tempfile.nc
         ${CDOCMOR} -s cmor,${MIPTABLE},info=${CDOCMORINFO},mapping_table=${MAPPINGTABLE} ${MODELESGF}_tmpdir/tempfile.nc
      fi
      rm -f ${MODELESGF}_tmpdir/tempfile.nc
   done

   STREAM=Aday  
   EXPID2=EXPID2_${EXPID1}
   MIPTABLE=/work/bm0162/b380459/cdocmor_input/TRACMIP_${STREAM}
   CDOCMORINFO=/work/bm0162/b380459/cdocmor_input/${MODELESGF}/cdocmorinfo_TRACMIP_${MODELESGF}_${!EXPID2}
   for var in ${VARLIST}; do
      rm -f ${MODELESGF}_tmpdir/tempfile.nc
      YEARSTART=YEARSTART_${EXPID1}_${STREAM}
      YEAREND=YEAREND_${EXPID1}_${STREAM}
      INFILE=/work/bm0162/b380459/TRACMIP/${MODELUOM}/${EXPID1}/day/${var}_day_${MODELUOM2}_${EXPID1}_r1i1p1_${!YEARSTART}0101-${!YEAREND}1231.nc
      echo "Currently working on:" ${MODELESGF} ${EXPID1} ${STREAM} ${var} ${INFILE}
      if [ -f ${INFILE} ]; then
         ${CDO} -s setcalendar,${CALENDAR} ${INFILE} ${MODELESGF}_tmpdir/tempfile.nc
         ${CDOCMOR} -s cmor,${MIPTABLE},info=${CDOCMORINFO},mapping_table=${MAPPINGTABLE} ${MODELESGF}_tmpdir/tempfile.nc
      fi
      rm -f ${MODELESGF}_tmpdir/tempfile.nc
   done

   STREAM=A3hr  
   EXPID2=EXPID2_${EXPID1}
   MIPTABLE=/work/bm0162/b380459/cdocmor_input/TRACMIP_${STREAM}
   CDOCMORINFO=/work/bm0162/b380459/cdocmor_input/${MODELESGF}/cdocmorinfo_TRACMIP_${MODELESGF}_${!EXPID2}
   for var in ${VARLIST}; do
      rm -f ${MODELESGF}_tmpdir/tempfile.nc
      YEARSTART=YEARSTART_${EXPID1}_${STREAM}
      YEAREND=YEAREND_${EXPID1}_${STREAM}
      INFILE=/work/bm0162/b380459/TRACMIP/${MODELUOM}/${EXPID1}/3hr/${var}_3hr_${MODELUOM2}_${EXPID1}_r1i1p1_${!YEARSTART}01010300-${!YEAREND}01010000.nc
      echo "Currently working on:" ${MODELESGF} ${EXPID1} ${STREAM} ${var} ${INFILE}
      if [ -f ${INFILE} ]; then
         ${CDO} -s setcalendar,${CALENDAR} ${INFILE} ${MODELESGF}_tmpdir/tempfile.nc
         ${CDOCMOR} -s cmor,${MIPTABLE},info=${CDOCMORINFO},mapping_table=${MAPPINGTABLE} ${MODELESGF}_tmpdir/tempfile.nc
      fi
      rm -f ${MODELESGF}_tmpdir/tempfile.nc
   done
done

# remove temporary directory for tempfile.nc
rm -rf ${MODELESGF}_tmpdir/
