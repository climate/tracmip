
import xarray as xr
import numpy as np
from scipy import interpolate
import sys

fpath=sys.argv[1]
fname=sys.argv[2]
varname=sys.argv[3]
print(fpath,fname)

ds = xr.open_dataset(fpath+'/'+fname+'.nc')

ntim = ds.time.size
nlat = ds.lat.size
nlon = ds.lon.size

plev_int = ( [100000.0, 92500.0, 85000.0, 70000.0, 60000.0, 50000.0, 40000.0, 30000.0, 25000.0, 20000.0, 15000.0, 10000.0,
              7000.0, 5000.0, 3000.0, 2000.0, 1000.0] )

# create new dataset with vertically interpolated 3d fields
ds_int = xr.Dataset()
ds_int.attrs = ds.attrs

var  = ds[varname].values
pmid = ds['PMID'].values

varint = np.zeros((ntim, 17, nlat, nlon)) + np.nan

for t in range(ntim):
    if (t%100==0):
        print(fname, varname, t)
    for j in range(nlat):
        for i in range(nlon):
            f_int = interpolate.interp1d(pmid[t,:,j,i], var[t,:,j,i], bounds_error=False, kind='linear', fill_value='extrapolate')
            varint[t,:,j,i] = f_int(plev_int)

# convert varint into dataarray
da_varint = xr.DataArray(varint, name=varname, dims=('time','lev','lat','lon'), coords={'time': ds.time, 'lev': plev_int, 'lat': ds.lat, 'lon': ds.lon})

ds_int[varname]=da_varint

ds_int.to_netcdf(path=fpath+'/'+fname+'.'+varname+'.cmiplev.nc')

