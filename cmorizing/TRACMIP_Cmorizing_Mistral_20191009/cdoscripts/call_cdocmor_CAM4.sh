#!/bin/bash
#=============================================================================
# btc batch job parameters
#-----------------------------------------------------------------------------
#SBATCH --account=bm0834
#SBATCH --job-name=call_cdocmor_CAM4.sh
#SBATCH --partition=compute,compute2
#SBATCH --ntasks=2
#SBATCH --cpus-per-task=8      # Specify number of CPUs per task
#SBATCH --output=/pf/b/b380459/TRACMIP_Cmorizing/cdoscripts/call_cdocmor_CAM4.sh.%j.o
#SBATCH --error=/pf/b/b380459/TRACMIP_Cmorizing/cdoscripts/call_cdocmor_CAM4.sh.%j.o
#SBATCH --time=08:00:00
#SBATCH --exclusive
#=============================================================================

# note the following:
#  - some postprocessed fields are in separate files
#  - for precipitation fields pr, prc, and prsn we need to multiply with 1000 to get units from units of m/s to kg/m/s

# Bind your OpenMP threads
export OMP_NUM_THREADS=8
export KMP_AFFINITY=verbose,granularity=thread,compact,1
export KMP_STACKSIZE=64m

set -e

module unload nco
module unload cdo

module load cdo/1.9.5-gcc64
module load nco/4.7.5-gcc64

CDO="cdo -P 8" 
CDOCMOR="/work/bm0021/cdo_incl_cmor/cdo_11_15_2017_cmor2/src/cdo -P 8" 

MODELUOM=CAM4   # model name at UoMiami server
MODELUOM2=CAM4  # for some models, there is a second UoMiami name needed for the actual variable file (differs from directory name by, e.g., small or capital letters)
MODELESGF=CAM4  # ESGF model name

MAPPINGTABLE=/pf/b/b380459/TRACMIP_Cmorizing/cdocmor_input/${MODELESGF}/mt_TRACMIP_${MODELESGF}.txt
echo "Using MAPPINGTABLE:" ${MAPPINGTABLE}

CALENDAR=365_day

EXPID2_AquaControl=aquaControlTRACMIP
EXPID2_Aqua4xCO2=aqua4xCO2TRACMIP
EXPID2_LandControl=landControlTRACMIP
EXPID2_Land4xCO2=land4xCO2TRACMIP
EXPID2_LandOrbit=landOrbitTRACMIP

VARLIST2D_Amon=" " #TGCLDIWP CLDTOT TGCLDLWP TREFHT TS TSMX TSMN PSL PS U10 QREFHT TAUX TAUY LHFLX SHFLX FLDS FLDSC FLUT FLUTC FSDS FSDSC SOLIN TMQ"
VARLIST2D_Aday=" " #TGCLDIWP CLDTOT TGCLDLWP TREFHT TS TSMX TSMN PSL PS U10 QREFHT TAUX TAUY LHFLX SHFLX FLDS FLDSC FLUT FLUTC FSDS FSDSC SOLIN TMQ"
VARLIST2D_A3hr=" " # TGCLDIWP CLDTOT TGCLDLWP TREFHT TS TSMX TSMN PSL PS U10 QREFHT TAUX TAUY LHFLX SHFLX FLDS FLDSC FLUT FLUTC FSDS FSDSC SOLIN TMQ"
VARLIST2D_sepfiles="PR PRC PRSN" # RLUS RSUS RSUSCS RSUT RSUTCS RTMT"
VARLIST3D_Amon=" " #CLOUD CLDICE CLDLIQ RELHUM Q T U V OMEGA Z3"
VARLIST3D_Aday=" " #CLOUD CLDICE CLDLIQ RELHUM Q T U V OMEGA Z3"
VARLIST3D_A3hr=" " #Q T U V OMEGA Z3"

# move into TRACMIP work directory to do actual work
cd /work/bm0162/b380459/TRACMIP_cmorized

pwd

# create temporary directory for this model for tempfile.nc
rm -rf ${MODELESGF}_tmpdir_${1}_${2}
mkdir ${MODELESGF}_tmpdir_${1}_${2}

#for EXPID1 in AquaControl Aqua4xCO2 LandControl Land4xCO2 LandOrbit; do
for EXPID1 in ${1}; do
#for STREAM in Amon Aday A3hr; do
for STREAM in ${2}; do
  
   if [ "${STREAM}" == "Amon" ]; then
      FILE="monthly"
      VARLIST2D=${VARLIST2D_Amon}
      VARLIST3D=${VARLIST3D_Amon}
   fi
   if [ "${STREAM}" == "Aday" ]; then
      FILE="daily"
      VARLIST2D=${VARLIST2D_Aday}
      VARLIST3D=${VARLIST3D_Aday}
   fi
   if [ "${STREAM}" == "A3hr" ]; then
      FILE="3hourly"
      VARLIST2D=${VARLIST2D_A3hr}
      VARLIST3D=${VARLIST3D_A3hr}
   fi
   echo $FILE, $VARLIST2D 
 
   EXPID2=EXPID2_${EXPID1}
   MIPTABLE=/pf/b/b380459/TRACMIP_Cmorizing/cdocmor_input/TRACMIP_${STREAM}
   CDOCMORINFO=/pf/b/b380459/TRACMIP_Cmorizing/cdocmor_input/${MODELESGF}/cdocmorinfo_TRACMIP_${MODELESGF}_${!EXPID2}
   for var in ${VARLIST2D}; do
      echo "Currently working on:" ${MODELESGF} ${EXPID1} ${STREAM} ${var}
      rm -f ${MODELESGF}_tmpdir_${1}_${2}/tempfile.nc
      INFILE=/work/bm0162/b380459/TRACMIP/${MODELUOM}/${EXPID1}.${FILE}.nc
      echo ${INFILE}
      ncks -O -v ${var} ${INFILE} ${MODELESGF}_tmpdir_${1}_${2}/ncks_tempfile.nc
      ${CDO} -s -setcalendar,${CALENDAR} ${MODELESGF}_tmpdir_${1}_${2}/ncks_tempfile.nc ${MODELESGF}_tmpdir_${1}_${2}/tempfile.nc
      ${CDOCMOR} -s cmor,${MIPTABLE},info=${CDOCMORINFO},mapping_table=${MAPPINGTABLE} ${MODELESGF}_tmpdir_${1}_${2}/tempfile.nc
      rm -f ${MODELESGF}_tmpdir_${1}_${2}/*
    done
    for var in ${VARLIST2D_sepfiles}; do
      echo "Currently working on:" ${MODELESGF} ${EXPID1} ${STREAM} ${var}
      rm -f ${MODELESGF}_tmpdir_${1}_${2}/tempfile.nc
      INFILE=/work/bm0162/b380459/TRACMIP/${MODELUOM}/${EXPID1}.${FILE}.${var}.nc
      MULC=" "
      if [ "${var}" == "PR" ] || [ "${var}" == "PRSN" ]  || [ "${var}" == "PRC" ]; then
         MULC="-mulc,1000.0"
      fi
      echo ${INFILE}, ${MULC}
      ${CDO} -s -setcalendar,${CALENDAR} ${MULC} ${INFILE} ${MODELESGF}_tmpdir_${1}_${2}/tempfile.nc
      ${CDOCMOR} -s cmor,${MIPTABLE},info=${CDOCMORINFO},mapping_table=${MAPPINGTABLE} ${MODELESGF}_tmpdir_${1}_${2}/tempfile.nc
      rm -f ${MODELESGF}_tmpdir_${1}_${2}/*
    done
    for var in ${VARLIST3D}; do
      echo "Currently working on:" ${MODELESGF} ${EXPID1} ${STREAM} ${var}
      rm -f ${MODELESGF}_tmpdir_${1}_${2}/tempfile.nc
      INFILE=/work/bm0162/b380459/TRACMIP/${MODELUOM}/${EXPID1}.${FILE}.${var}.cmiplev.nc
      echo ${INFILE}
      ${CDO} -s -setcalendar,${CALENDAR} -setzaxis,/pf/b/b380459/TRACMIP_Cmorizing/cdoscripts/zaxisdes_cmiplev.txt ${INFILE} ${MODELESGF}_tmpdir_${1}_${2}/tempfile.nc
      ${CDOCMOR} -s cmor,${MIPTABLE},info=${CDOCMORINFO},mapping_table=${MAPPINGTABLE} ${MODELESGF}_tmpdir_${1}_${2}/tempfile.nc
      rm -f ${MODELESGF}_tmpdir_${1}_${2}/*
    done

done
done

# remove temporary directory
rm -rf ${MODELESGF}_tmpdir_${1}_${2} 

# move back to cdo script directory
cd /pf/b/b380459/TRACMIP_Cmorizing/cdoscripts
