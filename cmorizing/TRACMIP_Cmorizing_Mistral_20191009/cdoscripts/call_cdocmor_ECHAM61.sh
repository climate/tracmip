#!/bin/bash
#=============================================================================
# btc batch job parameters
#-----------------------------------------------------------------------------
#SBATCH --account=bm0834
#SBATCH --job-name=call_cdocmor_ECHAM61.sh
#SBATCH --partition=prepost
#SBATCH --ntasks=8
#SBATCH --output=/work/bm0162/b380459/TRACMIP_cmorized/call_cdocmor_ECHAM61.sh.%j.o
#SBATCH --error=/work/bm0162/b380459/TRACMIP_cmorized/call_cdocmor_ECHAM61.sh.%j.o
#SBATCH --time=08:00:00
#=============================================================================

set -e

CDO="cdo -P 8"

set -e

CDOCMOR="/work/bm0021/cdo_incl_cmor/cdo_11_15_2017_cmor2/src/cdo -P 8" 

MAPPINGTABLE=/work/bm0162/b380459/cdocmor_input/ECHAM61/mt_TRACMIP_ECHAM61.txt

VARLIST="cl cli clivi clt clw clwvi evspsbl hfls hfss hur hus pr prc prsn prw ps psl rlds rldscs rlus rlut rlutcs rsds rsdscs rsdt rsus rsuscs rsut rsutcs sfcWind ta tas tasmax tasmin tauu tauv ts ua uas va vas wap zg"

make_AquaControl_Amon="false"
make_Aqua4xCO2_Amon="false"
make_LandControl_Amon="false"
make_Land4xCO2_Amon="false"
make_LandOrbit_Amon="false"

make_AquaControl_Aday="false"
make_Aqua4xCO2_Aday="false"
make_LandControl_Aday="false"
make_Land4xCO2_Aday="false"
make_LandOrbit_Aday="false"

make_AquaControl_A3hr="false"
make_Aqua4xCO2_A3hr="false"
make_LandControl_A3hr="false"
make_Land4xCO2_A3hr="false"
make_LandOrbit_A3hr="true"

# create temporary directory for this model for ECHAM61_tmpdir/tempfile.nc
rm -rf ECHAM61_tmpdir
mkdir ECHAM61_tmpdir

#---------------------
# AquaControl Amon
#---------------------
if [ "x$make_AquaControl_Amon" == "xtrue" ] ; then
   EXPID1=AquaControl
   EXPID2=aquaControlTRACMIP
   MIPTABLE=/work/bm0162/b380459/cdocmor_input/TRACMIP_Amon
   CDOCMORINFO=/work/bm0162/b380459/cdocmor_input/ECHAM61/cdocmorinfo_TRACMIP_ECHAM61_${EXPID2}
   for var in ${VARLIST}; do
      rm -f ECHAM61_tmpdir/tempfile.nc
      echo "Processing Amon" ${EXPID1} ${var}
      DIVC=1.0
      if [ "${var}" == "cli" ] || [ "${var}" == "clw" ]; then
         DIVC=100.0
         #echo ${DIVC}
      fi
      YEARSTART=0016
      YEAREND=0045
      if [ "${var}" == "tasmin" ] || [ "${var}" == "tasmax" ]; then
         YEARSTART=0034
         #echo ${YEARSTART}
      fi
      ${CDO} -s divc,${DIVC} -settaxis,${YEARSTART}-01-01,0,30day -setcalendar,360_day /work/bm0162/b380459/TRACMIP/ECHAM-6.1/${EXPID1}/Amon/${var}_Amon_echam6-1_${EXPID1}_r1i1p1_${YEARSTART}01-${YEAREND}12.nc ECHAM61_tmpdir/tempfile.nc
      ${CDOCMOR} -s cmor,${MIPTABLE},info=${CDOCMORINFO},mapping_table=${MAPPINGTABLE} ECHAM61_tmpdir/tempfile.nc
      rm ECHAM61_tmpdir/tempfile.nc
   done
fi

#---------------------
# AquaControl Aday
#---------------------
if [ "x$make_AquaControl_Aday" == "xtrue" ] ; then
   EXPID1=AquaControl
   EXPID2=aquaControlTRACMIP
   MIPTABLE=/work/bm0162/b380459/cdocmor_input/TRACMIP_Aday
   CDOCMORINFO=/work/bm0162/b380459/cdocmor_input/ECHAM61/cdocmorinfo_TRACMIP_ECHAM61_${EXPID2}
   for var in ${VARLIST}; do
      rm -f ECHAM61_tmpdir/tempfile.nc
      echo "Processing Aday" ${EXPID1} ${var}
      DIVC=1.0
      if [ "${var}" == "cli" ] || [ "${var}" == "clw" ]; then
         DIVC=100.0
         #echo ${DIVC}
      fi
      YEARSTART=0036
      YEAREND=0045
      ${CDO} -s divc,${DIVC} -settaxis,${YEARSTART}-01-01,12,1day -setcalendar,360_day /work/bm0162/b380459/TRACMIP/ECHAM-6.1/${EXPID1}/Aday/${var}_Aday_echam6-1_${EXPID1}_r1i1p1_${YEARSTART}01-${YEAREND}12.nc ECHAM61_tmpdir/tempfile.nc
      ${CDOCMOR} -s cmor,${MIPTABLE},info=${CDOCMORINFO},mapping_table=${MAPPINGTABLE} ECHAM61_tmpdir/tempfile.nc
      rm ECHAM61_tmpdir/tempfile.nc
   done
fi

#---------------------
# AquaControl A3hr
#---------------------
if [ "x$make_AquaControl_A3hr" == "xtrue" ] ; then
   EXPID1=AquaControl
   EXPID2=aquaControlTRACMIP
   MIPTABLE=/work/bm0162/b380459/cdocmor_input/TRACMIP_A3hr
   CDOCMORINFO=/work/bm0162/b380459/cdocmor_input/ECHAM61/cdocmorinfo_TRACMIP_ECHAM61_${EXPID2}
   for var in ${VARLIST}; do
      rm -f ECHAM61_tmpdir/tempfile.nc
      echo "Processing A3hr" ${EXPID1} ${var}
      DIVC=1.0
      if [ "${var}" == "cli" ] || [ "${var}" == "clw" ]; then
         DIVC=100.0
         #echo ${DIVC}
      fi
      YEARSTART=0043
      YEAREND=0045
      ${CDO} -s divc,${DIVC} -settaxis,${YEARSTART}-01-01,00:00,3hour -setcalendar,360_day /work/bm0162/b380459/TRACMIP/ECHAM-6.1/${EXPID1}/A3hr/${var}_A3hr_echam6-1_${EXPID1}_r1i1p1_${YEARSTART}01-${YEAREND}12.nc ECHAM61_tmpdir/tempfile.nc
      ${CDOCMOR} -s cmor,${MIPTABLE},info=${CDOCMORINFO},mapping_table=${MAPPINGTABLE} ECHAM61_tmpdir/tempfile.nc
      rm ECHAM61_tmpdir/tempfile.nc
   done
fi

#---------------------
# Aqua4xCO2 Amon
#---------------------
if [ "x$make_Aqua4xCO2_Amon" == "xtrue" ] ; then
   EXPID1=Aqua4xCO2
   EXPID2=aqua4xCO2TRACMIP
   MIPTABLE=/work/bm0162/b380459/cdocmor_input/TRACMIP_Amon
   CDOCMORINFO=/work/bm0162/b380459/cdocmor_input/ECHAM61/cdocmorinfo_TRACMIP_ECHAM61_${EXPID2}
   for var in ${VARLIST}; do
      rm -f ECHAM61_tmpdir/tempfile.nc
      echo "Processing Amon" ${EXPID1} ${var}
      DIVC=1.0
      if [ "${var}" == "cli" ] || [ "${var}" == "clw" ]; then
         DIVC=100.0
         #echo ${DIVC}
      fi
      YEARSTART=0046
      YEAREND=0090
      ${CDO} -s divc,${DIVC} -settaxis,${YEARSTART}-1-15,0,30day -setcalendar,360_day /work/bm0162/b380459/TRACMIP/ECHAM-6.1/${EXPID1}/Amon/${var}_Amon_echam6-1_${EXPID1}_r1i1p1_${YEARSTART}01-${YEAREND}12.nc ECHAM61_tmpdir/tempfile.nc
      ${CDOCMOR} -s cmor,${MIPTABLE},info=${CDOCMORINFO},mapping_table=${MAPPINGTABLE} ECHAM61_tmpdir/tempfile.nc
      rm ECHAM61_tmpdir/tempfile.nc
   done
fi

#---------------------
# Aqua4xCO2 Aday
#---------------------
if [ "x$make_Aqua4xCO2_Aday" == "xtrue" ] ; then
   EXPID1=Aqua4xCO2
   EXPID2=aqua4xCO2TRACMIP
   MIPTABLE=/work/bm0162/b380459/cdocmor_input/TRACMIP_Aday
   CDOCMORINFO=/work/bm0162/b380459/cdocmor_input/ECHAM61/cdocmorinfo_TRACMIP_ECHAM61_${EXPID2}
   for var in ${VARLIST}; do
      rm -f ECHAM61_tmpdir/tempfile.nc
      echo "Processing Aday" ${EXPID1} ${var}
      DIVC=1.0
      if [ "${var}" == "cli" ] || [ "${var}" == "clw" ]; then
         DIVC=100.0
         #echo ${DIVC}
      fi
      YEARSTART=0081
      YEAREND=0090
      ${CDO} -s divc,${DIVC} -settaxis,${YEARSTART}-1-1,12,1day -setcalendar,360_day /work/bm0162/b380459/TRACMIP/ECHAM-6.1/${EXPID1}/Aday/${var}_Aday_echam6-1_${EXPID1}_r1i1p1_${YEARSTART}01-${YEAREND}12.nc ECHAM61_tmpdir/tempfile.nc
      ${CDOCMOR} -s cmor,${MIPTABLE},info=${CDOCMORINFO},mapping_table=${MAPPINGTABLE} ECHAM61_tmpdir/tempfile.nc
      rm ECHAM61_tmpdir/tempfile.nc
   done
fi

#---------------------
# Aqua4xCO2 A3hr
#---------------------
if [ "x$make_Aqua4xCO2_A3hr" == "xtrue" ] ; then
   EXPID1=Aqua4xCO2
   EXPID2=aqua4xCO2TRACMIP
   MIPTABLE=/work/bm0162/b380459/cdocmor_input/TRACMIP_A3hr
   CDOCMORINFO=/work/bm0162/b380459/cdocmor_input/ECHAM61/cdocmorinfo_TRACMIP_ECHAM61_${EXPID2}
   for var in ${VARLIST}; do
      rm -f ECHAM61_tmpdir/tempfile.nc
      echo "Processing A3hr" ${EXPID1} ${var}
      DIVC=1.0
      if [ "${var}" == "cli" ] || [ "${var}" == "clw" ]; then
         DIVC=100.0
         #echo ${DIVC}
      fi
      YEARSTART=0088
      YEAREND=0090
      ${CDO} -s divc,${DIVC} -settaxis,${YEARSTART}-1-1,00:00,3hour -setcalendar,360_day /work/bm0162/b380459/TRACMIP/ECHAM-6.1/${EXPID1}/A3hr/${var}_A3hr_echam6-1_${EXPID1}_r1i1p1_${YEARSTART}01-${YEAREND}12.nc ECHAM61_tmpdir/tempfile.nc
      ${CDOCMOR} -s cmor,${MIPTABLE},info=${CDOCMORINFO},mapping_table=${MAPPINGTABLE} ECHAM61_tmpdir/tempfile.nc
      rm ECHAM61_tmpdir/tempfile.nc
   done
fi

#---------------------
# LandControl Amon
#---------------------
if [ "x$make_LandControl_Amon" == "xtrue" ] ; then
   EXPID1=LandControl
   EXPID2=landControlTRACMIP
   MIPTABLE=/work/bm0162/b380459/cdocmor_input/TRACMIP_Amon
   CDOCMORINFO=/work/bm0162/b380459/cdocmor_input/ECHAM61/cdocmorinfo_TRACMIP_ECHAM61_${EXPID2}
   for var in ${VARLIST}; do
      rm -f ECHAM61_tmpdir/tempfile.nc
      echo "Processing Amon" ${EXPID1} ${var}
      DIVC=1.0
      if [ "${var}" == "cli" ] || [ "${var}" == "clw" ]; then
         DIVC=100.0
         #echo ${DIVC}
      fi
      YEARSTART=0046
      YEAREND=0090
      ${CDO} -s divc,${DIVC} -settaxis,${YEARSTART}-1-15,0,30day -setcalendar,360_day /work/bm0162/b380459/TRACMIP/ECHAM-6.1/${EXPID1}/Amon/${var}_Amon_echam6-1_${EXPID1}_r1i1p1_${YEARSTART}01-${YEAREND}12.nc ECHAM61_tmpdir/tempfile.nc
      ${CDOCMOR} -s cmor,${MIPTABLE},info=${CDOCMORINFO},mapping_table=${MAPPINGTABLE} ECHAM61_tmpdir/tempfile.nc
      rm ECHAM61_tmpdir/tempfile.nc
   done
fi

#---------------------
# LandControl Aday
#---------------------
if [ "x$make_LandControl_Aday" == "xtrue" ] ; then
   EXPID1=LandControl
   EXPID2=landControlTRACMIP
   MIPTABLE=/work/bm0162/b380459/cdocmor_input/TRACMIP_Aday
   CDOCMORINFO=/work/bm0162/b380459/cdocmor_input/ECHAM61/cdocmorinfo_TRACMIP_ECHAM61_${EXPID2}
   for var in ${VARLIST}; do
      rm -f ECHAM61_tmpdir/tempfile.nc
      echo "Processing Aday" ${EXPID1} ${var}
      DIVC=1.0
      if [ "${var}" == "cli" ] || [ "${var}" == "clw" ]; then
         DIVC=100.0
         #echo ${DIVC}
      fi
      YEARSTART=0081
      YEAREND=0090
      ${CDO} -s divc,${DIVC} -settaxis,${YEARSTART}-1-1,12,1day -setcalendar,360_day /work/bm0162/b380459/TRACMIP/ECHAM-6.1/${EXPID1}/Aday/${var}_Aday_echam6-1_${EXPID1}_r1i1p1_${YEARSTART}01-${YEAREND}12.nc ECHAM61_tmpdir/tempfile.nc
      ${CDOCMOR} -s cmor,${MIPTABLE},info=${CDOCMORINFO},mapping_table=${MAPPINGTABLE} ECHAM61_tmpdir/tempfile.nc
      rm ECHAM61_tmpdir/tempfile.nc
   done
fi

#---------------------
# LandControl A3hr
#---------------------
if [ "x$make_LandControl_A3hr" == "xtrue" ] ; then
   EXPID1=LandControl
   EXPID2=landControlTRACMIP
   MIPTABLE=/work/bm0162/b380459/cdocmor_input/TRACMIP_A3hr
   CDOCMORINFO=/work/bm0162/b380459/cdocmor_input/ECHAM61/cdocmorinfo_TRACMIP_ECHAM61_${EXPID2}
   for var in ${VARLIST}; do
      rm -f ECHAM61_tmpdir/tempfile.nc
      echo "Processing A3hr" ${EXPID1} ${var}
      DIVC=1.0
      if [ "${var}" == "cli" ] || [ "${var}" == "clw" ]; then
         DIVC=100.0
         #echo ${DIVC}
      fi
      YEARSTART=0088
      YEAREND=0090
      ${CDO} -s divc,${DIVC} -settaxis,${YEARSTART}-1-1,00:00,3hour -setcalendar,360_day /work/bm0162/b380459/TRACMIP/ECHAM-6.1/${EXPID1}/A3hr/${var}_A3hr_echam6-1_${EXPID1}_r1i1p1_${YEARSTART}01-${YEAREND}12.nc ECHAM61_tmpdir/tempfile.nc
      ${CDOCMOR} -s cmor,${MIPTABLE},info=${CDOCMORINFO},mapping_table=${MAPPINGTABLE} ECHAM61_tmpdir/tempfile.nc
      rm ECHAM61_tmpdir/tempfile.nc
   done
fi

#---------------------
# Land4xCO2 Amon
#---------------------
if [ "x$make_Land4xCO2_Amon" == "xtrue" ] ; then
   EXPID1=Land4xCO2
   EXPID2=land4xCO2TRACMIP
   MIPTABLE=/work/bm0162/b380459/cdocmor_input/TRACMIP_Amon
   CDOCMORINFO=/work/bm0162/b380459/cdocmor_input/ECHAM61/cdocmorinfo_TRACMIP_ECHAM61_${EXPID2}
   for var in ${VARLIST}; do
      rm -f ECHAM61_tmpdir/tempfile.nc
      echo "Processing Amon" ${EXPID1} ${var}
      DIVC=1.0
      if [ "${var}" == "cli" ] || [ "${var}" == "clw" ]; then
         DIVC=100.0
         #echo ${DIVC}
      fi
      YEARSTART=0091
      YEAREND=0135
      ${CDO} -s divc,${DIVC} -settaxis,${YEARSTART}-1-15,0,30day -setcalendar,360_day /work/bm0162/b380459/TRACMIP/ECHAM-6.1/${EXPID1}/Amon/${var}_Amon_echam6-1_${EXPID1}_r1i1p1_${YEARSTART}01-${YEAREND}12.nc ECHAM61_tmpdir/tempfile.nc
      ${CDOCMOR} -s cmor,${MIPTABLE},info=${CDOCMORINFO},mapping_table=${MAPPINGTABLE} ECHAM61_tmpdir/tempfile.nc
      rm ECHAM61_tmpdir/tempfile.nc
   done
fi

#---------------------
# Land4xCO2 Aday
#---------------------
if [ "x$make_Land4xCO2_Aday" == "xtrue" ] ; then
   EXPID1=Land4xCO2
   EXPID2=land4xCO2TRACMIP
   MIPTABLE=/work/bm0162/b380459/cdocmor_input/TRACMIP_Aday
   CDOCMORINFO=/work/bm0162/b380459/cdocmor_input/ECHAM61/cdocmorinfo_TRACMIP_ECHAM61_${EXPID2}
   for var in ${VARLIST}; do
      rm -f ECHAM61_tmpdir/tempfile.nc
      echo "Processing Aday" ${EXPID1} ${var}
      DIVC=1.0
      if [ "${var}" == "cli" ] || [ "${var}" == "clw" ]; then
         DIVC=100.0
         #echo ${DIVC}
      fi
      YEARSTART=0126
      YEAREND=0135
      ${CDO} -s divc,${DIVC} -settaxis,${YEARSTART}-1-1,12,1day -setcalendar,360_day /work/bm0162/b380459/TRACMIP/ECHAM-6.1/${EXPID1}/Aday/${var}_Aday_echam6-1_${EXPID1}_r1i1p1_${YEARSTART}01-${YEAREND}12.nc ECHAM61_tmpdir/tempfile.nc
      ${CDOCMOR} -s cmor,${MIPTABLE},info=${CDOCMORINFO},mapping_table=${MAPPINGTABLE} ECHAM61_tmpdir/tempfile.nc
      rm ECHAM61_tmpdir/tempfile.nc
   done
fi

#---------------------
# Land4xCO2 A3hr
#---------------------
if [ "x$make_Land4xCO2_A3hr" == "xtrue" ] ; then
   EXPID1=Land4xCO2
   EXPID2=land4xCO2TRACMIP
   MIPTABLE=/work/bm0162/b380459/cdocmor_input/TRACMIP_A3hr
   CDOCMORINFO=/work/bm0162/b380459/cdocmor_input/ECHAM61/cdocmorinfo_TRACMIP_ECHAM61_${EXPID2}
   for var in ${VARLIST}; do
      rm -f ECHAM61_tmpdir/tempfile.nc
      echo "Processing A3hr" ${EXPID1} ${var}
      DIVC=1.0
      if [ "${var}" == "cli" ] || [ "${var}" == "clw" ]; then
         DIVC=100.0
         #echo ${DIVC}
      fi
      YEARSTART=0133
      YEAREND=0135
      ${CDO} -s divc,${DIVC} -settaxis,${YEARSTART}-1-1,00:00,3hour -setcalendar,360_day /work/bm0162/b380459/TRACMIP/ECHAM-6.1/${EXPID1}/A3hr/${var}_A3hr_echam6-1_${EXPID1}_r1i1p1_${YEARSTART}01-${YEAREND}12.nc ECHAM61_tmpdir/tempfile.nc
      ${CDOCMOR} -s cmor,${MIPTABLE},info=${CDOCMORINFO},mapping_table=${MAPPINGTABLE} ECHAM61_tmpdir/tempfile.nc
      rm ECHAM61_tmpdir/tempfile.nc
   done
fi

#---------------------
# LandOrbit Amon
#---------------------
if [ "x$make_LandOrbit_Amon" == "xtrue" ] ; then
   EXPID1=LandOrbit
   EXPID2=landOrbitTRACMIP
   MIPTABLE=/work/bm0162/b380459/cdocmor_input/TRACMIP_Amon
   CDOCMORINFO=/work/bm0162/b380459/cdocmor_input/ECHAM61/cdocmorinfo_TRACMIP_ECHAM61_${EXPID2}
   for var in ${VARLIST}; do
      rm -f ECHAM61_tmpdir/tempfile.nc
      echo "Processing Amon" ${EXPID1} ${var}
      DIVC=1.0
      if [ "${var}" == "cli" ] || [ "${var}" == "clw" ]; then
         DIVC=100.0
         #echo ${DIVC}
      fi
      YEARSTART=0091
      YEAREND=0135
      ${CDO} -s divc,${DIVC} -settaxis,${YEARSTART}-1-15,0,30day -setcalendar,360_day /work/bm0162/b380459/TRACMIP/ECHAM-6.1/${EXPID1}/Amon/${var}_Amon_echam6-1_${EXPID1}_r1i1p1_${YEARSTART}01-${YEAREND}12.nc ECHAM61_tmpdir/tempfile.nc
      ${CDOCMOR} -s cmor,${MIPTABLE},info=${CDOCMORINFO},mapping_table=${MAPPINGTABLE} ECHAM61_tmpdir/tempfile.nc
      rm ECHAM61_tmpdir/tempfile.nc
   done
fi

#---------------------
# LandOrbit Aday
#---------------------
if [ "x$make_LandOrbit_Aday" == "xtrue" ] ; then
   EXPID1=LandOrbit
   EXPID2=landOrbitTRACMIP
   MIPTABLE=/work/bm0162/b380459/cdocmor_input/TRACMIP_Aday
   CDOCMORINFO=/work/bm0162/b380459/cdocmor_input/ECHAM61/cdocmorinfo_TRACMIP_ECHAM61_${EXPID2}
   for var in ${VARLIST}; do
      rm -f ECHAM61_tmpdir/tempfile.nc
      echo "Processing Aday" ${EXPID1} ${var}
      DIVC=1.0
      if [ "${var}" == "cli" ] || [ "${var}" == "clw" ]; then
         DIVC=100.0
         #echo ${DIVC}
      fi
      YEARSTART=0126
      YEAREND=0135
      ${CDO} -s divc,${DIVC} -settaxis,${YEARSTART}-1-1,12,1day -setcalendar,360_day /work/bm0162/b380459/TRACMIP/ECHAM-6.1/${EXPID1}/Aday/${var}_Aday_echam6-1_${EXPID1}_r1i1p1_${YEARSTART}01-${YEAREND}12.nc ECHAM61_tmpdir/tempfile.nc
      ${CDOCMOR} -s cmor,${MIPTABLE},info=${CDOCMORINFO},mapping_table=${MAPPINGTABLE} ECHAM61_tmpdir/tempfile.nc
      rm ECHAM61_tmpdir/tempfile.nc
   done
fi

#---------------------
# LandOrbit A3hr
#---------------------
if [ "x$make_LandOrbit_A3hr" == "xtrue" ] ; then
   EXPID1=LandOrbit
   EXPID2=landOrbitTRACMIP
   MIPTABLE=/work/bm0162/b380459/cdocmor_input/TRACMIP_A3hr
   CDOCMORINFO=/work/bm0162/b380459/cdocmor_input/ECHAM61/cdocmorinfo_TRACMIP_ECHAM61_${EXPID2}
   for var in ${VARLIST}; do
      rm -f ECHAM61_tmpdir/tempfile.nc
      echo "Processing A3hr" ${EXPID1} ${var}
      DIVC=1.0
      if [ "${var}" == "cli" ] || [ "${var}" == "clw" ]; then
         DIVC=100.0
         #echo ${DIVC}
      fi
      YEARSTART=0133
      YEAREND=0135
      ${CDO} -s divc,${DIVC} -settaxis,${YEARSTART}-1-1,00:00,3hour -setcalendar,360_day /work/bm0162/b380459/TRACMIP/ECHAM-6.1/${EXPID1}/A3hr/${var}_A3hr_echam6-1_${EXPID1}_r1i1p1_${YEARSTART}01-${YEAREND}12.nc ECHAM61_tmpdir/tempfile.nc
      ${CDOCMOR} -s cmor,${MIPTABLE},info=${CDOCMORINFO},mapping_table=${MAPPINGTABLE} ECHAM61_tmpdir/tempfile.nc
      rm ECHAM61_tmpdir/tempfile.nc
   done
fi

# remove temporary working directory
rm -rf ECHAM61_tmpdir
