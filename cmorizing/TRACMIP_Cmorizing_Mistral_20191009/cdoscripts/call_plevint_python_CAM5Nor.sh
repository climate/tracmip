#!/bin/bash
# note the following:
# we use python xarray and scipy interpolation to remap 3d data to 17 CMIP5 pressure levels

set -e

# we use the anaconda bleeding edge python version, as it contains cftime, which allows us to circumvent the dates issues that I saw with python/3.5.2
module unload netcdf_c/4.3.2-gcc48
module load anaconda3/bleeding_edge

rm -f plev_interpolate_CAM5Nor.py
cat << 'EOF' >> plev_interpolate_CAM5Nor.py

import xarray as xr
import numpy as np
from scipy import interpolate
import sys

fpath=sys.argv[1]
fname=sys.argv[2]
varname=sys.argv[3]
print(fpath,fname)

ds = xr.open_dataset(fpath+'/'+fname+'.nc')

# check if variable is in data set: only in this case go ahead with the script
if (varname in ds.keys()):

    ntim = ds.time.size
    nlat = ds.lat.size
    nlon = ds.lon.size

    plev_int = ( [100000.0, 92500.0, 85000.0, 70000.0, 60000.0, 50000.0, 40000.0, 30000.0, 25000.0, 20000.0, 15000.0, 10000.0,
                  7000.0, 5000.0, 3000.0, 2000.0, 1000.0] )

    # create new dataset with vertically interpolated 3d fields
    ds_int = xr.Dataset()
    ds_int.attrs = ds.attrs

    var  = ds[varname].values
    ps   = ds['ps'].values
    a    = ds['a'].values
    b    = ds['b'].values
    p0   = ds['p0'].values

    pmid = np.zeros((ntim, 30, nlat, nlon)) + np.nan
    for t in range(ntim):
        for k in range(30):
             pmid[t,k] = a[k]*p0 + b[k]*ps[t]

    varint = np.zeros((ntim, 17, nlat, nlon)) + np.nan

    for t in range(ntim):
        if (t%100==0):
            print(fname, varname, t)
        for j in range(nlat):
            for i in range(nlon):
                f_int = interpolate.interp1d(pmid[t,:,j,i], var[t,:,j,i], bounds_error=False, kind='linear', fill_value='extrapolate')
                varint[t,:,j,i] = f_int(plev_int)

    # if variable is cloud cover or relative humidity, then restrict to physical range of 0...100
    if varname=='cl':
        varint = np.maximum(np.minimum(varint,100.0),0.0)
    if varname=='hur':
        varint = np.maximum(np.minimum(varint,100.0),0.0)


    # convert varint into dataarray
    da_varint = xr.DataArray(varint, name=varname, dims=('time','plev','lat','lon'), coords={'time': ds.time, 'plev': plev_int, 'lat': ds.lat, 'lon': ds.lon})

    ds_int[varname]=da_varint
    # add time_bnds if available in original file
    if ('time_bnds' in ds.keys()): 
       ds_int['time_bnds'] = ds['time_bnds']
    ds_int['lon_bnds'] = ds['lon_bnds']
    ds_int['lat_bnds'] = ds['lat_bnds']

    ds_int.to_netcdf(path=fpath+'/'+fname+'.cmiplev.nc')

EOF

# monthly fields
#for var in cl cli clw; do
#   python3 plev_interpolate_CAM5Nor.py "/work/bm0162/b380459/TRACMIP/NorESM2/AquaControl/amip/mon/atmos/${var}/r1i1p1/" "${var}_Amon_AquaControl_amip_r1i1p1_000201-004512" "${var}"
#   python3 plev_interpolate_CAM5Nor.py "/work/bm0162/b380459/TRACMIP/NorESM2/Aqua4xCO2/Amon/" "${var}_Amon_Aqua4xCO2_amip_r1i1p1_004601-008612" "${var}"
#   python3 plev_interpolate_CAM5Nor.py "/work/bm0162/b380459/TRACMIP/NorESM2/LandControl/amip/mon/atmos/${var}/r1i1p1/" "${var}_Amon_LandControl_amip_r1i1p1_004603-008512" "${var}"
#   python3 plev_interpolate_CAM5Nor.py "/work/bm0162/b380459/TRACMIP/NorESM2/Land4xCO2/Amon/" "${var}_Amon_Land4xCO2_amip_r1i1p1_008601-012602" "${var}"
#done

# note: there is no daily 3d cloud data, so no interpolation needed
# daily 3d wind, humidity, geopot and temp fields are already on cmip levels

# UPDATE: 6hourly data was found to be all 0/nan, so not used further here ...

# 6 hourly 3d data
for var in hus ta ua va; do
   python3 plev_interpolate_CAM5Nor.py "/work/bm0162/b380459/TRACMIP/NorESM2/AquaControl/amip/6hr/atmos/${var}/r1i1p1/" "${var}_6hrLev_AquaControl_amip_r1i1p1_year0036-year0045" "${var}"
   python3 plev_interpolate_CAM5Nor.py "/work/bm0162/b380459/TRACMIP/NorESM2/Aqua4xCO2/A6hr/" "${var}_6hrLev_Aqua4xCO2_amip_r1i1p1_year0077-year0086" "${var}"
   python3 plev_interpolate_CAM5Nor.py "/work/bm0162/b380459/TRACMIP/NorESM2/LandControl/amip/6hr/atmos/${var}/r1i1p1/" "${var}_6hrLev_LandControl_amip_r1i1p1_year0076-year0085" "${var}"
   python3 plev_interpolate_CAM5Nor.py "/work/bm0162/b380459/TRACMIP/NorESM2/Land4xCO2/A6hr/" "${var}_6hrLev_Land4xCO2_amip_r1i1p1_year0116-year0125" "${var}"
done

rm -f plev_interpolate_CAM5Nor.py
