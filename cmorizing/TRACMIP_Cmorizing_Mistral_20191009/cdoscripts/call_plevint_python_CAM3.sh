#!/bin/bash
# note the following:
# we use python xarray and scipy interpolation to remap 3d data to 17 CMIP5 pressure levels

set -e

# we use the anaconda bleeding edge python version, as it contains cftime, which allows us to circumvent the dates issues that I saw with python/3.5.2
module unload netcdf_c/4.3.2-gcc48
module load anaconda3/bleeding_edge

rm -f plev_interpolate_CAM3.py
cat << 'EOF' >> plev_interpolate_CAM3.py

import xarray as xr
import numpy as np
from scipy import interpolate
import sys

fpath=sys.argv[1]
fname=sys.argv[2]
varname=sys.argv[3]
print(fpath,fname)

ds = xr.open_dataset(fpath+'/'+fname+'.nc')

# check if variable is in data set: only in this case go ahead with the script
if (varname in ds.keys()):

    ntim = ds.time.size
    nlat = ds.lat.size
    nlon = ds.lon.size

    plev_int = ( [100000.0, 92500.0, 85000.0, 70000.0, 60000.0, 50000.0, 40000.0, 30000.0, 25000.0, 20000.0, 15000.0, 10000.0,
                  7000.0, 5000.0, 3000.0, 2000.0, 1000.0] )

    # create new dataset with vertically interpolated 3d fields
    ds_int = xr.Dataset()
    ds_int.attrs = ds.attrs

    var  = ds[varname].values
    ps   = ds['PS'].values
    
    # in some files, hyam, hybm and p0 are missing, so we set them manually here
    #hyam = ds['hyam'].values
    #hybm = ds['hybm'].values
    #p0   = ds['P0'].values

    hyam = np.array([ 0.00354464,  0.00738881,  0.01396721,  0.02394462,  0.03723029,
        0.05311461,  0.07005915,  0.07791257,  0.07660701,  0.07507109,
        0.07326415,  0.07113839,  0.06863753,  0.06569541,  0.06223416,
        0.05816217,  0.05337168,  0.04773593,  0.04110576,  0.0333057 ,
        0.02496844,  0.01709591,  0.01021471,  0.00480318,  0.00126068,  0.        ])
    hybm = np.array([ 0.        ,  0.        ,  0.        ,  0.        ,  0.        ,
        0.        ,  0.        ,  0.00752655,  0.02390769,  0.04317925,
        0.06585125,  0.09252369,  0.1239024 ,  0.16081785,  0.204247  ,
        0.2553391 ,  0.3154463 ,  0.3861593 ,  0.4693495 ,  0.5672185 ,
        0.67182785,  0.77060615,  0.85694605,  0.9248457 ,  0.96929415,
        0.9925561 ])
    p0 = 100000.0

    pmid = np.zeros((ntim, 26, nlat, nlon)) + np.nan
    for t in range(ntim):
        for k in range(26):
             pmid[t,k] = hyam[k]*p0 + hybm[k]*ps[t]

    varint = np.zeros((ntim, 17, nlat, nlon)) + np.nan

    for t in range(ntim):
        if (t%100==0):
            print(fname, varname, t)
        for j in range(nlat):
            for i in range(nlon):
                f_int = interpolate.interp1d(pmid[t,:,j,i], var[t,:,j,i], bounds_error=False, kind='linear', fill_value='extrapolate')
                varint[t,:,j,i] = f_int(plev_int)

    # if variable is cloud cover or relative humidity, then restrict to physical range of 0..1 for CLOUD, and 0..100 for RELHUM
    if varname=='CLOUD':
        varint = np.maximum(np.minimum(varint,1.0),0.0)
    if varname=='RELHUM':
        varint = np.maximum(np.minimum(varint,100.0),0.0)


    # convert varint into dataarray
    da_varint = xr.DataArray(varint, name=varname, dims=('time','lev','lat','lon'), coords={'time': ds.time, 'lev': plev_int, 'lat': ds.lat, 'lon': ds.lon})

    ds_int[varname]=da_varint

    ds_int.to_netcdf(path=fpath+'/'+fname+'.'+varname+'.cmiplev.nc')

EOF

# note: 3hourly data missing for Aqua4xCO2
#       3d cloud data missing for 3hourly land simulations
#       maybe more data is missing ... but this is caught in the python script

#for EXPID in AquaControl; do
#for VAR in CLOUD CLDICE CLDLIQ RELHUM Q T U V OMEGA Z3; do
#   python3 plev_interpolate_CAM3.py "/work/bm0162/b380459/TRACMIP/CAM3/${EXPID}/" "${EXPID}_monthly" "${VAR}" 
#   python3 plev_interpolate_CAM3.py "/work/bm0162/b380459/TRACMIP/CAM3/${EXPID}/" "${EXPID}_daily"   "${VAR}" 
#   python3 plev_interpolate_CAM3.py "/work/bm0162/b380459/TRACMIP/CAM3/${EXPID}/" "${EXPID}_3hourly" "${VAR}" 
#done
#done

#for EXPID in Aqua4xCO2; do
#for VAR in CLOUD CLDICE CLDLIQ RELHUM Q T U V OMEGA Z3; do
#   python3 plev_interpolate_CAM3.py "/work/bm0162/b380459/TRACMIP/CAM3/${EXPID}/" "${EXPID}_monthly" "${VAR}" 
#   python3 plev_interpolate_CAM3.py "/work/bm0162/b380459/TRACMIP/CAM3/${EXPID}/" "${EXPID}_daily"   "${VAR}" 
#done
#done

for EXPID in LandControl Land4xCO2 LandOrbit; do
for VAR in CLOUD CLDICE CLDLIQ RELHUM Q T U V OMEGA Z3; do
   python3 plev_interpolate_CAM3.py "/work/bm0162/b380459/TRACMIP/CAM3/${EXPID}/" "${EXPID}_monthly" "${VAR}" 
   python3 plev_interpolate_CAM3.py "/work/bm0162/b380459/TRACMIP/CAM3/${EXPID}/" "${EXPID}_daily"   "${VAR}" 
   python3 plev_interpolate_CAM3.py "/work/bm0162/b380459/TRACMIP/CAM3/${EXPID}/" "${EXPID}_3hourly" "${VAR}" 
done


done


rm -f plev_interpolate_CAM3.py
