#!/bin/bash
#=============================================================================
# btc batch job parameters
#-----------------------------------------------------------------------------
#SBATCH --account=bm0834
#SBATCH --job-name=call_cdocmor_AM21.sh
#SBATCH --partition=prepost
#SBATCH --ntasks=2
#SBATCH --cpus-per-task=8      # Specify number of CPUs per task
#SBATCH --output=/work/bm0162/b380459/TRACMIP_cmorized/call_cdocmor_AM21.sh.%j.o
#SBATCH --error=/work/bm0162/b380459/TRACMIP_cmorized/call_cdocmor_AM21.sh.%j.o
#SBATCH --time=08:00:00
#=============================================================================

# note: no LandOrbit data
# - daily and 8xdaily streams do not have all variables
# - daily 2d fields have only 5 years of data (1800 time steps instead of 3600), nevertheless cmorized
# - 8xdaily 2d fields have only 100 or 2000 timesteps, so not even a full year -> not cmorized (3d data has all timesteps and is cmorized)
# - there is no land orbit data

# Bind your OpenMP threads
export OMP_NUM_THREADS=8
export KMP_AFFINITY=verbose,granularity=thread,compact,1
export KMP_STACKSIZE=64m

set -ex

module unload nco
module unload cdo

module load cdo/1.9.5-gcc64
module load nco/4.7.5-gcc64

CDO="cdo -P 8" 
CDOCMOR="/work/bm0021/cdo_incl_cmor/cdo_11_15_2017_cmor2/src/cdo -P 8" 

MODELUOM=AM2   # model name at UoMiami server
MODELUOM2=AM2 # for some models, there is a second UoMiami name needed for the actual variable file (differs from directory name by, e.g., small or capital letters)
MODELESGF=AM21  # ESGF model name

MAPPINGTABLE=/pf/b/b380459/TRACMIP_Cmorizing/cdocmor_input/${MODELESGF}/mt_TRACMIP_${MODELESGF}.txt
echo "Using MAPPINGTABLE:" ${MAPPINGTABLE}

EXPID2_AquaControl=aquaControlTRACMIP
EXPID2_Aqua4xCO2=aqua4xCO2TRACMIP
EXPID2_LandControl=landControlTRACMIP
EXPID2_Land4xCO2=land4xCO2TRACMIP

# move into TRACMIP work directory to do actual work
cd /work/bm0162/b380459/TRACMIP_cmorized
pwd

# create temporary directory for this model for tempfile.nc
rm -rf ${MODELESGF}_tmpdir
mkdir ${MODELESGF}_tmpdir

#############################
# Amon Stream

VARLIST_2D="tas ts psl uas vas sfcWind hurs pr prc evspsbl tauu tauv hfss rlds rlus rsds rsus rsdscs rsuscs rldscs rsdt rsut rlut rlutcs rsutcs prw clt clivi rtmt"
VARLIST_3D="hur hus ta ua va wap zg cl cli clw"

make_Amon=true
if [ "x$make_Amon" == "xtrue" ] ; then

for EXPID1 in AquaControl Aqua4xCO2 LandControl Land4xCO2; do
for STREAM in Amon; do

   SETTAXIS="ERROR"  # so that script stops if none of the if conditions is met
   if [ "${EXPID1}" == "AquaControl" ]; then
      SETTAXIS="-setcalendar,360_day -settaxis,0001-01-16,00:00:00,30day"
   elif [ "${EXPID1}" == "Aqua4xCO2" ]; then
      SETTAXIS="-setcalendar,360_day -settaxis,0046-01-16,00:00:00,30day"
   elif [ "${EXPID1}" == "LandControl" ]; then
      SETTAXIS="-setcalendar,360_day -settaxis,0046-01-16,00:00:00,30day"
   elif [ "${EXPID1}" == "Land4xCO2" ]; then
      SETTAXIS="-setcalendar,360_day -settaxis,0086-01-16,00:00:00,30day"
   fi
   

   EXPID2=EXPID2_${EXPID1}
   MIPTABLE=/pf/b/b380459/TRACMIP_Cmorizing/cdocmor_input/TRACMIP_${STREAM}
   CDOCMORINFO=/pf/b/b380459/TRACMIP_Cmorizing/cdocmor_input/${MODELESGF}/cdocmorinfo_TRACMIP_${MODELESGF}_${!EXPID2}

   for var in ${VARLIST_2D}; do
      echo "Currently working on:" ${MODELESGF} ${EXPID1} ${STREAM} ${var}
      rm -f ${MODELESGF}_tmpdir/tempfile.nc
      INFILE=/work/bm0834/b380459/TRACMIP/${MODELUOM}/${EXPID1}/${EXPID1}_month.${var}.fixed_dimensions.nc
      echo ${INFILE}
      ${CDOCMOR} -s cmor,${MIPTABLE},info=${CDOCMORINFO},mapping_table=${MAPPINGTABLE} -setgrid,/pf/b/b380459/TRACMIP_Cmorizing/cdocmor_input/AM21/AM21_griddes.txt ${SETTAXIS} -selvar,${var} ${INFILE}
      rm -f ${MODELESGF}_tmpdir/*
    done

   for var in ${VARLIST_3D}; do
      echo "Currently working on:" ${MODELESGF} ${EXPID1} ${STREAM} ${var}
      rm -f ${MODELESGF}_tmpdir/tempfile.nc
      INFILE=/work/bm0834/b380459/TRACMIP/${MODELUOM}/${EXPID1}/${EXPID1}_month.${var}.fixed_dimensions.nc
      echo ${INFILE}
      ${CDOCMOR} -s cmor,${MIPTABLE},info=${CDOCMORINFO},mapping_table=${MAPPINGTABLE} -setzaxis,/pf/b/b380459/TRACMIP_Cmorizing/cdocmor_input/AM21/AM21_zaxisdes.txt -setgrid,/pf/b/b380459/TRACMIP_Cmorizing/cdocmor_input/AM21/AM21_griddes.txt ${SETTAXIS} -selvar,${var} ${INFILE}
      rm -f ${MODELESGF}_tmpdir/*
    done

done
done

fi

#############################
# Aday Stream

VARLIST_2D="ts psl pr prc evspsbl tauu tauv hfss rlds rlus rsds rsus rsdscs rsuscs rldscs rsdt rsut rlut rlutcs rsutcs clt"
VARLIST_3D="hur hus ta ua va wap zg"

make_Aday=true
if [ "x$make_Aday" == "xtrue" ] ; then

for EXPID1 in AquaControl Aqua4xCO2 LandControl Land4xCO2; do
for STREAM in Aday; do

   SETTAXIS="ERROR"  # so that script stops if none of the if conditions is met
   if [ "${EXPID1}" == "AquaControl" ]; then
      SETTAXIS="-setcalendar,360_day -settaxis,0036-01-01,12:00:00,1day"
   elif [ "${EXPID1}" == "Aqua4xCO2" ]; then
      SETTAXIS="-setcalendar,360_day -settaxis,0076-01-01,12:00:00,1day"
   elif [ "${EXPID1}" == "LandControl" ]; then
      SETTAXIS="-setcalendar,360_day -settaxis,0076-01-01,12:00:00,1day"
   elif [ "${EXPID1}" == "Land4xCO2" ]; then
      SETTAXIS="-setcalendar,360_day -settaxis,0116-01-01,12:00:00,1day"
   fi

   EXPID2=EXPID2_${EXPID1}
   MIPTABLE=/pf/b/b380459/TRACMIP_Cmorizing/cdocmor_input/TRACMIP_${STREAM}
   CDOCMORINFO=/pf/b/b380459/TRACMIP_Cmorizing/cdocmor_input/${MODELESGF}/cdocmorinfo_TRACMIP_${MODELESGF}_${!EXPID2}

   for var in ${VARLIST_2D}; do
      echo "Currently working on:" ${MODELESGF} ${EXPID1} ${STREAM} ${var}
      rm -f ${MODELESGF}_tmpdir/tempfile.nc
      INFILE=/work/bm0834/b380459/TRACMIP/${MODELUOM}/${EXPID1}/${EXPID1}_daily.${var}.fixed_dimensions.nc
      echo ${INFILE}
      ${CDOCMOR} -s cmor,${MIPTABLE},info=${CDOCMORINFO},mapping_table=${MAPPINGTABLE} -setgrid,/pf/b/b380459/TRACMIP_Cmorizing/cdocmor_input/AM21/AM21_griddes.txt ${SETTAXIS} -selvar,${var} ${INFILE}
      rm -f ${MODELESGF}_tmpdir/*
    done

   for var in ${VARLIST_3D}; do
      echo "Currently working on:" ${MODELESGF} ${EXPID1} ${STREAM} ${var}
      rm -f ${MODELESGF}_tmpdir/tempfile.nc
      INFILE=/work/bm0834/b380459/TRACMIP/${MODELUOM}/${EXPID1}/${EXPID1}_daily.${var}.fixed_dimensions.nc
      echo ${INFILE}
      ${CDOCMOR} -s cmor,${MIPTABLE},info=${CDOCMORINFO},mapping_table=${MAPPINGTABLE} -setzaxis,/pf/b/b380459/TRACMIP_Cmorizing/cdocmor_input/AM21/AM21_zaxisdes.txt -setgrid,/pf/b/b380459/TRACMIP_Cmorizing/cdocmor_input/AM21/AM21_griddes.txt ${SETTAXIS} -selvar,${var} ${INFILE}
      rm -f ${MODELESGF}_tmpdir/*
    done

done
done

fi 

#############################
# A3hr Stream

# 2d 3hourly data not cmorized due to lack of sufficient timesteps

#VARLIST_2D="ts psl pr evspsbl hfss rlds rlus rsds rsus rsdscs rsuscs rldscs rsdt rsut rlut rlutcs rsutcs"
VARLIST_3D="hur hus ta ua va wap zg"

make_A3hr=true
if [ "x$make_A3hr" == "xtrue" ] ; then

for EXPID1 in AquaControl Aqua4xCO2 LandControl Land4xCO2; do
for STREAM in A3hr; do

   SETTAXIS="ERROR"  # so that script stops if none of the if conditions is met
   if [ "${EXPID1}" == "AquaControl" ]; then
      SETTAXIS="-setcalendar,360_day -settaxis,0043-01-01,01:30:00,3hour"
   elif [ "${EXPID1}" == "Aqua4xCO2" ]; then
      SETTAXIS="-setcalendar,360_day -settaxis,0083-01-01,01:30:00,3hour"
   elif [ "${EXPID1}" == "LandControl" ]; then
      SETTAXIS="-setcalendar,360_day -settaxis,0083-01-01,01:30:00,3hour"
   elif [ "${EXPID1}" == "Land4xCO2" ]; then
      SETTAXIS="-setcalendar,360_day -settaxis,0123-01-01,01:30:00,3hour"
   fi

   EXPID2=EXPID2_${EXPID1}
   MIPTABLE=/pf/b/b380459/TRACMIP_Cmorizing/cdocmor_input/TRACMIP_${STREAM}
   CDOCMORINFO=/pf/b/b380459/TRACMIP_Cmorizing/cdocmor_input/${MODELESGF}/cdocmorinfo_TRACMIP_${MODELESGF}_${!EXPID2}

#   for var in ${VARLIST_2D}; do
#      echo "Currently working on:" ${MODELESGF} ${EXPID1} ${STREAM} ${var}
#      rm -f ${MODELESGF}_tmpdir/tempfile.nc
#      INFILE=/work/bm0834/b380459/TRACMIP/${MODELUOM}/${EXPID1}/${EXPID1}_8xdaily.${var}.fixed_dimensions.nc
#      echo ${INFILE}
#      ${CDOCMOR} -s cmor,${MIPTABLE},info=${CDOCMORINFO},mapping_table=${MAPPINGTABLE} -setgrid,/pf/b/b380459/TRACMIP_Cmorizing/cdocmor_input/AM21/AM21_griddes.txt ${SETTAXIS} -selvar,${var} ${INFILE}
#      rm -f ${MODELESGF}_tmpdir/*
#    done

   for var in ${VARLIST_3D}; do
      echo "Currently working on:" ${MODELESGF} ${EXPID1} ${STREAM} ${var}
      rm -f ${MODELESGF}_tmpdir/tempfile.nc
      INFILE=/work/bm0834/b380459/TRACMIP/${MODELUOM}/${EXPID1}/${EXPID1}_8xdaily.${var}.fixed_dimensions.nc
      echo ${INFILE}
      ${CDOCMOR} -s cmor,${MIPTABLE},info=${CDOCMORINFO},mapping_table=${MAPPINGTABLE} -setzaxis,/pf/b/b380459/TRACMIP_Cmorizing/cdocmor_input/AM21/AM21_zaxisdes.txt -setgrid,/pf/b/b380459/TRACMIP_Cmorizing/cdocmor_input/AM21/AM21_griddes.txt ${SETTAXIS} -selvar,${var} ${INFILE}
      rm -f ${MODELESGF}_tmpdir/*
    done

done
done

fi

# remove temporary directory
rm -rf ${MODELESGF}_tmpdir 

# move back to cdo script directory
cd /pf/b/b380459/TRACMIP_Cmorizing/cdoscripts


