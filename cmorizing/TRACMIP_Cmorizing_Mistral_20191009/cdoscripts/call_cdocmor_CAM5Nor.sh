#!/bin/bash
#=============================================================================
# btc batch job parameters
#-----------------------------------------------------------------------------
#SBATCH --account=bm0834
#SBATCH --job-name=call_cdocmor_CAM5Nor.sh
#SBATCH --partition=prepost
#SBATCH --ntasks=2
#SBATCH --cpus-per-task=8      # Specify number of CPUs per task
#SBATCH --output=/work/bm0162/b380459/TRACMIP_cmorized/call_cdocmor_CAM5Nor.sh.%j.o
#SBATCH --error=/work/bm0162/b380459/TRACMIP_cmorized/call_cdocmor_CAM5Nor.sh.%j.o
#SBATCH --time=08:00:00
#=============================================================================

# note the following:
# - daily 3d data is on 8 pressure levels only, so is not used here
# - there is 6hourly snapshot data for some 3d fields, but this data appears to be all zero/nan???
#
 
# Bind your OpenMP threads
export OMP_NUM_THREADS=8
export KMP_AFFINITY=verbose,granularity=thread,compact,1
export KMP_STACKSIZE=64m

set -ex

module unload nco
module unload cdo

module load cdo/1.9.5-gcc64
module load nco/4.7.5-gcc64

CDO="cdo -P 8" 
CDOCMOR="/work/bm0021/cdo_incl_cmor/cdo_11_15_2017_cmor2/src/cdo -P 8" 

MODELUOM=NorESM2   # model name at UoMiami server
MODELUOM2=""       # not needed it seems as file names do not contain model name
MODELESGF=CAM5Nor  # ESGF model name

MAPPINGTABLE=/pf/b/b380459/TRACMIP_Cmorizing/cdocmor_input/${MODELESGF}/mt_TRACMIP_${MODELESGF}.txt
echo "Using MAPPINGTABLE:" ${MAPPINGTABLE}

EXPID2_AquaControl=aquaControlTRACMIP
EXPID2_Aqua4xCO2=aqua4xCO2TRACMIP
EXPID2_LandControl=landControlTRACMIP
EXPID2_Land4xCO2=land4xCO2TRACMIP
EXPID2_LandOrbit=landOrbitTRACMIP

# move into TRACMIP work directory to do actual work
cd /work/bm0162/b380459/TRACMIP_cmorized
pwd

# create temporary directory for this model for tempfile.nc
rm -rf ${MODELESGF}_tmpdir
mkdir ${MODELESGF}_tmpdir



#########################################################
# Amon STREAM

make_Amon=false
if [ "x$make_Amon" == "xtrue" ] ; then

STREAM1=Amon
STREAM2=mon

VARLIST_2D="clivi clt clwvi evspsbl hfls hfss huss pr prc prsn prw ps psl rlds rlus rlut rlutcs rsds rsdscs rsdt rsus rsuscs rsut rsutcs rtmt tas tauu tauv ts uas vas"
VARLIST_3D="hur hus ta ua va wap zg"
VARLIST_3D_cmiplev="cl cli clw"

for EXPID1 in AquaControl Aqua4xCO2 LandControl Land4xCO2; do

   EXPID2=EXPID2_${EXPID1}
   MIPTABLE=/pf/b/b380459/TRACMIP_Cmorizing/cdocmor_input/TRACMIP_${STREAM1}
   CDOCMORINFO=/pf/b/b380459/TRACMIP_Cmorizing/cdocmor_input/${MODELESGF}/cdocmorinfo_TRACMIP_${MODELESGF}_${!EXPID2}
   
   for var in ${VARLIST_2D} ${VARLIST_3D}; do
      echo "Currently working on:" ${MODELESGF} ${EXPID1} ${STREAM1} ${var}
      rm -f ${MODELESGF}_tmpdir/tempfile.nc
      if [ "${EXPID1}" == "AquaControl" ]; then
         INFILE=/work/bm0162/b380459/TRACMIP/NorESM2/${EXPID1}/amip/${STREAM2}/atmos/${var}/r1i1p1/${var}_${STREAM1}_${EXPID1}_amip_r1i1p1_0*-0*.nc
      elif [ "${EXPID1}" == "Aqua4xCO2" ]; then
         INFILE=/work/bm0162/b380459/TRACMIP/NorESM2/${EXPID1}/${STREAM1}/${var}_${STREAM1}_${EXPID1}_amip_r1i1p1_0*-0*.nc
      elif [ "${EXPID1}" == "LandControl" ]; then
         INFILE=/work/bm0162/b380459/TRACMIP/NorESM2/${EXPID1}/amip/${STREAM2}/atmos/${var}/r1i1p1/${var}_${STREAM1}_${EXPID1}_amip_r1i1p1_0*-0*.nc
      elif [ "${EXPID1}" == "Land4xCO2" ]; then
         INFILE=/work/bm0162/b380459/TRACMIP/NorESM2/${EXPID1}/${STREAM1}/${var}_${STREAM1}_${EXPID1}_amip_r1i1p1_0*-0*.nc
      fi
      echo ${INFILE}
      ${CDOCMOR} -s cmor,${MIPTABLE},info=${CDOCMORINFO},mapping_table=${MAPPINGTABLE} ${INFILE}
      rm -f ${MODELESGF}_tmpdir/*
    done

   for var in ${VARLIST_3D_cmiplev}; do
      echo "Currently working on:" ${MODELESGF} ${EXPID1} ${STREAM1} ${var}
      rm -f ${MODELESGF}_tmpdir/tempfile.nc
      if [ "${EXPID1}" == "AquaControl" ]; then
         INFILE=/work/bm0162/b380459/TRACMIP/NorESM2/${EXPID1}/amip/${STREAM2}/atmos/${var}/r1i1p1/${var}_${STREAM1}_${EXPID1}_amip_r1i1p1_0*-0*.cmiplev.nc
      elif [ "${EXPID1}" == "Aqua4xCO2" ]; then
         INFILE=/work/bm0162/b380459/TRACMIP/NorESM2/${EXPID1}/${STREAM1}/${var}_${STREAM1}_${EXPID1}_amip_r1i1p1_0*-0*.cmiplev.nc
      elif [ "${EXPID1}" == "LandControl" ]; then
         INFILE=/work/bm0162/b380459/TRACMIP/NorESM2/${EXPID1}/amip/${STREAM2}/atmos/${var}/r1i1p1/${var}_${STREAM1}_${EXPID1}_amip_r1i1p1_0*-0*.cmiplev.nc
      elif [ "${EXPID1}" == "Land4xCO2" ]; then
         INFILE=/work/bm0162/b380459/TRACMIP/NorESM2/${EXPID1}/${STREAM1}/${var}_${STREAM1}_${EXPID1}_amip_r1i1p1_0*-0*.cmiplev.nc
      fi
      echo ${INFILE}
      ${CDOCMOR} -s cmor,${MIPTABLE},info=${CDOCMORINFO},mapping_table=${MAPPINGTABLE} -setzaxis,/pf/b/b380459/TRACMIP_Cmorizing/cdoscripts/zaxisdes_cmiplev.txt ${INFILE}
      rm -f ${MODELESGF}_tmpdir/*
    done

done

fi

#########################################################
# Aday STREAM

make_Aday=false
if [ "x$make_Aday" == "xtrue" ] ; then

STREAM1=Aday
STREAM2=day

VARLIST_2D="clt hfls hfss huss pr prc prsn psl rhs rlds rlus rlut rsds rsus tas tasmax uas vas"
VARLIST_3D=" "

for EXPID1 in AquaControl Aqua4xCO2 LandControl Land4xCO2; do

   EXPID2=EXPID2_${EXPID1}
   MIPTABLE=/pf/b/b380459/TRACMIP_Cmorizing/cdocmor_input/TRACMIP_${STREAM1}
   CDOCMORINFO=/pf/b/b380459/TRACMIP_Cmorizing/cdocmor_input/${MODELESGF}/cdocmorinfo_TRACMIP_${MODELESGF}_${!EXPID2}
   
   for var in ${VARLIST_2D} ${VARLIST_3D}; do
      echo "Currently working on:" ${MODELESGF} ${EXPID1} ${STREAM1} ${var}
      rm -f ${MODELESGF}_tmpdir/tempfile.nc
      if [ "${EXPID1}" == "AquaControl" ]; then
         INFILE=/work/bm0162/b380459/TRACMIP/NorESM2/${EXPID1}/amip/${STREAM2}/atmos/${var}/r1i1p1/${var}_${STREAM2}_${EXPID1}_amip_r1i1p1_0*-0*.nc
      elif [ "${EXPID1}" == "Aqua4xCO2" ]; then
         INFILE=/work/bm0162/b380459/TRACMIP/NorESM2/${EXPID1}/${STREAM1}/${var}_${STREAM2}_${EXPID1}_amip_r1i1p1_0*-0*.nc
      elif [ "${EXPID1}" == "LandControl" ]; then
         INFILE=/work/bm0162/b380459/TRACMIP/NorESM2/${EXPID1}/amip/${STREAM2}/atmos/${var}/r1i1p1/${var}_${STREAM2}_${EXPID1}_amip_r1i1p1_0*-0*.nc
      elif [ "${EXPID1}" == "Land4xCO2" ]; then
         INFILE=/work/bm0162/b380459/TRACMIP/NorESM2/${EXPID1}/${STREAM1}/${var}_${STREAM2}_${EXPID1}_amip_r1i1p1_0*-0*.nc
      fi
      echo ${INFILE}
      ${CDOCMOR} -s cmor,${MIPTABLE},info=${CDOCMORINFO},mapping_table=${MAPPINGTABLE} ${INFILE}
      rm -f ${MODELESGF}_tmpdir/*
    done

done

fi

#########################################################
# A3hr STREAM

make_A3hr=false
if [ "x$make_A3hr" == "xtrue" ] ; then

STREAM1=A3hr
STREAM2=3hr

for EXPID1 in AquaControl Aqua4xCO2 LandControl Land4xCO2; do

   EXPID2=EXPID2_${EXPID1}
   MIPTABLE=/pf/b/b380459/TRACMIP_Cmorizing/cdocmor_input/TRACMIP_${STREAM1}
   CDOCMORINFO=/pf/b/b380459/TRACMIP_Cmorizing/cdocmor_input/${MODELESGF}/cdocmorinfo_TRACMIP_${MODELESGF}_${!EXPID2}
  
   # experiments have different variables available, so set VARLIST_2D here in dependence of experiment
   if [ "${EXPID1}" == "AquaControl" ]; then
      VARLIST_2D="clt hfls hfss huss pr prc prsn ps rlds rldscs rlus rsdscs rsuscs tas"
   elif [ "${EXPID1}" == "Aqua4xCO2" ]; then
      VARLIST_2D="clt hfls hfss pr prc prsn rlds rldscs rlus rsdscs rsuscs tas"
   elif [ "${EXPID1}" == "LandControl" ]; then
      VARLIST_2D="clt hfls hfss huss pr prc prsn ps rlds rldscs rlus rsdscs rsuscs tas"
   elif [ "${EXPID1}" == "Land4xCO2" ]; then
      VARLIST_2D="clt hfls hfss pr prc prsn rlds rldscs rlus rsdscs rsuscs"
   fi
    
   for var in ${VARLIST_2D}; do
      echo "Currently working on:" ${MODELESGF} ${EXPID1} ${STREAM1} ${var}
      rm -f ${MODELESGF}_tmpdir/tempfile.nc
      if [ "${EXPID1}" == "AquaControl" ]; then
         INFILE=/work/bm0162/b380459/TRACMIP/NorESM2/${EXPID1}/amip/${STREAM2}/atmos/${var}/r1i1p1/${var}_${STREAM2}_${EXPID1}_amip_r1i1p1_0*-0*.nc
      elif [ "${EXPID1}" == "Aqua4xCO2" ]; then
         INFILE=/work/bm0162/b380459/TRACMIP/NorESM2/${EXPID1}/${STREAM1}/${var}_${STREAM2}_${EXPID1}_amip_r1i1p1_0*-0*.nc
      elif [ "${EXPID1}" == "LandControl" ]; then
         INFILE=/work/bm0162/b380459/TRACMIP/NorESM2/${EXPID1}/amip/${STREAM2}/atmos/${var}/r1i1p1/${var}_${STREAM2}_${EXPID1}_amip_r1i1p1_0*-0*.nc
      elif [ "${EXPID1}" == "Land4xCO2" ]; then
         INFILE=/work/bm0162/b380459/TRACMIP/NorESM2/${EXPID1}/${STREAM1}/${var}_${STREAM2}_${EXPID1}_amip_r1i1p1_0*-0*.nc
      fi
      echo ${INFILE}
      ${CDOCMOR} -s cmor,${MIPTABLE},info=${CDOCMORINFO},mapping_table=${MAPPINGTABLE} ${INFILE}
      rm -f ${MODELESGF}_tmpdir/*
    done

done

fi

# remove temporary directory
#rm -rf ${MODELESGF}_tmpdir 

# move back to cdo script directory
cd /pf/b/b380459/TRACMIP_Cmorizing/cdoscripts


