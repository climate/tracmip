#!/bin/bash
#=============================================================================
# btc batch job parameters
#-----------------------------------------------------------------------------
#SBATCH --account=bm0834
#SBATCH --job-name=call_cdocmor_CAM5Nor.sh
#SBATCH --partition=prepost
#SBATCH --ntasks=2
#SBATCH --cpus-per-task=8      # Specify number of CPUs per task
#SBATCH --output=/work/bm0162/b380459/TRACMIP_cmorized/call_cdocmor_CAM5Nor.sh.%j.o
#SBATCH --error=/work/bm0162/b380459/TRACMIP_cmorized/call_cdocmor_CAM5Nor.sh.%j.o
#SBATCH --time=08:00:00
#=============================================================================

# note the following:
# - daily 3d data is on 8 pressure levels only, so is not used here
# - there is 6hourly snapshot data for some 3d fields, but this data appears to be all zero/nan???
#
 
# Bind your OpenMP threads
export OMP_NUM_THREADS=8
export KMP_AFFINITY=verbose,granularity=thread,compact,1
export KMP_STACKSIZE=64m

set -ex

module unload nco
module unload cdo

module load cdo/1.9.5-gcc64
module load nco/4.7.5-gcc64

CDO="cdo -P 8" 
CDOCMOR="/work/bm0021/cdo_incl_cmor/cdo_11_15_2017_cmor2/src/cdo -P 8" 

MODELUOM=NorESM2   # model name at UoMiami server
MODELUOM2=""       # not needed it seems as file names do not contain model name
MODELESGF=CAM5Nor  # ESGF model name

MAPPINGTABLE=/pf/b/b380459/TRACMIP_Cmorizing/cdocmor_input/${MODELESGF}/mt_TRACMIP_${MODELESGF}.txt
echo "Using MAPPINGTABLE:" ${MAPPINGTABLE}

EXPID2_AquaControl=aquaControlTRACMIP
EXPID2_Aqua4xCO2=aqua4xCO2TRACMIP
EXPID2_LandControl=landControlTRACMIP
EXPID2_Land4xCO2=land4xCO2TRACMIP
EXPID2_LandOrbit=landOrbitTRACMIP

# move into TRACMIP work directory to do actual work
cd /work/bm0162/b380459/TRACMIP_cmorized
pwd

# create temporary directory for this model for tempfile.nc
rm -rf ${MODELESGF}_tmpdir
mkdir ${MODELESGF}_tmpdir




#########################################################
# Aday STREAM

make_Aday=true
if [ "x$make_Aday" == "xtrue" ] ; then

STREAM1=Aday
STREAM2=day

VARLIST_2D="tasmin"

for EXPID1 in AquaControl Aqua4xCO2 LandControl Land4xCO2; do

   EXPID2=EXPID2_${EXPID1}
   MIPTABLE=/pf/b/b380459/TRACMIP_Cmorizing/cdocmor_input/TRACMIP_${STREAM1}
   CDOCMORINFO=/pf/b/b380459/TRACMIP_Cmorizing/cdocmor_input/${MODELESGF}/cdocmorinfo_TRACMIP_${MODELESGF}_${!EXPID2}
   
   for var in ${VARLIST_2D}; do
      echo "Currently working on:" ${MODELESGF} ${EXPID1} ${STREAM1} ${var}
      rm -f ${MODELESGF}_tmpdir/tempfile.nc
      INFILE=/work/bm0162/b380459/TRACMIP/NorESM2/${var}_${STREAM2}_${EXPID1}_amip_r1i1p1_0*-0*.nc
      echo ${INFILE}
      ${CDOCMOR} -s cmor,${MIPTABLE},info=${CDOCMORINFO},mapping_table=${MAPPINGTABLE} ${INFILE}
      rm -f ${MODELESGF}_tmpdir/*
    done

done

fi

# remove temporary directory
#rm -rf ${MODELESGF}_tmpdir 

# move back to cdo script directory
cd /pf/b/b380459/TRACMIP_Cmorizing/cdoscripts


