#!/bin/bash
#=============================================================================
# btc batch job parameters
#-----------------------------------------------------------------------------
#SBATCH --account=bm0834
#SBATCH --job-name=call_mergetime_Aday_A3hr_MPAS.sh
#SBATCH --partition=prepost
#SBATCH --ntasks=2
#SBATCH --cpus-per-task=8      # Specify number of CPUs per task
#SBATCH --output=/pf/b/b380459/TRACMIP_Cmorizing/cdoscripts/call_mergetime_Aday_A3hr_MPAS.sh.%j.o
#SBATCH --error=/pf/b/b380459/TRACMIP_Cmorizing/cdoscripts/call_mergetime_Aday_A3hr_MPAS.sh.%j.o
#SBATCH --time=08:00:00
#=============================================================================

# note the following:
# 3hourly data for the land simulations has only one timestep per day, so is not actually 3hourly
# it is therefore not merged

# Bind your OpenMP threads
export OMP_NUM_THREADS=8
export KMP_AFFINITY=verbose,granularity=thread,compact,1
export KMP_STACKSIZE=64m

set -e

module load nco/4.7.5-gcc64

MODELUOM=MPAS   # model name at UoMiami server

#EXPID="AquaControl"
#STREAM="Aday"
#cd /work/bm0834/b380459/TRACMIP/${MODELUOM}/${EXPID}/${STREAM}/
#echo "Currently merging:" ${MODELUOM} ${EXPID} ${STREAM}
#ncrcat Aqua_SOM_newmpas_cam4_240km_daily.cam.h0.00*nc Aqua_SOM_newmpas_cam4_240km_daily.cam.h0.mergetime.nc

#EXPID="AquaControl"
#STREAM="A3hr"
#cd /work/bm0834/b380459/TRACMIP/${MODELUOM}/${EXPID}/${STREAM}/
#echo "Currently merging:" ${MODELUOM} ${EXPID} ${STREAM}
#ncrcat Aqua_SOM_newmpas_cam4_240km_3hrly.cam.h0.00*nc Aqua_SOM_newmpas_cam4_240km_3hrly.cam.h0.mergetime.nc

#EXPID="Aqua4xCO2"
#STREAM="Aday"
#cd /work/bm0834/b380459/TRACMIP/${MODELUOM}/${EXPID}/${STREAM}/
#echo "Currently merging:" ${MODELUOM} ${EXPID} ${STREAM}
#ncrcat Aqua_SOM_newmpas_cam4_240km_4xco2_daily.cam.h0.00*nc Aqua_SOM_newmpas_cam4_240km_4xco2_daily.cam.h0.mergetime.nc

#EXPID="Aqua4xCO2"
#STREAM="A3hr"
#cd /work/bm0834/b380459/TRACMIP/${MODELUOM}/${EXPID}/${STREAM}/
#echo "Currently merging:" ${MODELUOM} ${EXPID} ${STREAM}
#ncrcat Aqua_SOM_newmpas_cam4_240km_4xco2_3hrly.cam.h0.00*nc Aqua_SOM_newmpas_cam4_240km_4xco2_3hrly.cam.h0.mergetime.nc

#EXPID="LandControl"
#STREAM="Aday"
#cd /work/bm0834/b380459/TRACMIP/${MODELUOM}/${EXPID}/${STREAM}/
#echo "Currently merging:" ${MODELUOM} ${EXPID} ${STREAM}
#ncrcat Aqua_SOM_newmpas_cam4_240km_landcontrol_daily.cam.h0.00*nc Aqua_SOM_newmpas_cam4_240km_landcontrol_daily.cam.h0.mergetime.nc

#EXPID="LandControl"
#STREAM="A3hr"
#cd /work/bm0834/b380459/TRACMIP/${MODELUOM}/${EXPID}/${STREAM}/
#echo "Currently merging:" ${MODELUOM} ${EXPID} ${STREAM}
#ncrcat Aqua_SOM_newmpas_cam4_240km_landcontrol_3hrly.cam.h0.00*nc Aqua_SOM_newmpas_cam4_240km_landcontrol_3hrly.cam.h0.mergetime.nc

EXPID="Land4xCO2"
STREAM="Aday"
cd /work/bm0834/b380459/TRACMIP/${MODELUOM}/${EXPID}/${STREAM}/
echo "Currently merging:" ${MODELUOM} ${EXPID} ${STREAM}
ncrcat Aqua_SOM_newmpas_cam4_240km_land4xco2_daily.cam.h0.0???-??-??-00000.nc Aqua_SOM_newmpas_cam4_240km_land4xco2_daily.cam.h0.mergetime.nc

#EXPID="Land4xCO2"
#STREAM="A3hr"
#cd /work/bm0834/b380459/TRACMIP/${MODELUOM}/${EXPID}/${STREAM}/
#echo "Currently merging:" ${MODELUOM} ${EXPID} ${STREAM}
#ncrcat Aqua_SOM_newmpas_cam4_240km_land4xco2_3hrly.cam.h0.00*nc Aqua_SOM_newmpas_cam4_240km_land4xco2_3hrly.cam.h0.mergetime.nc

EXPID="LandOrbit"
STREAM="Aday"
cd /work/bm0834/b380459/TRACMIP/${MODELUOM}/${EXPID}/${STREAM}/
echo "Currently merging:" ${MODELUOM} ${EXPID} ${STREAM}
ncrcat Aqua_SOM_newmpas_cam4_240km_landorbit_daily.cam.h0.0???-??-??-00000.nc Aqua_SOM_newmpas_cam4_240km_landorbit_daily.cam.h0.mergetime.nc

#EXPID="LandOrbit"
#STREAM="A3hr"
#cd /work/bm0834/b380459/TRACMIP/${MODELUOM}/${EXPID}/${STREAM}/
#echo "Currently merging:" ${MODELUOM} ${EXPID} ${STREAM}
#ncrcat Aqua_SOM_newmpas_cam4_240km_landorbit_3hrly.cam.h0.00*nc Aqua_SOM_newmpas_cam4_240km_landorbit_3hrly.cam.h0.mergetime.nc
