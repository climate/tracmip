#!/bin/bash
#=============================================================================
# btc batch job parameters
#-----------------------------------------------------------------------------
#SBATCH --account=bm0834
#SBATCH --job-name=call_cdocmor_MPAS.sh
#SBATCH --partition=prepost
#SBATCH --ntasks=2
#SBATCH --cpus-per-task=8      # Specify number of CPUs per task
#SBATCH --output=/pf/b/b380459/TRACMIP_Cmorizing/cdoscripts/call_cdocmor_MPAS.sh.%j.o
#SBATCH --error=/pf/b/b380459/TRACMIP_Cmorizing/cdoscripts/call_cdocmor_MPAS.sh.%j.o
#SBATCH --time=08:00:00
#=============================================================================

# note the following:
#  - 3d data was interpolated to CMIP pressure levels before, there are separate files for each variable
#  - the 2d fields PR, PRSN, RLUS, RSUS, RSUSCS, RSUT, RSUTCS are in separate files
#  - for precipitation fields PR, PRECC and PRSN we need to multiply with 1000 to get units from units of m/s to kg/m/s
#  - 3hr land data is faulty (only one timestep per day) and thus not cmorized

# Bind your OpenMP threads
export OMP_NUM_THREADS=8
export KMP_AFFINITY=verbose,granularity=thread,compact,1
export KMP_STACKSIZE=64m

set -ex

module unload nco
module unload cdo

module load cdo/1.9.5-gcc64
module load nco/4.7.5-gcc64

CDO="cdo -P 8" 
CDOCMOR="/work/bm0021/cdo_incl_cmor/cdo_11_15_2017_cmor2/src/cdo -P 8" 

MODELUOM=MPAS   # model name at UoMiami server
MODELUOM2=MPAS  # for some models, there is a second UoMiami name needed for the actual variable file (differs from directory name by, e.g., small or capital letters)
MODELESGF=MPAS  # ESGF model name

MAPPINGTABLE=/pf/b/b380459/TRACMIP_Cmorizing/cdocmor_input/${MODELESGF}/mt_TRACMIP_${MODELESGF}.txt
echo "Using MAPPINGTABLE:" ${MAPPINGTABLE}

CALENDAR=365_day

EXPID2_AquaControl=aquaControlTRACMIP
EXPID2_Aqua4xCO2=aqua4xCO2TRACMIP
EXPID2_LandControl=landControlTRACMIP
EXPID2_Land4xCO2=land4xCO2TRACMIP
EXPID2_LandOrbit=landOrbitTRACMIP

YEARSTART_AquaControl_Amon=0016
YEARSTART_Aqua4xCO2_Amon=0046
YEARSTART_LandControl_Amon=0046
YEARSTART_Land4xCO2_Amon=0086
YEARSTART_LandOrbit_Amon=0086

YEAREND_AquaControl_Amon=0045
YEAREND_Aqua4xCO2_Amon=0085
YEAREND_LandControl_Amon=0085
YEAREND_Land4xCO2_Amon=0125
YEAREND_LandOrbit_Amon=0125

YEARSTART_AquaControl_Aday=0036
YEARSTART_Aqua4xCO2_Aday=0076
YEARSTART_LandControl_Aday=0076
YEARSTART_Land4xCO2_Aday=0116
YEARSTART_LandOrbit_Aday=0116

YEAREND_AquaControl_Aday=0045
YEAREND_Aqua4xCO2_Aday=0085
YEAREND_LandControl_Aday=0085
YEAREND_Land4xCO2_Aday=0125
YEAREND_LandOrbit_Aday=0125

YEARSTART_AquaControl_A3hr=0043
YEARSTART_Aqua4xCO2_A3hr=0083
YEARSTART_LandControl_A3hr=0083
YEARSTART_Land4xCO2_A3hr=0123
YEARSTART_LandOrbit_A3hr=0123

YEAREND_AquaControl_A3hr=0045
YEAREND_Aqua4xCO2_A3hr=0085
YEAREND_LandControl_A3hr=0085
YEAREND_Land4xCO2_A3hr=0125
YEAREND_LandOrbit_A3hr=0125

VARLIST2D="TGCLDIWP CLDTOT TGCLDLWP LHFLX SHFLX PRECC TMQ PS PSL FLDS FLDSC FLUT FLUTC FSDS FSDSC SOLIN TREFHT TSMX TSMN TAUX TAUY SST"
VARLIST2D_sepfiles="PR PRSN RLUS RSUS RSUSCS RSUT RSUTCS"
VARLIST3D="CLOUD CLDICE CLDLIQ RELHUM Q T U V OMEGA Z3"

# move into TRACMIP work directory to do actual work
cd /work/bm0162/b380459/TRACMIP_cmorized

pwd

# create temporary directory for this model for tempfile.nc
rm -rf ${MODELESGF}_tmpdir
mkdir ${MODELESGF}_tmpdir

for EXPID1 in AquaControl Aqua4xCO2 LandControl Land4xCO2 LandOrbit; do
#for STREAM in Amon Aday A3hr; do
for STREAM in Aday; do
  
   case $EXPID1 in 
        AquaControl)
            case $STREAM in
                Amon)
                     FILE="Amon/Aqua_SOM_newmpas_cam4_240km.cam.h0.001601-004512"
                     ;;
                Aday)
                     FILE="Aday/Aqua_SOM_newmpas_cam4_240km_daily.cam.h0.mergetime"
                     ;;
                A3hr)
                     FILE="A3hr/Aqua_SOM_newmpas_cam4_240km_3hrly.cam.h0.mergetime"
                     ;;
                esac
            ;;
        Aqua4xCO2)
            case $STREAM in
                Amon)
                     FILE="Amon/Aqua_SOM_newmpas_cam4_240km_4xco2.cam.h0.004601-008512"
                     ;;
                Aday)
                     FILE="Aday/Aqua_SOM_newmpas_cam4_240km_4xco2_daily.cam.h0.mergetime"
                     ;;
                A3hr)
                     FILE="A3hr/Aqua_SOM_newmpas_cam4_240km_4xco2_3hrly.cam.h0.mergetime"
                     ;;
                esac
            ;;   
        LandControl)
            case $STREAM in
                Amon)
                     FILE="Amon/Aqua_SOM_newmpas_cam4_240km_landcontrol.cam.h0.004601-008512"
                     ;;
                Aday)
                     FILE="Aday/Aqua_SOM_newmpas_cam4_240km_landcontrol_daily.cam.h0.mergetime"
                     ;;
                A3hr)
                     FILE="A3hr/MISSING"
                     ;;
                esac
            ;;   
        Land4xCO2)
            case $STREAM in
                Amon)
                     FILE="Amon/Aqua_SOM_newmpas_cam4_240km_land4xco2.cam.h0.008601-012512"
                     ;;
                Aday)
                     FILE="Aday/Aqua_SOM_newmpas_cam4_240km_land4xco2_daily.cam.h0.mergetime"
                     ;;
                A3hr)
                     FILE="A3hr/MISSING"
                     ;;
                esac
            ;;   
        LandOrbit)
            case $STREAM in
                Amon)
                     FILE="Amon/Aqua_SOM_newmpas_cam4_240km_landorbit.cam.h0.008601-012512"
                     ;;
                Aday)
                     FILE="Aday/Aqua_SOM_newmpas_cam4_240km_landorbit_daily.cam.h0.mergetime"
                     ;;
                A3hr)
                     FILE="A3hr/MISSING"
                     ;;
                esac
            ;;  
   esac
   
   echo $FILE 
 
   EXPID2=EXPID2_${EXPID1}
   MIPTABLE=/pf/b/b380459/TRACMIP_Cmorizing/cdocmor_input/TRACMIP_${STREAM}
   CDOCMORINFO=/pf/b/b380459/TRACMIP_Cmorizing/cdocmor_input/${MODELESGF}/cdocmorinfo_TRACMIP_${MODELESGF}_${!EXPID2}
   #for var in ${VARLIST2D}; do
   #   echo "Currently working on:" ${MODELESGF} ${EXPID1} ${STREAM} ${var}
   #   rm -f ${MODELESGF}_tmpdir/tempfile.nc
   #   INFILE=/work/bm0834/b380459/TRACMIP/${MODELUOM}/${EXPID1}/${FILE}.nc
   #   MULC=" "
   #   if [ "${var}" == "PRECC" ]; then
   #      MULC="-mulc,1000.0"
   #   fi
   #   echo ${INFILE}, ${MULC}
   #   ncks -v ${var},lon,lat ${INFILE} ${MODELESGF}_tmpdir/ncks_tempfile.nc
   #   ${CDO} -s -setcalendar,${CALENDAR} ${MULC} ${MODELESGF}_tmpdir/ncks_tempfile.nc ${MODELESGF}_tmpdir/tempfile.nc
   #   ${CDOCMOR} -s cmor,${MIPTABLE},info=${CDOCMORINFO},mapping_table=${MAPPINGTABLE} ${MODELESGF}_tmpdir/tempfile.nc
   #   rm -f ${MODELESGF}_tmpdir/*
   # done
   # for var in ${VARLIST2D_sepfiles}; do
   #   echo "Currently working on:" ${MODELESGF} ${EXPID1} ${STREAM} ${var}
   #   rm -f ${MODELESGF}_tmpdir/tempfile.nc
   #   INFILE=/work/bm0834/b380459/TRACMIP/${MODELUOM}/${EXPID1}/${FILE}_${var}.nc
   #   MULC=" "
   #   if [ "${var}" == "PR" ] || [ "${var}" == "PRSN" ]; then
   #      MULC="-mulc,1000.0"
   #   fi
   #   echo ${INFILE}, ${MULC}
   #   ncks -v ${var},lon,lat ${INFILE} ${MODELESGF}_tmpdir/ncks_tempfile.nc
   #   ${CDO} -s -setcalendar,${CALENDAR} ${MULC} ${MODELESGF}_tmpdir/ncks_tempfile.nc ${MODELESGF}_tmpdir/tempfile.nc
   #   ${CDOCMOR} -s cmor,${MIPTABLE},info=${CDOCMORINFO},mapping_table=${MAPPINGTABLE} ${MODELESGF}_tmpdir/tempfile.nc
   #   rm -f ${MODELESGF}_tmpdir/*
   # done
    for var in ${VARLIST3D}; do
      echo "Currently working on:" ${MODELESGF} ${EXPID1} ${STREAM} ${var}
      rm -f ${MODELESGF}_tmpdir/tempfile.nc
      INFILE=/work/bm0834/b380459/TRACMIP/${MODELUOM}/${EXPID1}/${FILE}.${var}.cmiplev.nc
      SHIFTTIME=" "
      if [ "${STREAM}" == "Amon" ]; then
         SHIFTTIME="-shifttime,-15days"
      elif [ "${STREAM}" == "Aday" ]; then
         SHIFTTIME="-shifttime,-12hours"
      fi
      echo ${INFILE}, ${SHIFTTIME}
      ncks -v ${var},lon,lat ${INFILE} ${MODELESGF}_tmpdir/ncks_tempfile.nc
      ${CDO} -s -setcalendar,${CALENDAR} ${SHIFTTIME} -setzaxis,/pf/b/b380459/TRACMIP_Cmorizing/cdoscripts/zaxisdes_cmiplev.txt ${MODELESGF}_tmpdir/ncks_tempfile.nc ${MODELESGF}_tmpdir/tempfile.nc
      ${CDOCMOR} -s cmor,${MIPTABLE},info=${CDOCMORINFO},mapping_table=${MAPPINGTABLE} ${MODELESGF}_tmpdir/tempfile.nc
      rm -f ${MODELESGF}_tmpdir/*
    done

done
done

# remove temporary directory
rm -rf ${MODELESGF}_tmpdir 

# move back to cdo script directory
cd /pf/b/b380459/TRACMIP_Cmorizing/cdoscripts
