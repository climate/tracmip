#!/bin/bash
#=============================================================================
# btc batch job parameters
#-----------------------------------------------------------------------------
#SBATCH --account=bm0834
#SBATCH --job-name=call_cdocmor_MIROC5.sh
#SBATCH --partition=prepost
#SBATCH --ntasks=2
#SBATCH --cpus-per-task=8      # Specify number of CPUs per task
#SBATCH --output=/pf/b/b380459/TRACMIP_Cmorizing/cdoscripts/call_clouds_ml2pl_MIROC5.sh.%j.o
#SBATCH --error=/pf/b/b380459/TRACMIP_Cmorizing/cdoscripts/call_clouds_ml2pl_MIROC5.sh.%j.o
#SBATCH --time=08:00:00
#=============================================================================

# note the following:
# 3d cloud fields are on hybrid sigma ("heta40" levels), before cmorizing we interpolate them to pressure levels
# we ignore cmflx here and in the cmorizing as it has 41 heta levels, for which I do not have the hybrid sigma coefficients

# the cdo ml2pl interpolation can give over and undershoots in case of strong vertical gradients, which in fact happen quite often for cloud fields
# we use the cdo setrtoc to restrict cl to values between 0 and 1, and cli and clw to values >0
# more generally the cdo ml2pl misses many of the interesting vertical features of the cloud field, as it has only
# 17 levels instead of 40 levels, ... so the vertically-interpolated cloud fields could be misleading, but this is what TRACMIP decided to do ...

# Bind your OpenMP threads
export OMP_NUM_THREADS=8
export KMP_AFFINITY=verbose,granularity=thread,compact,1
export KMP_STACKSIZE=64m

set -e

module load nco

CDO="cdo -P 8" 
CDOCMOR="/work/bm0021/cdo_incl_cmor/cdo_11_15_2017_cmor2/src/cdo -P 8" 

MODELUOM=MIROC5   # model name at UoMiami server

YEARSTART_AquaControl_Amon=1016
YEARSTART_Aqua4xCO2_Amon=1046
YEARSTART_LandControl_Amon=1046
YEARSTART_Land4xCO2_Amon=1086
YEARSTART_LandOrbit_Amon=1086

YEAREND_AquaControl_Amon=1045
YEAREND_Aqua4xCO2_Amon=1085
YEAREND_LandControl_Amon=1085
YEAREND_Land4xCO2_Amon=1125
YEAREND_LandOrbit_Amon=1125

YEARSTART_AquaControl_Aday=1036
YEARSTART_Aqua4xCO2_Aday=1076
YEARSTART_LandControl_Aday=1076
YEARSTART_Land4xCO2_Aday=1116
YEARSTART_LandOrbit_Aday=1116

YEAREND_AquaControl_Aday=1045
YEAREND_Aqua4xCO2_Aday=1085
YEAREND_LandControl_Aday=1085
YEAREND_Land4xCO2_Aday=1125
YEAREND_LandOrbit_Aday=1125

YEARSTART_AquaControl_A3hr=1043
YEARSTART_Aqua4xCO2_A3hr=1083
YEARSTART_LandControl_A3hr=1083
YEARSTART_Land4xCO2_A3hr=1123
YEARSTART_LandOrbit_A3hr=1123

YEAREND_AquaControl_A3hr=1045
YEAREND_Aqua4xCO2_A3hr=1085
YEAREND_LandControl_A3hr=1085
YEAREND_Land4xCO2_A3hr=1125
YEAREND_LandOrbit_A3hr=1125

VARLIST3D="cl cli clw"

for EXPID1 in AquaControl Aqua4xCO2 LandControl Land4xCO2 LandOrbit; do
   
   for STREAM in Amon; do  
   
   for var in ${VARLIST3D}; do
      echo "Currently interpolating:" ${MODELUOM} ${EXPID1} ${STREAM} ${var}
      YEARSTART=YEARSTART_${EXPID1}_${STREAM}
      YEAREND=YEAREND_${EXPID1}_${STREAM}
      INFILE_CLOUD=/work/bm0162/b380459/TRACMIP/${MODELUOM}/${EXPID1}/${STREAM}/${var}_${STREAM}_${MODELUOM}_${EXPID1}_r1i1p1_${!YEARSTART}01-${!YEAREND}12.nc
      INFILE_PS=/work/bm0162/b380459/TRACMIP/${MODELUOM}/${EXPID1}/${STREAM}/ps_${STREAM}_${MODELUOM}_${EXPID1}_r1i1p1_${!YEARSTART}01-${!YEAREND}12.nc
      echo ${INFILE_CLOUD} ${INFILE_PS}
      ${CDO} -O setzaxis,/pf/b/b380459/TRACMIP_Cmorizing/cdocmor_input/MIROC5/miroc5_zaxisdes.txt -invertlev ${INFILE_CLOUD} tmpin1
      ${CDO} -O merge -mulc,100 ${INFILE_PS} tmpin1 tmpin2
      SETTORANGE=" "
      if [ "${var}" == "cli" ] || [ "${var}" == "clw" ]; then
         SETTORANGE="-setrtoc,-10000,0,0"
      fi 
      if [ "${var}" == "cl" ]; then
         SETTORANGE="-setrtoc,-10000,0,0 -setrtoc,1,10000,1"
      fi 
      ${CDO} -O ${SETTORANGE} -delvar,ps -ml2pl,1000,2000,3000,5000,7000,10000,15000,20000,25000,30000,40000,50000,60000,70000,85000,92500,100000 tmpin2 ${INFILE_CLOUD}_plevels
      rm -f tmpin1 tmpin2
   done
   
   done

   for STREAM in Aday A3hr; do  
   
   for var in ${VARLIST3D}; do
      echo "Currently interpolating:" ${MODELUOM} ${EXPID1} ${STREAM} ${var}
      YEARSTART=YEARSTART_${EXPID1}_${STREAM}
      YEAREND=YEAREND_${EXPID1}_${STREAM}
      INFILE_CLOUD=/work/bm0162/b380459/TRACMIP/${MODELUOM}/${EXPID1}/${STREAM}/${var}_${STREAM}_${MODELUOM}_${EXPID1}_r1i1p1_${!YEARSTART}0101-${!YEAREND}1230.nc
      INFILE_PS=/work/bm0162/b380459/TRACMIP/${MODELUOM}/${EXPID1}/${STREAM}/ps_${STREAM}_${MODELUOM}_${EXPID1}_r1i1p1_${!YEARSTART}0101-${!YEAREND}1230.nc
      echo ${INFILE_CLOUD} ${INFILE_PS}
      ${CDO} -O setzaxis,/pf/b/b380459/TRACMIP_Cmorizing/cdocmor_input/MIROC5/miroc5_zaxisdes.txt -invertlev ${INFILE_CLOUD} tmpin1
      ${CDO} -O merge -mulc,100 ${INFILE_PS} tmpin1 tmpin2
      SETTORANGE=" "
      if [ "${var}" == "cli" ] || [ "${var}" == "clw" ]; then
         SETTORANGE="-setrtoc,-10000,0,0"
      fi 
      if [ "${var}" == "cl" ]; then
         SETTORANGE="-setrtoc,-10000,0,0 -setrtoc,1,10000,1"
      fi 
      ${CDO} -O ${SETTORANGE} -delvar,ps -ml2pl,1000,2000,3000,5000,7000,10000,15000,20000,25000,30000,40000,50000,60000,70000,85000,92500,100000 tmpin2 ${INFILE_CLOUD}_plevels
      ${CDO} -O ${SETTORANGE} -delvar,ps -ml2pl,1000,2000,3000,5000,7000,10000,15000,20000,25000,30000,40000,50000,60000,70000,85000,92500,100000 tmpin2 ${INFILE_CLOUD}_plevels
      rm -f tmpin1 tmpin2
   done
   
   done

done
