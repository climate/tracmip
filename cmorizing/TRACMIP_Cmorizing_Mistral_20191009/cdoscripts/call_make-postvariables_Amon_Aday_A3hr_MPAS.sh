#!/bin/bash
#=============================================================================
# btc batch job parameters
#-----------------------------------------------------------------------------
#SBATCH --account=bm0834
#SBATCH --job-name=call_call_make-postvariables_Amon_Aday_A3hr_MPAS.sh
#SBATCH --partition=prepost
#SBATCH --ntasks=2
#SBATCH --cpus-per-task=8      # Specify number of CPUs per task
#SBATCH --output=/pf/b/b380459/TRACMIP_Cmorizing/cdoscripts/call_make-postvariables_Amon_Aday_A3hr_MPAS.sh.%j.o
#SBATCH --error=/pf/b/b380459/TRACMIP_Cmorizing/cdoscripts/call_make-postvariables_Amon_Aday_A3hr_MPAS.sh.%j.o
#SBATCH --time=08:00:00
#=============================================================================

# we manually calculate those variables that are not part in the MPAS raw output but can be calculated from other variables
# this includes:
# PR = PRECC + PRECL
# PRSN = PRECSC + PRECSL
# RLUS = FLDS + FLNS
# RSUS = FSDS - FSNS
# RSUSCS = FSDSC - FSNSC
# RSUT = SOLIN - FSNT
# RSUTCS = SOLIN - FSNTC

# Bind your OpenMP threads
export OMP_NUM_THREADS=8
export KMP_AFFINITY=verbose,granularity=thread,compact,1
export KMP_STACKSIZE=64m

set -e

module load nco/4.7.5-gcc64

CDO="cdo -P 8" 
CDOCMOR="/work/bm0021/cdo_incl_cmor/cdo_11_15_2017_cmor2/src/cdo -P 8" 

MODELUOM=MPAS   # model name at UoMiami server

#for EXPID in AquaControl Aqua4xCO2; do # LandControl Land4xCO2 LandOrbit; do
#for EXPID in LandControl Land4xCO2 LandOrbit; do
for EXPID in LandOrbit; do
#for STREAM in Amon Aday A3hr; do
for STREAM in Amon Aday; do
   
   case $EXPID in 
        AquaControl)
            case $STREAM in
                Amon)
                     FNAME="Aqua_SOM_newmpas_cam4_240km.cam.h0.001601-004512"
                     ;;
                Aday)
                     FNAME="Aqua_SOM_newmpas_cam4_240km_daily.cam.h0.mergetime"
                     ;;
                A3hr)
                     FNAME="Aqua_SOM_newmpas_cam4_240km_3hrly.cam.h0.mergetime"
                     ;;
                esac
            ;;
        Aqua4xCO2)
            case $STREAM in
                Amon)
                     FNAME="Aqua_SOM_newmpas_cam4_240km_4xco2.cam.h0.004601-008512"
                     ;;
                Aday)
                     FNAME="Aqua_SOM_newmpas_cam4_240km_4xco2_daily.cam.h0.mergetime"
                     ;;
                A3hr)
                     FNAME="Aqua_SOM_newmpas_cam4_240km_4xco2_3hrly.cam.h0.mergetime"
                     ;;
                esac
            ;;   
        LandControl)
            case $STREAM in
                Amon)
                     FNAME="Aqua_SOM_newmpas_cam4_240km_landcontrol.cam.h0.004601-008512"
                     ;;
                Aday)
                     FNAME="Aqua_SOM_newmpas_cam4_240km_landcontrol_daily.cam.h0.mergetime"
                     ;;
                A3hr)
                     FNAME="MISSING"
                     ;;
                esac
            ;;   
        Land4xCO2)
            case $STREAM in
                Amon)
                     FNAME="Aqua_SOM_newmpas_cam4_240km_land4xco2.cam.h0.008601-012512"
                     ;;
                Aday)
                     FNAME="Aqua_SOM_newmpas_cam4_240km_land4xco2_daily.cam.h0.mergetime"
                     ;;
                A3hr)
                     FNAME="MISSING"
                     ;;
                esac
            ;;   
        LandOrbit)
            case $STREAM in
                Amon)
                     FNAME="Aqua_SOM_newmpas_cam4_240km_landorbit.cam.h0.008601-012512"
                     ;;
                Aday)
                     FNAME="Aqua_SOM_newmpas_cam4_240km_landorbit_daily.cam.h0.mergetime"
                     ;;
                A3hr)
                     FNAME="MISSING"
                     ;;
                esac
            ;;   
   esac
   
   echo $EXPID $STREAM $FNAME

   cd /work/bm0834/b380459/TRACMIP/${MODELUOM}/${EXPID}/${STREAM}   

   # PR
   ${CDO} -setname,PR -add -selvar,PRECC ${FNAME}.nc -selvar,PRECL ${FNAME}.nc ${FNAME}_PR.nc
   ncatted -O -a longname,PR,o,c,precipitation -a long_name,PR,o,c,precipitation ${FNAME}_PR.nc
   
   # PRSN
   ${CDO} -setname,PRSN -add -selvar,PRECSC ${FNAME}.nc -selvar,PRECSL ${FNAME}.nc ${FNAME}_PRSN.nc
   ncatted -O -a longname,PRSN,o,c,"snow fall (water equivalent)" -a long_name,PRSN,o,c,"snow fall (water equivalent)" ${FNAME}_PRSN.nc

   # RLUS
   ${CDO} -setname,RLUS -add -selvar,FLDS ${FNAME}.nc -selvar,FLNS ${FNAME}.nc ${FNAME}_RLUS.nc
   ncatted -O -a longname,RLUS,o,c,surface_upwelling_longwave_flux -a long_name,RLUS,o,c,surface_upwelling_longwave_flux ${FNAME}_RLUS.nc
   
   # RSUS
   ${CDO} -setname,RSUS -add -selvar,FSDS ${FNAME}.nc -mulc,-1 -selvar,FSNS ${FNAME}.nc ${FNAME}_RSUS.nc
   ncatted -O -a longname,RSUS,o,c,surface_upwelling_shortwave_flux -a long_name,RSUS,o,c,surface_upwelling_shortwave_flux ${FNAME}_RSUS.nc

   # RSUSCS
   ${CDO} -setname,RSUSCS -add -selvar,FSDSC ${FNAME}.nc -mulc,-1 -selvar,FSNSC ${FNAME}.nc ${FNAME}_RSUSCS.nc
   ncatted -O -a longname,RSUSCS,o,c,surface_upwelling_shortwave_flux_clearsky -a long_name,RSUSCS,o,c,surface_upwelling_shortwave_flux_clearsky ${FNAME}_RSUSCS.nc
   
   # RSUT
   ${CDO} -setname,RSUT -add -selvar,SOLIN ${FNAME}.nc -mulc,-1 -selvar,FSNT ${FNAME}.nc ${FNAME}_RSUT.nc
   ncatted -O -a longname,RSUT,o,c,top_upwelling_shortwave_flux -a long_name,RSUT,o,c,top_upwelling_shortwave_flux ${FNAME}_RSUT.nc
   
   # RSUTCS
   ${CDO} -setname,RSUTCS -add -selvar,SOLIN ${FNAME}.nc -mulc,-1 -selvar,FSNTC ${FNAME}.nc ${FNAME}_RSUTCS.nc
   ncatted -O -a longname,RSUTCS,o,c,top_upwelling_shortwave_flux_clearsky -a long_name,RSUTCS,o,c,top_upwelling_shortwave_flux_clearsky ${FNAME}_RSUTCS.nc

done
done

cd /pf/b/b380459/TRACMIP_Cmorizing/cdoscripts
