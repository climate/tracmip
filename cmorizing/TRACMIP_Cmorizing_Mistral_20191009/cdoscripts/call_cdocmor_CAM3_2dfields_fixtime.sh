#!/bin/bash
#=============================================================================
# btc batch job parameters
#-----------------------------------------------------------------------------
#SBATCH --account=bm0834
#SBATCH --job-name=call_cdocmor_CAM3.sh
#SBATCH --partition=prepost
#SBATCH --ntasks=2
#SBATCH --cpus-per-task=8      # Specify number of CPUs per task
#SBATCH --output=/pf/b/b380459/TRACMIP_Cmorizing/cdoscripts/call_cdocmor_CAM3.sh.%j.o
#SBATCH --error=/pf/b/b380459/TRACMIP_Cmorizing/cdoscripts/call_cdocmor_CAM3.sh.%j.o
#SBATCH --time=08:00:00
#=============================================================================

# note the following:
#  - we need to interpolate to pressure for 3d variables, using cdo ml2pl
#  - the 2d fields PR, PRSN, RLUS, RSUS, RSUSCS, RSUT, RSUTCS are in separate files
#  - for precipitation fields PR, PRECC and PRSN we need to multiply with 1000 to get units from units of m/s to kg/m/s
#  - for the montly fields, the time is at the end of each month, but cdo cmor corrects for this automatically 
#  - land4xco2 was initialized from aquacontrol, need to add this to the metadata
#  - Aqua4xCO2 3hourly data is missing
#  - SFQ is in monthly data but is equal to zero, SFQ not available for daily and 3hourly data
#  - cdo selvar is very slow for daily and 3hourly data, so we use ncks -v instead
#  - Land simulations 3hourly: 3d fields CLOUD, CLDICE, CLDLIQ, RELHUM are missing 
#  - Land simulations 3hourly: 3d fields are missing vertical grid info, so not postprocessed now (maybe could be fixed manually?)

# Bind your OpenMP threads
export OMP_NUM_THREADS=8
export KMP_AFFINITY=verbose,granularity=thread,compact,1
export KMP_STACKSIZE=64m

set -xe

module unload nco
module unload cdo

module load cdo/1.9.5-gcc64
module load nco/4.7.5-gcc64

echo "----------------------------------------------------------------------------------"
echo "ATTENTION: MAKE SURE THAT CDO VERSION 1.9.5 IS USED FOR THE ML2PL INTERPOLATION!!!"
echo "----------------------------------------------------------------------------------"

CDO="cdo -P 8" 
CDOCMOR="/work/bm0021/cdo_incl_cmor/cdo_11_15_2017_cmor2/src/cdo -P 8" 

MODELUOM=CAM3   # model name at UoMiami server
MODELUOM2=CAM3  # for some models, there is a second UoMiami name needed for the actual variable file (differs from directory name by, e.g., small or capital letters)
MODELESGF=CAM3  # ESGF model name

MAPPINGTABLE=/pf/b/b380459/TRACMIP_Cmorizing/cdocmor_input/${MODELESGF}/mt_TRACMIP_${MODELESGF}.txt
echo "Using MAPPINGTABLE:" ${MAPPINGTABLE}

CALENDAR=365_day

EXPID2_AquaControl=aquaControlTRACMIP
EXPID2_Aqua4xCO2=aqua4xCO2TRACMIP
EXPID2_LandControl=landControlTRACMIP
EXPID2_Land4xCO2=land4xCO2TRACMIP
EXPID2_LandOrbit=landOrbitTRACMIP

YEARSTART_AquaControl_Amon=0016
YEARSTART_Aqua4xCO2_Amon=0046
YEARSTART_LandControl_Amon=0046
YEARSTART_Land4xCO2_Amon=0046
YEARSTART_LandOrbit_Amon=0086

YEAREND_AquaControl_Amon=0045
YEAREND_Aqua4xCO2_Amon=0085
YEAREND_LandControl_Amon=0085
YEAREND_Land4xCO2_Amon=0085
YEAREND_LandOrbit_Amon=0125

YEARSTART_AquaControl_Aday=0036
YEARSTART_Aqua4xCO2_Aday=0076
YEARSTART_LandControl_Aday=0076
YEARSTART_Land4xCO2_Aday=0076
YEARSTART_LandOrbit_Aday=0116

YEAREND_AquaControl_Aday=0045
YEAREND_Aqua4xCO2_Aday=0085
YEAREND_LandControl_Aday=0085
YEAREND_Land4xCO2_Aday=0085
YEAREND_LandOrbit_Aday=0125

YEARSTART_AquaControl_A3hr=0043
YEARSTART_Aqua4xCO2_A3hr=0083
YEARSTART_LandControl_A3hr=0083
YEARSTART_Land4xCO2_A3hr=0083
YEARSTART_LandOrbit_A3hr=1123

YEAREND_AquaControl_A3hr=0045
YEAREND_Aqua4xCO2_A3hr=0085
YEAREND_LandControl_A3hr=0085
YEAREND_Land4xCO2_A3hr=0085
YEAREND_LandOrbit_A3hr=0125

# SFQ zero for all Amon data
VARLIST2D_Amon_Aday="TGCLDIWP CLDTOT TGCLDLWP     LHFLX SHFLX PRECC TMQ PS PSL FLDS FLDSC FLUT FLUTC FSDS FSDSC SOLIN TREFHT TSMX TSMN TAUX TAUY TS"
VARLIST2D_A3hr="TGCLDIWP CLDTOT TGCLDLWP     LHFLX SHFLX PRECC TMQ PS PSL FLDS FLDSC FLUT FLUTC FSDS FSDSC SOLIN TSMX TSMN TAUX TAUY"
VARLIST2D_sepfiles="PR PRSN RLUS RSUS RSUSCS RSUT RSUTCS"

# move into TRACMIP work directory to do actual work
cd /work/bm0162/b380459/TRACMIP_cmorized

pwd

# create temporary directory for this model for tempfile.nc
rm -rf ${MODELESGF}_tmpdir
mkdir ${MODELESGF}_tmpdir

# A3hr missing for Aqua4xCO2
for EXPID1 in AquaControl Aqua4xCO2 LandControl Land4xCO2 LandOrbit; do
#for STREAM in Amon Aday A3hr; do
for STREAM in Aday; do

   SHIFTTIME="ERROR"  # so that script stops if none of the if conditions is met
   if [ "${EXPID1}" == "AquaControl" ]; then
      if [ "${STREAM}" == "Amon" ]; then
         SHIFTTIME="-settaxis,0016-01-15,00:00:00,1month"
      elif [ "${STREAM}" == "Aday" ]; then
         SHIFTTIME="-settaxis,0036-01-01,12:00:00,1day"
      elif [ "${STREAM}" == "A3hr" ]; then
         SHIFTTIME="-settaxis,0043-01-01,01:30:00,3hour"
      fi
   elif [ "${EXPID1}" == "Aqua4xCO2" ]; then
      if [ "${STREAM}" == "Amon" ]; then
         SHIFTTIME="-settaxis,0046-01-15,00:00:00,1month"
      elif [ "${STREAM}" == "Aday" ]; then
         SHIFTTIME="-settaxis,0076-01-01,12:00:00,1day"
      elif [ "${STREAM}" == "A3hr" ]; then
         SHIFTTIME="MISSING DATA"
      fi
   elif [ "${EXPID1}" == "LandControl" ]; then
      if [ "${STREAM}" == "Amon" ]; then
         SHIFTTIME="-settaxis,0046-01-15,00:00:00,1month"
      elif [ "${STREAM}" == "Aday" ]; then
         SHIFTTIME="-settaxis,0076-01-01,12:00:00,1day"
      elif [ "${STREAM}" == "A3hr" ]; then
         SHIFTTIME="-settaxis,0083-01-01,01:30:00,3hour"
      fi
   elif [ "${EXPID1}" == "Land4xCO2" ]; then
      if [ "${STREAM}" == "Amon" ]; then
         SHIFTTIME="-settaxis,0046-01-15,00:00:00,1month"
      elif [ "${STREAM}" == "Aday" ]; then
         SHIFTTIME="-settaxis,0076-01-01,12:00:00,1day"
      elif [ "${STREAM}" == "A3hr" ]; then
         SHIFTTIME="-settaxis,0083-01-01,01:30:00,3hour"
      fi
   elif [ "${EXPID1}" == "LandOrbit" ]; then
      if [ "${STREAM}" == "Amon" ]; then
         SHIFTTIME="-settaxis,0086-01-15,00:00:00,1month"
      elif [ "${STREAM}" == "Aday" ]; then
         SHIFTTIME="-settaxis,0116-01-01,12:00:00,1day"
      elif [ "${STREAM}" == "A3hr" ]; then
         SHIFTTIME="-settaxis,0123-01-01,01:30:00,3hour"
      fi
   fi
   
   if [ "${STREAM}" == "Amon" ]; then
      FILE="monthly"
      VARLIST2D=${VARLIST2D_Amon_Aday}
   fi
   if [ "${STREAM}" == "Aday" ]; then
      FILE="daily"
      VARLIST2D=${VARLIST2D_Amon_Aday}
   fi
   if [ "${STREAM}" == "A3hr" ]; then
      FILE="3hourly"
      VARLIST2D=${VARLIST2D_A3hr}
   fi
   echo $FILE, $VARLIST2D 
 
   EXPID2=EXPID2_${EXPID1}
   MIPTABLE=/pf/b/b380459/TRACMIP_Cmorizing/cdocmor_input/TRACMIP_${STREAM}
   CDOCMORINFO=/pf/b/b380459/TRACMIP_Cmorizing/cdocmor_input/${MODELESGF}/cdocmorinfo_TRACMIP_${MODELESGF}_${!EXPID2}
   for var in ${VARLIST2D}; do
      echo "Currently working on:" ${MODELESGF} ${EXPID1} ${STREAM} ${var}
      rm -f ${MODELESGF}_tmpdir/tempfile.nc
      INFILE=/work/bm0162/b380459/TRACMIP/${MODELUOM}/${EXPID1}/${EXPID1}_${FILE}.nc
      MULC=" "
      if [ "${var}" == "PRECC" ]; then
         MULC="-mulc,1000.0"
      fi
      echo ${INFILE}, ${MULC}
      ncks -v ${var} ${INFILE} ${MODELESGF}_tmpdir/ncks_tempfile.nc
      ${CDO} -s ${SHIFTTIME} -setcalendar,${CALENDAR} ${MULC} ${MODELESGF}_tmpdir/ncks_tempfile.nc ${MODELESGF}_tmpdir/tempfile.nc
      ${CDOCMOR} -s cmor,${MIPTABLE},info=${CDOCMORINFO},mapping_table=${MAPPINGTABLE} ${MODELESGF}_tmpdir/tempfile.nc
      rm -f ${MODELESGF}_tmpdir/*
    done
    for var in ${VARLIST2D_sepfiles}; do
      echo "Currently working on:" ${MODELESGF} ${EXPID1} ${STREAM} ${var}
      rm -f ${MODELESGF}_tmpdir/tempfile.nc
      INFILE=/work/bm0162/b380459/TRACMIP/${MODELUOM}/${EXPID1}/${EXPID1}_${FILE}_${var}.nc
      MULC=" "
      if [ "${var}" == "PR" ] || [ "${var}" == "PRSN" ]; then
         MULC="-mulc,1000.0"
      fi
      echo ${INFILE}, ${MULC}
      ncks -v ${var} ${INFILE} ${MODELESGF}_tmpdir/ncks_tempfile.nc
      ${CDO} -s ${SHIFTTIME} -setcalendar,${CALENDAR} ${MULC} ${MODELESGF}_tmpdir/ncks_tempfile.nc ${MODELESGF}_tmpdir/tempfile.nc
      ${CDOCMOR} -s cmor,${MIPTABLE},info=${CDOCMORINFO},mapping_table=${MAPPINGTABLE} ${MODELESGF}_tmpdir/tempfile.nc
      rm -f ${MODELESGF}_tmpdir/*
    done

done
done

# remove temporary directory
rm -rf ${MODELESGF}_tmpdir 

# move back to cdo script directory
cd /pf/b/b380459/TRACMIP_Cmorizing/cdoscripts
