#!/bin/bash
#=============================================================================
# btc batch job parameters
#-----------------------------------------------------------------------------
#SBATCH --account=bm0834
#SBATCH --job-name=call_cdocmor_MIROC5.sh
#SBATCH --partition=prepost
#SBATCH --ntasks=2
#SBATCH --cpus-per-task=8      # Specify number of CPUs per task
#SBATCH --output=/pf/b/b380459/TRACMIP_Cmorizing/cdoscripts/call_cdocmor_MIROC5.sh.%j.o
#SBATCH --error=/pf/b/b380459/TRACMIP_Cmorizing/cdoscripts/call_cdocmor_MIROC5.sh.%j.o
#SBATCH --time=08:00:00
#=============================================================================

# note the following:
# we need to multiply clt, cl, hur, hurs with 100 to go from fraction to per cent
# we ignore cmflx
# for cl, cli, clw the file names are *_plevels, as these have been interpolated to pressure levels by the script call_clouds_ml2pl_MIROC5.sh
# for 3-d variables that are not cl, cli, clw, pressure is in hPa instead of Pa, and so need to be converted to Pa
# fro Aday and A3hr, we first needed to merge individual years to a single file for entire Aday/A3hr output periods

# Bind your OpenMP threads
export OMP_NUM_THREADS=8
export KMP_AFFINITY=verbose,granularity=thread,compact,1
export KMP_STACKSIZE=64m

set -e

module load nco

CDO="cdo -P 8" 
CDOCMOR="/work/bm0021/cdo_incl_cmor/cdo_11_15_2017_cmor2/src/cdo -P 8" 

MODELUOM=MIROC5   # model name at UoMiami server
MODELUOM2=MIROC5  # for some models, there is a second UoMiami name needed for the actual variable file (differs from directory name by, e.g., small or capital letters)
MODELESGF=MIROC5  # ESGF model name

MAPPINGTABLE=/pf/b/b380459/TRACMIP_Cmorizing/cdocmor_input/${MODELESGF}/mt_TRACMIP_${MODELESGF}.txt
echo "Using MAPPINGTABLE:" ${MAPPINGTABLE}

CALENDAR=360_day

EXPID2_AquaControl=aquaControlTRACMIP
EXPID2_Aqua4xCO2=aqua4xCO2TRACMIP
EXPID2_LandControl=landControlTRACMIP
EXPID2_Land4xCO2=land4xCO2TRACMIP
EXPID2_LandOrbit=landOrbitTRACMIP

YEARSTART_AquaControl_Amon=1016
YEARSTART_Aqua4xCO2_Amon=1046
YEARSTART_LandControl_Amon=1046
YEARSTART_Land4xCO2_Amon=1086
YEARSTART_LandOrbit_Amon=1086

YEAREND_AquaControl_Amon=1045
YEAREND_Aqua4xCO2_Amon=1085
YEAREND_LandControl_Amon=1085
YEAREND_Land4xCO2_Amon=1125
YEAREND_LandOrbit_Amon=1125

YEARSTART_AquaControl_Aday=1036
YEARSTART_Aqua4xCO2_Aday=1076
YEARSTART_LandControl_Aday=1076
YEARSTART_Land4xCO2_Aday=1116
YEARSTART_LandOrbit_Aday=1116

YEAREND_AquaControl_Aday=1045
YEAREND_Aqua4xCO2_Aday=1085
YEAREND_LandControl_Aday=1085
YEAREND_Land4xCO2_Aday=1125
YEAREND_LandOrbit_Aday=1125

YEARSTART_AquaControl_A3hr=1043
YEARSTART_Aqua4xCO2_A3hr=1083
YEARSTART_LandControl_A3hr=1083
YEARSTART_Land4xCO2_A3hr=1123
YEARSTART_LandOrbit_A3hr=1123

YEAREND_AquaControl_A3hr=1045
YEAREND_Aqua4xCO2_A3hr=1085
YEAREND_LandControl_A3hr=1085
YEAREND_Land4xCO2_A3hr=1125
YEAREND_LandOrbit_A3hr=1125

VARLIST2D="cct clivi clt clwvi evspsbl hfls hfss hurs huss pr prc prsn prw ps psl rlds rldscs rlus rlut rlutcs rsds rsdscs rsdt rsus rsuscs rsut rsutcs sfcWind tas tasmax tasmin tauu tauv ts uas vas"
VARLIST3D="cl cli clw hur hus ta ua va wap zg"

# move into TRACMIP work directory to do actual work
cd /work/bm0162/b380459/TRACMIP_cmorized

pwd

# create temporary directory for this model for tempfile.nc
rm -rf ${MODELESGF}_tmpdir
mkdir ${MODELESGF}_tmpdir

#for EXPID1 in AquaControl Aqua4xCO2 LandControl Land4xCO2 LandOrbit; do
for EXPID1 in AquaControl Aqua4xCO2; do
   
   STREAM=Amon  
   EXPID2=EXPID2_${EXPID1}
   MIPTABLE=/pf/b/b380459/TRACMIP_Cmorizing/cdocmor_input/TRACMIP_${STREAM}
   CDOCMORINFO=/pf/b/b380459/TRACMIP_Cmorizing/cdocmor_input/${MODELESGF}/cdocmorinfo_TRACMIP_${MODELESGF}_${!EXPID2}.txt
   for var in " "; do #${VARLIST2D}; do
      echo "Currently working on:" ${MODELESGF} ${EXPID1} ${STREAM} ${var}
      rm -f ${MODELESGF}_tmpdir/tempfile.nc
      YEARSTART=YEARSTART_${EXPID1}_${STREAM}
      YEAREND=YEAREND_${EXPID1}_${STREAM}
      INFILE=/work/bm0162/b380459/TRACMIP/${MODELUOM}/${EXPID1}/${STREAM}/${var}_${STREAM}_${MODELUOM2}_${EXPID1}_r1i1p1_${!YEARSTART}01-${!YEAREND}12.nc
      echo ${INFILE}
      MULC=" "
      if [ "${var}" == "clt" ] || [ "${var}" == "hurs" ]; then
         MULC="-mulc,100.0"
      fi
      if [ -f ${INFILE} ]; then
         ${CDO} -s ${MULC} -setcalendar,${CALENDAR} ${INFILE} ${MODELESGF}_tmpdir/tempfile.nc
         ${CDOCMOR} -s cmor,${MIPTABLE},info=${CDOCMORINFO},mapping_table=${MAPPINGTABLE} ${MODELESGF}_tmpdir/tempfile.nc
      fi
      rm -f ${MODELESGF}_tmpdir/tempfile.nc
   done
   for var in " "; do #${VARLIST3D}; do
      echo "Currently working on:" ${MODELESGF} ${EXPID1} ${STREAM} ${var}
      rm -f ${MODELESGF}_tmpdir/tempfile.nc
      YEARSTART=YEARSTART_${EXPID1}_${STREAM}
      YEAREND=YEAREND_${EXPID1}_${STREAM}
      INFILE=/work/bm0162/b380459/TRACMIP/${MODELUOM}/${EXPID1}/${STREAM}/${var}_${STREAM}_${MODELUOM2}_${EXPID1}_r1i1p1_${!YEARSTART}01-${!YEAREND}12.nc
      if [ "${var}" == "cl" ] || [ "${var}" == "cli" ] || [ "${var}" == "clw" ] ; then
         INFILE=/work/bm0162/b380459/TRACMIP/${MODELUOM}/${EXPID1}/${STREAM}/${var}_${STREAM}_${MODELUOM2}_${EXPID1}_r1i1p1_${!YEARSTART}01-${!YEAREND}12.nc_plevels
      fi
      echo ${INFILE}
      MULC=" "
      if [ "${var}" == "cl" ] || [ "${var}" == "hur" ]; then
         MULC="-mulc,100.0"
      fi
      if [ -f ${INFILE} ]; then
         ${CDO} -s ${MULC} -setcalendar,${CALENDAR} ${INFILE} ${MODELESGF}_tmpdir/tempfile.nc
         # if not cloud fields, than plev is in hPa, so must be converted to Pa 
         if [ "${var}" != "cl" ] && [ "${var}" != "cli" ] && [ "${var}" != "clw" ]; then         
            ncap2 -O -s 'plev*=100'  ${MODELESGF}_tmpdir/tempfile.nc ${MODELESGF}_tmpdir/tempfile.nc  
            ncatted -O -a units,plev,m,c,"Pa" ${MODELESGF}_tmpdir/tempfile.nc       
         fi
         ${CDOCMOR} -s cmor,${MIPTABLE},info=${CDOCMORINFO},mapping_table=${MAPPINGTABLE} ${MODELESGF}_tmpdir/tempfile.nc
      fi
      rm -f ${MODELESGF}_tmpdir/tempfile.nc
   done

   STREAM=Aday  
   EXPID2=EXPID2_${EXPID1}
   MIPTABLE=/pf/b/b380459/TRACMIP_Cmorizing/cdocmor_input/TRACMIP_${STREAM}
   CDOCMORINFO=/pf/b/b380459/TRACMIP_Cmorizing/cdocmor_input/${MODELESGF}/cdocmorinfo_TRACMIP_${MODELESGF}_${!EXPID2}.txt
   for var in " "; do #${VARLIST2D}; do
      echo "Currently working on:" ${MODELESGF} ${EXPID1} ${STREAM} ${var}
      rm -f ${MODELESGF}_tmpdir/tempfile.nc
      YEARSTART=YEARSTART_${EXPID1}_${STREAM}
      YEAREND=YEAREND_${EXPID1}_${STREAM}
      INFILE=/work/bm0162/b380459/TRACMIP/${MODELUOM}/${EXPID1}/${STREAM}/${var}_${STREAM}_${MODELUOM2}_${EXPID1}_r1i1p1_${!YEARSTART}0101-${!YEAREND}1230.nc
      echo ${INFILE}
      MULC=" "
      if [ "${var}" == "clt" ] || [ "${var}" == "hurs" ]; then
         MULC="-mulc,100.0"
      fi
      if [ -f ${INFILE} ]; then
         ${CDO} -s ${MULC} -setcalendar,${CALENDAR} ${INFILE} ${MODELESGF}_tmpdir/tempfile.nc
         ${CDOCMOR} -s cmor,${MIPTABLE},info=${CDOCMORINFO},mapping_table=${MAPPINGTABLE} ${MODELESGF}_tmpdir/tempfile.nc
      fi
      rm -f ${MODELESGF}_tmpdir/tempfile.nc
   done
   for var in " "; do # ${VARLIST3D}; do
      echo "Currently working on:" ${MODELESGF} ${EXPID1} ${STREAM} ${var}
      rm -f ${MODELESGF}_tmpdir/tempfile.nc
      YEARSTART=YEARSTART_${EXPID1}_${STREAM}
      YEAREND=YEAREND_${EXPID1}_${STREAM}
      INFILE=/work/bm0162/b380459/TRACMIP/${MODELUOM}/${EXPID1}/${STREAM}/${var}_${STREAM}_${MODELUOM2}_${EXPID1}_r1i1p1_${!YEARSTART}0101-${!YEAREND}1230.nc
      if [ "${var}" == "cl" ] || [ "${var}" == "cli" ] || [ "${var}" == "clw" ] ; then
         INFILE=/work/bm0162/b380459/TRACMIP/${MODELUOM}/${EXPID1}/${STREAM}/${var}_${STREAM}_${MODELUOM2}_${EXPID1}_r1i1p1_${!YEARSTART}0101-${!YEAREND}1230.nc_plevels
      fi
      echo ${INFILE}
      MULC=" "
      if [ "${var}" == "cl" ] || [ "${var}" == "hur" ]; then
         MULC="-mulc,100.0"
      fi
      if [ -f ${INFILE} ]; then
         ${CDO} -s ${MULC} -setcalendar,${CALENDAR} ${INFILE} ${MODELESGF}_tmpdir/tempfile.nc
         # if not cloud fields, than plev is in hPa, so must be converted to Pa 
         if [ "${var}" != "cl" ] && [ "${var}" != "cli" ] && [ "${var}" != "clw" ]; then         
            ncap2 -O -s 'plev*=100'  ${MODELESGF}_tmpdir/tempfile.nc ${MODELESGF}_tmpdir/tempfile.nc  
            ncatted -O -a units,plev,m,c,"Pa" ${MODELESGF}_tmpdir/tempfile.nc       
         fi
         ${CDOCMOR} -s cmor,${MIPTABLE},info=${CDOCMORINFO},mapping_table=${MAPPINGTABLE} ${MODELESGF}_tmpdir/tempfile.nc
      fi
      rm -f ${MODELESGF}_tmpdir/tempfile.nc
   done

   STREAM=A3hr  
   MIPTABLE=/pf/b/b380459/TRACMIP_Cmorizing/cdocmor_input/TRACMIP_${STREAM}
   CDOCMORINFO=/pf/b/b380459/TRACMIP_Cmorizing/cdocmor_input/${MODELESGF}/cdocmorinfo_TRACMIP_${MODELESGF}_${!EXPID2}.txt
   SETTAXIS=" "
   if [ ${EXPID1} = "AquaControl" ]; then
      SETTAXIS="-settaxis,1043-01-01,1:30,3h"
   elif [ ${EXPID1} = "Aqua4xCO2" ]; then
      SETTAXIS="-settaxis,1083-01-01,1:30,3h"
   elif [ ${EXPID1} = "LandControl" ]; then
      SETTAXIS="-settaxis,1083-01-01,1:30,3h"
   elif [ ${EXPID1} = "Land4xCO2" ]; then
      SETTAXIS="-settaxis,1123-01-01,1:30,3h"
   elif [ ${EXPID1} = "LandOrbit" ]; then
      SETTAXIS="-settaxis,1123-01-01,1:30,3h"
   fi
   
   for var in ${VARLIST2D}; do
      echo "Currently working on:" ${MODELESGF} ${EXPID1} ${STREAM} ${var}
      rm -f ${MODELESGF}_tmpdir/tempfile.nc
      YEARSTART=YEARSTART_${EXPID1}_${STREAM}
      YEAREND=YEAREND_${EXPID1}_${STREAM}
      INFILE=/work/bm0162/b380459/TRACMIP/${MODELUOM}/${EXPID1}/${STREAM}/${var}_${STREAM}_${MODELUOM2}_${EXPID1}_r1i1p1_${!YEARSTART}0101-${!YEAREND}1230.nc
      echo ${INFILE}
      MULC=" "
      if [ "${var}" == "clt" ] || [ "${var}" == "hurs" ]; then
         MULC="-mulc,100.0"
      fi
      if [ -f ${INFILE} ]; then
         ${CDO} -s ${MULC} ${SETTAXIS} -setcalendar,${CALENDAR} ${INFILE} ${MODELESGF}_tmpdir/tempfile.nc
         ${CDOCMOR} -s cmor,${MIPTABLE},info=${CDOCMORINFO},mapping_table=${MAPPINGTABLE} ${MODELESGF}_tmpdir/tempfile.nc
      fi
      rm -f ${MODELESGF}_tmpdir/tempfile.nc
   done
   for var in ${VARLIST3D}; do
      echo "Currently working on:" ${MODELESGF} ${EXPID1} ${STREAM} ${var}
      rm -f ${MODELESGF}_tmpdir/tempfile.nc
      YEARSTART=YEARSTART_${EXPID1}_${STREAM}
      YEAREND=YEAREND_${EXPID1}_${STREAM}
      INFILE=/work/bm0162/b380459/TRACMIP/${MODELUOM}/${EXPID1}/${STREAM}/${var}_${STREAM}_${MODELUOM2}_${EXPID1}_r1i1p1_${!YEARSTART}0101-${!YEAREND}1230.nc
      echo ${INFILE}
      if [ "${var}" == "cl" ] || [ "${var}" == "cli" ] || [ "${var}" == "clw" ] ; then
         INFILE=/work/bm0162/b380459/TRACMIP/${MODELUOM}/${EXPID1}/${STREAM}/${var}_${STREAM}_${MODELUOM2}_${EXPID1}_r1i1p1_${!YEARSTART}0101-${!YEAREND}1230.nc_plevels
      fi
      MULC=" "
      if [ "${var}" == "cl" ] || [ "${var}" == "hur" ]; then
         MULC="-mulc,100.0"
      fi
      if [ -f ${INFILE} ]; then
         ${CDO} -s ${MULC} ${SETTAXIS} -setcalendar,${CALENDAR} ${INFILE} ${MODELESGF}_tmpdir/tempfile.nc
         # if not cloud fields, than plev is in hPa, so must be converted to Pa 
         if [ "${var}" != "cl" ] && [ "${var}" != "cli" ] && [ "${var}" != "clw" ]; then         
            ncap2 -O -s 'plev*=100'  ${MODELESGF}_tmpdir/tempfile.nc ${MODELESGF}_tmpdir/tempfile.nc  
            ncatted -O -a units,plev,m,c,"Pa" ${MODELESGF}_tmpdir/tempfile.nc       
         fi
         ${CDOCMOR} -s cmor,${MIPTABLE},info=${CDOCMORINFO},mapping_table=${MAPPINGTABLE} ${MODELESGF}_tmpdir/tempfile.nc
      fi
      rm -f ${MODELESGF}_tmpdir/tempfile.nc
   done

done

# remove temporary directory
rm -rf ${MODELESGF}_tmpdir 

# move back to cdo script directory
cd /pf/b/b380459/TRACMIP_Cmorizing/cdoscripts
