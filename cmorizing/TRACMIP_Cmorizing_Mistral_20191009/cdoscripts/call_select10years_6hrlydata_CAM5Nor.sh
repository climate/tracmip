#!/bin/bash

echo "UPDATE: 6hourly data is all 0/nan, so not used further ... but 6hrly Cam5Nor scripts kept for reference ..."

set -xe

# daily data: select only last 10 years via ncks
module load nco/4.7.5-gcc64

#for var in hus ps ta ua va; do
#
#   ncks -O -d time,'0036-01-01','0045-12-31' /work/bm0162/b380459/TRACMIP/NorESM2/AquaControl/amip/6hr/atmos/${var}/r1i1p1/${var}_6hrLev_AquaControl_amip_r1i1p1_0002010103-0045123121.nc /work/bm0162/b380459/TRACMIP/NorESM2/AquaControl/amip/6hr/atmos/${var}/r1i1p1/${var}_6hrLev_AquaControl_amip_r1i1p1_year0036-year0045.nc
#
#   ncks -O -d time,'0077-01-01','0086-12-31' /work/bm0162/b380459/TRACMIP/NorESM2/Aqua4xCO2/A6hr/${var}_6hrLev_Aqua4xCO2_amip_r1i1p1_0046010103-0086123121.nc /work/bm0162/b380459/TRACMIP/NorESM2/Aqua4xCO2/A6hr/${var}_6hrLev_Aqua4xCO2_amip_r1i1p1_year0077-year0086.nc
#
#   ncks -O -d time,'0076-01-01','0085-12-31' /work/bm0162/b380459/TRACMIP/NorESM2/LandControl/amip/6hr/atmos/${var}/r1i1p1/${var}_6hrLev_LandControl_amip_r1i1p1_0046030121-0085123121.nc /work/bm0162/b380459/TRACMIP/NorESM2/LandControl/amip/6hr/atmos/${var}/r1i1p1/${var}_6hrLev_LandControl_amip_r1i1p1_year0076-year0085.nc
#
#   ncks -O -d time,'0116-01-01','0125-12-31' /work/bm0162/b380459/TRACMIP/NorESM2/Land4xCO2/A6hr/${var}_6hrLev_Land4xCO2_amip_r1i1p1_0086010103-0126022821.nc /work/bm0162/b380459/TRACMIP/NorESM2/Land4xCO2/A6hr/${var}_6hrLev_Land4xCO2_amip_r1i1p1_year0116-year0125.nc
#
#done

for var in psl; do

   ncks -O -d time,'0036-01-01','0045-12-31' /work/bm0162/b380459/TRACMIP/NorESM2/AquaControl/amip/6hr/atmos/psl/r1i1p1/psl_6hrPlev_AquaControl_amip_r1i1p1_0002010103-0045123121.nc /work/bm0162/b380459/TRACMIP/NorESM2/AquaControl/amip/6hr/atmos/psl/r1i1p1/psl_6hrPlev_AquaControl_amip_r1i1p1_year0036-year0045.nc

   ncks -O -d time,'0077-01-01','0086-12-31' /work/bm0162/b380459/TRACMIP/NorESM2/Aqua4xCO2/A6hr/psl_6hrPlev_Aqua4xCO2_amip_r1i1p1_0046010103-0086123121.nc /work/bm0162/b380459/TRACMIP/NorESM2/Aqua4xCO2/A6hr/psl_6hrPlev_Aqua4xCO2_amip_r1i1p1_year0077-year0086.nc

   ncks -O -d time,'0076-01-01','0085-12-31' /work/bm0162/b380459/TRACMIP/NorESM2/LandControl/amip/6hr/atmos/psl/r1i1p1/psl_6hrPlev_LandControl_amip_r1i1p1_0046030121-0085123121.nc /work/bm0162/b380459/TRACMIP/NorESM2/LandControl/amip/6hr/atmos/psl/r1i1p1/psl_6hrPlev_LandControl_amip_r1i1p1_year0076-year0085.nc

   ncks -O -d time,'0116-01-01','0125-12-31' /work/bm0162/b380459/TRACMIP/NorESM2/Land4xCO2/A6hr/psl_6hrPlev_Land4xCO2_amip_r1i1p1_0086010103-0126022821.nc /work/bm0162/b380459/TRACMIP/NorESM2/Land4xCO2/A6hr/psl_6hrPlev_Land4xCO2_amip_r1i1p1_year0116-year0125.nc

done
