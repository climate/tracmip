#!/bin/bash
#=============================================================================
# btc batch job parameters
#-----------------------------------------------------------------------------
#SBATCH --account=bb1018
#SBATCH --job-name=call_cdocmor_CALTECH_landN.sh
#SBATCH --partition=prepost
#SBATCH --ntasks=2
#SBATCH --cpus-per-task=8      # Specify number of CPUs per task
#SBATCH --output=/pf/b/b380459/TRACMIP_Cmorizing/cdoscripts/call_cdocmor_CALTECH.sh.%j.o
#SBATCH --error=/pf/b/b380459/TRACMIP_Cmorizing/cdoscripts/call_cdocmor_CALTECH.sh.%j.o
#SBATCH --time=08:00:00
#=============================================================================

# note the following:

# Bind your OpenMP threads
export OMP_NUM_THREADS=8
export KMP_AFFINITY=verbose,granularity=thread,compact,1
export KMP_STACKSIZE=64m

set -e

module load nco

CDO="cdo -P 8" 
CDOCMOR="/work/bm0021/cdo_incl_cmor/cdo_11_15_2017_cmor2/src/cdo -P 8" 

MODELUOM=CaltechGray   # model name at UoMiami server
MODELESGF=CALTECH      # ESGF model name

MAPPINGTABLE=/pf/b/b380459/TRACMIP_Cmorizing/cdocmor_input/${MODELESGF}/mt_TRACMIP_${MODELESGF}.txt
echo "Using MAPPINGTABLE:" ${MAPPINGTABLE}

CALENDAR=360_day

EXPID2_LandControlN=landControlTRACMIP
EXPID2_LandAbs07N=landAbs07TRACMIP
EXPID2_LandAbs15N=landAbs15TRACMIP
EXPID2_LandAbs20N=landAbs20TRACMIP
EXPID2_LandOrbitN=landOrbitTRACMIP

YEARSTART_LandControlN_Amon=0046
YEARSTART_LandAbs07N_Amon=0086
YEARSTART_LandAbs15N_Amon=0086
YEARSTART_LandAbs20N_Amon=0086
YEARSTART_LandOrbitN_Amon=0086

YEARSTART_LandControlN_Aday=0076
YEARSTART_LandAbs07N_Aday=0116
YEARSTART_LandAbs15N_Aday=0116
YEARSTART_LandAbs20N_Aday=0116
YEARSTART_LandOrbitN_Aday=0116

VARLIST2D="hfls hfss hurs huss pr prc prw ps psl rlds rlus rlut rsds rsdt rsus rsut sfcWind tas tauu tauv ts uas vas"
VARLIST3D="hur hus ta ua va omega z_full"

# move into TRACMIP work directory to do actual work
cd /work/bm0162/b380459/TRACMIP_cmorized

pwd

# create temporary directory for this model for tempfile.nc
rm -rf ${MODELESGF}_tmpdir
mkdir ${MODELESGF}_tmpdir

for EXPID1 in LandControlN LandAbs07N LandAbs15N LandAbs20N LandOrbitN; do
   
   STREAM=Amon  
   EXPID2=EXPID2_${EXPID1}
   MIPTABLE=/pf/b/b380459/TRACMIP_Cmorizing/cdocmor_input/TRACMIP_${STREAM}
   CDOCMORINFO=/pf/b/b380459/TRACMIP_Cmorizing/cdocmor_input/${MODELESGF}/cdocmorinfo_TRACMIP_${MODELESGF}_${!EXPID2}.txt
   for var in ${VARLIST2D}; do
      echo "Currently working on:" ${MODELESGF} ${EXPID1} ${STREAM} ${var}
      rm -f ${MODELESGF}_tmpdir/tempfile.nc
      YEARSTART=YEARSTART_${EXPID1}_${STREAM}
      INFILE=/work/bm0162/b380459/TRACMIP/${MODELUOM}/${EXPID1}/${EXPID1}.monthly_newTcoord.nc
      cdo -s -settaxis,${!YEARSTART}-01-16,0:00,30d -setcalendar,${CALENDAR} -selvar,${var} ${INFILE} ${MODELESGF}_tmpdir/tempfile.nc
      ${CDOCMOR} -s cmor,${MIPTABLE},info=${CDOCMORINFO},mapping_table=${MAPPINGTABLE} ${MODELESGF}_tmpdir/tempfile.nc
      rm -f ${MODELESGF}_tmpdir/tempfile.nc
   done
   for var in ${VARLIST3D}; do
      echo "Currently working on:" ${MODELESGF} ${EXPID1} ${STREAM} ${var}
      rm -f ${MODELESGF}_tmpdir/tempfile.nc
      YEARSTART=YEARSTART_${EXPID1}_${STREAM}
      INFILE=/work/bm0162/b380459/TRACMIP/${MODELUOM}/${EXPID1}/${EXPID1}.monthly_newTcoord.nc
      cdo -s -settaxis,${!YEARSTART}-01-16,0:00,30d -setcalendar,${CALENDAR} -selvar,${var} ${INFILE} ${MODELESGF}_tmpdir/tempfile.nc
      ncap2 -O -s 'lev_p*=100'  ${MODELESGF}_tmpdir/tempfile.nc ${MODELESGF}_tmpdir/tempfile.nc  
      ncatted -O -a units,lev_p,m,c,"Pa" ${MODELESGF}_tmpdir/tempfile.nc   
      ${CDOCMOR} -s cmor,${MIPTABLE},info=${CDOCMORINFO},mapping_table=${MAPPINGTABLE} ${MODELESGF}_tmpdir/tempfile.nc
      rm -f ${MODELESGF}_tmpdir/tempfile.nc
   done

   STREAM=Aday  
   EXPID2=EXPID2_${EXPID1}
   MIPTABLE=/pf/b/b380459/TRACMIP_Cmorizing/cdocmor_input/TRACMIP_${STREAM}
   CDOCMORINFO=/pf/b/b380459/TRACMIP_Cmorizing/cdocmor_input/${MODELESGF}/cdocmorinfo_TRACMIP_${MODELESGF}_${!EXPID2}.txt
   for var in ${VARLIST2D}; do
      echo "Currently working on:" ${MODELESGF} ${EXPID1} ${STREAM} ${var}
      rm -f ${MODELESGF}_tmpdir/tempfile.nc
      YEARSTART=YEARSTART_${EXPID1}_${STREAM}
      INFILE=/work/bm0162/b380459/TRACMIP/${MODELUOM}/${EXPID1}/${EXPID1}.daily_newTcoord.nc
      cdo -s -settaxis,${!YEARSTART}-01-01,12:00,1d -setcalendar,${CALENDAR} -selvar,${var} ${INFILE} ${MODELESGF}_tmpdir/tempfile.nc
      ${CDOCMOR} -s cmor,${MIPTABLE},info=${CDOCMORINFO},mapping_table=${MAPPINGTABLE} ${MODELESGF}_tmpdir/tempfile.nc
      rm -f ${MODELESGF}_tmpdir/tempfile.nc
   done
   for var in ${VARLIST3D}; do
      echo "Currently working on:" ${MODELESGF} ${EXPID1} ${STREAM} ${var}
      rm -f ${MODELESGF}_tmpdir/tempfile.nc
      YEARSTART=YEARSTART_${EXPID1}_${STREAM}
      INFILE=/work/bm0162/b380459/TRACMIP/${MODELUOM}/${EXPID1}/${EXPID1}.daily_newTcoord.nc
      cdo -s -settaxis,${!YEARSTART}-01-01,12:00,1d -setcalendar,${CALENDAR} -selvar,${var} ${INFILE} ${MODELESGF}_tmpdir/tempfile.nc
      ncap2 -O -s 'lev_p*=100'  ${MODELESGF}_tmpdir/tempfile.nc ${MODELESGF}_tmpdir/tempfile.nc  
      ncatted -O -a units,lev_p,m,c,"Pa" ${MODELESGF}_tmpdir/tempfile.nc   
      ${CDOCMOR} -s cmor,${MIPTABLE},info=${CDOCMORINFO},mapping_table=${MAPPINGTABLE} ${MODELESGF}_tmpdir/tempfile.nc
      rm -f ${MODELESGF}_tmpdir/tempfile.nc
   done

done

# remove temporary directory
rm -rf ${MODELESGF}_tmpdir 

# move back to cdo script directory
cd /pf/b/b380459/TRACMIP_Cmorizing/cdoscripts
