#!/bin/bash
#=============================================================================
# btc batch job parameters
#-----------------------------------------------------------------------------
#SBATCH --account=bm0834
#SBATCH --job-name=call_cdocmor_ECHAM63.sh
#SBATCH --partition=prepost
#SBATCH --ntasks=2
#SBATCH --cpus-per-task=8      # Specify number of CPUs per task
#SBATCH --output=/work/bm0162/b380459/TRACMIP_cmorized/call_cdocmor_ECHAM63.sh.%j.o
#SBATCH --error=/work/bm0162/b380459/TRACMIP_cmorized/call_cdocmor_ECHAM63.sh.%j.o
#SBATCH --time=08:00:00
#=============================================================================

# Bind your OpenMP threads
export OMP_NUM_THREADS=8
export KMP_AFFINITY=verbose,granularity=thread,compact,1
export KMP_STACKSIZE=64m

set -ex

module unload nco
module unload cdo

module load cdo/1.9.5-gcc64
module load nco/4.7.5-gcc64

CDO="cdo -P 8" 
CDOCMOR="/work/bm0021/cdo_incl_cmor/cdo_11_15_2017_cmor2/src/cdo -P 8" 

MODELUOM=ECHAM6.3   # model name at UoMiami server
MODELUOM2=echam6-3 # for some models, there is a second UoMiami name needed for the actual variable file (differs from directory name by, e.g., small or capital letters)
MODELESGF=ECHAM63  # ESGF model name

MAPPINGTABLE=/pf/b/b380459/TRACMIP_Cmorizing/cdocmor_input/${MODELESGF}/mt_TRACMIP_${MODELESGF}.txt
echo "Using MAPPINGTABLE:" ${MAPPINGTABLE}

VARLIST="cl cli clivi clt clw clwvi evspsbl hfls hfss hur hus pr prc prsn prw ps psl rlds rldscs rlus rlut rlutcs rsds rsdscs rsdt rsus rsuscs rsut rsutcs sfcWind ta tas tauu tauv ts ua uas va vas wap zg"

EXPID2_AquaControl=aquaControlTRACMIP
EXPID2_Aqua4xCO2=aqua4xCO2TRACMIP
EXPID2_LandControl=landControlTRACMIP
EXPID2_Land4xCO2=land4xCO2TRACMIP
EXPID2_LandOrbit=landOrbitTRACMIP

CALENDAR=360_day
#CALENDAR=proleptic_gregorian

# move into TRACMIP work directory to do actual work
cd /work/bm0162/b380459/TRACMIP_cmorized
pwd

# create temporary directory for this model for tempfile.nc
rm -rf ${MODELESGF}_tmpdir
mkdir ${MODELESGF}_tmpdir

for EXPID1 in AquaControl Aqua4xCO2 LandControl Land4xCO2 LandOrbit; do
for STREAM in Amon Aday A3hr; do

   SHIFTTIME="ERROR"  # so that script stops if none of the if conditions is met
   if [ "${EXPID1}" == "AquaControl" ]; then
      if [ "${STREAM}" == "Amon" ]; then
         SHIFTTIME="-settaxis,1996-01-01,00:00:00,30day"
      elif [ "${STREAM}" == "Aday" ]; then
         SHIFTTIME="-settaxis,2016-01-01,12:00:00,1day"
      elif [ "${STREAM}" == "A3hr" ]; then
         SHIFTTIME="-settaxis,2023-01-01,01:30:00,3hour"
      fi
   elif [ "${EXPID1}" == "Aqua4xCO2" ]; then
      if [ "${STREAM}" == "Amon" ]; then
         SHIFTTIME="-settaxis,2026-01-01,00:00:00,30day"
      elif [ "${STREAM}" == "Aday" ]; then
         SHIFTTIME="-settaxis,2056-01-01,12:00:00,1day"
      elif [ "${STREAM}" == "A3hr" ]; then
         SHIFTTIME="-settaxis,2063-01-01,01:30:00,3hour"
      fi
   elif [ "${EXPID1}" == "LandControl" ]; then
      if [ "${STREAM}" == "Amon" ]; then
         SHIFTTIME="-settaxis,2026-01-01,00:00:00,30day"
      elif [ "${STREAM}" == "Aday" ]; then
         SHIFTTIME="-settaxis,2056-01-01,12:00:00,1day"
      elif [ "${STREAM}" == "A3hr" ]; then
         SHIFTTIME="-settaxis,2063-01-01,01:30:00,3hour"
      fi
   elif [ "${EXPID1}" == "Land4xCO2" ]; then
      if [ "${STREAM}" == "Amon" ]; then
         SHIFTTIME="-settaxis,2066-01-01,00:00:00,30day"
      elif [ "${STREAM}" == "Aday" ]; then
         SHIFTTIME="-settaxis,2096-01-01,12:00:00,1day"
      elif [ "${STREAM}" == "A3hr" ]; then
         SHIFTTIME="-settaxis,2103-01-01,01:30:00,3hour"
      fi
   elif [ "${EXPID1}" == "LandOrbit" ]; then
      if [ "${STREAM}" == "Amon" ]; then
         SHIFTTIME="-settaxis,2066-01-01,00:00:00,30day"
      elif [ "${STREAM}" == "Aday" ]; then
         SHIFTTIME="-settaxis,2096-01-01,12:00:00,1day"
      elif [ "${STREAM}" == "A3hr" ]; then
         SHIFTTIME="-settaxis,2103-01-01,01:30:00,3hour"
      fi
   fi  

   EXPID2=EXPID2_${EXPID1}
   MIPTABLE=/pf/b/b380459/TRACMIP_Cmorizing/cdocmor_input/TRACMIP_${STREAM}
   CDOCMORINFO=/pf/b/b380459/TRACMIP_Cmorizing/cdocmor_input/${MODELESGF}/cdocmorinfo_TRACMIP_${MODELESGF}_${!EXPID2}
   for var in ${VARLIST}; do
      echo "Currently working on:" ${MODELESGF} ${EXPID1} ${STREAM} ${var}
      rm -f ${MODELESGF}_tmpdir/tempfile.nc
      INFILE=/work/bm0834/b380459/TRACMIP/${MODELUOM}/${EXPID1}/${STREAM}/${var}_${STREAM}_${MODELUOM2}_${EXPID1}_r1i1p1_????01-????12.nc
      DIVC=" "
      if [ "${var}" == "cli" ] || [ "${var}" == "clw" ]; then
         DIVC="-divc,100.0"
      fi
      echo ${INFILE}, ${DIVC}
      ncks -v ${var} ${INFILE} ${MODELESGF}_tmpdir/ncks_tempfile.nc
      ${CDO} -s ${SHIFTTIME} -setcalendar,${CALENDAR} ${DIVC} ${MODELESGF}_tmpdir/ncks_tempfile.nc ${MODELESGF}_tmpdir/tempfile.nc
      ${CDOCMOR} -s cmor,${MIPTABLE},info=${CDOCMORINFO},mapping_table=${MAPPINGTABLE} ${MODELESGF}_tmpdir/tempfile.nc
      rm -f ${MODELESGF}_tmpdir/*
    done

done
done

# remove temporary directory
rm -rf ${MODELESGF}_tmpdir 

# move back to cdo script directory
cd /pf/b/b380459/TRACMIP_Cmorizing/cdoscripts


