#!/bin/bash
#=============================================================================
# btc batch job parameters
#-----------------------------------------------------------------------------
#SBATCH --account=bm0834
#SBATCH --job-name=call_plevint_python_CAM4.sh
#SBATCH --partition=compute2
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=1      # Specify number of CPUs per task
#SBATCH --output=/pf/b/b380459/TRACMIP_Cmorizing/cdoscripts/call_plevint_python_CAM4.sh.%j.o
#SBATCH --error=/pf/b/b380459/TRACMIP_Cmorizing/cdoscripts/call_plevint_python_CAM4.sh.%j.o
#SBATCH --exclusive
#SBATCH --time=08:00:00
#=============================================================================

set -ex

# note the following:
# we use python xarray and scipy interpolation to remap 3d data to 17 CMIP5 pressure levels

# we use the anaconda bleeding edge python version, as it contains cftime, which allows us to circumvent the dates issues that I saw with python/3.5.2
module unload netcdf_c/4.3.2-gcc48
module load anaconda3/bleeding_edge

rm -f plev_interpolate_CAM4.py
cat << 'EOF' >> plev_interpolate_CAM4.py_${1}_${2}_${3}

import xarray as xr
import numpy as np
from scipy import interpolate
import sys

fpath=sys.argv[1]
fname=sys.argv[2]
varname=sys.argv[3]
print(fpath,fname)

ds = xr.open_dataset(fpath+'/'+fname+'.nc')

# check if variable is in data set: only in this case go ahead with the script
if (varname in ds.keys()):

    ntim = ds.time.size
    nlat = ds.lat.size
    nlon = ds.lon.size

    plev_int = ( [100000.0, 92500.0, 85000.0, 70000.0, 60000.0, 50000.0, 40000.0, 30000.0, 25000.0, 20000.0, 15000.0, 10000.0,
                  7000.0, 5000.0, 3000.0, 2000.0, 1000.0] )

    # create new dataset with vertically interpolated 3d fields
    ds_int = xr.Dataset()
    ds_int.attrs = ds.attrs

    var  = ds[varname].values
    ps   = ds['PS'].values
    
    hyam = ds['hyam'].values
    hybm = ds['hybm'].values
    p0   = ds['P0'].values

    pmid = np.zeros((ntim, 26, nlat, nlon)) + np.nan
    for t in range(ntim):
        for k in range(26):
             pmid[t,k] = hyam[k]*p0 + hybm[k]*ps[t]

    varint = np.zeros((ntim, 17, nlat, nlon)) + np.nan

    for t in range(ntim):
        if (t%100==0):
            print(fname, varname, t)
        for j in range(nlat):
            for i in range(nlon):
                f_int = interpolate.interp1d(pmid[t,:,j,i], var[t,:,j,i], bounds_error=False, kind='linear', fill_value='extrapolate')
                varint[t,:,j,i] = f_int(plev_int)

    # if variable is cloud cover or relative humidity, then restrict to physical range of 0..1 for CLOUD, and 0..100 for RELHUM
    if varname=='CLOUD':
        varint = np.maximum(np.minimum(varint,1.0),0.0)
    if varname=='RELHUM':
        varint = np.maximum(np.minimum(varint,100.0),0.0)


    # convert varint into dataarray
    da_varint = xr.DataArray(varint, name=varname, dims=('time','lev','lat','lon'), coords={'time': ds.time, 'lev': plev_int, 'lat': ds.lat, 'lon': ds.lon})

    ds_int[varname]=da_varint
    ds_int['time_bnds']=ds['time_bnds']

    ds_int.to_netcdf(path=fpath+'/'+fname+'.'+varname+'.cmiplev.nc')

EOF

python3 plev_interpolate_CAM4.py_${1}_${2}_${3} "/work/bm0162/b380459/TRACMIP/CAM4/" "${1}.$2" "${3}" 

rm -f plev_interpolate_CAM4.py_${1}_${2}_${3}
