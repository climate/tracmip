#!/bin/bash
# note the following:
# we use python xarray and scipy interpolation to remap 3d data to 17 CMIP5 pressure levels

set -e

# we use the anaconda bleeding edge python version, as it contains cftime, which allows us to circumvent the dates issues that I saw with python/3.5.2
module unload netcdf_c/4.3.2-gcc48
module load anaconda3/bleeding_edge

rm -f plev_interpolate_MPAS.py
cat << 'EOF' >> plev_interpolate_MPAS.py

import xarray as xr
import numpy as np
from scipy import interpolate
import sys

fpath=sys.argv[1]
fname=sys.argv[2]
varname=sys.argv[3]
print(fpath,fname)

ds = xr.open_dataset(fpath+'/'+fname+'.nc')

ntim = ds.time.size
nlat = ds.lat.size
nlon = ds.lon.size

plev_int = ( [100000.0, 92500.0, 85000.0, 70000.0, 60000.0, 50000.0, 40000.0, 30000.0, 25000.0, 20000.0, 15000.0, 10000.0,
              7000.0, 5000.0, 3000.0, 2000.0, 1000.0] )

# create new dataset with vertically interpolated 3d fields
ds_int = xr.Dataset()
ds_int.attrs = ds.attrs

var  = ds[varname].values
pmid = ds['PMID'].values

varint = np.zeros((ntim, 17, nlat, nlon)) + np.nan

for t in range(ntim):
    if (t%100==0):
        print(fname, varname, t)
    for j in range(nlat):
        for i in range(nlon):
            f_int = interpolate.interp1d(pmid[t,:,j,i], var[t,:,j,i], bounds_error=False, kind='linear', fill_value='extrapolate')
            varint[t,:,j,i] = f_int(plev_int)

# if variable is cloud cover or relative humidity, then restrict to physical range of 0..1 for CLOUD, and 0..100 for RELHUM
if varname=='CLOUD':
    varint = np.maximum(np.minimum(varint,1.0),0.0)
if varname=='RELHUM':
    varint = np.maximum(np.minimum(varint,100.0),0.0)


# convert varint into dataarray
da_varint = xr.DataArray(varint, name=varname, dims=('time','lev','lat','lon'), coords={'time': ds.time, 'lev': plev_int, 'lat': ds.lat, 'lon': ds.lon})

ds_int[varname]=da_varint

ds_int.to_netcdf(path=fpath+'/'+fname+'.'+varname+'.cmiplev.nc')

EOF

for VAR in CLOUD CLDICE CLDLIQ RELHUM Q T U V OMEGA Z3; do

# AquaControl
#python3 plev_interpolate_MPAS.py "/work/bm0834/b380459/TRACMIP/MPAS/AquaControl/Amon/" "Aqua_SOM_newmpas_cam4_240km.cam.h0.001601-004512" "${VAR}" 
#python3 plev_interpolate_MPAS.py "/work/bm0834/b380459/TRACMIP/MPAS/AquaControl/Aday/" "Aqua_SOM_newmpas_cam4_240km_daily.cam.h0.mergetime" "${VAR}"
#python3 plev_interpolate_MPAS.py "/work/bm0834/b380459/TRACMIP/MPAS/AquaControl/A3hr/" "Aqua_SOM_newmpas_cam4_240km_3hrly.cam.h0.mergetime" "${VAR}"

# Aqua4xCO2
#python3 plev_interpolate_MPAS.py "/work/bm0834/b380459/TRACMIP/MPAS/Aqua4xCO2/Amon/" "Aqua_SOM_newmpas_cam4_240km_4xco2.cam.h0.004601-008512" "${VAR}"
#python3 plev_interpolate_MPAS.py "/work/bm0834/b380459/TRACMIP/MPAS/Aqua4xCO2/Aday/" "Aqua_SOM_newmpas_cam4_240km_4xco2_daily.cam.h0.mergetime" "${VAR}"
#python3 plev_interpolate_MPAS.py "/work/bm0834/b380459/TRACMIP/MPAS/Aqua4xCO2/A3hr/" "Aqua_SOM_newmpas_cam4_240km_4xco2_3hrly.cam.h0.mergetime" "${VAR}"

# LandControl
python3 plev_interpolate_MPAS.py "/work/bm0834/b380459/TRACMIP/MPAS/LandControl/Amon/" "Aqua_SOM_newmpas_cam4_240km_landcontrol.cam.h0.004601-008512" "${VAR}"
python3 plev_interpolate_MPAS.py "/work/bm0834/b380459/TRACMIP/MPAS/LandControl/Aday/" "Aqua_SOM_newmpas_cam4_240km_landcontrol_daily.cam.h0.mergetime" "${VAR}"

# Land4xCO2
python3 plev_interpolate_MPAS.py "/work/bm0834/b380459/TRACMIP/MPAS/Land4xCO2/Amon/" "Aqua_SOM_newmpas_cam4_240km_land4xco2.cam.h0.008601-012512" "${VAR}"
python3 plev_interpolate_MPAS.py "/work/bm0834/b380459/TRACMIP/MPAS/Land4xCO2/Aday/" "Aqua_SOM_newmpas_cam4_240km_land4xco2_daily.cam.h0.mergetime" "${VAR}"

# LandOrbit
python3 plev_interpolate_MPAS.py "/work/bm0834/b380459/TRACMIP/MPAS/LandOrbit/Amon/" "Aqua_SOM_newmpas_cam4_240km_landorbit.cam.h0.008601-012512" "${VAR}"
python3 plev_interpolate_MPAS.py "/work/bm0834/b380459/TRACMIP/MPAS/LandOrbit/Aday/" "Aqua_SOM_newmpas_cam4_240km_landorbit_daily.cam.h0.mergetime" "${VAR}"

done

rm -f plev_interpolate_MPAS.py
