#!/bin/bash
#=============================================================================
# btc batch job parameters
#-----------------------------------------------------------------------------
#SBATCH --account=bm0834
#SBATCH --job-name=call_cdocmor_MetUM-ENT.sh
#SBATCH --partition=prepost
#SBATCH --ntasks=2
#SBATCH --cpus-per-task=8      # Specify number of CPUs per task
#SBATCH --output=/work/bm0162/b380459/TRACMIP_cmorized/call_cdocmor_MetUM-ENT.sh.%j.o
#SBATCH --error=/work/bm0162/b380459/TRACMIP_cmorized/call_cdocmor_MetUM-ENT.sh.%j.o
#SBATCH --time=08:00:00
#=============================================================================

# note the following:
# Land4xCO2 A3hr data covers year 0113-0115 instead of 0123-0125
# LandOrbit A3hr data is missing
# time axis for AquaControl A3hr is broken and repaired manually

# Bind your OpenMP threads
export OMP_NUM_THREADS=8
export KMP_AFFINITY=verbose,granularity=thread,compact,1
export KMP_STACKSIZE=64m

set -e

module load nco

CDO="cdo -P 8" 
CDOCMOR="/work/bm0021/cdo_incl_cmor/cdo_11_15_2017_cmor2/src/cdo -P 8" 

MODELUOM=MetUM-GA6-ENT   # model name at UoMiami server
MODELUOM2=MetUM-GA6-ENT  # for some models, there is a second UoMiami name needed for the actual variable file (differs from directory name by, e.g., small or capital letters)
MODELESGF=MetUM-ENT      # ESGF model name

MAPPINGTABLE=/work/bm0162/b380459/cdocmor_input/${MODELESGF}/mt_TRACMIP_${MODELESGF}.txt
echo "Using MAPPINGTABLE:" ${MAPPINGTABLE}

CALENDAR=360_day

EXPID2_AquaControl=aquaControlTRACMIP
EXPID2_Aqua4xCO2=aqua4xCO2TRACMIP
EXPID2_LandControl=landControlTRACMIP
EXPID2_Land4xCO2=land4xCO2TRACMIP
EXPID2_LandOrbit=landOrbitTRACMIP

YEARSTART_AquaControl_Amon=0016
YEARSTART_Aqua4xCO2_Amon=0046
YEARSTART_LandControl_Amon=0046
YEARSTART_Land4xCO2_Amon=0086
YEARSTART_LandOrbit_Amon=0086

YEAREND_AquaControl_Amon=0045
YEAREND_Aqua4xCO2_Amon=0085
YEAREND_LandControl_Amon=0085
YEAREND_Land4xCO2_Amon=0125
YEAREND_LandOrbit_Amon=0125

YEARSTART_AquaControl_Aday=0036
YEARSTART_Aqua4xCO2_Aday=0076
YEARSTART_LandControl_Aday=0076
YEARSTART_Land4xCO2_Aday=0116
YEARSTART_LandOrbit_Aday=0116

YEAREND_AquaControl_Aday=0045
YEAREND_Aqua4xCO2_Aday=0085
YEAREND_LandControl_Aday=0085
YEAREND_Land4xCO2_Aday=0125
YEAREND_LandOrbit_Aday=0125

YEARSTART_AquaControl_A3hr=0043
YEARSTART_Aqua4xCO2_A3hr=0083
YEARSTART_LandControl_A3hr=0083
YEARSTART_Land4xCO2_A3hr=0113
YEARSTART_LandOrbit_A3hr=0123

YEAREND_AquaControl_A3hr=0045
YEAREND_Aqua4xCO2_A3hr=0085
YEAREND_LandControl_A3hr=0085
YEAREND_Land4xCO2_A3hr=0115
YEAREND_LandOrbit_A3hr=0125

VARLIST2D="clivi clt clwvi evspsbl hfls hfss pr prc prsn prw ps psl rlds rldscs rlus rlut rlutcs rsds rsdscs rsdt rsus rsuscs rsut rsutcs sfcWind tas tauu tauv ts uas vas"
VARLIST3D="cl cli clw hur hus ta ua va wap zg"

# create temporary directory for this model for tempfile.nc and griddes.txt
rm -rf ${MODELESGF}_tmpdir
mkdir ${MODELESGF}_tmpdir

rm -f ${MODELESGF}_tmpdir/griddes.txt
${CDO} -s griddes /work/bm0162/b380459/TRACMIP/MetUM-GA6-CTL/AquaControl/Amon/hurs_Amon_MetUM-GA6-CTL_AquaControl_r1i1p1_001601-004512.nc > ${MODELESGF}_tmpdir/griddes.txt

#for EXPID1 in AquaControl Aqua4xCO2 LandControl Land4xCO2 LandOrbit; do
for EXPID1 in LandOrbit; do
   
   STREAM=Amon  
   EXPID2=EXPID2_${EXPID1}
   MIPTABLE=/work/bm0162/b380459/cdocmor_input/TRACMIP_${STREAM}
   CDOCMORINFO=/work/bm0162/b380459/cdocmor_input/${MODELESGF}/cdocmorinfo_TRACMIP_${MODELESGF}_${!EXPID2}
   # in some cases we need to repair time axis
   SETTAXIS=" "
   if [ ${EXPID1} = "Aqua4xCO2" ]; then
      SETTAXIS="-settaxis,0046-01-16,0:00,30d"
   elif [ ${EXPID1} = "LandControl" ]; then
      SETTAXIS="-settaxis,0046-01-16,0:00,30d"
   elif [ ${EXPID1} = "Land4xCO2" ]; then
      SETTAXIS="-settaxis,0086-01-16,0:00,30d"
   elif [ ${EXPID1} = "LandOrbit" ]; then
      SETTAXIS="-settaxis,0086-01-16,0:00,30d"
   fi
   for var in ${VARLIST2D}; do
      echo "Currently working on:" ${MODELESGF} ${EXPID1} ${STREAM} ${var}
      rm -f ${MODELESGF}_tmpdir/tempfile.nc
      YEARSTART=YEARSTART_${EXPID1}_${STREAM}
      YEAREND=YEAREND_${EXPID1}_${STREAM}
      INFILE=/work/bm0162/b380459/TRACMIP/${MODELUOM}/${EXPID1}/${STREAM}/${var}_${STREAM}_${MODELUOM2}_${EXPID1}_r1i1p1_${!YEARSTART}01-${!YEAREND}12.nc
      if [ -f ${INFILE} ]; then
         ${CDO} -s ${SETTAXIS} -remapcon,${MODELESGF}_tmpdir/griddes.txt -setcalendar,${CALENDAR} ${INFILE} ${MODELESGF}_tmpdir/tempfile.nc
         ${CDOCMOR} -s cmor,${MIPTABLE},info=${CDOCMORINFO},mapping_table=${MAPPINGTABLE} ${MODELESGF}_tmpdir/tempfile.nc
      fi
      rm -f ${MODELESGF}_tmpdir/tempfile.nc
   done
   for var in ${VARLIST3D}; do
      echo "Currently working on:" ${MODELESGF} ${EXPID1} ${STREAM} ${var}
      rm -f ${MODELESGF}_tmpdir/tempfile.nc
      YEARSTART=YEARSTART_${EXPID1}_${STREAM}
      YEAREND=YEAREND_${EXPID1}_${STREAM}
      INFILE=/work/bm0162/b380459/TRACMIP/${MODELUOM}/${EXPID1}/${STREAM}/${var}_${STREAM}_${MODELUOM2}_${EXPID1}_r1i1p1_${!YEARSTART}01-${!YEAREND}12.nc
      if [ -f ${INFILE} ]; then
         ${CDO} -s ${SETTAXIS} -remapcon,${MODELESGF}_tmpdir/griddes.txt -setcalendar,${CALENDAR} ${INFILE} ${MODELESGF}_tmpdir/tempfile.nc
         ncap2 -O -s 'plev*=100'  ${MODELESGF}_tmpdir/tempfile.nc ${MODELESGF}_tmpdir/tempfile.nc  
         ncatted -O -a units,plev,m,c,"Pa" ${MODELESGF}_tmpdir/tempfile.nc
         ${CDOCMOR} -s cmor,${MIPTABLE},info=${CDOCMORINFO},mapping_table=${MAPPINGTABLE} ${MODELESGF}_tmpdir/tempfile.nc
      fi
      rm -f ${MODELESGF}_tmpdir/tempfile.nc
   done

   STREAM=Aday  
   EXPID2=EXPID2_${EXPID1}
   MIPTABLE=/work/bm0162/b380459/cdocmor_input/TRACMIP_${STREAM}
   CDOCMORINFO=/work/bm0162/b380459/cdocmor_input/${MODELESGF}/cdocmorinfo_TRACMIP_${MODELESGF}_${!EXPID2}
   # in some cases we need to repair time axis
   SETTAXIS=" "
   if [ ${EXPID1} = "LandOrbit" ]; then
      SETTAXIS="-settaxis,0116-01-01,12:00,1d"
   fi
   for var in ${VARLIST2D}; do
      echo "Currently working on:" ${MODELESGF} ${EXPID1} ${STREAM} ${var}
      rm -f ${MODELESGF}_tmpdir/tempfile.nc
      YEARSTART=YEARSTART_${EXPID1}_${STREAM}
      YEAREND=YEAREND_${EXPID1}_${STREAM}
      INFILE=/work/bm0162/b380459/TRACMIP/${MODELUOM}/${EXPID1}/${STREAM}/${var}_${STREAM}_${MODELUOM2}_${EXPID1}_r1i1p1_${!YEARSTART}01-${!YEAREND}12.nc
      if [ -f ${INFILE} ]; then
         ${CDO} -s ${SETTAXIS} -remapcon,${MODELESGF}_tmpdir/griddes.txt -setcalendar,${CALENDAR} ${INFILE} ${MODELESGF}_tmpdir/tempfile.nc
         ${CDOCMOR} -s cmor,${MIPTABLE},info=${CDOCMORINFO},mapping_table=${MAPPINGTABLE} ${MODELESGF}_tmpdir/tempfile.nc
      fi
      rm -f ${MODELESGF}_tmpdir/tempfile.nc
   done
   for var in ${VARLIST3D}; do
      echo "Currently working on:" ${MODELESGF} ${EXPID1} ${STREAM} ${var}
      rm -f ${MODELESGF}_tmpdir/tempfile.nc
      YEARSTART=YEARSTART_${EXPID1}_${STREAM}
      YEAREND=YEAREND_${EXPID1}_${STREAM}
      INFILE=/work/bm0162/b380459/TRACMIP/${MODELUOM}/${EXPID1}/${STREAM}/${var}_${STREAM}_${MODELUOM2}_${EXPID1}_r1i1p1_${!YEARSTART}01-${!YEAREND}12.nc
      if [ -f ${INFILE} ]; then
         ${CDO} -s ${SETTAXIS} -remapcon,${MODELESGF}_tmpdir/griddes.txt -setcalendar,${CALENDAR} ${INFILE} ${MODELESGF}_tmpdir/tempfile.nc
         ncap2 -O -s 'plev*=100' ${MODELESGF}_tmpdir/tempfile.nc ${MODELESGF}_tmpdir/tempfile.nc  
         ncatted -O -a units,plev,m,c,"Pa" ${MODELESGF}_tmpdir/tempfile.nc
         ${CDOCMOR} -s cmor,${MIPTABLE},info=${CDOCMORINFO},mapping_table=${MAPPINGTABLE} ${MODELESGF}_tmpdir/tempfile.nc
      fi
      rm -f ${MODELESGF}_tmpdir/tempfile.nc
   done

   STREAM=A3hr  
   EXPID2=EXPID2_${EXPID1}
   # we need to catch that A3hr files have 4XCO2 instead of 4xCO2 in their file names
   EXPID3=${EXPID1}
   if [ ${EXPID1} = "Aqua4xCO2" ]; then
      EXPID3=Aqua4XCO2
   elif [ ${EXPID1} = "Land4xCO2" ];  then
      EXPID3=Land4XCO2
   fi
   MIPTABLE=/work/bm0162/b380459/cdocmor_input/TRACMIP_${STREAM}
   CDOCMORINFO=/work/bm0162/b380459/cdocmor_input/${MODELESGF}/cdocmorinfo_TRACMIP_${MODELESGF}_${!EXPID2}
   # in some cases we need to repair time axis
   SETTAXIS=" "
   if [ ${EXPID1} = "AquaControl" ]; then
      SETTAXIS="-settaxis,0043-01-01,1:30,3h"
   elif [ ${EXPID1} = "Aqua4xCO2" ]; then
      SETTAXIS="-settaxis,0082-01-01,1:30,3h"
   fi
   for var in ${VARLIST2D}; do
      echo "Currently working on:" ${MODELESGF} ${EXPID1} ${STREAM} ${var}
      rm -f ${MODELESGF}_tmpdir/tempfile.nc
      YEARSTART=YEARSTART_${EXPID1}_${STREAM}
      YEAREND=YEAREND_${EXPID1}_${STREAM}
      INFILE=/work/bm0162/b380459/TRACMIP/${MODELUOM}/${EXPID1}/${STREAM}/${var}_${STREAM}_${MODELUOM2}_${EXPID3}_r1i1p1_${!YEARSTART}01-${!YEAREND}12.nc
      if [ -f ${INFILE} ]; then
         ${CDO} -s ${SETTAXIS} -remapcon,${MODELESGF}_tmpdir/griddes.txt -setcalendar,${CALENDAR} ${INFILE} ${MODELESGF}_tmpdir/tempfile.nc
         ${CDOCMOR} -s cmor,${MIPTABLE},info=${CDOCMORINFO},mapping_table=${MAPPINGTABLE} ${MODELESGF}_tmpdir/tempfile.nc
      fi
      rm -f ${MODELESGF}_tmpdir/tempfile.nc
   done
   for var in ${VARLIST3D}; do
      echo "Currently working on:" ${MODELESGF} ${EXPID1} ${STREAM} ${var}
      rm -f ${MODELESGF}_tmpdir/tempfile.nc
      YEARSTART=YEARSTART_${EXPID1}_${STREAM}
      YEAREND=YEAREND_${EXPID1}_${STREAM}
      INFILE=/work/bm0162/b380459/TRACMIP/${MODELUOM}/${EXPID1}/${STREAM}/${var}_${STREAM}_${MODELUOM2}_${EXPID3}_r1i1p1_${!YEARSTART}01-${!YEAREND}12.nc
      if [ -f ${INFILE} ]; then
         ${CDO} -s ${SETTAXIS} -remapcon,${MODELESGF}_tmpdir/griddes.txt -setcalendar,${CALENDAR} ${INFILE} ${MODELESGF}_tmpdir/tempfile.nc
         ncap2 -O -s 'plev*=100' ${MODELESGF}_tmpdir/tempfile.nc ${MODELESGF}_tmpdir/tempfile.nc  
         ncatted -O -a units,plev,m,c,"Pa" ${MODELESGF}_tmpdir/tempfile.nc
         ${CDOCMOR} -s cmor,${MIPTABLE},info=${CDOCMORINFO},mapping_table=${MAPPINGTABLE} ${MODELESGF}_tmpdir/tempfile.nc
      fi
      rm -f ${MODELESGF}_tmpdir/tempfile.nc
   done

done

# remove temporary directory
rm -rf ${MODELESGF}_tmpdir 
