The start date and the end date.

AquaControl:  0001-01-01 --- 0045-12-30  (45 years)
AquaAbs0.7:   0046-01-01 --- 0085-12-30  (40 years)
AquaAbs1.5:   0046-01-01 --- 0085-12-30  (40 years)
AquaAbs2.0:   0046-01-01 --- 0085-12-30  (40 years)

LandControl:  0046-01-01 --- 0085-12-30  (40 years)
LandAbs0.7:   0086-01-01 --- 0125-12-30  (40 years)
LandAbs1.5:   0086-01-01 --- 0125-12-30  (40 years)
LandAbs2.0:   0086-01-01 --- 0125-12-30  (40 years)

LandOrbit:   0086-01-01 --- 0125-12-30  (40 years)



for each simulation, which output stream is available for which time period:

AquaControl Amon 0016-01-01 – 0045-12-30 (30 years)
AquaControl Aday 0016-01-01 – 0045-12-30 (30 years)

AquaAbs0.7 Amon 0046-01-01 – 0085-12-30 (40 years)
AquaAbs0.7 Aday 0046-01-01 – 0085-12-30 (40 years)

AquaAbs1.5 Amon 0046-01-01 – 0085-12-30 (40 years)
AquaAbs1.5 Aday 0046-01-01 – 0085-12-30 (40 years)

AquaAbs2.0 Amon 0046-01-01 – 0085-12-30 (40 years)
AquaAbs2.0 Aday 0046-01-01 – 0085-12-30 (40 years)

LandControl Amon 0046-01-01 – 0085-12-30 (40 years)
LandControl Aday 0076-01-01 – 0085-12-30 (10 years)

LandAbs0.7 Amon 0086-01-01 – 0125-12-30 (40 years)
LandAbs0.7 Aday 0116-01-01 – 0125-12-30 (10 years)

LandAbs1.5 Amon 0086-01-01 – 0125-12-30 (40 years)
LandAbs1.5 Aday 0116-01-01 – 0125-12-30 (10 years)

LandAbs2.0 Amon 0086-01-01 – 0125-12-30 (40 years)
LandAbs2.0 Aday 0116-01-01 – 0125-12-30 (10 years)

LandOrbit Amon 0086-01-01 – 0125-12-30 (40 years)
LandOrbit Aday 0116-01-01 – 0125-12-30 (10 years)
