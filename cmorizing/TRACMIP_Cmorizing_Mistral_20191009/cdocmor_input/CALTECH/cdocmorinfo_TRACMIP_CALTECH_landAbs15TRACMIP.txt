# experiment info:
#
# none of these can be set in the command line
#
EXPERIMENT_ID="landAbs15TRACMIP"
MEMBER=r1i1p1
PROJECT_ID=TRACMIP
PRODUCT=output
COMMENT="idealized land of TRACMIP with longwave optical thickness scaled by 1.5 compared to landControlTRACMIP; for TRACMIP see Voigt et al., 2016, The Tropical Rain belts with an Annual cycle and a Continent Model Intercomparison Project: TRACMIP, 9, 1868–1891, doi:10.1002/2016MS000748"
HISTORY="N/A"
PARENT_EXPERIMENT="landControlTRACMIP"
PARENT_EXPERIMENT_RIP="r1i1p1"
PARENT_EXPERIMENT_ID="landControlTRACMIP"
FORCING="CO2"
BRANCH_TIME="0086-01-01"
REQUIRED_TIME_UNITS="days since -0001-12-30 00:00:00"

#model info:
#
# only variable MAPPING_TABLE and model GRID_FILE can be set in the command line
#
MODEL_ID=CALTECH
REFERENCES="CALTECH: O’Gorman and Schneider, 2008, The hydrological cycle over a wide range of climates simulated with an idealized GCM, J. Clim., 21, 3815–3832, doi:10.1175/2007JCLI2065.1; Bordoni and Schneider, 2008, Monsoons as eddy-mediated regime transitions of the tropical overturning circulation, Nat. Geosci., 1, 515–519, doi:10.1038/ngeo248"
SOURCE="CALTECH, T42, 30 levels;"
CALENDAR=360_day
# institution and contact info:
#
# none of these can be set in the command line
#
INSTITUTE_ID="CALTECH"
INSTITUTION="California Institute of Technology"
CONTACT="bordoni@gps.caltech.edu; hwei@caltech.edu"
