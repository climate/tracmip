! This table may contain information that has to be provided with the data files in order to use the CDO cmor operator.
! E.g. if your data are GRIB formatted the information is needed which code number corresponds to the short name 
!      required to be given to a requested variable. The keyword has to be 'code'.
! The table format:
!  Each line has to start with &!parameter '
!  Each line has to end   with ' /' (Note the blank!)
!  The keyword-assignments may appear at any place
!  There must be at least 1 space between assignments
!  Any keyword-value may be written with double-quotes
!  If a keyword-values contains spaces, it must be delimited by double-quotes
! Keywords interpreted by cdo cmor,... are :
!
!Entry  	Type 	Description
!----------------------------------------------------------------------------------------		
!name 	   	WORD 	Name of the variable		
!cmor_name/cn 	WORD 	CMIP/CMOR variable name (CV)
!               Note:   This is the name that has to be passed to the CMOR functions.
!                       It is not necessarily the variable name in the NetCDF output file
!                       nor the 1st DRS element of the file name. Short form: 'cn'
!param 		WORD 	Parameter identifier (GRIB1: code[.tabnum]; GRIB2: num[.cat[.dis]])		
!units 		STRING 	units of the variable
!comment 	STRING 	Information concerning the variable
!cell_methods 	STRING 	Information concerning the time:cell_methods
!		
! Other keywords may be interpreted by other cdo-operators or tools
! 
! CAM3 mapping table for TRACMIP for
!
!! Amon
&parameter name=CLOUD       cmor_name=cl              units="1"            cell_methods="m" project_mip_table=Amon / 
&parameter name=CLDICE      cmor_name=cli             units="kg/kg"        cell_methods="m" project_mip_table=Amon /
&parameter name=TGCLDIWP    cmor_name=clivi           units="g m-2"        cell_methods="m" project_mip_table=Amon /
&parameter name=CLDTOT      cmor_name=clt             units="1"            cell_methods="m" project_mip_table=Amon /
&parameter name=CLDLIQ      cmor_name=clw             units="kg/kg"        cell_methods="m" project_mip_table=Amon /
&parameter name=TGCLDLWP    cmor_name=clwvi           units="g m-2"        cell_methods="m" project_mip_table=Amon /
&parameter name=SFQ         cmor_name=evspsbl         units="kg m-2 s-1"   cell_methods="m" project_mip_table=Amon /
&parameter name=LHFLX       cmor_name=hfls     p="u"  units="W m-2"        cell_methods="m" project_mip_table=Amon /
&parameter name=SHFLX       cmor_name=hfss     p="u"  units="W m-2"        cell_methods="m" project_mip_table=Amon /
&parameter name=RELHUM      cmor_name=hur             units="%"            cell_methods="m" project_mip_table=Amon /
&parameter name=Q           cmor_name=hus             units="kg/kg"        cell_methods="m" project_mip_table=Amon /
!!&parameter name=PR          cmor_name=pr              units="m s-1"        cell_methods="m" project_mip_table=Amon /
!!&parameter name=PRECC       cmor_name=prc             units="m s-1"        cell_methods="m" project_mip_table=Amon /
!!&parameter name=PRSN        cmor_name=prsn            units="m s-1"        cell_methods="m" project_mip_table=Amon /
&parameter name=PR          cmor_name=pr              units="kg m-2 s-1"   cell_methods="m" project_mip_table=Amon /
&parameter name=PRECC       cmor_name=prc             units="kg m-2 s-1"   cell_methods="m" project_mip_table=Amon /
&parameter name=PRSN        cmor_name=prsn            units="kg m-2 s-1"   cell_methods="m" project_mip_table=Amon /
&parameter name=TMQ         cmor_name=prw             units="kg m-2"       cell_methods="m" project_mip_table=Amon /
&parameter name=PS          cmor_name=ps              units="Pa"           cell_methods="m" project_mip_table=Amon /
&parameter name=PSL         cmor_name=psl             units="Pa"           cell_methods="m" project_mip_table=Amon /
&parameter name=FLDS        cmor_name=rlds     p="d"  units="W m-2"        cell_methods="m" project_mip_table=Amon /
&parameter name=FLDSC       cmor_name=rldscs   p="d"  units="W m-2"        cell_methods="m" project_mip_table=Amon /
&parameter name=RLUS        cmor_name=rlus     p="u"  units="W m-2"        cell_methods="m" project_mip_table=Amon /
&parameter name=FLUT        cmor_name=rlut     p="u"  units="W m-2"        cell_methods="m" project_mip_table=Amon /
&parameter name=FLUTC       cmor_name=rlutcs   p="u"  units="W m-2"        cell_methods="m" project_mip_table=Amon /
&parameter name=FSDS        cmor_name=rsds     p="d"  units="W m-2"        cell_methods="m" project_mip_table=Amon /
&parameter name=FSDSC       cmor_name=rsdscs   p="d"  units="W m-2"        cell_methods="m" project_mip_table=Amon /
&parameter name=SOLIN       cmor_name=rsdt     p="d"  units="W m-2"        cell_methods="m" project_mip_table=Amon /
&parameter name=RSUS        cmor_name=rsus     p="u"  units="W m-2"        cell_methods="m" project_mip_table=Amon /
&parameter name=RSUSCS      cmor_name=rsuscs   p="u"  units="W m-2"        cell_methods="m" project_mip_table=Amon /
&parameter name=RSUT        cmor_name=rsut     p="u"  units="W m-2"        cell_methods="m" project_mip_table=Amon /
&parameter name=RSUTCS      cmor_name=rsutcs   p="u"  units="W m-2"        cell_methods="m" project_mip_table=Amon /
!! &parameter name=sfcWind  cmor_name=sfcWind         units="m/s"          cell_methods="m" project_mip_table=Amon /
&parameter name=T           cmor_name=ta              units="K"            cell_methods="m" project_mip_table=Amon /
&parameter name=TREFHT      cmor_name=tas             units="K"            cell_methods="m" project_mip_table=Amon /
&parameter name=TSMX        cmor_name=tasmax          units="K"            cell_methods="m" project_mip_table=Amon /
&parameter name=TSMN        cmor_name=tasmin          units="K"            cell_methods="m" project_mip_table=Amon /
&parameter name=TAUX        cmor_name=tauu     p="d"  units="Pa"           cell_methods="m" project_mip_table=Amon /
&parameter name=TAUY        cmor_name=tauv     p="d"  units="Pa"           cell_methods="m" project_mip_table=Amon /
&parameter name=TS          cmor_name=ts              units="K"            cell_methods="m" project_mip_table=Amon /
&parameter name=U           cmor_name=ua              units="m/s"          cell_methods="m" project_mip_table=Amon /
!! &parameter name=uas      cmor_name=uas             units="m/s"          cell_methods="m" project_mip_table=Amon /
&parameter name=V           cmor_name=va              units="m/s"          cell_methods="m" project_mip_table=Amon /
!! &parameter name=vas      cmor_name=vas             units="m/s"          cell_methods="m" project_mip_table=Amon /
&parameter name=OMEGA       cmor_name=wap             units="Pa/s"         cell_methods="m" project_mip_table=Amon /
&parameter name=Z3          cmor_name=zg              units="m"            cell_methods="m" project_mip_table=Amon /
!! day
&parameter name=CLOUD       cmor_name=cl              units="1"            cell_methods="m" project_mip_table=Aday / 
&parameter name=CLDICE      cmor_name=cli             units="kg/kg"        cell_methods="m" project_mip_table=Aday /
&parameter name=TGCLDIWP    cmor_name=clivi           units="g m-2"        cell_methods="m" project_mip_table=Aday /
&parameter name=CLDTOT      cmor_name=clt             units="1"            cell_methods="m" project_mip_table=Aday /
&parameter name=CLDLIQ      cmor_name=clw             units="kg/kg"        cell_methods="m" project_mip_table=Aday /
&parameter name=TGCLDLWP    cmor_name=clwvi           units="g m-2"        cell_methods="m" project_mip_table=Aday /
&parameter name=SFQ         cmor_name=evspsbl         units="kg m-2 s-1"   cell_methods="m" project_mip_table=Aday /
&parameter name=LHFLX       cmor_name=hfls     p="u"  units="W m-2"        cell_methods="m" project_mip_table=Aday /
&parameter name=SHFLX       cmor_name=hfss     p="u"  units="W m-2"        cell_methods="m" project_mip_table=Aday /
&parameter name=RELHUM      cmor_name=hur             units="%"            cell_methods="m" project_mip_table=Aday /
&parameter name=Q           cmor_name=hus             units="kg/kg"        cell_methods="m" project_mip_table=Aday /
!!&parameter name=PR          cmor_name=pr              units="m s-1"        cell_methods="m" project_mip_table=Aday /
!!&parameter name=PRECC       cmor_name=prc             units="m s-1"        cell_methods="m" project_mip_table=Aday /
!!&parameter name=PRSN        cmor_name=prsn            units="m s-1"        cell_methods="m" project_mip_table=Aday /
&parameter name=PR          cmor_name=pr              units="kg m-2 s-1"   cell_methods="m" project_mip_table=Aday /
&parameter name=PRECC       cmor_name=prc             units="kg m-2 s-1"   cell_methods="m" project_mip_table=Aday /
&parameter name=PRSN        cmor_name=prsn            units="kg m-2 s-1"   cell_methods="m" project_mip_table=Aday /
&parameter name=TMQ         cmor_name=prw             units="kg m-2"       cell_methods="m" project_mip_table=Aday /
&parameter name=PS          cmor_name=ps              units="Pa"           cell_methods="m" project_mip_table=Aday /
&parameter name=PSL         cmor_name=psl             units="Pa"           cell_methods="m" project_mip_table=Aday /
&parameter name=FLDS        cmor_name=rlds     p="d"  units="W m-2"        cell_methods="m" project_mip_table=Aday /
&parameter name=FLDSC       cmor_name=rldscs   p="d"  units="W m-2"        cell_methods="m" project_mip_table=Aday /
&parameter name=RLSU        cmor_name=rlus     p="u"  units="W m-2"        cell_methods="m" project_mip_table=Aday /
&parameter name=FLUT        cmor_name=rlut     p="u"  units="W m-2"        cell_methods="m" project_mip_table=Aday /
&parameter name=FLUTC       cmor_name=rlutcs   p="u"  units="W m-2"        cell_methods="m" project_mip_table=Aday /
&parameter name=FSDS        cmor_name=rsds     p="d"  units="W m-2"        cell_methods="m" project_mip_table=Aday /
&parameter name=FSDSC       cmor_name=rsdscs   p="d"  units="W m-2"        cell_methods="m" project_mip_table=Aday /
&parameter name=SOLIN       cmor_name=rsdt     p="d"  units="W m-2"        cell_methods="m" project_mip_table=Aday /
&parameter name=RSUS        cmor_name=rsus     p="u"  units="W m-2"        cell_methods="m" project_mip_table=Aday /
&parameter name=RSUSCS      cmor_name=rsuscs   p="u"  units="W m-2"        cell_methods="m" project_mip_table=Aday /
&parameter name=RSUT        cmor_name=rsut     p="u"  units="W m-2"        cell_methods="m" project_mip_table=Aday /
&parameter name=RSUTCS      cmor_name=rsutcs   p="u"  units="W m-2"        cell_methods="m" project_mip_table=Aday /
!! &parameter name=sfcWind  cmor_name=sfcWind         units="m/s"          cell_methods="m" project_mip_table=Aday /
&parameter name=T           cmor_name=ta              units="K"            cell_methods="m" project_mip_table=Aday /
&parameter name=TREFHT      cmor_name=tas             units="K"            cell_methods="m" project_mip_table=Aday /
&parameter name=TSMX        cmor_name=tasmax          units="K"            cell_methods="m" project_mip_table=Aday /
&parameter name=TSMN        cmor_name=tasmin          units="K"            cell_methods="m" project_mip_table=Aday /
&parameter name=TAUX        cmor_name=tauu     p="d"  units="Pa"           cell_methods="m" project_mip_table=Aday /
&parameter name=TAUY        cmor_name=tauv     p="d"  units="Pa"           cell_methods="m" project_mip_table=Aday /
&parameter name=TS          cmor_name=ts              units="K"            cell_methods="m" project_mip_table=Aday /
&parameter name=U           cmor_name=ua              units="m/s"          cell_methods="m" project_mip_table=Aday /
!! &parameter name=uas      cmor_name=uas             units="m/s"          cell_methods="m" project_mip_table=Aday /
&parameter name=V           cmor_name=va              units="m/s"          cell_methods="m" project_mip_table=Aday /
!! &parameter name=vas      cmor_name=vas             units="m/s"          cell_methods="m" project_mip_table=Aday /
&parameter name=OMEGA       cmor_name=wap             units="Pa/s"         cell_methods="m" project_mip_table=Aday /
&parameter name=Z3          cmor_name=zg              units="m"            cell_methods="m" project_mip_table=Aday /
!! 3hr
&parameter name=CLOUD       cmor_name=cl              units="1"            cell_methods="m" project_mip_table=A3hr / 
&parameter name=CLDICE      cmor_name=cli             units="kg/kg"        cell_methods="m" project_mip_table=A3hr /
&parameter name=TGCLDIWP    cmor_name=clivi           units="g m-2"        cell_methods="m" project_mip_table=A3hr /
&parameter name=CLDTOT      cmor_name=clt             units="1"            cell_methods="m" project_mip_table=A3hr /
&parameter name=CLDLIQ      cmor_name=clw             units="kg/kg"        cell_methods="m" project_mip_table=A3hr /
&parameter name=TGCLDLWP    cmor_name=clwvi           units="g m-2"        cell_methods="m" project_mip_table=A3hr /
&parameter name=SFQ         cmor_name=evspsbl         units="kg m-2 s-1"   cell_methods="m" project_mip_table=A3hr /
&parameter name=LHFLX       cmor_name=hfls     p="u"  units="W m-2"        cell_methods="m" project_mip_table=A3hr /
&parameter name=SHFLX       cmor_name=hfss     p="u"  units="W m-2"        cell_methods="m" project_mip_table=A3hr /
&parameter name=RELHUM      cmor_name=hur             units="%"            cell_methods="p" project_mip_table=A3hr /
&parameter name=Q           cmor_name=hus             units="kg/kg"        cell_methods="p" project_mip_table=A3hr /
!!&parameter name=PR          cmor_name=pr              units="m s-1"        cell_methods="m" project_mip_table=A3hr /
!!&parameter name=PRECC       cmor_name=prc             units="m s-1"        cell_methods="m" project_mip_table=A3hr /
!!&parameter name=PRSN        cmor_name=prsn            units="m s-1"        cell_methods="m" project_mip_table=A3hr /
&parameter name=PR          cmor_name=pr              units="kg m-2 s-1"   cell_methods="m" project_mip_table=A3hr /
&parameter name=PRECC       cmor_name=prc             units="kg m-2 s-1"   cell_methods="m" project_mip_table=A3hr /
&parameter name=PRSN        cmor_name=prsn            units="kg m-2 s-1"   cell_methods="m" project_mip_table=A3hr /
&parameter name=TMQ         cmor_name=prw             units="kg m-2"       cell_methods="m" project_mip_table=A3hr /
&parameter name=PS          cmor_name=ps              units="Pa"           cell_methods="m" project_mip_table=A3hr /
&parameter name=PSL         cmor_name=psl             units="Pa"           cell_methods="m" project_mip_table=A3hr /
&parameter name=FLDS        cmor_name=rlds     p="d"  units="W m-2"        cell_methods="m" project_mip_table=A3hr /
&parameter name=FLDSC       cmor_name=rldscs   p="d"  units="W m-2"        cell_methods="m" project_mip_table=A3hr /
&parameter name=RLUS        cmor_name=rlus     p="u"  units="W m-2"        cell_methods="m" project_mip_table=A3hr /
&parameter name=FLUT        cmor_name=rlut     p="u"  units="W m-2"        cell_methods="m" project_mip_table=A3hr /
&parameter name=FLUTC       cmor_name=rlutcs   p="u"  units="W m-2"        cell_methods="m" project_mip_table=A3hr /
&parameter name=FSDS        cmor_name=rsds     p="d"  units="W m-2"        cell_methods="m" project_mip_table=A3hr /
&parameter name=FSDSC       cmor_name=rsdscs   p="d"  units="W m-2"        cell_methods="m" project_mip_table=A3hr /
&parameter name=SOLIN       cmor_name=rsdt     p="d"  units="W m-2"        cell_methods="m" project_mip_table=A3hr /
&parameter name=RSUS        cmor_name=rsus     p="u"  units="W m-2"        cell_methods="m" project_mip_table=A3hr /
&parameter name=RSUSCS      cmor_name=rsuscs   p="u"  units="W m-2"        cell_methods="m" project_mip_table=A3hr /
&parameter name=RSUT        cmor_name=rsut     p="u"  units="W m-2"        cell_methods="m" project_mip_table=A3hr /
&parameter name=RSUTCS      cmor_name=rsutcs   p="u"  units="W m-2"        cell_methods="m" project_mip_table=A3hr /
!! &parameter name=sfcWind  cmor_name=sfcWind         units="m/s"          cell_methods="m" project_mip_table=A3hr /
&parameter name=T           cmor_name=ta              units="K"            cell_methods="p" project_mip_table=A3hr /
&parameter name=TREFHT      cmor_name=tas             units="K"            cell_methods="p" project_mip_table=A3hr /
&parameter name=TSMX        cmor_name=tasmax          units="K"            cell_methods="m" project_mip_table=A3hr /
&parameter name=TSMN        cmor_name=tasmin          units="K"            cell_methods="m" project_mip_table=A3hr /
&parameter name=TAUX        cmor_name=tauu     p="d"  units="Pa"           cell_methods="m" project_mip_table=A3hr /
&parameter name=TAUY        cmor_name=tauv     p="d"  units="Pa"           cell_methods="m" project_mip_table=A3hr /
&parameter name=TS          cmor_name=ts              units="K"            cell_methods="p" project_mip_table=A3hr /
&parameter name=U           cmor_name=ua              units="m/s"          cell_methods="p" project_mip_table=A3hr /
!! &parameter name=uas      cmor_name=uas             units="m/s"          cell_methods="p" project_mip_table=A3hr /
&parameter name=V           cmor_name=va              units="m/s"          cell_methods="p" project_mip_table=A3hr /
!! &parameter name=vas      cmor_name=vas             units="m/s"          cell_methods="p" project_mip_table=A3hr /
&parameter name=OMEGA       cmor_name=wap             units="Pa/s"         cell_methods="p" project_mip_table=A3hr /
&parameter name=Z3          cmor_name=zg              units="m"            cell_methods="p" project_mip_table=A3hr /
