! This table may contain information that has to be provided with the data files in order to use the CDO cmor operator.
! E.g. if your data are GRIB formatted the information is needed which code number corresponds to the short name 
!      required to be given to a requested variable. The keyword has to be 'code'.
! The table format:
!  Each line has to start with &!parameter '
!  Each line has to end   with ' /' (Note the blank!)
!  The keyword-assignments may appear at any place
!  There must be at least 1 space between assignments
!  Any keyword-value may be written with double-quotes
!  If a keyword-values contains spaces, it must be delimited by double-quotes
! Keywords interpreted by cdo cmor,... are :
!
!Entry  	Type 	Description
!----------------------------------------------------------------------------------------		
!name 	   	WORD 	Name of the variable		
!cmor_name/cn 	WORD 	CMIP/CMOR variable name (CV)
!               Note:   This is the name that has to be passed to the CMOR functions.
!                       It is not necessarily the variable name in the NetCDF output file
!                       nor the 1st DRS element of the file name. Short form: 'cn'
!param 		WORD 	Parameter identifier (GRIB1: code[.tabnum]; GRIB2: num[.cat[.dis]])		
!units 		STRING 	units of the variable
!comment 	STRING 	Information concerning the variable
!cell_methods 	STRING 	Information concerning the time:cell_methods
!		
! Other keywords may be interpreted by other cdo-operators or tools
! 
! ECHAM61 mapping table for TRACMIP for
!
!! Amon
&parameter name=cl       cmor_name=cl              units="%"            cell_methods="m" project_mip_table=Amon / 
&parameter name=cli      cmor_name=cli             units="kg/kg"        cell_methods="m" project_mip_table=Amon /
&parameter name=clivi    cmor_name=clivi           units="kg m-2"       cell_methods="m" project_mip_table=Amon /
&parameter name=clt      cmor_name=clt             units="%"            cell_methods="m" project_mip_table=Amon /
&parameter name=clw      cmor_name=clw             units="kg/kg"        cell_methods="m" project_mip_table=Amon /
&parameter name=clwvi    cmor_name=clwvi           units="kg m-2"       cell_methods="m" project_mip_table=Amon /
&parameter name=evspsbl  cmor_name=evspsbl         units="kg m-2 s-1"   cell_methods="m" project_mip_table=Amon /
&parameter name=hfls     cmor_name=hfls     p="u"  units="W m-2"        cell_methods="m" project_mip_table=Amon /
&parameter name=hfss     cmor_name=hfss     p="u"  units="W m-2"        cell_methods="m" project_mip_table=Amon /
&parameter name=hur      cmor_name=hur             units="%"            cell_methods="m" project_mip_table=Amon /
&parameter name=hus      cmor_name=hus             units="kg/kg"        cell_methods="m" project_mip_table=Amon /
&parameter name=pr       cmor_name=pr              units="kg m-2 s-1"   cell_methods="m" project_mip_table=Amon /
&parameter name=prc      cmor_name=prc             units="kg m-2 s-1"   cell_methods="m" project_mip_table=Amon /
&parameter name=prsn     cmor_name=prsn            units="kg m-2 s-1"   cell_methods="m" project_mip_table=Amon /
&parameter name=prw      cmor_name=prw             units="kg m-2"       cell_methods="m" project_mip_table=Amon /
&parameter name=ps       cmor_name=ps              units="Pa"           cell_methods="m" project_mip_table=Amon /
&parameter name=psl      cmor_name=psl             units="Pa"           cell_methods="m" project_mip_table=Amon /
&parameter name=rlds     cmor_name=rlds     p="d"  units="W m-2"        cell_methods="m" project_mip_table=Amon /
&parameter name=rldscs   cmor_name=rldscs   p="d"  units="W m-2"        cell_methods="m" project_mip_table=Amon /
&parameter name=rlus     cmor_name=rlus     p="u"  units="W m-2"        cell_methods="m" project_mip_table=Amon /
&parameter name=rlut     cmor_name=rlut     p="u"  units="W m-2"        cell_methods="m" project_mip_table=Amon /
&parameter name=rlutcs   cmor_name=rlutcs   p="u"  units="W m-2"        cell_methods="m" project_mip_table=Amon /
&parameter name=rsds     cmor_name=rsds     p="d"  units="W m-2"        cell_methods="m" project_mip_table=Amon /
&parameter name=rsdscs   cmor_name=rsdscs   p="d"  units="W m-2"        cell_methods="m" project_mip_table=Amon /
&parameter name=rsdt     cmor_name=rsdt     p="d"  units="W m-2"        cell_methods="m" project_mip_table=Amon /
&parameter name=rsus     cmor_name=rsus     p="u"  units="W m-2"        cell_methods="m" project_mip_table=Amon /
&parameter name=rsuscs   cmor_name=rsuscs   p="u"  units="W m-2"        cell_methods="m" project_mip_table=Amon /
&parameter name=rsut     cmor_name=rsut     p="u"  units="W m-2"        cell_methods="m" project_mip_table=Amon /
&parameter name=rsutcs   cmor_name=rsutcs   p="u"  units="W m-2"        cell_methods="m" project_mip_table=Amon /
&parameter name=sfcWind  cmor_name=sfcWind         units="m/s"          cell_methods="m" project_mip_table=Amon /
&parameter name=ta       cmor_name=ta              units="K"            cell_methods="m" project_mip_table=Amon /
&parameter name=tas      cmor_name=tas             units="K"            cell_methods="m" project_mip_table=Amon /
&parameter name=tasmax   cmor_name=tasmax          units="K"            cell_methods="m" project_mip_table=Amon /
&parameter name=tasmin   cmor_name=tasmin          units="K"            cell_methods="m" project_mip_table=Amon /
&parameter name=tauu     cmor_name=tauu     p="d"  units="Pa"           cell_methods="m" project_mip_table=Amon /
&parameter name=tauv     cmor_name=tauv     p="d"  units="Pa"           cell_methods="m" project_mip_table=Amon /
&parameter name=ts       cmor_name=ts              units="K"            cell_methods="m" project_mip_table=Amon /
&parameter name=ua       cmor_name=ua              units="m/s"          cell_methods="m" project_mip_table=Amon /
&parameter name=uas      cmor_name=uas             units="m/s"          cell_methods="m" project_mip_table=Amon /
&parameter name=va       cmor_name=va              units="m/s"          cell_methods="m" project_mip_table=Amon /
&parameter name=vas      cmor_name=vas             units="m/s"          cell_methods="m" project_mip_table=Amon /
&parameter name=wap      cmor_name=wap             units="Pa/s"         cell_methods="m" project_mip_table=Amon /
&parameter name=zg       cmor_name=zg              units="m"            cell_methods="m" project_mip_table=Amon /
!! day
&parameter name=cl       cmor_name=cl              units="%"            cell_methods="m" project_mip_table=Aday / 
&parameter name=cli      cmor_name=cli             units="kg/kg"        cell_methods="m" project_mip_table=Aday /
&parameter name=clivi    cmor_name=clivi           units="kg m-2"       cell_methods="m" project_mip_table=Aday /
&parameter name=clt      cmor_name=clt             units="%"            cell_methods="m" project_mip_table=Aday /
&parameter name=clw      cmor_name=clw             units="kg/kg"        cell_methods="m" project_mip_table=Aday /
&parameter name=clwvi    cmor_name=clwvi           units="kg m-2"       cell_methods="m" project_mip_table=Aday /
&parameter name=evspsbl  cmor_name=evspsbl         units="kg m-2 s-1"   cell_methods="m" project_mip_table=Aday /
&parameter name=hfls     cmor_name=hfls     p="u"  units="W m-2"        cell_methods="m" project_mip_table=Aday /
&parameter name=hfss     cmor_name=hfss     p="u"  units="W m-2"        cell_methods="m" project_mip_table=Aday /
&parameter name=hur      cmor_name=hur             units="%"            cell_methods="m" project_mip_table=Aday /
&parameter name=hus      cmor_name=hus             units="kg/kg"        cell_methods="m" project_mip_table=Aday /
&parameter name=pr       cmor_name=pr              units="kg m-2 s-1"   cell_methods="m" project_mip_table=Aday /
&parameter name=prc      cmor_name=prc             units="kg m-2 s-1"   cell_methods="m" project_mip_table=Aday /
&parameter name=prsn     cmor_name=prsn            units="kg m-2 s-1"   cell_methods="m" project_mip_table=Aday /
&parameter name=prw      cmor_name=prw             units="kg m-2"       cell_methods="m" project_mip_table=Aday /
&parameter name=ps       cmor_name=ps              units="Pa"           cell_methods="m" project_mip_table=Aday /
&parameter name=psl      cmor_name=psl             units="Pa"           cell_methods="m" project_mip_table=Aday /
&parameter name=rlds     cmor_name=rlds     p="d"  units="W m-2"        cell_methods="m" project_mip_table=Aday /
&parameter name=rldscs   cmor_name=rldscs   p="d"  units="W m-2"        cell_methods="m" project_mip_table=Aday /
&parameter name=rlus     cmor_name=rlus     p="u"  units="W m-2"        cell_methods="m" project_mip_table=Aday /
&parameter name=rlut     cmor_name=rlut     p="u"  units="W m-2"        cell_methods="m" project_mip_table=Aday /
&parameter name=rlutcs   cmor_name=rlutcs   p="u"  units="W m-2"        cell_methods="m" project_mip_table=Aday /
&parameter name=rsds     cmor_name=rsds     p="d"  units="W m-2"        cell_methods="m" project_mip_table=Aday /
&parameter name=rsdscs   cmor_name=rsdscs   p="d"  units="W m-2"        cell_methods="m" project_mip_table=Aday /
&parameter name=rsdt     cmor_name=rsdt     p="d"  units="W m-2"        cell_methods="m" project_mip_table=Aday /
&parameter name=rsus     cmor_name=rsus     p="u"  units="W m-2"        cell_methods="m" project_mip_table=Aday /
&parameter name=rsuscs   cmor_name=rsuscs   p="u"  units="W m-2"        cell_methods="m" project_mip_table=Aday /
&parameter name=rsut     cmor_name=rsut     p="u"  units="W m-2"        cell_methods="m" project_mip_table=Aday /
&parameter name=rsutcs   cmor_name=rsutcs   p="u"  units="W m-2"        cell_methods="m" project_mip_table=Aday /
&parameter name=sfcWind  cmor_name=sfcWind         units="m/s"          cell_methods="m" project_mip_table=Aday /
&parameter name=ta       cmor_name=ta              units="K"            cell_methods="m" project_mip_table=Aday /
&parameter name=tas      cmor_name=tas             units="K"            cell_methods="m" project_mip_table=Aday /
&parameter name=tasmax   cmor_name=tasmax          units="K"            cell_methods="m" project_mip_table=Aday /
&parameter name=tasmin   cmor_name=tasmin          units="K"            cell_methods="m" project_mip_table=Aday /
&parameter name=tauu     cmor_name=tauu     p="d"  units="Pa"           cell_methods="m" project_mip_table=Aday /
&parameter name=tauv     cmor_name=tauv     p="d"  units="Pa"           cell_methods="m" project_mip_table=Aday /
&parameter name=ts       cmor_name=ts              units="K"            cell_methods="m" project_mip_table=Aday /
&parameter name=ua       cmor_name=ua              units="m/s"          cell_methods="m" project_mip_table=Aday /
&parameter name=uas      cmor_name=uas             units="m/s"          cell_methods="m" project_mip_table=Aday /
&parameter name=va       cmor_name=va              units="m/s"          cell_methods="m" project_mip_table=Aday /
&parameter name=vas      cmor_name=vas             units="m/s"          cell_methods="m" project_mip_table=Aday /
&parameter name=wap      cmor_name=wap             units="Pa/s"         cell_methods="m" project_mip_table=Aday /
&parameter name=zg       cmor_name=zg              units="m"            cell_methods="m" project_mip_table=Aday /
!! 3hr
&parameter name=cl       cmor_name=cl              units="%"            cell_methods="m" project_mip_table=A3hr / 
&parameter name=cli      cmor_name=cli             units="kg/kg"        cell_methods="m" project_mip_table=A3hr /
&parameter name=clivi    cmor_name=clivi           units="kg m-2"       cell_methods="m" project_mip_table=A3hr /
&parameter name=clt      cmor_name=clt             units="%"            cell_methods="m" project_mip_table=A3hr /
&parameter name=clw      cmor_name=clw             units="kg/kg"        cell_methods="m" project_mip_table=A3hr /
&parameter name=clwvi    cmor_name=clwvi           units="kg m-2"       cell_methods="m" project_mip_table=A3hr /
&parameter name=evspsbl  cmor_name=evspsbl         units="kg m-2 s-1"   cell_methods="m" project_mip_table=A3hr /
&parameter name=hfls     cmor_name=hfls     p="u"  units="W m-2"        cell_methods="m" project_mip_table=A3hr /
&parameter name=hfss     cmor_name=hfss     p="u"  units="W m-2"        cell_methods="m" project_mip_table=A3hr /
&parameter name=hur      cmor_name=hur             units="%"            cell_methods="p" project_mip_table=A3hr /
&parameter name=hus      cmor_name=hus             units="kg/kg"        cell_methods="p" project_mip_table=A3hr /
&parameter name=pr       cmor_name=pr              units="kg m-2 s-1"   cell_methods="m" project_mip_table=A3hr /
&parameter name=prc      cmor_name=prc             units="kg m-2 s-1"   cell_methods="m" project_mip_table=A3hr /
&parameter name=prsn     cmor_name=prsn            units="kg m-2 s-1"   cell_methods="m" project_mip_table=A3hr /
&parameter name=prw      cmor_name=prw             units="kg m-2"       cell_methods="m" project_mip_table=A3hr /
&parameter name=ps       cmor_name=ps              units="Pa"           cell_methods="m" project_mip_table=A3hr /
&parameter name=psl      cmor_name=psl             units="Pa"           cell_methods="m" project_mip_table=A3hr /
&parameter name=rlds     cmor_name=rlds     p="d"  units="W m-2"        cell_methods="m" project_mip_table=A3hr /
&parameter name=rldscs   cmor_name=rldscs   p="d"  units="W m-2"        cell_methods="m" project_mip_table=A3hr /
&parameter name=rlus     cmor_name=rlus     p="u"  units="W m-2"        cell_methods="m" project_mip_table=A3hr /
&parameter name=rlut     cmor_name=rlut     p="u"  units="W m-2"        cell_methods="m" project_mip_table=A3hr /
&parameter name=rlutcs   cmor_name=rlutcs   p="u"  units="W m-2"        cell_methods="m" project_mip_table=A3hr /
&parameter name=rsds     cmor_name=rsds     p="d"  units="W m-2"        cell_methods="m" project_mip_table=A3hr /
&parameter name=rsdscs   cmor_name=rsdscs   p="d"  units="W m-2"        cell_methods="m" project_mip_table=A3hr /
&parameter name=rsdt     cmor_name=rsdt     p="d"  units="W m-2"        cell_methods="m" project_mip_table=A3hr /
&parameter name=rsus     cmor_name=rsus     p="u"  units="W m-2"        cell_methods="m" project_mip_table=A3hr /
&parameter name=rsuscs   cmor_name=rsuscs   p="u"  units="W m-2"        cell_methods="m" project_mip_table=A3hr /
&parameter name=rsut     cmor_name=rsut     p="u"  units="W m-2"        cell_methods="m" project_mip_table=A3hr /
&parameter name=rsutcs   cmor_name=rsutcs   p="u"  units="W m-2"        cell_methods="m" project_mip_table=A3hr /
&parameter name=sfcWind  cmor_name=sfcWind         units="m/s"          cell_methods="m" project_mip_table=A3hr /
&parameter name=ta       cmor_name=ta              units="K"            cell_methods="p" project_mip_table=A3hr /
&parameter name=tas      cmor_name=tas             units="K"            cell_methods="p" project_mip_table=A3hr /
&parameter name=tasmax   cmor_name=tasmax          units="K"            cell_methods="m" project_mip_table=A3hr /
&parameter name=tasmin   cmor_name=tasmin          units="K"            cell_methods="m" project_mip_table=A3hr /
&parameter name=tauu     cmor_name=tauu     p="d"  units="Pa"           cell_methods="m" project_mip_table=A3hr /
&parameter name=tauv     cmor_name=tauv     p="d"  units="Pa"           cell_methods="m" project_mip_table=A3hr /
&parameter name=ts       cmor_name=ts              units="K"            cell_methods="p" project_mip_table=A3hr /
&parameter name=ua       cmor_name=ua              units="m/s"          cell_methods="p" project_mip_table=A3hr /
&parameter name=uas      cmor_name=uas             units="m/s"          cell_methods="p" project_mip_table=A3hr /
&parameter name=va       cmor_name=va              units="m/s"          cell_methods="p" project_mip_table=A3hr /
&parameter name=vas      cmor_name=vas             units="m/s"          cell_methods="p" project_mip_table=A3hr /
&parameter name=wap      cmor_name=wap             units="Pa/s"         cell_methods="p" project_mip_table=A3hr /
&parameter name=zg       cmor_name=zg              units="m"            cell_methods="p" project_mip_table=A3hr /
