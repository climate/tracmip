import netCDF4 as nc
import numpy as np
from scipy.interpolate import griddata
import sys 

# note:
# the xarray roll command used to shift longitude grid to -180 .. +180 deg lon appears to need xr.__version__ '0.11.3'

# general setup

# number of months to be read in and averaged over --> last 20 years
nmth = 240
# latint is the latiudes on which data will be interpolated, likewise for lonint
latint  = np.linspace(-89.5, 89.5, 180)     
nlatint = latint.size
lonint  = np.linspace(-179.0, 179.0, 180)     
nlonint = lonint.size

# 17 standard CMIP pressure levels in hPa, from surface to top of atmosphere
lev = np.array([100000,  92500,  85000,  70000,  60000,  50000,  40000,  30000,
              25000,  20000,  15000,  10000,   7000,   5000,   3000,   2000, 1000])

nlev = lev.size

# list of models
modnames = ['AM21', 'CAM3', 'CAM4', 'CAM5Nor', 'CNRM-AM5', 'ECHAM61', 'ECHAM63', 'GISS-ModelE2',
           'LMDZ5A', 'MetUM-CTL', 'MetUM-ENT', 'MIROC5', 'MPAS', 'CALTECH']
modnames_4plots = ['AM2.1', 'CAM3', 'CAM4', 'CAM5Nor', 'CNRM-AM5', 'ECHAM6.1', 'ECHAM6.3', 'GISS-ModelE2',
           'LMDZ5A', 'MetUM-CTL', 'MetUM-ENT', 'MIROC5', 'MPAS', 'CALTECH']
nmod    = len(modnames)
modnumbers = ['1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12', '13', '14']

# models available for specific simulations that we implemented correctly
modlist_aqct = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13]
modlist_aq4x = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13]
modlist_ldct = [   1, 2, 3, 4, 5,                 11, 12, 13]
modlist_ld4x = [   1, 2, 3, 4, 5,                 11, 12, 13]
modlist_ldor = [   1, 2,    4, 5,                 11, 12, 13]

# colors as in Voigt 2016 JAMES TRACMIP paper
modcolors = [ np.array([255,204,153])/255,
              np.array([128,128,128])/255,
              np.array([148,255,181])/255,
              np.array([194,  0,136])/255,
              np.array([  0, 51,128])/255,
              np.array([  0,117,220])/255,
              np.array([153, 63,  0])/255,
              np.array([157,204,  0])/255,
              np.array([ 76,  0, 92])/255,
              np.array([ 25, 25, 25])/255,
              np.array([  0, 92, 49])/255,
              np.array([ 43,206, 72])/255,
              np.array([143,124,  0])/255,
              np.array([255,164,  5])/255 ]

##############################
# global-mean time-mean values
def get_gmtm(exp, var):
    data = np.zeros(nmod) + np.nan
    for m in range(nmod):
        data[m] = get_data_gmtm(model=modnames[m], exp=exp, var=var)
    return data
    
def get_data_gmtm(model, exp, var):
    # load dataset
    ds = get_dataset(model=model, exp=exp, var=var, stream='Amon')
    # calculate global-mean time-mean    
    if (ds != None):
        # latitude and last n months
        lat = ds['lat'].values
        dat = ds[var][-nmth:].values
        # average over longitude and time
        dat_zmtm = np.nanmean(np.nanmean(dat, axis=2), axis=0)
        # average over lattitude    
        area = np.cos(lat*np.pi/180.0)
        data_gmtm = np.nansum(dat_zmtm*area)/np.nansum(area)
    else:
        data_gmtm=np.nan
    
    return data_gmtm

##############################
# zonal-mean time-mean values

# 2d-fields
def get_zmtm(exp, var):
    data = np.zeros((nmod, nlatint)) + np.nan
    for m in range(nmod):
        data[m] = get_data_zmtm(model=modnames[m], exp=exp, var=var)
    return data

def get_data_zmtm(model, exp, var):
    # load dataset
    ds = get_dataset(model=model, exp=exp, var=var, stream='Amon')
    # calculate zonal-mean time-mean and remap to common latint grid    
    if (ds != None):
        # latitude and last n months
        lat = ds['lat'].values
        dat = ds[var][-nmth:].values
        # average over longitude and time
        dat_zmtm = np.nanmean(np.nanmean(dat, axis=2), axis=0)
        # interpolate to common latitude grid latint
        out = np.zeros(nlatint) + np.nan
        # if lat is from NP to SP we need to flip lat, otherwise np.interp gives meaningless result
        if lat[0] > lat[1]: 
            dat_zmtm = dat_zmtm[::-1]
            lat   = lat[::-1]
        out = np.interp(latint, lat, dat_zmtm) 
    else:
        out = np.zeros(nlatint) + np.nan
    return out

# 3d fields
def get_zmtm3d(exp, var):
    data = np.zeros((nmod, nlev, nlatint)) + np.nan
    for m in range(nmod):
        data[m] = get_data_zmtm3d(model=modnames[m], exp=exp, var=var)
    return data

def get_data_zmtm3d(model, exp, var):
    # load dataset
    ds = get_dataset(model=model, exp=exp, var=var, stream='Amon')
    # calculate zonal-mean time-mean and remap to common latint grid    
    if (ds != None):
        # latitude and last n months
        lat = ds['lat'].values
        dat = ds[var][-nmth:].values
        # average over longitude and time
        dat_zmtm = np.nanmean(np.nanmean(dat, axis=3), axis=0)
        # interpolate to common latitude grid latint
        out = np.zeros((nlev, nlatint)) + np.nan
        # if lat is from NP to SP we need to flip lat, otherwise np.interp gives meaningless result
        if lat[0] > lat[1]: 
            dat_zmtm = dat_zmtm[::-1]
            lat   = lat[::-1]
        for k in range(nlev):    
            out[k] = np.interp(latint, lat, dat_zmtm[k]) 
    else:
        out = np.zeros(nlatint) + np.nan
    return out

##############################
# zonal-mean monthly-mean values
def get_zmmm(exp, var):
    data = np.zeros((nmod, 12, nlatint)) + np.nan
    for m in range(nmod):
        data[m] = get_data_zmmm(model=modnames[m], exp=exp, var=var)
    return data

def get_data_zmmm(model, exp, var):
    # load dataset
    ds = get_dataset(model=model, exp=exp, var=var, stream='Amon')
    # calculate zonal-mean monthly-mean and remap to common latint grid    
    if (ds != None):
        # latitude and last n months
        lat = ds['lat'].values
        dat = ds[var][-nmth:].groupby('time.month').mean('time').values
        # average over longitude
        dat_zmmm = np.nanmean(dat, axis=2)
        # interpolate to common latitude grid latint
        out = np.zeros((12,nlatint)) + np.nan
        # if lat is from NP to SP we need to flip lat, otherwise np.interp gives meaningless result
        if lat[0] > lat[1]: 
            dat_zmmm[:,:] = dat_zmmm[:,::-1]
            lat   = lat[::-1]
        for mon in range(12):
            out[mon,:] = np.interp(latint, lat, dat_zmmm[mon,:].ravel()) 
    else:
        out = np.zeros(nlatint) + np.nan
    return out

##############################
# time-mean values
def get_tm(exp, var):
    data = np.zeros((nmod, nlatint, nlonint)) + np.nan
    for m in range(nmod):
        data[m] = get_data_tm(model=modnames[m], exp=exp, var=var)
    return data
    
def get_data_tm(model, exp, var):
    # load dataset
    ds = get_dataset(model=model, exp=exp, var=var, stream='Amon')
    # calculate time-mean and remap to common latintxlonint grid    
    # output is on common latitude-longitude grid latint
    out = np.zeros((nlatint, nlonint)) + np.nan
    if (ds != None):
        # ESGF data is on 0-360 deg longitude grid, but we want to data on from -180E to 180E longitude grid
        # so that continent is in the middle of lat-lon plots 
        ds = ds.roll(lon=(ds['lon'].size//2))
        auxlon = ds['lon'].values
        auxlon[0:ds['lon'].size//2] -= 360
        ds['lon'] = auxlon
        # latitude, longitude and time.mean over last nmth months
        lat = ds['lat'].values
        lon = ds['lon'].values
        dat = np.nanmean(ds[var][-nmth:].values, axis=0)
        # grid of original model data      
        x, y   = np.meshgrid(lon, lat)
        # grid on which we interpolate
        xint, yint = np.meshgrid(lonint, latint)
        # interpolated data
        out = griddata((x.ravel(), y.ravel()), dat.ravel(), (xint, yint))
    return out 
 
##############################
# monthly-mean values
def get_mm(exp, var):
    data = np.zeros((nmod, 12, nlatint, nlonint)) + np.nan
    for m in range(nmod):
        data[m] = get_data_mm(model=modnames[m], exp=exp, var=var)
    return data

def get_data_mm(model, exp, var):
    # load dataset
    ds = get_dataset(model=model, exp=exp, var=var, stream='Amon')
    # calculate monthly-mean and remap to common latintxlonint grid    
    # output is on common latitude-longitude grid latint
    out = np.zeros((12,nlatint, nlonint)) + np.nan
    if (ds != None):
        # ESGF data is on 0-360 deg longitude grid, but we want to data on from -180E to 180E longitude grid
        # so that continent is in the middle of lat-lon plots 
        ds = ds.roll(lon=(ds['lon'].size//2))
        auxlon = ds['lon'].values
        auxlon[0:ds['lon'].size//2] -= 360
        ds['lon'] = auxlon
        # latitude, longitude and time.mean over last nmth months
        lat = ds['lat'].values
        lon = ds['lon'].values
        dat = ds[var][-nmth:].groupby('time.month').mean('time').values
        # grid of original model data      
        x, y   = np.meshgrid(lon, lat)
        # grid on which we interpolate
        xint, yint = np.meshgrid(lonint, latint)
        # interpolate data
        for mon in range(12):
            out[mon] = griddata((x.ravel(), y.ravel()), dat[mon].ravel(), (xint, yint))
    return out        

##############################
# daily values of last 10 days
def get_day_last10days(exp, var):
    data = np.zeros((nmod, 10, nlatint, nlonint)) + np.nan
    for m in range(nmod):
        data[m] = get_data_day_last10days(model=modnames[m], exp=exp, var=var)
    return data

def get_data_day_last10days(model, exp, var):
    # load dataset
    ds = get_dataset(model=model, exp=exp, var=var, stream='Aday')
    # remap to common latintxlonint grid    
    # output is on common latitude-longitude grid latint
    out = np.zeros((10,nlatint, nlonint)) + np.nan
    if (ds != None):
        # ESGF data is on 0-360 deg longitude grid, but we want to data on from -180E to 180E longitude grid
        # so that continent is in the middle of lat-lon plots 
        ds = ds.roll(lon=(ds['lon'].size//2))
        auxlon = ds['lon'].values
        auxlon[0:ds['lon'].size//2] -= 360
        ds['lon'] = auxlon
        # latitude, longitude and select last 10 days
        lat = ds['lat'].values
        lon = ds['lon'].values
        dat = ds[var][-10:].values
        # grid of original model data      
        x, y   = np.meshgrid(lon, lat)
        # grid on which we interpolate
        xint, yint = np.meshgrid(lonint, latint)
        # interpolate data
        for tim in range(8):
            out[tim] = griddata((x.ravel(), y.ravel()), dat[tim].ravel(), (xint, yint))
    return out        

##############################
# 3-hourly values of last 10 days
def get_3hr_last10days(exp, var):
    data = np.zeros((nmod, 10*8, nlatint, nlonint)) + np.nan
    for m in range(nmod):
        data[m] = get_data_3hr_last10days(model=modnames[m], exp=exp, var=var)
    return data

def get_data_3hr_last10days(model, exp, var):
    # load dataset
    ds = get_dataset(model=model, exp=exp, var=var, stream='A3hr')
    # remap to common latintxlonint grid    
    # output is on common latitude-longitude grid latint
    out = np.zeros((10*8,nlatint, nlonint)) + np.nan
    if (ds != None):
        # ESGF data is on 0-360 deg longitude grid, but we want to data on from -180E to 180E longitude grid
        # so that continent is in the middle of lat-lon plots 
        ds = ds.roll(lon=(ds['lon'].size//2))
        auxlon = ds['lon'].values
        auxlon[0:ds['lon'].size//2] -= 360
        ds['lon'] = auxlon
        # latitude, longitude and select last 10 days
        lat = ds['lat'].values
        lon = ds['lon'].values
        dat = ds[var][-8*10:].values
        # grid of original model data      
        x, y   = np.meshgrid(lon, lat)
        # grid on which we interpolate
        xint, yint = np.meshgrid(lonint, latint)
        # interpolate data
        for tim in range(10*8):
            out[tim] = griddata((x.ravel(), y.ravel()), dat[tim].ravel(), (xint, yint))
    return out        

##############################
# infrastructure functions
def get_center(model):
    # for a given model, provide the center
    # e.g., LDEO for ECHAM61
    center=None
    if model=='AM21':
        center='ULSAN'
    elif model=='CAM3':
        center='UW-MADISON'
    elif model=='CAM4':
        center='UAlbany'
    elif model=='CAM5Nor':
        center='NCC'
    elif model=='CNRM-AM5':
        center='CNRM'
    elif model=='ECHAM61':
        center='LDEO'
    elif model=='ECHAM63':
        center='MPI-M'
    elif model=='GISS-ModelE2':
        center='NASA-GISS'
    elif model=='LMDZ5A':
        center='LOCEAN'
    elif model=='MetUM-CTL':
        center='NCAS'
    elif model=='MetUM-ENT':
        center='NCAS'
    elif model=='MIROC5':
        center='HU'
    elif model=='MPAS':
        center='PNNL'
    elif model=='CALTECH':
        center='CALTECH'
    else:
        sys.exit('loaddata.py, get_center: No center found for desired model!')
    return center

def get_dataset(model, exp, var, stream):
    # provides dataset for given model, experiment and variable from lsdfonline TRACMIP_ESGFCOPY
    # stream determines whether Amon, Aday or A3hr dataset is loaded
    import xarray as xr
    center = get_center(model)
    # CALTECH simulations with perturbed optical thickness are named Abs15 instead of 4xCO2
    if model=='CALTECH':
        if exp=='aqua4xCO2TRACMIP': exp='aquaAbs15TRACMIP'
        elif exp=='land4xCO2TRACMIP': exp='landAbs15TRACMIP'
    path = '/home/fd8940/lsdfonline/Gruppe_Voigt/TRACMIP_ESGFCOPY/'+center+'/'+model+'/'+exp+'/'+stream[1:]+'/atmos/'+stream+'/r1i1p1/'+var
    # check if variable file exists, if so open dataset, if not set dataset to None
    import glob
    import os
    flist = glob.glob(path+'/v*/'+var+'_'+stream+'_'+model+'_'+exp+'_r1i1p1_*.nc')
    #print(model, exp, flist) # for debugging purposes
    # for CALTECH land there are two versions, make sure to use newer version v20190408
    if model=='CALTECH':
        if (exp=='landControlTRACMIP') or (exp=='landAbs15TRACMIP') or (exp=='landOrbitTRACMIP'):
            flist = glob.glob(path+'/v20190408/'+var+'_'+stream+'_'+model+'_'+exp+'_r1i1p1_*.nc')
    if (len(flist)==1):        
        #print(flist)
        ds = xr.open_mfdataset(flist[0]).squeeze()
    elif (len(flist)>1):
        print(flist)
        import sys
        sys.exit('There are several file versions, not sure which one I should used. STOP!')
    else:
        ds=None
    return ds
