\documentclass[letterpaper,10pt]{article}

\usepackage[utf8]{inputenc}
\usepackage{fullpage}
\usepackage{natbib}
\usepackage{graphicx}
\usepackage{color}
\usepackage{gensymb}
\usepackage{amsmath}

% adapt section titles
\usepackage{titlesec}


\usepackage[font=small,labelfont=bf]{caption}

% adapt page setup (margins etc.)
\textwidth 6.8in   %6.5in
\oddsidemargin -0.15in 
\evensidemargin -0.15in
\textheight = 9.2 in
\topmargin = -0.1 in
\headheight = 0.0 in
\headsep = 0.0 in

%opening
\title{Sanity checks for TRACMIP ESGF data}
\author{Aiko Voigt}

\begin{document}

\maketitle

\begin{abstract}
This document describes the sanity checks done for the TRACMIP ESGF data. This is in preparation for the corrigendum paper.
\end{abstract}

\section{The big take-home of the sanity checks}

Everything is correctly implemented except for:
\begin{itemize}
 \item insolation: CALTECH is 3 months out of phase
 \item q-flux: not zeroed out over land in ECHAM6.3, some smaller deviation in high-latitudes for some models, CAM3 has suspicous kink in aqua4xCO2TRACMIP near 30 deg N	
 \item surface albedo: too strong increase over land in LMDZ5A
 \item evaporative resistence over land: AM21 does not show expected change in Bowen ratio
 \item land heat capacity: diurnal cycle of temperature shows that MetUM-CTL and MetUM-ENT did not decrease heat capacity over land; can't check for CALTECH because
       there is no tasmax/tasmin or 3hr data, might look at Rick's metric of summer warming/winter cooling over land as some kind of indication
\end{itemize}


\section{Insolation rsdt}

Take-home points:
\begin{itemize}
 \item CALTECH is three months out of phase, but otherwise rsdt seems correct
 \item there are some smaller differences between models that might have to do with different calendar choices,
       yet overall rsdt and its changes in landOrbitTRACMIP were implemented properly 
\end{itemize}

\subsection{Zonal-mean time-mean insolation, and difference from model median}

\begin{figure}[h!]
 \begin{center}
    \includegraphics[width=0.7\textwidth]{./figs/rsdt_aquaControlTRACMIP_zonalmeantimemean.pdf}\\
    \includegraphics[width=0.7\textwidth]{./figs/rsdt_aqua4xCO2TRACMIP_zonalmeantimemean.pdf}
 \end{center}
 \caption{Zonal-mean time-mean rsdt in aquaControlTRACMIP (top) and aqua4xCO2TRACMIP (bottom). Model colors as in 2016 JAMES paper.}
\end{figure} 

\begin{figure}[h!]
 \begin{center}
    \includegraphics[width=0.7\textwidth]{./figs/rsdt_landControlTRACMIP_zonalmeantimemean.pdf}\\
    \includegraphics[width=0.7\textwidth]{./figs/rsdt_land4xCO2TRACMIP_zonalmeantimemean.pdf}\\
    \includegraphics[width=0.7\textwidth]{./figs/rsdt_landOrbitTRACMIP_zonalmeantimemean.pdf}\\
 \end{center}
 \caption{Zonal-mean time-mean rsdt in landControlTRACMIP (top), land4xCO2TRACMIP (middle) and landOrbitTRACMIP (bottom). Model colors as in 2016 JAMES paper.}
\end{figure} 

\clearpage
\newpage

\subsection{Seasonal cycle of zonal-mean insolation, and difference to model median}

\begin{figure}[h!]
 \begin{center}
    \includegraphics[width=0.6\textwidth]{./figs/rsdt_aquaControlTRACMIP.pdf}
    \includegraphics[width=0.6\textwidth]{./figs/rsdt_aquaControlTRACMIP_diff2modelmedian.pdf}
 \end{center}
 \caption{rsdt in aquaControlTRACMIP. Upper plot: full values. Lower plot: difference to model median.}
\end{figure} 

\begin{figure}[h!]
 \begin{center}
    \includegraphics[width=0.6\textwidth]{./figs/rsdt_aqua4xCO2TRACMIP.pdf}
    \includegraphics[width=0.6\textwidth]{./figs/rsdt_aqua4xCO2TRACMIP_diff2modelmedian.pdf}
 \end{center}
 \caption{rsdt in aqua4xCO2TRACMIP. Upper plot: full values. Lower plot: difference to model median.}
\end{figure} 

\begin{figure}[h!]
 \begin{center}
    \includegraphics[width=0.6\textwidth]{./figs/rsdt_landControlTRACMIP.pdf}
    \includegraphics[width=0.6\textwidth]{./figs/rsdt_landControlTRACMIP_diff2modelmedian.pdf}
 \end{center}
 \caption{rsdt in landControlTRACMIP. Upper plot: full values. Lower plot: difference to model median.}
\end{figure} 

\begin{figure}[h!]
 \begin{center}
    \includegraphics[width=0.6\textwidth]{./figs/rsdt_land4xCO2TRACMIP.pdf}
    \includegraphics[width=0.6\textwidth]{./figs/rsdt_land4xCO2TRACMIP_diff2modelmedian.pdf}
 \end{center}
 \caption{rsdt in land4xCO2TRACMIP. Upper plot: full values. Lower plot: difference to model median.}
\end{figure} 

\begin{figure}[h!]
 \begin{center}
    \includegraphics[width=0.6\textwidth]{./figs/rsdt_landOrbitTRACMIP.pdf}
    \includegraphics[width=0.6\textwidth]{./figs/rsdt_landOrbitTRACMIP_diff2modelmedian.pdf}
 \end{center}
 \caption{rsdt in landOrbitTRACMIP. Upper plot: full values. Lower plot: difference to model median.}
\end{figure} 

\clearpage
\newpage

\subsection{Seasonal cycle of zonal-mean insolation: difference between landOrbitTRACMIP and landControlTRACMIP}

\begin{figure}[h!]
 \begin{center}
    \includegraphics[width=\textwidth]{./figs/rsdt_diffbetween_landOrbitTRACMIP_and_landControlTRACMIP.pdf}
 \end{center}
 \caption{rsdt of landOrbitTRACMIP - landControlTRACMIP.}
\end{figure} 

\clearpage
\newpage

\section{Ocean heat transport}

Ocean heat transport is inferred from the surface energy balance as:
\begin{equation}
 qflux = -1*(rsds-rsus+rlds-rlus-hfss-hfls).
\end{equation}
Note that for AM2, hfls is not available, but we use 2.5008e6*evspsbl (constant taken from ICON model hared/mo\_physical\_constants.f90). 
For LMDZ5A, hfls and hfss must be multiplied by -1, as they are directed
downward in this model.

Take-home points:
\begin{itemize}
 \item ECHAM6.3 did not zero out q-flux over land; this can also be seen in the zonal-mean q-flux
 \item Otherwise, all models seem to be okay
 \item Apart from the ECHAM6.3 land simulations, the global-mean time-mean inferred q-flux is 0 within typically 0.3 Wm-2. In ECHAM6.3 land simulations,
 the global-mean is around -0.6 Wm-2.
\end{itemize}

\subsection{Zonal-mean time-mean ocean heat transport}

\begin{figure}[h!]
 \begin{center}
    \includegraphics[width=\textwidth]{./figs/qflux_fromsfcbalance_aquaControlTRACMIP_zonalmeantimemean.pdf}
 \end{center}
 \caption{Zonal-mean time-mean ocean heat transport in aquaControlTRACMIP.}
\end{figure} 

\begin{figure}[h!]
 \begin{center}
    \includegraphics[width=\textwidth]{./figs/qflux_fromsfcbalance_aqua4xCO2TRACMIP_zonalmeantimemean.pdf}
 \end{center}
 \caption{Zonal-mean time-mean ocean heat transport in aqua4xCO2TRACMIP.}
\end{figure} 

\begin{figure}[h!]
 \begin{center}
    \includegraphics[width=\textwidth]{./figs/qflux_fromsfcbalance_landControlTRACMIP_zonalmeantimemean.pdf}
 \end{center}
 \caption{Zonal-mean time-mean ocean heat transport in landControlTRACMIP.}
\end{figure} 

\begin{figure}[h!]
 \begin{center}
    \includegraphics[width=\textwidth]{./figs/qflux_fromsfcbalance_land4xCO2TRACMIP_zonalmeantimemean.pdf}
 \end{center}
 \caption{Zonal-mean time-mean ocean heat transport in land4xCO2TRACMIP.}
\end{figure} 

\begin{figure}[h!]
 \begin{center}
    \includegraphics[width=\textwidth]{./figs/qflux_fromsfcbalance_landOrbitTRACMIP_zonalmeantimemean.pdf}
 \end{center}
 \caption{Zonal-mean time-mean ocean heat transport in landOrbitTRACMIP.}
\end{figure} 

\clearpage

\subsection{Lat-lon time-mean ocean heat transport}

\begin{figure}[h!]
 \begin{center}
    \includegraphics[width=\textwidth]{./figs/qflux_fromsfcbalance_aquaControlTRACMIP_timemean.pdf}
 \end{center}
 \caption{Time-mean ocean heat transport in aquaControlTRACMIP.}
\end{figure} 

\begin{figure}[h!]
 \begin{center}
    \includegraphics[width=\textwidth]{./figs/qflux_fromsfcbalance_aqua4xCO2TRACMIP_timemean.pdf}
 \end{center}
 \caption{Time-mean ocean heat transport in aqua4xCO2TRACMIP.}
\end{figure} 

\begin{figure}[h!]
 \begin{center}
    \includegraphics[width=\textwidth]{./figs/qflux_fromsfcbalance_landControlTRACMIP_timemean.pdf}
 \end{center}
 \caption{Time-mean ocean heat transport in landControlTRACMIP.}
\end{figure} 

\begin{figure}[h!]
 \begin{center}
    \includegraphics[width=\textwidth]{./figs/qflux_fromsfcbalance_land4xCO2TRACMIP_timemean.pdf}
 \end{center}
 \caption{Time-mean ocean heat transport in land4xCO2TRACMIP.}
\end{figure} 

\begin{figure}[h!]
 \begin{center}
    \includegraphics[width=\textwidth]{./figs/qflux_fromsfcbalance_landOrbitTRACMIP_timemean.pdf}
 \end{center}
 \caption{Time-mean ocean heat transport in landOrbitTRACMIP.}
\end{figure} 


\clearpage
\newpage

\section{Surface albedo over land}

Surface albedo over land should be increased by 0.07 compared to the ocean albedo. Surface
albedo is diagnosed by rsus/rsds.

Take-home points:
\begin{itemize}
 \item LMDZ5A has too high land albedo increase (by 0.14)
 \item other models have correct albedo increase over land
 \item note: CALTECH surface albedo much higher over ocean compared to other models, as this model does not have clouds
\end{itemize}


\subsection{Lat-lon time-mean surface albedo}

\begin{figure}[h!]
 \begin{center}
    \includegraphics[width=\textwidth]{./figs/sfcalbedo_aquaControlTRACMIP_timemean.pdf}
 \end{center}
 \caption{Time-mean surface albedo in aquaControlTRACMIP.}
\end{figure} 

\begin{figure}[h!]
 \begin{center}
    \includegraphics[width=\textwidth]{./figs/sfcalbedo_aqua4xCO2TRACMIP_timemean.pdf}
 \end{center}
 \caption{Time-mean surface albedo in aqua4xCO2TRACMIP.}
\end{figure} 

\begin{figure}[h!]
 \begin{center}
    \includegraphics[width=\textwidth]{./figs/sfcalbedo_landControlTRACMIP_timemean.pdf}
 \end{center}
 \caption{Time-mean surface albedo in landControlTRACMIP.}
\end{figure} 

\begin{figure}[h!]
 \begin{center}
    \includegraphics[width=\textwidth]{./figs/sfcalbedo_land4xCO2TRACMIP_timemean.pdf}
 \end{center}
 \caption{Time-mean surface albedo in land4xCO2TRACMIP.}
\end{figure} 

\begin{figure}[h!]
 \begin{center}
    \includegraphics[width=\textwidth]{./figs/sfcalbedo_landOrbitTRACMIP_timemean.pdf}
 \end{center}
 \caption{Time-mean surface albedo in landOrbitTRACMIP.}
\end{figure} 

\clearpage
\newpage

\subsection{Lat-lon time-mean difference between landControlTRACMIP and aquaControlTRACMIP}

\begin{figure}[h!]
 \begin{center}
    \includegraphics[width=\textwidth]{./figs/sfcalbedo_timemean_differencebetween_landControlTRACMIP_and_aquaControlTRACMIP.pdf}
 \end{center}
 \caption{Time-mean surface albedo in landControlTRACMIP-aquaControlTRACMIP.}
\end{figure} 

\clearpage
\newpage

\subsection{Special look at CALTECH}

\begin{figure}[h!]
 \begin{center}
    \includegraphics[width=\textwidth]{./figs/sfcalbedo_timemean_CALTECH_landControlTRACMIP-aquaControlTRACMIP.pdf}
 \end{center}
 \caption{Time-mean surface albedo in all CALTECH simulations.}
\end{figure} 

\clearpage
\newpage

\section{Evaporative resistance over land: Bowen ratio}

Bowen ratio calculated from hfss and hfls. Note that for LMDZ5A, hfss and hfls need to be multiplied by -1. For AM21 we
need to use evspsbl for hfls (see section on q-flux).

Take-home points:
\begin{itemize}
 \item AM21 does not show expected increase in Bowen ratio over land
 \item other models have correctly implemented evaporative resistance
\end{itemize}

\subsection{Lat-lon time-mean Bowen ratio}

\begin{figure}[h!]
 \begin{center}
    \includegraphics[width=\textwidth]{./figs/bowenratio_aquaControlTRACMIP_timemean.pdf}
 \end{center}
 \caption{Time-mean Bowen ratio in aquaControlTRACMIP.}
\end{figure} 

\begin{figure}[h!]
 \begin{center}
    \includegraphics[width=\textwidth]{./figs/bowenratio_aqua4xCO2TRACMIP_timemean.pdf}
 \end{center}
 \caption{Time-mean Bowen ratio in aqua4xCO2TRACMIP.}
\end{figure} 

\begin{figure}[h!]
 \begin{center}
    \includegraphics[width=\textwidth]{./figs/bowenratio_landControlTRACMIP_timemean.pdf}
 \end{center}
 \caption{Time-mean Bowen ratio in landControlTRACMIP.}
\end{figure} 

\begin{figure}[h!]
 \begin{center}
    \includegraphics[width=\textwidth]{./figs/bowenratio_land4xCO2TRACMIP_timemean.pdf}
 \end{center}
 \caption{Time-mean Bowen ratio in land4xCO2TRACMIP.}
\end{figure} 

\begin{figure}[h!]
 \begin{center}
    \includegraphics[width=\textwidth]{./figs/bowenratio_landOrbitTRACMIP_timemean.pdf}
 \end{center}
 \caption{Time-mean Bowen ratio in landOrbitTRACMIP.}
\end{figure} 


\clearpage
\newpage

\section{Diurnal cycle over land: land heat capacity}

The shallow slab ocean over land should lead to a much stronger diurnal cycle of ts or tas. This
has been checked. For most models, this has been done by looking at 3hr data for ts or tas. For some models,
3hr data was not available, and so daily data for tasmax and tasmin, and the difference thereof, was used.

Take-home points:
\begin{itemize}
 \item MetUM-CTL and MetUM-ENT did not properly implement the reduced heat capacity
 \item for CALTECH we can't check because there is no tasmax/tasmin or 3hr data
 \item all other models have the increased diurnal cycle (at least for one of the analyis options taken here)
\end{itemize}

\subsection{Lat-lon diurnal cycle diagnosed based on 3hr ts}

\begin{figure}[h!]
 \begin{center}
    \includegraphics[width=\textwidth]{./figs/ts_dailyamplitude_from3hrdata_aquaControlTRACMIP.pdf}
 \end{center}
 \caption{Diurnal cycle from 3hr ts data in aquaControlTRACMIP.}
\end{figure} 

\begin{figure}[h!]
 \begin{center}
    \includegraphics[width=\textwidth]{./figs/ts_dailyamplitude_from3hrdata_aqua4xCO2TRACMIP.pdf}
 \end{center}
 \caption{Diurnal cycle from 3hr ts data in aqua4xCO2TRACMIP.}
\end{figure} 

\begin{figure}[h!]
 \begin{center}
    \includegraphics[width=\textwidth]{./figs/ts_dailyamplitude_from3hrdata_landControlTRACMIP.pdf}
 \end{center}
 \caption{Diurnal cycle from 3hr ts data in landControlTRACMIP.}
\end{figure} 

\begin{figure}[h!]
 \begin{center}
    \includegraphics[width=\textwidth]{./figs/ts_dailyamplitude_from3hrdata_land4xCO2TRACMIP.pdf}
 \end{center}
 \caption{Diurnal cycle from 3hr ts data in land4xCO2TRACMIP.}
\end{figure} 

\begin{figure}[h!]
 \begin{center}
    \includegraphics[width=\textwidth]{./figs/ts_dailyamplitude_from3hrdata_landOrbitTRACMIP.pdf}
 \end{center}
 \caption{Diurnal cycle from 3hr ts data in landOrbitTRACMIP.}
\end{figure} 

\clearpage
\newpage

\subsection{Lat-lon diurnal cycle diagnosed based on 3hr tas}

\begin{figure}[h!]
 \begin{center}
    \includegraphics[width=\textwidth]{./figs/tas_dailyamplitude_from3hrdata_aquaControlTRACMIP.pdf}
 \end{center}
 \caption{Diurnal cycle from 3hr tas data in aquaControlTRACMIP.}
\end{figure} 

\begin{figure}[h!]
 \begin{center}
    \includegraphics[width=\textwidth]{./figs/tas_dailyamplitude_from3hrdata_aqua4xCO2TRACMIP.pdf}
 \end{center}
 \caption{Diurnal cycle from 3hr tas data in aqua4xCO2TRACMIP.}
\end{figure} 

\begin{figure}[h!]
 \begin{center}
    \includegraphics[width=\textwidth]{./figs/tas_dailyamplitude_from3hrdata_landControlTRACMIP.pdf}
 \end{center}
 \caption{Diurnal cycle from 3hr tas data in landControlTRACMIP.}
\end{figure} 

\begin{figure}[h!]
 \begin{center}
    \includegraphics[width=\textwidth]{./figs/tas_dailyamplitude_from3hrdata_land4xCO2TRACMIP.pdf}
 \end{center}
 \caption{Diurnal cycle from 3hr tas data in land4xCO2TRACMIP.}
\end{figure} 

\begin{figure}[h!]
 \begin{center}
    \includegraphics[width=\textwidth]{./figs/tas_dailyamplitude_from3hrdata_landOrbitTRACMIP.pdf}
 \end{center}
 \caption{Diurnal cycle from 3hr tas data in landOrbitTRACMIP.}
\end{figure} 

\clearpage
\newpage

\subsection{Lat-lon daily tasmax - tasmin}

The last 10 days are used. Plotted is the 10-day mean of (tasmax-tasmin).

\begin{figure}[h!]
 \begin{center}
    \includegraphics[width=\textwidth]{./figs/ts_dailyamplitude_fromdaily_tasmax_tasmin_aquaControlTRACMIP.pdf}
 \end{center}
 \caption{tasmax-tasmin from day data in aquaControlTRACMIP.}
\end{figure} 

\begin{figure}[h!]
 \begin{center}
    \includegraphics[width=\textwidth]{./figs/ts_dailyamplitude_fromdaily_tasmax_tasmin_aqua4xCO2TRACMIP.pdf}
 \end{center}
 \caption{tasmax-tasmin from day data in aqua4xCO2TRACMIP.}
\end{figure} 

\begin{figure}[h!]
 \begin{center}
    \includegraphics[width=\textwidth]{./figs/ts_dailyamplitude_fromdaily_tasmax_tasmin_landControlTRACMIP.pdf}
 \end{center}
 \caption{tasmax-tasmin from day data in landControlTRACMIP.}
\end{figure} 

\begin{figure}[h!]
 \begin{center}
    \includegraphics[width=\textwidth]{./figs/ts_dailyamplitude_fromdaily_tasmax_tasmin_land4xCO2TRACMIP.pdf}
 \end{center}
 \caption{tasmax-tasmin from day data in land4xCO2TRACMIP.}
\end{figure} 

\begin{figure}[h!]
 \begin{center}
    \includegraphics[width=\textwidth]{./figs/ts_dailyamplitude_fromdaily_tasmax_tasmin_landOrbitTRACMIP.pdf}
 \end{center}
 \caption{tasmax-tasmin from day data in landOrbitTRACMIP.}
\end{figure} 

\clearpage
\newpage

\subsection{Custom analysis for AM21}

There is no tasmax/tasmin data for AM21 landControlTRACMIP on ESGF, and no 3hr ts or tas data. 
There is no 3hr data because that data was found to only contain less than a year of timesteps, and so it was not cmorized. 
Yet, we have copied the last timesteps of the AM21 landControlTRACMIP 3hr ts data from the UMiami server

\begin{figure}[h!]
 \begin{center}
    \includegraphics[width=\textwidth]{./figs/ts_diurnalcycle_am21_landControlTRACMIP_land4xCO2TRACMIP.pdf}
 \end{center}
 \caption{Diurnal ts cycle for AM21 based on some 3hr data.}
\end{figure}

\subsection{Custom analysis for LMDZ5A}

The UMiami server has daily tasmin and tasmax for LMDZ5A, but they were inadvertendly not included in the cmorizing and hence are not on ESGF.

\begin{figure}[h!]
 \begin{center}
    \includegraphics[width=\textwidth]{./figs/tas_diurnalcycle_lmdz5a_landControlTRACMIP_land4xCO2TRACMIP_landOrbitTRACMIP.pdf}
 \end{center}
 \caption{Mean of day tasmax-tasmin for LMDZ5A from some day data.}
\end{figure}

\subsection{Custom analysis for CAM5Nor}

The UMiami server has daily tasmin and tasmax for CAM5Nor LandControlTRACMIP and Land4xCO2TRACMIP, 
but tasmin was inadvertendly not included in the cmorizing and hence is not on ESGF.

\begin{figure}[h!]
 \begin{center}
    \includegraphics[width=\textwidth]{./figs/tas_diurnalcycle_cam5nor_landControlTRACMIP_land4xCO2TRACMIP.pdf}
 \end{center}
 \caption{Mean of day tasmax-tasmin for CAM5Nor from some day data.}
\end{figure}

\end{document}
