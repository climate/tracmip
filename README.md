# TRACMIP - Understanding the ITCZ and monsoons through a new ensemble of climate model simulations


![](tracmip_teaser.png)


TRACMIP is a climate modeling project that aims to understand the ITCZ and monsoons through a new ensemble of climate model simulations. TRACMIP is led by Aiko Voigt (University of Vienna) and Michela Biasutti (Lamont-Doherty Earth Observatory).

Understanding and modeling tropical rainfall has proven to be one of the most stubborn challenges in climate science. Tropical rainfall biases such as a double inter-tropical convergence zone (ITCZ) in the East Pacific have now persisted more than two decades despite the general improvements of climate models, and projections for the ITCZ  and the monsoon systems remain uncertain in magnitude and sign.

Connected to a workshop on ITCZ and monsoons that was held at Columbia University in September 2015, we have designed the "Tropical Rain belts with an Annual cycle and Continent - Model Intercomparison Project." TRAC-MIP involves five experiments using idealized aquaplanet and land setups to explore the dynamics of tropical rainfall. By using interactive sea-surface temperatures and seasonally-varying insolation TRAC-MIP fills the gap between idealized aquaplanet simulations with prescribed SSTs and the fully-coupled realistic model simulations of CMIP5.

TRACMIP mostly involves state-of-the art comprehensive climate models, but it also includes a simplified model that neglects cloud and water-vapor radiative feedbacks. This will allow us to better connect the results from the TRAC-MIP comprehensive models to theoretical studies of tropical rain belt dynamics.

TRACMIP is part of the World Climate Research Program and its Grand Challenge on "Clouds, circulation and climate sensitivity." It addresses the question "What controls the position, strength and variability of the tropical rain belts?" raised by Bony et al. (2015).

TRACMIP is a free and public data set that is available via the Earth System Grid Foundation (which also host the simulations from CMIP) and via the Pangeo project.

This repository serves to document the analysis scripts used in the JAMES introduction paper and its corrigendum. It also documents 
the cmorizing of TRACMIP data which was needed to include TRACMIP in the ESGF archive, and it includes information on the simulation
protocal. It is hoped that this will facilitate the use of TRACMIP data and will be helpful if others intedn to run their own TRACMIP-inspired simulations.


The TRACMIP introduction paper was published in 2016 the Journal of Advances in Modelling Earth Systems.
The paper is available at https://agupubs.onlinelibrary.wiley.com/doi/full/10.1002/2016MS000748. Its bibtex reference is:


> @article{https://doi.org/10.1002/2016MS000748,  
> author = {Voigt, Aiko and Biasutti, Michela and Scheff, Jacob and Bader, Jürgen and Bordoni, Simona and Codron, Francis and Dixon, Ross D. and Jonas, Jeffrey and Kang, Sarah M. and Klingaman, Nicholas P. and Leung, Ruby and Lu, Jian and Mapes, Brian and Maroon, Elizabeth A. and McDermid, Sonali and Park, Jong-yeon and Roehrig, Romain and Rose, Brian E. J. and Russell, Gary L. and Seo, Jeongbin and Toniazzo, Thomas and Wei, Ho-Hsuan and Yoshimori, Masakazu and Vargas Zeppetello, Lucas R.},  
> title = {The tropical rain belts with an annual cycle and a continent model intercomparison project: TRACMIP},  
> journal = {Journal of Advances in Modeling Earth Systems},  
> volume = {8},  
> number = {4},  
> pages = {1868-1891},  
> keywords = {rain belts, ITCZ, monsoon, model hierarchy, model intercomparison project},  
> doi = {https://doi.org/10.1002/2016MS000748},  
> url = {https://agupubs.onlinelibrary.wiley.com/doi/abs/10.1002/2016MS000748},  
> eprint = {https://agupubs.onlinelibrary.wiley.com/doi/pdf/10.1002/2016MS000748},  
> year = {2016}  
> }  



The TRACMIP repository includes the following directories:

**ramadda**

Analysis scripts and figures of the 2016 introduction paper in JAMES. The scripts were developed when the TRACMIP data was 
still hosted at the Uni Miami OpenDAP Ramadda server (hence the directory name). As of 2019, the data is no longer hosted at Uni Miami and 
was moved to ESGF and the Pangeo Cloud. The scripts will thus not run out of the box, and the data source would need to be adapted.

**corrigendum-james-2019**

Analysis scripts and figures of the 2019 corrigendum of the introduction paper. The analysis scripts assume that the data
is located on a file server at KIT that hosts a local copy of the ESGF TRACMIP archive. This would need to be adapted if one wanted to run them again.

**sim-protocol**

Scripts and documentation of the TRACMIP simulation protocol, in particular including the definition of the slab ocean q-flux.

**cmorizing**

Scripts and input data used by Aiko Voigt when cmorizing the TRACMIP data using the DKRZ Mistral HPC system.

**pangeo**

Example scripts showing how to access TRACMIP data from the Pangeo cloud, and how to use this to redo a 
couple of figures from the 2016 JAMES introduction paper.

--------------------------------------------

**Known data errors**

- The surface sensible heat flux in LMDZ5A has the wrong sign (negative instead of positive as in the other models). This can be corrected for by multiplying hfss with -1.

