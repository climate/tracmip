# load modules
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.patches import Polygon

# load own modules
import sys
sys.path.append('/home/aiko/Dropbox/Projects/current/Tracmip/analysis/python3/ramadda/mymodules/')
import tracmipmodels as tracmipmodels

# load global mean pr and ts data
file    = np.load('pr_globaltimemean.npz')
pr_aqct = file['pr_aqct']
pr_ldct = file['pr_ldct']
file    = np.load('ts_globaltimemean.npz')
ts_aqct = file['ts_aqct']
ts_ldct = file['ts_ldct']

nmod = pr_aqct.size

# get model colors, names and numbers
modelcolors   = tracmipmodels.get_modelcolors(nmod)
modelnames    = tracmipmodels.get_modelnames()
modelnumbers  = tracmipmodels.get_modelnumbers()

# list of available models
modlist_aqct = tracmipmodels.get_availablemodels('aqct')
modlist_ldct = tracmipmodels.get_availablemodels('ldct')

# plotting
fig = plt.figure( figsize=(12, 4.5), dpi=80, facecolor='w', edgecolor='k' )

ax = plt.subplot(1, 2, 1)
ax.spines['right'].set_color('none')
ax.spines['top'].set_color('none')
ax.xaxis.set_ticks_position('bottom')
ax.yaxis.set_ticks_position('left')
#xfit = np.linspace(286, 310, 100)
#yfit = 2.67 * ( 1 + 0.02*(xfit - (273.15+14.3)) ); plt.plot(xfit, yfit, 'gray', alpha=0.4)
#yfit = 2.67 * ( 1 + 0.03*(xfit - (273.15+14.3)) ); plt.plot(xfit, yfit, 'gray', alpha=0.4)
ax.add_patch(Polygon([[273.15+14.3, 2.67], [273.15+14.3+20, 2.67*(1+0.02*20)], [273.15+14.3+20, 2.67*(1+0.03*20)]], True, alpha=0.4, facecolor='gray', edgecolor='none', linewidth=2))
ax.add_patch(Polygon([[273.15+14.3, 2.67], [273.15+14.3-20, 2.67*(1+0.02*-20)], [273.15+14.3-20, 2.67*(1+0.03*-20)]], True, alpha=0.4, facecolor='gray', edgecolor='none', linewidth=2))
plt.plot(273.15+14.3, 2.67, 'kx', ms=12, mew=3)  
for i in modlist_aqct:
    plt.text(ts_aqct[i], pr_aqct[i], modelnumbers[i], ha='center', va='center', color=modelcolors[i], fontsize=14, fontweight='normal')
for i in modlist_ldct:   
    plt.text(ts_ldct[i], pr_ldct[i], modelnumbers[i], ha='center', va='center', color=modelcolors[i], fontsize=14, fontweight='normal')
    plt.text(ts_ldct[i], pr_ldct[i]-0.06, '_', ha='center', va='bottom', color=modelcolors[i], fontsize=14, fontweight='normal')
plt.xlim(287, 302), plt.ylim(2.6, 4.5)
plt.xlabel('Global surface temperature (K)')
plt.ylabel('Global precipitation (mm/day)')
plt.text(302, 3.75, '2-3%/K', fontsize=12, rotation=-90, fontstyle='italic'  )
plt.text(288.2, 2.65, 'present-day', fontsize=12, fontstyle='italic')

ax = plt.subplot(1, 2, 2)
plt.xlim(0, 1), plt.ylim(0, 1)
plt.axis('off')
for m in modlist_aqct:
    plt.text(0.0, 1.06-0.08*np.float(modelnumbers[m]), modelnumbers[m], color=modelcolors[m], fontsize=14)
    plt.text(0.1, 1.06-0.08*np.float(modelnumbers[m]), modelnames[m], color=modelcolors[m], fontsize=14)

plt.tight_layout

plt.savefig('figs/pr_globalmean_AquaControl_LandControl.pdf')

print(pr_aqct[modlist_aqct])
print(np.min(pr_aqct[modlist_aqct]), np.max(pr_aqct[modlist_aqct]), np.median(pr_aqct[modlist_aqct]))

plt.show()
