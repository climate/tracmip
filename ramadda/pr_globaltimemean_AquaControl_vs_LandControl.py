# load modules
import numpy as np
import matplotlib.pyplot as plt

# load own modules
import sys
sys.path.append('/home/aiko/Dropbox/Projects/current/Tracmip/analysis/python3/ramadda/mymodules/')
sys.path.append('/home/aiko/Dropbox/Python3/mymodules/')
import tracmipmodels as tracmipmodels

# loading data
print('Load global time mean pr')
file    = np.load('pr_globaltimemean.npz')
pr_aqct = file['pr_aqct']
pr_ldct = file['pr_ldct']

nmod = pr_aqct.size

# get model colors, names and numbers
modelcolors   = tracmipmodels.get_modelcolors(nmod)
modelnames    = tracmipmodels.get_modelnames()
modelnumbers  = tracmipmodels.get_modelnumbers()

# get available models
modlist_aqct = tracmipmodels.get_availablemodels('aqct')
modlist_ldct = tracmipmodels.get_availablemodels('ldct')

# plotting
plt.figure( figsize=(12, 4), dpi=80, facecolor='w', edgecolor='k' )

ax = plt.subplot(1, 2, 1)
ax.spines['right'].set_color('none')
ax.spines['top'].set_color('none')
ax.xaxis.set_ticks_position('bottom')
ax.yaxis.set_ticks_position('left')

plt.plot([0, 10], [0, 10], 'k--')
for i in modlist_ldct:
    plt.text(pr_aqct[i], pr_ldct[i], modelnumbers[i], fontweight='bold', color=modelcolors[i], fontsize=14)
plt.title('Global precipitation (mm/day)', fontsize=14)
plt.xlabel('AquaControl', fontsize=12)
plt.ylabel('LandControl', fontsize=12)
plt.xlim(3.2, 4.6), plt.ylim(3.2, 4.6)

ax = plt.subplot(1, 2, 2)
ax.spines['right'].set_color('none')
ax.spines['top'].set_color('none')
ax.xaxis.set_ticks_position('bottom')
ax.yaxis.set_ticks_position('left')

plt.plot([0, 10], [0, 0], 'k--')
for i in modlist_ldct:
    plt.text(pr_aqct[i], (pr_ldct-pr_aqct)[i], modelnumbers[i], fontweight='bold', color=modelcolors[i], fontsize=14)
plt.title('Global precipitation (mm/day)', fontsize=14)
plt.xlabel('AquaControl', fontsize=12)
plt.ylabel('LandControl - AquaControl', fontsize=12)
plt.xlim(3.2, 4.6), plt.ylim(-0.3, 0.3)

plt.tight_layout
plt.savefig('figs/pr_globaltimemean_AquaControl_LandControl.pdf')
    
print(np.nanmedian((pr_ldct-pr_aqct)[modlist_ldct]))    

print((pr_ldct-pr_aqct)[modlist_ldct])  
    
plt.show()
