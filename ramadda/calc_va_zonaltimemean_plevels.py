# load modules
import numpy as np

# load own modules
import sys
sys.path.append('/home/aiko/Dropbox/Projects/current/Tracmip/analysis/python3/ramadda/mymodules/')
#sys.path.append('/home/aiko/Dropbox/Python3/mymodules/')

import loaddata as loaddata

# load data from ramadda server
va_aqct, lev, lat = loaddata.get_data_zmtm_plevels('AQUA'   , 'va')
va_aq4x, _  , _   = loaddata.get_data_zmtm_plevels('AQUACO2', 'va')
va_ldct, _  , _   = loaddata.get_data_zmtm_plevels('LAND'   , 'va')
va_ld4x, _  , _   = loaddata.get_data_zmtm_plevels('LANDCO2', 'va')
va_ldor, _   , _  = loaddata.get_data_zmtm_plevels('LORBIT' , 'va')

# saving data for later use
print('Saving zonal time mean va on pressure levels')
np.savez('va_zonaltimemean_plevels.npz', \
         **{'va_aqct':va_aqct, 'va_aq4x':va_aq4x, 'va_ldct':va_ldct, \
            'va_ld4x':va_ld4x, 'va_ldor':va_ldor, 'lev':lev, 'lat':lat} )
         