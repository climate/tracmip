# load modules
import numpy as np

# load own modules
import sys
sys.path.append('/home/aiko/Dropbox/Projects/current/Tracmip/analysis/python3/ramadda/mymodules/')
#sys.path.append('/home/aiko/Dropbox/Python3/mymodules/')

import loaddata as loaddata

# load data from ramadda server
ua_aqct, lev, lat = loaddata.get_data_zmtm_plevels('AQUA'   , 'ua')
ua_aq4x, _  , _   = loaddata.get_data_zmtm_plevels('AQUACO2', 'ua')
ua_ldct, _  , _   = loaddata.get_data_zmtm_plevels('LAND'   , 'ua')
ua_ld4x, _  , _   = loaddata.get_data_zmtm_plevels('LANDCO2', 'ua')
ua_ldor, _   , _  = loaddata.get_data_zmtm_plevels('LORBIT' , 'ua')

# saving data for later use
print('Saving zonal time mean ua on pressure levels')
np.savez('ua_zonaltimemean_plevels.npz', \
         **{'ua_aqct':ua_aqct, 'ua_aq4x':ua_aq4x, 'ua_ldct':ua_ldct, \
            'ua_ld4x':ua_ld4x, 'ua_ldor':ua_ldor, 'lev':lev, 'lat':lat} )
         