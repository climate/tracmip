# load modules
import numpy as np
import matplotlib.pyplot as plt

# load own modules
import sys
sys.path.append('/home/aiko/Dropbox/Projects/current/Tracmip/analysis/python3/ramadda/mymodules/')
sys.path.append('/home/aiko/Dropbox/Python3/mymodules/')
import tracmipmodels as tracmipmodels
import atmosphere as atm

# loading data
print('Load global time mean ts')
file    = np.load('ts_globaltimemean.npz')
ts_aqct = file['ts_aqct']
ts_aq4x = file['ts_aq4x']
ts_ldct = file['ts_ldct']
ts_ld4x = file['ts_ld4x'] 
ts_ldor = file['ts_ldor'] 

print('Load zonal mean time mean pr')
file    = np.load('pr_zonaltimemean.npz')
pr_aqct = file['pr_aqct']
pr_aq4x = file['pr_aq4x']
pr_ldct = file['pr_ldct']
pr_ld4x = file['pr_ld4x'] 
pr_ldor = file['pr_ldor'] 
lat     = file['lat']

# list of available models
modlist_aqct = tracmipmodels.get_availablemodels('aqct')
modlist_aq4x = tracmipmodels.get_availablemodels('aq4x')
modlist_ldct = tracmipmodels.get_availablemodels('ldct')
modlist_ld4x = tracmipmodels.get_availablemodels('ld4x')
modlist_ldor = tracmipmodels.get_availablemodels('ldor')

nmod = ts_aqct.size

# get model colors, names and numbers
modelcolors   = tracmipmodels.get_modelcolors(nmod)
modelnames    = tracmipmodels.get_modelnames()
modelnumbers  = tracmipmodels.get_modelnumbers()

# calculate itcz
# calculate time-mean ITCZ position
itcz_aqct = np.zeros(nmod)
itcz_aq4x = np.zeros(nmod)
itcz_ldct = np.zeros(nmod)
itcz_ld4x = np.zeros(nmod)
itcz_ldor = np.zeros(nmod)
for i in range(0, nmod):
    itcz_aqct[i] = atm.get_itczposition(pr_aqct[i, :], lat, 30, 0.1)
    itcz_aq4x[i] = atm.get_itczposition(pr_aq4x[i, :], lat, 30, 0.1)
    itcz_ldct[i] = atm.get_itczposition(pr_ldct[i, :], lat, 30, 0.1)
    itcz_ld4x[i] = atm.get_itczposition(pr_ld4x[i, :], lat, 30, 0.1)
    itcz_ldor[i] = atm.get_itczposition(pr_ldor[i, :], lat, 30, 0.1)

# print to screen
c=0
for i in [7, 8, 9, 12, 13, 1, 2, 11, 3, 4, 5, 6, 10, 14]:
    c=c+1
    print(c, '&', modelnames[i], '&', np.round(ts_aqct[i], decimals=1), '/', np.round(itcz_aqct[i], decimals=1), '\degree{} &', \
                                 np.round(ts_ldct[i], decimals=1), '/', np.round(itcz_ldct[i], decimals=1), '\degree{} &', \
                                 np.round(ts_aq4x[i]-ts_aqct[i], decimals=1), '/', np.round(itcz_aq4x[i]-itcz_aqct[i], decimals=1), '\degree{} &', \
                                 np.round(ts_ld4x[i]-ts_ldct[i], decimals=1), '/', np.round(itcz_ld4x[i]-itcz_ldct[i], decimals=1), '\degree{} &', \
                                 np.round(ts_ldor[i]-ts_ldct[i], decimals=1), '/', np.round(itcz_ldor[i]-itcz_ldct[i], decimals=1), '\degree{} \\')
print(' ', '&', 'Model median', np.round(np.nanmedian(ts_aqct[modlist_aqct]), decimals=1), '/', np.round(np.nanmedian(itcz_aqct[modlist_aqct]), decimals=1), '\degree{} &', \
       np.round(np.nanmedian(ts_ldct[modlist_ldct]), decimals=1), '/', np.round(np.nanmedian(itcz_ldct[modlist_ldct]), decimals=1), '\degree{} &', \
       np.round(np.nanmedian((ts_aq4x-ts_aqct)[modlist_aq4x]), decimals=1), '/', np.round(np.nanmedian((itcz_aq4x-itcz_aqct)[modlist_aq4x]), decimals=1), '\degree{} &', \
       np.round(np.nanmedian((ts_ld4x-ts_ldct)[modlist_ld4x]), decimals=1), '/', np.round(np.nanmedian((itcz_ld4x-itcz_ldct)[modlist_ld4x]), decimals=1), '\degree{} &', \
       np.round(np.nanmedian((ts_ldor-ts_ldct)[modlist_ldor]), decimals=1), '/', np.round(np.nanmedian((itcz_ldor-itcz_ldct)[modlist_ldor]), decimals=1), '\degree{} \\')

