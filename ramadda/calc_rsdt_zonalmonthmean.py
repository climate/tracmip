# load modules
import numpy as np

# load own modules
import sys
sys.path.append('/home/aiko/Dropbox/Projects/current/Tracmip/analysis/python3/ramadda/mymodules/')

import loaddata as loaddata

# load data from ramadda server
rsdt_aqct, lat = loaddata.get_data_zmmm('AQUA'   , 'rsdt')
rsdt_aq4x, lat = loaddata.get_data_zmmm('AQUACO2', 'rsdt')
rsdt_ldct, lat = loaddata.get_data_zmmm('LAND'   , 'rsdt')
rsdt_ld4x, lat = loaddata.get_data_zmmm('LANDCO2', 'rsdt')
rsdt_ldor, lat = loaddata.get_data_zmmm('LORBIT' , 'rsdt')

# saving data for later use
print('Saving zonal month mean rsdt')
np.savez('rsdt_zonalmonthmean.npz', \
         **{'rsdt_aqct':rsdt_aqct, 'rsdt_aq4x':rsdt_aq4x, 'rsdt_ldct':rsdt_ldct, \
            'rsdt_ld4x':rsdt_ld4x, 'rsdt_ldor':rsdt_ldor, 'lat':lat})