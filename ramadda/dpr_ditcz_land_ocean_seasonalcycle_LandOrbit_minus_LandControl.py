# load modules
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import cm

# load own modules
import sys
sys.path.append('/home/aiko/Dropbox/Projects/current/Tracmip/analysis/python3/ramadda/mymodules/')
sys.path.append('/home/aiko/Dropbox/Python3/mymodules/')
import tracmipmodels as tracmipmodels
import atmosphere as atm

def make_niceplot(ax, modelname):
    ax.xaxis.set_ticks([-120, -60, 0, 60, 120])
    ax.xaxis.set_ticklabels([''], fontsize=11)
    ax.yaxis.set_ticks([-0.5, 0, 0.5])
    ax.yaxis.set_ticklabels([''], fontsize=11) 
    plt.text(0.03, 0.93, modelname, fontsize=15, ha='left', va='center', \
             transform=ax.transAxes, backgroundcolor='white')
    plt.xlim(-175, 175), plt.ylim(-0.6, 0.6)         

# load pr data
file    = np.load('pr_latlonmonthmean.npz')
lat     = file['lat']
lon     = file['lon']
pr_ldct = file['pr_ldct']
pr_ldor = file['pr_ldor']

nmod = pr_ldct[:, 0, 0, 0].size
nlat = lat.size
nlon = lon.size

print('Number of available models:', nmod)

# list of available models
modlist_ldct = tracmipmodels.get_availablemodels('ldct')
modlist_ldor = tracmipmodels.get_availablemodels('ldor')

month  = np.array([1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12])
nmonth = month.size

sinlat = np.sin(lat*np.pi/180)

# get model colors, names and numbers
modelcolors   = tracmipmodels.get_modelcolors(nmod)
modelnames    = tracmipmodels.get_modelnames()
modelnumbers  = tracmipmodels.get_modelnumbers()

# precip over land and ocean separately
#print(lon[90:112])
pr_land_ldct = np.nanmedian(pr_ldct[:, :, :, 90:112], axis=3)
pr_land_ldor = np.nanmedian(pr_ldor[:, :, :, 90:112], axis=3)
pr_ocea_ldct = np.nanmedian(np.concatenate((pr_ldct[:, :, :, 0:90], pr_ldct[:, :, :, 112:]), axis=3), axis=3)
pr_ocea_ldor = np.nanmedian(np.concatenate((pr_ldor[:, :, :, 0:90], pr_ldor[:, :, :, 112:]), axis=3), axis=3)

# itcz position for precip averaged over land and ocean
itcz_land_ldct = np.zeros((nmod, nmonth)) + np.nan
itcz_land_ldor = np.zeros((nmod, nmonth)) + np.nan
itcz_ocea_ldct = np.zeros((nmod, nmonth)) + np.nan
itcz_ocea_ldor = np.zeros((nmod, nmonth)) + np.nan
for m in range(0, nmod):
    for i in range(0, nmonth):
        itcz_land_ldct[m, i] = atm.get_itczposition(pr_land_ldct[m, i, :], lat, 30.0, 0.1)
        itcz_land_ldor[m, i] = atm.get_itczposition(pr_land_ldor[m, i, :], lat, 30.0, 0.1)
        itcz_ocea_ldct[m, i] = atm.get_itczposition(pr_ocea_ldct[m, i, :], lat, 30.0, 0.1)
        itcz_ocea_ldor[m, i] = atm.get_itczposition(pr_ocea_ldor[m, i, :], lat, 30.0, 0.1)
 
# plotting
plt.figure(figsize=(12, 12), dpi=80, facecolor='w', edgecolor='k')
clev = np.array([-1.8, -1.4, -1.0, -0.6, -0.2, 0.2, 0.6, 1.0, 1.4, 1.8])

ax = plt.subplot(3, 2, 1)
c = plt.contourf(month, sinlat, np.transpose(np.nanmedian((pr_ocea_ldor-pr_ocea_ldct)[modlist_ldor], axis=0)), clev, extend='both', cmap=cm.BrBG)
plt.plot([-200, 200], [0, 0], 'k--')
plt.plot(month, np.sin(np.nanmedian(itcz_ocea_ldor, axis=0)*np.pi/180), 'k', linewidth=3)
plt.title('Over ocean', fontsize=14)
plt.xlim(1, 12), plt.ylim(-0.6, 0.6)
ax.xaxis.set_ticks(month)
ax.xaxis.set_ticklabels(['Jan', '', '', 'Apr', '', '', 'Jul', '', '' ,'Oct', '', ''], fontsize=10)
ax.yaxis.set_ticks([-0.5, 0, 0.5])
ax.yaxis.set_ticklabels(['30S', 'Eq', '30N'], fontsize=10) 
plt.text(0.02, 0.92, 'a)', fontsize=14, ha='left', va='center', transform=ax.transAxes, backgroundcolor='white')

ax = plt.subplot(3, 2, 5)
ax.axis('off')
cbar = plt.colorbar(c, ticks=[-1.8, -1.0, -0.2, 0.2, 1.0, 1.8], orientation='vertical', aspect=30)
cbar.ax.tick_params(labelsize=10)
ax.text(1, 1, 'mm/day', fontsize=10)

ax = plt.subplot(3, 2, 2)
plt.contourf(month, sinlat, np.transpose(np.nanmedian((pr_land_ldor-pr_land_ldct)[modlist_ldor], axis=0)), clev, extend='both', cmap=cm.BrBG)
plt.plot([-200, 200], [0, 0], 'k--')
plt.plot(month, np.sin(np.nanmedian(itcz_land_ldct, axis=0)*np.pi/180), 'k', linewidth=3)
plt.title('Over land', fontsize=14)
plt.xlim(1, 12), plt.ylim(-0.6, 0.6)
ax.xaxis.set_ticks(month)
ax.xaxis.set_ticklabels(['Jan', '', '', 'Apr', '', '', 'Jul', '', '' ,'Oct', '', ''], fontsize=10)
ax.yaxis.set_ticks([-0.5, 0, 0.5])
ax.yaxis.set_ticklabels(['30S', 'Eq', '30N'], fontsize=10) 
plt.text(0.02, 0.92, 'b)', fontsize=14, ha='left', va='center', transform=ax.transAxes, backgroundcolor='white')

ax = plt.subplot(3, 2, 3)
ax.spines['right'].set_color('none')
ax.spines['top'].set_color('none')
ax.xaxis.set_ticks_position('bottom')
ax.yaxis.set_ticks_position('left')

plt.plot([-100, 100], [0, 0], 'k--')
for m in modlist_ldor:
    plt.plot(month, (itcz_ocea_ldor-itcz_ocea_ldct)[m,:], color=modelcolors[m])
plt.plot(month, np.nanmedian((itcz_ocea_ldor-itcz_ocea_ldct)[modlist_ldor], axis=0), 'k', linewidth=3)
plt.xlabel('month')
plt.ylabel('ITCZ shift (deg lat)')
plt.xlim(1, 12), plt.ylim(-3, 3)
ax.xaxis.set_ticks(month)
ax.xaxis.set_ticklabels(['Jan', '', '', 'Apr', '', '', 'Jul', '', '' ,'Oct', '', ''], fontsize=10)
ax.yaxis.set_ticks([-3, -2, -1, 0, 1, 2, 3])
ax.yaxis.set_ticklabels([-3, -2, -1, 0, 1, 2, 3], fontsize=10) 
plt.text(0.02, 0.95, 'c)', fontsize=14, ha='left', va='center', transform=ax.transAxes, backgroundcolor='white')

ax = plt.subplot(3, 2, 4)
ax.spines['right'].set_color('none')
ax.spines['top'].set_color('none')
ax.xaxis.set_ticks_position('bottom')
ax.yaxis.set_ticks_position('left')

plt.plot([-100, 100], [0, 0], 'k--')
for m in modlist_ldor:
    plt.plot(month, (itcz_land_ldor-itcz_land_ldct)[m,:], color=modelcolors[m])
plt.plot(month, np.nanmedian((itcz_land_ldor-itcz_land_ldct)[modlist_ldor], axis=0), 'k', linewidth=3)
plt.xlabel('month')
plt.xlim(1, 12), plt.ylim(-3, 3)
ax.xaxis.set_ticks(month)
ax.xaxis.set_ticklabels(['Jan', '', '', 'Apr', '', '', 'Jul', '', '' ,'Oct', '', ''], fontsize=10)
ax.yaxis.set_ticks([-3, -2, -1, 0, 1, 2, 3])
ax.yaxis.set_ticklabels([-3, -2, -1, 0, 1, 2, 3], fontsize=10) 
plt.text(0.02, 0.95, 'd)', fontsize=14, ha='left', va='center', transform=ax.transAxes, backgroundcolor='white')

#plt.subplots_adjust(wspace=0.04, hspace=0.05)  
plt.tight_layout

plt.savefig('figs/dpr_ditcz_land_ocean_seasonalcycle_LandOrbit_minus_LandControl.pdf')

# time mean ITCZ over land and ocean
# itcz position for precip averaged over land and ocean
itcz_tm_land_ldct = np.zeros(nmod) + np.nan
itcz_tm_land_ldor = np.zeros(nmod) + np.nan
itcz_tm_ocea_ldct = np.zeros(nmod) + np.nan
itcz_tm_ocea_ldor = np.zeros(nmod) + np.nan
for m in range(0, nmod):
    itcz_tm_land_ldct[m] = atm.get_itczposition(np.nanmean(pr_land_ldct[m, :, :], axis=0), lat, 30.0, 0.1)
    itcz_tm_land_ldor[m] = atm.get_itczposition(np.nanmean(pr_land_ldor[m, :, :], axis=0), lat, 30.0, 0.1)
    itcz_tm_ocea_ldct[m] = atm.get_itczposition(np.nanmean(pr_ocea_ldct[m, :, :], axis=0), lat, 30.0, 0.1)
    itcz_tm_ocea_ldor[m] = atm.get_itczposition(np.nanmean(pr_ocea_ldor[m, :, :], axis=0), lat, 30.0, 0.1)    

print(np.nanmedian((itcz_tm_ocea_ldor-itcz_tm_ocea_ldct)[modlist_ldor]))
print(np.nanmax((itcz_tm_ocea_ldor-itcz_tm_ocea_ldct)[modlist_ldor]))
print(np.nanmin((itcz_tm_ocea_ldor-itcz_tm_ocea_ldct)[modlist_ldor]))

print(np.nanmedian((itcz_tm_land_ldor-itcz_tm_land_ldct)[modlist_ldor]))
print(np.nanmax((itcz_tm_land_ldor-itcz_tm_land_ldct)[modlist_ldor]))
print(np.nanmin((itcz_tm_land_ldor-itcz_tm_land_ldct)[modlist_ldor]))

#print('Diagnostics for cam3')
#print('itcz_ocea_ldct:', itcz_ocea_ldct[8, :])
#print('itcz_land_ldct:', itcz_land_ldct[8, :])
#print('itcz_ocea_ldor:', itcz_ocea_ldor[8, :])
#print('itcz_land_ldor:', itcz_land_ldor[8, :])
#print(np.nanmean(pr_land_ldct[8, :, :], axis=0))
#print(pr_ldct[8, 0, :, :])

#print('Diagnostics for cam4')
#print('itcz_ocea_ldct:', itcz_ocea_ldct[9, :])
#print('itcz_land_ldct:', itcz_land_ldct[9, :])
#print('itcz_ocea_ldor:', itcz_ocea_ldor[9, :])
#print('itcz_land_ldor:', itcz_land_ldor[9, :])
#print(np.nanmean(pr_land_ldct[9, :, :], axis=0))
#print(pr_ldct[9, 0, :, :])


plt.show()
