# load modules
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import cm

# load own modules
import sys
sys.path.append('/home/aiko/Dropbox/Projects/current/Tracmip/analysis/python3/ramadda/mymodules/')
sys.path.append('/home/aiko/Dropbox/Python3/mymodules/')
import atmosphere as atm
import tracmipmodels as tracmipmodels

# load data
file       = np.load('ts_zonaltimemean.npz')
lat        = file['lat']
ts_aqct_tm = file['ts_aqct']
ts_aq4x_tm = file['ts_aq4x']
file       = np.load('pr_zonaltimemean.npz')
pr_aqct_tm = file['pr_aqct']
pr_aq4x_tm = file['pr_aq4x']
file       = np.load('pr_zonalmonthmean.npz')
pr_aqct_mm = file['pr_aqct']
pr_aq4x_mm = file['pr_aq4x']
file       = np.load('ua_zonaltimemean_plevels.npz')
lev        = file['lev']
ua_aqct    = file['ua_aqct']
ua_aq4x    = file['ua_aq4x']
file       = np.load('va_zonaltimemean_plevels.npz')
va_aqct    = file['va_aqct']
va_aq4x    = file['va_aq4x']

nmod   = ts_aqct_tm[:, 0].size
month  = np.array([1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12])
nmonth = month.size
nlat   = lat.size

sinlat = np.sin(lat*np.pi/180)

# get model colors, names and numbers
modelcolors   = tracmipmodels.get_modelcolors(nmod)
modelnames    = tracmipmodels.get_modelnames()
modelnumbers  = tracmipmodels.get_modelnumbers()

# analysis
# calculate time-mean ITCZ position
itcz_aqct_tm = np.zeros(nmod)
itcz_aq4x_tm = np.zeros(nmod)
for i in range(0, nmod):
    itcz_aqct_tm[i] = atm.get_itczposition(pr_aqct_tm[i, :], lat, 30, 0.1)
    itcz_aq4x_tm[i] = atm.get_itczposition(pr_aq4x_tm[i, :], lat, 30, 0.1)
    
# calculate monthly-mean ITCZ position
itcz_aqct_mm = np.zeros((nmod, 12))
itcz_aq4x_mm = np.zeros((nmod, 12))
for i in range(0, nmod):
    for t in range(0, nmonth):
        itcz_aqct_mm[i, t] = atm.get_itczposition(pr_aqct_mm[i, t, :], lat, 30, 0.1)
        itcz_aq4x_mm[i, t] = atm.get_itczposition(pr_aq4x_mm[i, t, :], lat, 30, 0.1)

# mass stream function
msf_aqct = np.zeros(va_aqct.shape) + np.nan
msf_aq4x = np.zeros(va_aqct.shape) + np.nan
for m in range(0, nmod):
    msf_aqct[m, :, :] = atm.get_massstreamfunction(va_aqct[m, :, :], lev, lat)
    msf_aq4x[m, :, :] = atm.get_massstreamfunction(va_aq4x[m, :, :], lev, lat)

# plotting
fig = plt.figure( figsize=(12, 12), dpi=80, facecolor='w', edgecolor='k' )

ax = plt.subplot(3, 2, 1)
ax.spines['right'].set_color('none')
ax.spines['top'].set_color('none')
ax.xaxis.set_ticks_position('bottom')
ax.yaxis.set_ticks_position('left')

for m in range(0, nmod):
    plt.plot(sinlat, (ts_aq4x_tm-ts_aqct_tm)[m, :], color=modelcolors[m])
plt.plot(sinlat, np.nanmedian(ts_aq4x_tm-ts_aqct_tm, axis=0), 'k', linewidth=3)
plt.xlim(-0.98, 0.98), plt.ylim(0, 16)
ax.xaxis.set_ticks([-0.866, -0.5, 0, 0.5, 0.866])
ax.xaxis.set_ticklabels(['60S', '30S', 'Eq', '30N', '60N'], fontsize=10)
ax.yaxis.set_ticks([0, 4, 8, 12, 16])
ax.yaxis.set_ticklabels([0, 4, 8, 12, 16], fontsize=10) 
plt.title('Surface temperature', fontsize=14)
plt.ylabel('K', fontsize=12)
plt.text(0.02, 0.92, 'a)', fontsize=14, ha='left', va='center', transform=ax.transAxes)
   
ax = plt.subplot(3, 2, 2)
ax.spines['right'].set_color('none')
ax.spines['top'].set_color('none')
ax.xaxis.set_ticks_position('bottom')
ax.yaxis.set_ticks_position('left')

for m in range(0, nmod):
    plt.plot(sinlat, (pr_aq4x_tm-pr_aqct_tm)[m, :], color=modelcolors[m])
plt.plot(sinlat, np.nanmedian(pr_aq4x_tm-pr_aqct_tm, axis=0), 'k', linewidth=3)   
plt.xlim(-0.98, 0.98), plt.ylim(-8, 8)
ax.xaxis.set_ticks([-0.866, -0.5, 0, 0.5, 0.866])
ax.xaxis.set_ticklabels(['60S', '30S', 'Eq', '30N', '60N'], fontsize=10)
ax.yaxis.set_ticks([-8, -4, 0, 4, 8])
ax.yaxis.set_ticklabels([-8, -4, 0, 4, 8], fontsize=10) 
plt.title('Precipitation', fontsize=14)
plt.ylabel('mm/day', fontsize=12)
plt.text(0.02, 0.92, 'b)', fontsize=14, ha='left', va='center', transform=ax.transAxes)

ax = plt.subplot(3, 2, 3)
ax.spines['right'].set_color('none')
ax.spines['top'].set_color('none')
ax.xaxis.set_ticks_position('bottom')
ax.yaxis.set_ticks_position('left')

plt.plot([-20, 20], [0, 0], 'k--')
for m in range(0, nmod):
    plt.plot(month, (itcz_aq4x_mm-itcz_aqct_mm)[m, :], color=modelcolors[m])
plt.plot(month, np.nanmedian(itcz_aq4x_mm-itcz_aqct_mm, axis=0), 'k', linewidth=3)
plt.xlim(1, 12), plt.ylim(-2, 16)
ax.xaxis.set_ticks(month)
ax.xaxis.set_ticklabels(['Jan', '', '', 'Apr', '', '', 'Jul', '', '' ,'Oct', '', ''], fontsize=10)
ax.yaxis.set_ticks([-2, 0, 5, 10, 15])
ax.yaxis.set_ticklabels([-2, 0, 5, 10, 15], fontsize=10) 
plt.title('ITCZ position', fontsize=14)
plt.xlabel('month', fontsize=12)
plt.ylabel('deg lat', fontsize=12)
plt.text(0.02, 0.92, 'c)', fontsize=14, ha='left', va='center', transform=ax.transAxes)
   
ax = plt.subplot(3, 2, 4)
ax.xaxis.set_ticks_position('bottom')
ax.yaxis.set_ticks_position('left')   

cu   = np.array([-8, -7, -6, -5, -4, -3, -2, -1, 1, 2, 3, 4, 5, 6, 7, 8])
cmsf = np.array([-55, -45, -35, -25, -15, -5, 5, 15, 25, 35, 45, 55])
c=plt.contourf(sinlat, lev/1e2, np.nanmedian(ua_aq4x-ua_aqct, axis=0), cu, cmap=cm.RdBu_r, extend='both')
plt.contour(sinlat, lev/1e2, np.nanmedian(msf_aq4x-msf_aqct, axis=0), cmsf, colors='k')
plt.xlim(-0.98, 0.98), plt.ylim(1000, 10)
ax.xaxis.set_ticks([-0.866, -0.5, 0, 0.5, 0.866])
ax.xaxis.set_ticklabels(['60S', '30S', 'Eq', '30N', '60N'], fontsize=10)
ax.yaxis.set_ticks([10, 200, 600, 1000])
ax.yaxis.set_ticklabels([10, 200, 600, 1000], fontsize=10)
plt.title('Zonal wind and mass stream function', fontsize=14)
plt.ylabel('hPa', fontsize=12)
plt.text(0.02, 0.938, 'd)', fontsize=14, ha='left', va='center', transform=ax.transAxes, backgroundcolor='white')
  
ax = plt.subplot(3, 2, 6)
ax.axis('off')
cbar = plt.colorbar(c, ticks=[-8, -6, -4, -2, 0, 2, 4, 6, 8], 
             orientation='vertical', fraction=0.09)
cbar.ax.tick_params(labelsize=10) 
plt.text(1, 0, 'm/s', fontsize=10, ha='left', va='center', transform=ax.transAxes)

  
plt.tight_layout
plt.savefig('figs/ts_pr_itcz_ua_msf_Aqua4xCO2_minus_AquaControl.pdf')

# itcz
print(np.nanmedian(itcz_aq4x_tm-itcz_aqct_tm))
print(np.nanmax(itcz_aq4x_tm-itcz_aqct_tm))
print(np.nanmin(itcz_aq4x_tm-itcz_aqct_tm))

# Hadley cell strength
# annual-mean Hadley cell strength
hc_nh = np.max(np.max((msf_aq4x-msf_aqct)[:, 2:15, :], axis=2), axis=1)
hc_sh = np.min(np.min((msf_aq4x-msf_aqct)[:, 2:15, :], axis=2), axis=1)
print(np.nanmax(hc_nh), np.nanmin(hc_nh), np.nanmedian(hc_nh))
print(np.nanmax(hc_sh), np.nanmin(hc_sh), np.nanmedian(hc_sh))  

# annual-mean jet position
# 850 hPa is lev = 14
jetlat_nh_aqct = np.zeros(nmod) + np.nan
jetlat_sh_aqct = np.zeros(nmod) + np.nan
jetlat_nh_aq4x = np.zeros(nmod) + np.nan
jetlat_sh_aq4x = np.zeros(nmod) + np.nan
for m in range(0, nmod):
    jetlat_nh_aqct[m], jetlat_sh_aqct[m] = atm.get_eddyjetlat(ua_aqct[m, 14, :], lat)
    jetlat_nh_aq4x[m], jetlat_sh_aq4x[m] = atm.get_eddyjetlat(ua_aq4x[m, 14, :], lat)
print(np.nanmax(jetlat_nh_aq4x-jetlat_nh_aqct), np.nanmin(jetlat_nh_aq4x-jetlat_nh_aqct), np.nanmedian(jetlat_nh_aq4x-jetlat_nh_aqct))
print(np.nanmax(jetlat_sh_aq4x-jetlat_sh_aqct), np.nanmin(jetlat_sh_aq4x-jetlat_sh_aqct), np.nanmedian(jetlat_sh_aq4x-jetlat_sh_aqct))

plt.show()
