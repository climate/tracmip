# load modules
import numpy as np

# load own modules
import sys
sys.path.append('/home/aiko/Dropbox/Projects/current/Tracmip/analysis/python3/ramadda/mymodules/')

import loaddata as loaddata

# load data from ramadda server
rsdt_aqct, lat, lon = loaddata.get_data_latlonmm('AQUA'   , 'rsdt')
rsdt_aq4x, lat, lon = loaddata.get_data_latlonmm('AQUACO2', 'rsdt')
rsdt_ldct, lat, lon = loaddata.get_data_latlonmm('LAND'   , 'rsdt')
rsdt_ld4x, lat, lon = loaddata.get_data_latlonmm('LANDCO2', 'rsdt')
rsdt_ldor, lat, lon = loaddata.get_data_latlonmm('LORBIT' , 'rsdt')

# saving data for later use
print('Saving lat-lon month mean rsdt')
np.savez('rsdt_latlonmonthmean.npz', \
         **{'rsdt_aqct':rsdt_aqct, 'rsdt_aq4x':rsdt_aq4x, 'rsdt_ldct':rsdt_ldct, \
            'rsdt_ld4x':rsdt_ld4x, 'rsdt_ldor':rsdt_ldor, 'lat':lat, 'lon':lon})