# load modules
import numpy as np

# load own modules
import sys
sys.path.append('/home/aiko/Dropbox/Projects/current/Tracmip/analysis/python3/ramadda/mymodules/')

import loaddata as loaddata

# load data from ramadda server
ts_aqct, lat, lon = loaddata.get_data_latlontm('AQUA'   , 'ts')
ts_aq4x, lat, lon = loaddata.get_data_latlontm('AQUACO2', 'ts')
ts_ldct, lat, lon = loaddata.get_data_latlontm('LAND'   , 'ts')
ts_ld4x, lat, lon = loaddata.get_data_latlontm('LANDCO2', 'ts')
ts_ldor, lat, lon = loaddata.get_data_latlontm('LORBIT' , 'ts')

# saving data for later use
print('Saving lat-lon time mean ts')
np.savez('ts_latlontimemean.npz', \
         **{'ts_aqct':ts_aqct, 'ts_aq4x':ts_aq4x, 'ts_ldct':ts_ldct, \
            'ts_ld4x':ts_ld4x, 'ts_ldor':ts_ldor, 'lat':lat, 'lon':lon})
