# load modules
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import cm

# load own modules
import sys
sys.path.append('/home/aiko/Dropbox/Python3/mymodules/')
sys.path.append('/home/aiko/Dropbox/Projects/current/Tracmip/analysis/python3/ramadda/mymodules/')
import tracmipmodels as tracmipmodels
import atmosphere as atm

def make_nice_plot(ax, title):
    plt.xlim(-0.98, 0.98), plt.ylim(1000, 10)
    ax.xaxis.set_ticks([-0.866, -0.5, 0, 0.5, 0.866])
    ax.xaxis.set_ticklabels([''], fontsize=10)
    ax.yaxis.set_ticks([10, 200, 600, 1000])
    ax.yaxis.set_ticklabels([''],fontsize=10)
    plt.title(title, fontsize=14)

# load zonal and meridional wind data
file    = np.load('ua_zonaltimemean_plevels.npz')
lev     = file['lev']
lat     = file['lat']
ua_aqct = file['ua_aqct']
ua_aq4x = file['ua_aq4x']
ua_ldct = file['ua_ldct']
ua_ld4x = file['ua_ld4x']
ua_ldor = file['ua_ldor']

file    = np.load('va_zonaltimemean_plevels.npz')
va_aqct = file['va_aqct']
va_aq4x = file['va_aq4x']
va_ldct = file['va_ldct']
va_ld4x = file['va_ld4x']
va_ldor = file['va_ldor']

nmod = ua_aqct[:, 0, 0].size
sinlat = np.sin(lat*np.pi/180)

# list of available models
modlist_aqct = tracmipmodels.get_availablemodels('aqct')
modlist_aq4x = tracmipmodels.get_availablemodels('aq4x')
modlist_ldct = tracmipmodels.get_availablemodels('ldct')
modlist_ld4x = tracmipmodels.get_availablemodels('ld4x')
modlist_ldor = tracmipmodels.get_availablemodels('ldor')

# model names
modnames = tracmipmodels.get_modelnames()

# mass stream function
msf_aqct = np.zeros(va_aqct.shape) + np.nan
msf_aq4x = np.zeros(va_aqct.shape) + np.nan
msf_ldct = np.zeros(va_aqct.shape) + np.nan
msf_ld4x = np.zeros(va_aqct.shape) + np.nan
msf_ldor = np.zeros(va_aqct.shape) + np.nan

for m in modlist_aqct:
    msf_aqct[m, :, :] = atm.get_massstreamfunction(va_aqct[m, :, :], lev, lat)
for m in modlist_aq4x:
    msf_aq4x[m, :, :] = atm.get_massstreamfunction(va_aq4x[m, :, :], lev, lat)
for m in modlist_ldct:
    msf_ldct[m, :, :] = atm.get_massstreamfunction(va_ldct[m, :, :], lev, lat)
for m in modlist_ld4x:
    msf_ld4x[m, :, :] = atm.get_massstreamfunction(va_ld4x[m, :, :], lev, lat)
for m in modlist_ldor:
    msf_ldor[m, :, :] = atm.get_massstreamfunction(va_ldor[m, :, :], lev, lat)
    
# ploting
for m in range(0, nmod):
    plt.subplot(4, 4, m+1)    
    c=plt.contourf(lat, lev, ua_aqct[m, :, :], np.linspace(-100, 100, 100))
    plt.colorbar(c)    
    plt.title(modnames[m])

plt.figure( figsize=(12, 12), dpi=80, facecolor='w', edgecolor='k')

cu   = np.array([-55, -45, -35, -25, -15, -5,\
                   5,  15, 25, 35, 45, 55])
cmsf = np.array([-170, -150, -130, -110, -90, -70, -50, -30, -10, 10, 30, 50, 70, 90, 110, 130, 150, 170])

ax = plt.subplot(3, 2, 1)
plt.contourf(sinlat, lev/1e2, np.nanmedian(ua_aqct[modlist_aqct], axis=0), cu, cmap=cm.RdBu_r, extend='both')
plt.contour(sinlat, lev/1e2, np.nanmedian(msf_aqct[modlist_aqct], axis=0), cmsf, colors='k')
make_nice_plot(ax, 'AquaControl'), plt.ylabel('hPa', fontsize=12)
ax.yaxis.set_ticklabels([10, 200, 600, 1000], fontsize=10)
plt.text(0.02, 0.93, 'a)', fontsize=14, ha='left', va='center', transform=ax.transAxes, backgroundcolor='white')

ax = plt.subplot(3, 2, 3)
c=plt.contourf(sinlat, lev/1e2, np.nanmedian(ua_aq4x[modlist_aq4x], axis=0), cu, cmap=cm.RdBu_r, extend='both')
plt.contour(sinlat, lev/1e2, np.nanmedian(msf_aq4x[modlist_aq4x], axis=0), cmsf, colors='k')
make_nice_plot(ax, 'Aqua4xCO2'), plt.ylabel('hPa', fontsize=12)
ax.yaxis.set_ticklabels([10, 200, 600, 1000], fontsize=10)
ax.xaxis.set_ticklabels(['60S', '30S', 'Eq', '30N', '60N'], fontsize=10)
plt.text(0.02, 0.93, 'c)', fontsize=14, ha='left', va='center', transform=ax.transAxes, backgroundcolor='white')

ax = plt.subplot(3, 2, 5)
ax.axis('off')
cbar = plt.colorbar(c, ticks=[-50, -40, -30, -20, -10, 0, 10, 20, 30, 40, 50], 
             orientation='horizontal', aspect=30)
cbar.ax.tick_params(labelsize=10) 
plt.text(0, -0.2, 'm/s', fontsize=10, ha='right', va='center', transform=ax.transAxes)

ax = plt.subplot(3, 2, 2)
plt.contourf(sinlat, lev/1e2, np.nanmedian(ua_ldct[modlist_ldct], axis=0), cu, cmap=cm.RdBu_r, extend='both')
plt.contour(sinlat, lev/1e2, np.nanmedian(msf_ldct[modlist_ldct], axis=0), cmsf, colors='k')
make_nice_plot(ax, 'LandControl')
plt.text(0.02, 0.93, 'b)', fontsize=14, ha='left', va='center', transform=ax.transAxes, backgroundcolor='white')

ax = plt.subplot(3, 2, 4)
plt.contourf(sinlat, lev/1e2, np.nanmedian(ua_ld4x[modlist_ld4x], axis=0), cu, cmap=cm.RdBu_r, extend='both')
plt.contour(sinlat, lev/1e2, np.nanmedian(msf_ld4x[modlist_ld4x], axis=0), cmsf, colors='k')
make_nice_plot(ax, 'Land4xCO2')
plt.text(0.02, 0.93, 'd)', fontsize=14, ha='left', va='center', transform=ax.transAxes, backgroundcolor='white')

ax = plt.subplot(3, 2, 6)
plt.contourf(sinlat, lev/1e2, np.nanmedian(ua_ldor[modlist_ldor], axis=0), cu, cmap=cm.RdBu_r, extend='both')
plt.contour(sinlat, lev/1e2, np.nanmedian(msf_ldor[modlist_ldor], axis=0), cmsf, colors='k')
make_nice_plot(ax, 'LandOrbit'), plt.ylabel('hPa', fontsize=12)
ax.yaxis.set_ticklabels([10, 200, 600, 1000], fontsize=10)
ax.xaxis.set_ticklabels(['60S', '30S', 'Eq', '30N', '60N'], fontsize=10)
plt.text(0.02, 0.93, 'e)', fontsize=14, ha='left', va='center', transform=ax.transAxes, backgroundcolor='white')

plt.tight_layout 

plt.savefig('figs/ua_msf_modelmedian_allexperiments_zonaltimemean.pdf')                      

# annual-mean Hadley cell strength
hc_nh_aqct = np.max(np.max(msf_aqct[modlist_aqct, 2:15, :], axis=2), axis=1)
hc_sh_aqct = np.min(np.min(msf_aqct[modlist_aqct, 2:15, :], axis=2), axis=1)
print(hc_nh_aqct, np.nanmin(hc_nh_aqct), np.nanmax(hc_nh_aqct), np.nanmedian(hc_nh_aqct))
print(hc_sh_aqct, np.nanmin(hc_sh_aqct), np.nanmax(hc_sh_aqct), np.nanmedian(hc_sh_aqct))

# annual-mean jet position
# 850 hPa is lev = 14
jetlat_nh_aqct = np.zeros(nmod) + np.nan
jetlat_sh_aqct = np.zeros(nmod) + np.nan
for m in modlist_aqct:
    jetlat_nh_aqct[m], jetlat_sh_aqct[m] = atm.get_eddyjetlat(ua_aqct[m, 14, :], lat)
print(jetlat_nh_aqct, np.nanmedian(jetlat_nh_aqct))
print(jetlat_sh_aqct, np.nanmedian(jetlat_sh_aqct))

plt.show()
