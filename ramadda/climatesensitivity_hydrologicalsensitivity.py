# load modules
import numpy as np
import matplotlib.pyplot as plt
import scipy.stats as spstats

# load own modules
import sys
#sys.path.append('/home/aiko/Dropbox/Projects/current/Tracmip/analysis/python3/ramadda/mymodules/')
#sys.path.append('/home/aiko/Dropbox/Python3/mymodules/')
sys.path.append('/home/fd8940/Dropbox/Projects/current/Tracmip/analysis/python3/ramadda/mymodules/')
sys.path.append('/home/fd8940/Dropbox/Python3/mymodules/')
import tracmipmodels as tracmipmodels

# loading data
print('Load global time mean ts and pr')
file    = np.load('ts_globaltimemean.npz')
ts_aqct = file['ts_aqct']
ts_aq4x = file['ts_aq4x']
ts_ldct = file['ts_ldct']
ts_ld4x = file['ts_ld4x'] 
ts_ldor = file['ts_ldor'] 
file    = np.load('pr_globaltimemean.npz')
pr_aqct = file['pr_aqct']
pr_aq4x = file['pr_aq4x']
pr_ldct = file['pr_ldct']
pr_ld4x = file['pr_ld4x'] 
pr_ldor = file['pr_ldor'] 

nmod = ts_aqct.size

# get model colors, names and numbers
modelcolors   = tracmipmodels.get_modelcolors(nmod)
modelnames    = tracmipmodels.get_modelnames()
modelnumbers  = tracmipmodels.get_modelnumbers()

# list of available models
modlist_aqct  = tracmipmodels.get_availablemodels('aqct')
modlist_aq4x  = tracmipmodels.get_availablemodels('aq4x')
modlist_ldct  = tracmipmodels.get_availablemodels('ldct')
modlist_ld4x  = tracmipmodels.get_availablemodels('ld4x')

# plotting
plt.figure( figsize=(12, 8), dpi=80, facecolor='w', edgecolor='k' )

ax = plt.subplot(2, 2, 1)
ax.spines['right'].set_color('none')
ax.spines['top'].set_color('none')
ax.xaxis.set_ticks_position('bottom')
ax.yaxis.set_ticks_position('left')

for i in modlist_aq4x:
    plt.text(ts_aqct[i], 0.5*(ts_aq4x - ts_aqct)[i], modelnumbers[i], fontweight='normal', color=modelcolors[i], ha='center', va='center', fontsize=14)
for i in modlist_ld4x:
    plt.text(ts_ldct[i], 0.5*(ts_ld4x - ts_ldct)[i], modelnumbers[i], fontweight='normal', color=modelcolors[i], ha='center', va='center', fontsize=14)
    plt.text(ts_ldct[i], 0.5*(ts_ld4x - ts_ldct)[i] - 0.17, '_', fontweight='normal', color=modelcolors[i], ha='center', va='bottom', fontsize=14)
plt.xlim(288, 302), plt.ylim(0.5, 5.7)
corr, r = spstats.spearmanr(ts_aqct[modlist_aq4x], 0.5*((ts_aq4x - ts_aqct)[modlist_aq4x]))
print(corr, r)
corr, r = spstats.spearmanr(ts_ldct[modlist_ld4x], 0.5*((ts_ld4x - ts_ldct)[modlist_ld4x]))
print(corr, r)
# do aquaplanet fit without mpas model
modlist_aq4x_nompas = [1, 2, 3, 4, 5, 6, 7, 8, 9, 11, 12, 13, 14]
corr, r = spstats.spearmanr(ts_aqct[modlist_aq4x_nompas], 0.5*((ts_aq4x - ts_aqct)[modlist_aq4x_nompas]))
print(corr, r)
plt.text(302, 1.0, 'Aqua: corr=0.31, p=0.27', ha='right', fontsize=12, fontstyle='italic')
plt.text(302, 0.7, 'Land: corr=0.47, p=0.10', ha='right', fontsize=12, fontstyle='italic')
plt.xlabel('Global surface temperature (K)', fontsize=12)
plt.ylabel('Climate sensitivity (K)', fontsize=12)
ax.xaxis.set_ticks([289, 291, 293, 295, 297, 299, 301])
ax.xaxis.set_ticklabels([289, 291, 293, 295, 297, 299, 301], fontsize=10)
ax.yaxis.set_ticks([1, 2, 3, 4, 5])
ax.yaxis.set_ticklabels([1, 2, 3, 4, 5], fontsize=10)
plt.text(0.02, 0.98, 'a)', fontsize=14, ha='left', va='center', transform=ax.transAxes)


ax = plt.subplot(2, 2, 2)
ax.spines['right'].set_color('none')
ax.spines['top'].set_color('none')
ax.xaxis.set_ticks_position('bottom')
ax.yaxis.set_ticks_position('left')

xfit = np.linspace(0, 15, 100)
yfit = 2.2*xfit; plt.plot(xfit, yfit, 'k')
for i in modlist_aq4x:
    plt.text((ts_aq4x-ts_aqct)[i], 100*(pr_aq4x[i] - pr_aqct[i])/pr_aqct[i], modelnumbers[i], fontweight='normal', color=modelcolors[i], ha='center', va='center', fontsize=14)
for i in modlist_ld4x:
    plt.text((ts_ld4x-ts_ldct)[i], 100*((pr_ld4x - pr_ldct)/pr_ldct)[i], modelnumbers[i], fontweight='normal', color=modelcolors[i], ha='center', va='center', fontsize=14)
    plt.text((ts_ld4x-ts_ldct)[i], 100*((pr_ld4x - pr_ldct)/pr_ldct)[i] - 0.7, '_', fontweight='normal', color=modelcolors[i], ha='center', va='bottom', fontsize=14)

plt.xlim(0, 10), plt.ylim(0, 22)

plt.xlabel('Global surface temperature increase (K)', fontsize=12)
plt.ylabel('Precipitation increase (%)', fontsize=12)
ax.xaxis.set_ticks([0, 2, 4, 6, 8, 10])
ax.xaxis.set_ticklabels([0, 2, 4, 6, 8, 10], fontsize=10)
ax.yaxis.set_ticks([0, 5, 10, 15, 20])
ax.yaxis.set_ticklabels([0, 5, 10, 15, 20], fontsize=10)
plt.text(0.02, 0.98, 'b)', fontsize=14, ha='left', va='center', transform=ax.transAxes)

plt.text(8.1, 21, '2.2%/K', fontsize=12, fontstyle='italic', rotation=90/2.5)

ax = plt.subplot(2, 2, 3)
plt.xlim(0, 1), plt.ylim(0, 1)
plt.axis('off')
ystart=1.0
delta=0.0666
for m in modlist_aqct:
    if np.int(modelnumbers[m])<4:
        plt.text(0.1, ystart-delta*np.float(modelnumbers[m]), modelnumbers[m], color=modelcolors[m], fontsize=14)
        plt.text(0.15, ystart-delta*np.float(modelnumbers[m]), modelnames[m], color=modelcolors[m], fontsize=14)
    elif np.int(modelnumbers[m])<7:
        plt.text(0.5, ystart-delta*(np.float(modelnumbers[m])-3), modelnumbers[m], color=modelcolors[m], fontsize=14)
        plt.text(0.55, ystart-delta*(np.float(modelnumbers[m])-3), modelnames[m], color=modelcolors[m], fontsize=14)
    elif np.int(modelnumbers[m])<10:
        plt.text(0.9, ystart-delta*(np.float(modelnumbers[m])-6), modelnumbers[m], color=modelcolors[m], fontsize=14)
        plt.text(0.95, ystart-delta*(np.float(modelnumbers[m])-6), modelnames[m], color=modelcolors[m], fontsize=14)
    elif np.int(modelnumbers[m])<13:
        plt.text(1.3, ystart-delta*(np.float(modelnumbers[m])-9), modelnumbers[m], color=modelcolors[m], fontsize=14)
        plt.text(1.4, ystart-delta*(np.float(modelnumbers[m])-9), modelnames[m], color=modelcolors[m], fontsize=14)
    elif np.int(modelnumbers[m])<15:
        plt.text(1.7, ystart-delta*(np.float(modelnumbers[m])-12), modelnumbers[m], color=modelcolors[m], fontsize=14)
        plt.text(1.8, ystart-delta*(np.float(modelnumbers[m])-12), modelnames[m], color=modelcolors[m], fontsize=14)
#    plt.text(1.75, ystart-delta*3, 'Model median', color='k', fontweight='bold', fontsize=14)

plt.tight_layout()
plt.savefig('figs/climatesensitivity_hydrologicalsensitivity.pdf')

# fit of dpr vs dts under 4xco2
r, _, corr, p, _ = spstats.linregress((ts_aq4x - ts_aqct)[modlist_aq4x], ((pr_aq4x - pr_aqct)/pr_aqct)[modlist_aq4x])
print(r, corr, p)

r, _, corr, p, _ = spstats.linregress((ts_ld4x - ts_ldct)[modlist_ld4x], ((pr_ld4x - pr_ldct)/pr_ldct)[modlist_ld4x])
print(r, corr, p)

#print(np.nanmin(ts_aqct), np.nanmax(ts_aqct),np.nanmedian(ts_aqct))
#print(np.nanmin(pr_aqct), np.nanmax(pr_aqct),np.nanmedian(pr_aqct))

#print(np.nanmin(ts_aq4x-ts_aqct)/2, np.nanmax(ts_aq4x-ts_aqct)/2,np.nanmedian(ts_aq4x-ts_aqct)/2)
#print(np.nanmin(ts_ld4x-ts_ldct)/2, np.nanmax(ts_ld4x-ts_ldct)/2,np.nanmedian(ts_ld4x-ts_ldct)/2)

plt.show()

