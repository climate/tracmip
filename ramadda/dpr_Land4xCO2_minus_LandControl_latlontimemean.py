# load modules
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import cm
from matplotlib.patches import Rectangle

# load own modules
import sys
sys.path.append('/home/aiko/Dropbox/Projects/current/Tracmip/analysis/python3/ramadda/mymodules/')
sys.path.append('/home/aiko/Dropbox/Python3/mymodules/')
import tracmipmodels as tracmipmodels
import atmosphere as atm

def make_niceplot(ax, modelname):
    ax.xaxis.set_ticks([-120, -60, 0, 60, 120])
    ax.xaxis.set_ticklabels([''], fontsize=11)
    ax.yaxis.set_ticks([-0.5, 0, 0.5])
    ax.yaxis.set_ticklabels([''], fontsize=11) 
    plt.text(0.03, 0.93, modelname, fontsize=15, ha='left', va='center', \
             transform=ax.transAxes, backgroundcolor='white')
    plt.xlim(-175, 175), plt.ylim(-0.6, 0.6)         

# load pr data
file    = np.load('pr_latlontimemean.npz')
lat     = file['lat']
lon     = file['lon']
pr_ldct = file['pr_ldct']
pr_ld4x = file['pr_ld4x']

nmod = pr_ldct[:, 0, 0].size
nlat = lat.size
nlon = lon.size

sinlat = np.sin(lat*np.pi/180)

# get model colors, names and numbers
modelcolors   = tracmipmodels.get_modelcolors(nmod)
modelnames    = tracmipmodels.get_modelnames()
modelnumbers  = tracmipmodels.get_modelnumbers()

# list of available models
modlist_ldct = tracmipmodels.get_availablemodels('ldct')
modlist_ld4x = tracmipmodels.get_availablemodels('ld4x')

# itcz position at each longitude
itcz_ldct = np.zeros((nmod, nlon)) + np.nan
itcz_ld4x = np.zeros((nmod, nlon)) + np.nan
for m in modlist_ldct:
    for i in range(0, nlon):
        itcz_ldct[m, i] = atm.get_itczposition(pr_ldct[m, :, i], lat, 30.0, 0.1)
for m in modlist_ld4x:
    for i in range(0, nlon):
        itcz_ld4x[m, i] = atm.get_itczposition(pr_ld4x[m, :, i], lat, 30.0, 0.1)

# plotting
plt.figure(figsize=(18, 20), dpi=80, facecolor='w', edgecolor='k')
clev = np.array([-6.5, -5.5, -4.5, -3.5, -2.5, -1.5, -0.5, 0.5, 1.5, 2.5, 3.5, 4.5, 5.5, 6.5])

ax = plt.subplot(5, 3, 1)
c = plt.contourf(lon, sinlat, np.nanmedian((pr_ld4x-pr_ldct)[modlist_ldct], axis=0), clev, extend='both', cmap=cm.BrBG)
ax.add_patch(Rectangle((0, -0.5), 45, 1, alpha=1, facecolor='none', edgecolor='gray', linewidth=2))
plt.plot([-200, 200], [0, 0], 'k--')
plt.plot(lon, np.sin(np.nanmedian(itcz_ldct[modlist_ldct], axis=0)*np.pi/180), 'royalblue', linewidth=2)
plt.plot(lon, np.sin(np.nanmedian(itcz_ld4x[modlist_ld4x], axis=0)*np.pi/180), 'firebrick', linewidth=2)
make_niceplot(ax, 'Model median')
ax.yaxis.set_ticklabels(['30S', 'Eq', '30N'], fontsize=11) 
ax = plt.subplot(5, 3, 2)
ax.axis('off')
cbar = plt.colorbar(c, ticks=[-6, -4, -2, 0, 2, 4, 6], orientation='horizontal', aspect=30)
cbar.ax.tick_params(labelsize=10)
plt.text(1, -0.17, 'mm/day', fontsize=11, ha='right')  

for m in modlist_ldct:
    if modelnames[m] == 'AM2.1'    : msubplot = 3
    if modelnames[m] == 'CAM3'     : msubplot = 4 
    if modelnames[m] == 'CAM4'     : msubplot = 5 
    if modelnames[m] == 'CAM5Nor'  : msubplot = 6
    if modelnames[m] == 'CNRM-AM5' : msubplot = 7 
    if modelnames[m] == 'ECHAM6.1' : msubplot = 8
    if modelnames[m] == 'ECHAM6.3' : msubplot = 9 
    if modelnames[m] == 'LMDZ5A'   : msubplot = 10
    if modelnames[m] == 'MetUM-CTL': msubplot = 11
    if modelnames[m] == 'MetUM-ENT': msubplot = 12
    if modelnames[m] == 'MIROC5'   : msubplot = 13  
    if modelnames[m] == 'MPAS'     : msubplot = 14
    if modelnames[m] == 'CALTECH'  : msubplot = 15
    ax = plt.subplot(5, 3, msubplot)
    c = plt.contourf(lon, sinlat, (pr_ld4x-pr_ldct)[m, :, :], clev, extend='both', cmap=cm.BrBG)
    ax.add_patch(Rectangle((0, -0.5), 45, 1, alpha=1, facecolor='none', edgecolor='gray', linewidth=2))
    plt.plot([-200, 200], [0, 0], 'k--')
    plt.plot(lon, np.sin(itcz_ldct[m, :]*np.pi/180), 'royalblue', linewidth=2)
    plt.plot(lon, np.sin(itcz_ld4x[m, :]*np.pi/180), 'firebrick', linewidth=2)
    make_niceplot(ax, modelnames[m])
    if (msubplot == 13) or (msubplot == 14) or (msubplot ==15):
        ax.xaxis.set_ticklabels(['120W', '60W', '0', '60E', '120E'], fontsize=11) 
    if msubplot in [1, 4, 7, 10, 13]:
        ax.yaxis.set_ticklabels(['30S', 'Eq', '30N'], fontsize=11) 

plt.subplots_adjust(wspace=0.04, hspace=0.05)  
plt.tight_layout

plt.savefig('figs/dpr_latlontimemean_Land4xCO2_minus_LandControl_allmodels.pdf')

plt.show()
