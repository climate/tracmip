# load modules
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import cm

# load own modules
import sys
sys.path.append('/home/aiko/Dropbox/Projects/current/Tracmip/analysis/python3/ramadda/mymodules/')
sys.path.append('/home/aiko/Dropbox/Python3/mymodules/')
import atmosphere as atm
import tracmipmodels as tracmipmodels

# load pr and ts data
file  = np.load('ts_zonaltimemean.npz')
lat   = file['lat']
ts_tm = file['ts_aqct']
file  = np.load('pr_zonaltimemean.npz')
pr_tm = file['pr_aqct']
file  = np.load('pr_zonalmonthmean.npz')
pr_mm = file['pr_aqct']
file  = np.load('ua_zonaltimemean_plevels.npz')
lev   = file['lev']
ua    = file['ua_aqct']
file  = np.load('va_zonaltimemean_plevels.npz')
va    = file['va_aqct']

nmod   = ts_tm[:, 0].size
month  = np.array([1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12])
nmonth = month.size
nlat   = lat.size

sinlat = np.sin(lat*np.pi/180)

# get model colors, names and numbers
modelcolors   = tracmipmodels.get_modelcolors(nmod)
modelnames    = tracmipmodels.get_modelnames()
modelnumbers  = tracmipmodels.get_modelnumbers()

# analysis
# calculate time-mean ITCZ position
itcz_tm = np.zeros(nmod)
for i in range(0, nmod):
    itcz_tm[i] = atm.get_itczposition(pr_tm[i, :], lat, 30, 0.1)

# calculate monthly-mean ITCZ position
itcz_mm = np.zeros((nmod, 12))
for i in range(0, nmod):
    for t in range(0, nmonth):
        itcz_mm[i, t] = atm.get_itczposition(pr_mm[i, t, :], lat, 30, 0.1)

# mass stream function
msf = np.zeros(va.shape) + np.nan
for m in range(0, nmod):
    msf[m, :, :] = atm.get_massstreamfunction(va[m, :, :], lev, lat)

# plotting
fig = plt.figure( figsize=(12, 12), dpi=80, facecolor='w', edgecolor='k' )

ax = plt.subplot(3, 2, 1)
ax.spines['right'].set_color('none')
ax.spines['top'].set_color('none')
ax.xaxis.set_ticks_position('bottom')
ax.yaxis.set_ticks_position('left')

for m in range(0, nmod):
    plt.plot(sinlat, ts_tm[m, :], color=modelcolors[m])
plt.plot(sinlat, np.nanmedian(ts_tm, axis=0), 'k', linewidth=3)
plt.xlim(-0.98, 0.98), plt.ylim(270, 312)
ax.xaxis.set_ticks([-0.866, -0.5, 0, 0.5, 0.866])
ax.xaxis.set_ticklabels(['60S', '30S', 'Eq', '30N', '60N'], fontsize=10)
ax.yaxis.set_ticks([270, 280, 290, 300, 310])
ax.yaxis.set_ticklabels([270, 280, 290, 300, 310], fontsize=10) 
plt.title('Surface temperature', fontsize=14)
plt.ylabel('K', fontsize=12)
plt.text(0.02, 0.92, 'a)', fontsize=14, ha='left', va='center', transform=ax.transAxes)
   
ax = plt.subplot(3, 2, 2)
ax.spines['right'].set_color('none')
ax.spines['top'].set_color('none')
ax.xaxis.set_ticks_position('bottom')
ax.yaxis.set_ticks_position('left')

for m in range(0, nmod):
    plt.plot(sinlat, pr_tm[m, :], color=modelcolors[m])
plt.plot(sinlat, np.nanmedian(pr_tm, axis=0), 'k', linewidth=3)   
plt.xlim(-0.98, 0.98), plt.ylim(0, 18)
ax.xaxis.set_ticks([-0.866, -0.5, 0, 0.5, 0.866])
ax.xaxis.set_ticklabels(['60S', '30S', 'Eq', '30N', '60N'], fontsize=10)
ax.yaxis.set_ticks([0, 6, 12, 18])
ax.yaxis.set_ticklabels([0, 6, 12, 18], fontsize=10) 
plt.title('Precipitation', fontsize=14)
plt.ylabel('mm/day', fontsize=12)
plt.text(0.02, 0.92, 'b)', fontsize=14, ha='left', va='center', transform=ax.transAxes)

ax = plt.subplot(3, 2, 3)
ax.spines['right'].set_color('none')
ax.spines['top'].set_color('none')
ax.xaxis.set_ticks_position('bottom')
ax.yaxis.set_ticks_position('left')

plt.plot([-20, 20], [0, 0], 'k--')
for m in range(0, nmod):
    plt.plot(month, itcz_mm[m, :], color=modelcolors[m])
plt.plot(month, np.nanmedian(itcz_mm, axis=0), 'k', linewidth=3)
plt.xlim(1, 12), plt.ylim(-15, 15)
ax.xaxis.set_ticks(month)
ax.xaxis.set_ticklabels(['Jan', '', '', 'Apr', '', '', 'Jul', '', '' ,'Oct', '', ''], fontsize=10)
ax.yaxis.set_ticks([-15, -10, -5, 0, 5, 10, 15])
ax.yaxis.set_ticklabels(['15S', '10S', '5S', 'Eq', '5N', '10N', '15N'], fontsize=10) 
plt.title('ITCZ position', fontsize=14)
plt.xlabel('month', fontsize=12)
plt.ylabel('deg lat', fontsize=12)
plt.text(0.02, 0.92, 'c)', fontsize=14, ha='left', va='center', transform=ax.transAxes)
   
ax = plt.subplot(3, 2, 4)
ax.xaxis.set_ticks_position('bottom')
ax.yaxis.set_ticks_position('left')   

cu   = np.array([-55, -45, -35, -25, -15, -5,\
                   5,  15, 25, 35, 45, 55])
cmsf = np.array([-170, -150, -130, -110, -90, -70, -50, -30, -10, 10, 30, 50, 70, 90, 110, 130, 150, 170])
c=plt.contourf(sinlat, lev/1e2, np.nanmedian(ua, axis=0), cu, cmap=cm.RdBu_r, extend='max')
plt.contour(sinlat, lev/1e2, np.nanmedian(msf, axis=0), cmsf, colors='k')
plt.xlim(-0.98, 0.98), plt.ylim(1000, 10)
ax.xaxis.set_ticks([-0.866, -0.5, 0, 0.5, 0.866])
ax.xaxis.set_ticklabels(['60S', '30S', 'Eq', '30N', '60N'], fontsize=10)
ax.yaxis.set_ticks([10, 200, 600, 1000])
ax.yaxis.set_ticklabels([10, 200, 600, 1000], fontsize=10)
plt.title('Zonal wind and mass stream function', fontsize=14)
plt.ylabel('hPa', fontsize=12)
plt.text(0.02, 0.938, 'd)', fontsize=14, ha='left', va='center', transform=ax.transAxes, backgroundcolor='white')
  
ax = plt.subplot(3, 2, 6)
ax.axis('off')
cbar = plt.colorbar(c, ticks=[-50, -40, -30, -20, -10, 0, 10, 20, 30, 40, 50], 
             orientation='vertical', fraction=0.09)
cbar.ax.tick_params(labelsize=10) 
plt.text(1, 0, 'm/s', fontsize=10, ha='left', va='center', transform=ax.transAxes)

  
plt.tight_layout
plt.savefig('figs/ts_pr_itcz_ua_msf_AquaControl.pdf')

# itcz
print(np.nanmedian(itcz_tm))
print(np.nanmax(itcz_tm))
print(np.nanmin(itcz_tm))

# hemispheric temperature difference
ts_hdiff = np.zeros(nmod) + np.nan
for m in range(0, nmod):
    ts_hdiff[m] = atm.get_hemispheric_diff(ts_tm[m, :], lat)
print(ts_hdiff)
print(np.nanmedian(ts_hdiff))
print(np.nanmax(ts_hdiff))
print(np.nanmin(ts_hdiff))

# nh-sh at each latitude
ts_asym = np.zeros((nmod, nlat)) + np.nan
for m in range(0, nmod):
    _, ts_asym[m, :] = atm.get_sym_and_asym_component(ts_tm[m, :], lat)
plt.figure()
for m in range(0, nmod):
    plt.plot(sinlat, ts_asym[m, :], color=modelcolors[m])
plt.plot(sinlat, np.nanmedian(ts_asym, axis=0), 'k', linewidth=3)
plt.plot([-1, 1], [0, 0], 'k--')

# seasonal precip evolution
plt.figure()
for m in range(0, nmod):
    plt.subplot(4, 3, m+1)
    plt.contourf(lat, month, pr_mm[m, :, :])
    plt.title(modelnames[m])
    plt.xlim(-30, 30)
    
# Hadley cell strength
  # annual-mean Hadley cell strength
hc_nh = np.max(np.max(msf[:, 2:15, :], axis=2), axis=1)
hc_sh = np.min(np.min(msf[:, 2:15, :], axis=2), axis=1)
print(np.nanmax(hc_nh), np.nanmin(hc_nh), np.nanmedian(hc_nh))
print(np.nanmax(hc_sh), np.nanmin(hc_sh), np.nanmedian(hc_sh))  

# annual-mean jet position
# 850 hPa is lev = 14
jetlat_nh = np.zeros(nmod) + np.nan
jetlat_sh= np.zeros(nmod) + np.nan
for m in range(0, nmod):
    jetlat_nh[m], jetlat_sh[m] = atm.get_eddyjetlat(ua[m, 14, :], lat)
print(np.nanmax(jetlat_nh), np.nanmin(jetlat_nh), np.nanmedian(jetlat_nh))
print(np.nanmax(jetlat_sh), np.nanmin(jetlat_sh), np.nanmedian(jetlat_sh))

plt.show()
