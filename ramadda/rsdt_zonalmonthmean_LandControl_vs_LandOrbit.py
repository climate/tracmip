# load modules
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import cm

# load own modules
import sys
sys.path.append('/home/aiko/Dropbox/Projects/current/Tracmip/analysis/python3/ramadda/mymodules/')
sys.path.append('/home/aiko/Dropbox/Python3/mymodules/')
import tracmipmodels as tracmipmodels

def make_niceplot_v1(ax):
    plt.xlim(1, 12), plt.ylim(-0.98, 0.98)
    ax.xaxis.set_ticks([1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12])
    ax.xaxis.set_ticklabels(['Jan', '', '', 'Apr', '', '', 'Jul', '', '' ,'Oct', '', ''], fontsize=10)
    ax.yaxis.set_ticks([-0.866, -0.5, 0, 0.5, 0.866])
    ax.yaxis.set_ticklabels(['60S', '30S', 'Eq', '30N', '60N'], fontsize=10)
    
def make_niceplot_v2(ax):
    ax.spines['right'].set_color('none')
    ax.spines['top'].set_color('none')
    ax.xaxis.set_ticks_position('bottom')
    ax.yaxis.set_ticks_position('left')
    ax.spines['left'].set_position(('data', 0.0))
    plt.xlim(-25, 25), plt.ylim(-0.98, 0.98)
    ax.xaxis.set_ticks([-20, -10, 0, 0, 10, 20])
    ax.xaxis.set_ticklabels([-20, -10, 0, 0, 10, 20], fontsize=10)
    ax.yaxis.set_ticks([-0.866, -0.5, 0, 0.5, 0.866])
    ax.yaxis.set_ticklabels(['60S', '30S', 'Eq', '30N', '60N'], fontsize=10)   
    plt.xlabel(r'W/m$^2$', fontsize=12)
    
# load data
file      = np.load('rsdt_zonalmonthmean.npz')
lat       = file['lat']
rsdt_ldct = file['rsdt_ldct']
rsdt_ldor = file['rsdt_ldor']

nmod  = rsdt_ldct[:, 0, 0].size
nmth  = rsdt_ldct[0, :, 0].size
month = np.arange(1, 13)

sinlat = np.sin(lat*np.pi/180)

# get model colors, names and numbers
modelcolors   = tracmipmodels.get_modelcolors(nmod)
modelnames    = tracmipmodels.get_modelnames()
modelnumbers  = tracmipmodels.get_modelnumbers()

# list of available models
modlist_ldct  = tracmipmodels.get_availablemodels('ldct')
modlist_ldor  = tracmipmodels.get_availablemodels('ldor')

# plotting LandOrbit - LandControl
plt.figure(figsize=(12, 12), dpi=80, facecolor='w', edgecolor='k' )

ax = plt.subplot(3, 2, 1)
clev = [0, 50, 100, 150, 200, 350, 400, 450, 500, 550]
make_niceplot_v1(ax)
c = plt.contourf(month, sinlat, np.transpose(np.nanmedian(rsdt_ldct[modlist_ldct], axis=0)), clev, cmap=cm.OrRd)
plt.text(0.02, 0.94, 'a)', fontsize=14, ha='left', va='center', backgroundcolor='white', transform=ax.transAxes)
plt.title('LandControl', fontsize=14)

ax = plt.subplot(3, 2, 5)
ax.axis('off')
cbar = plt.colorbar(c, ticks=clev, orientation='horizontal', aspect=30)
cbar.ax.tick_params(labelsize=10)
plt.text(1, -0.17, r'W/m$^2$', fontsize=11, ha='right')  

ax = plt.subplot(3, 2, 2)
make_niceplot_v1(ax)
clev = [-26, -22, -18, -14, -10, -6, -2, 2, 6, 10, 14, 18, 22, 26]
c = plt.contourf(month, sinlat, np.transpose(np.nanmedian((rsdt_ldor - rsdt_ldct)[modlist_ldor], axis=0)), clev, cmap=cm.RdBu_r)
plt.text(0.02, 0.94, 'b)', fontsize=14, ha='left', va='center', backgroundcolor='white', transform=ax.transAxes)
plt.title('LandOrbit - LandControl', fontsize=14)

ax = plt.subplot(3, 2, 6)
ax.axis('off')
cbar = plt.colorbar(c, ticks=[-22, -14, -6, 0, 6, 14, 22], orientation='horizontal', aspect=30)
cbar.ax.tick_params(labelsize=10)
plt.text(1, -0.17, r'W/m$^2$', fontsize=11, ha='right')  

ax = plt.subplot(3, 2, 3)
make_niceplot_v2(ax)
plt.plot(np.nanmedian((rsdt_ldor-rsdt_ldct)[modlist_ldor, 5, :], axis=0), sinlat, 'royalblue', linewidth=2)
plt.plot(np.nanmedian((rsdt_ldor-rsdt_ldct)[modlist_ldor, 11, :], axis=0), sinlat, 'firebrick', linewidth=2)
plt.text(0.02, 0.94, 'c)', fontsize=14, ha='left', va='center', transform=ax.transAxes)
plt.title('LandOrbit - LandControl', fontsize=14)
plt.text(0.1, 0.8, 'June', fontsize=14, color='royalblue', ha='right', va='center', transform=ax.transAxes)
plt.text(0.68, 0.2, 'December', fontsize=14, color='firebrick', ha='left', va='center', transform=ax.transAxes)

plt.tight_layout()
plt.savefig('figs/rsdt_zonalmonthmean_LandOrbit_minus_LandControl_version2.pdf')

   
plt.show()
