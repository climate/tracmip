# load modules
import numpy as np

# load own modules
import sys
sys.path.append('/home/aiko/Dropbox/Projects/current/Tracmip/analysis/python3/ramadda/mymodules/')

import loaddata as loaddata

# load data from ramadda server
rsuscs_aqct, lat = loaddata.get_data_zmtm('AQUA'   , 'rsuscs')
rsuscs_aq4x, lat = loaddata.get_data_zmtm('AQUACO2', 'rsuscs')
rsuscs_ldct, lat = loaddata.get_data_zmtm('LAND'   , 'rsuscs')
rsuscs_ld4x, lat = loaddata.get_data_zmtm('LANDCO2', 'rsuscs')
rsuscs_ldor, lat = loaddata.get_data_zmtm('LORBIT' , 'rsuscs')

# we also need rsdscs and rsnscs to reconstruct rsuscs for cam3, cam4 and mpas
rsdscs_aqct, lat = loaddata.get_data_zmtm('AQUA'   , 'rsdscs')
rsdscs_aq4x, lat = loaddata.get_data_zmtm('AQUACO2', 'rsdscs')
rsdscs_ldct, lat = loaddata.get_data_zmtm('LAND'   , 'rsdscs')
rsdscs_ld4x, lat = loaddata.get_data_zmtm('LANDCO2', 'rsdscs')
rsdscs_ldor, lat = loaddata.get_data_zmtm('LORBIT' , 'rsdscs')

rsnscs_aqct, lat = loaddata.get_data_zmtm('AQUA'   , 'rsnscs')
rsnscs_aq4x, lat = loaddata.get_data_zmtm('AQUACO2', 'rsnscs')
rsnscs_ldct, lat = loaddata.get_data_zmtm('LAND'   , 'rsnscs')
rsnscs_ld4x, lat = loaddata.get_data_zmtm('LANDCO2', 'rsnscs')
rsnscs_ldor, lat = loaddata.get_data_zmtm('LORBIT' , 'rsnscs')

# for cam3, cam4 and mpas, rsuscs is negative because it is upward
rsuscs_aqct[8:11, :] = (np.abs(rsdscs_aqct[8:11, :]) - np.abs(rsnscs_aqct[8:11, :]))
rsuscs_aq4x[8:11, :] = (np.abs(rsdscs_aq4x[8:11, :]) - np.abs(rsnscs_aq4x[8:11, :]))
rsuscs_ldct[8:11, :] = (np.abs(rsdscs_ldct[8:11, :]) - np.abs(rsnscs_ldct[8:11, :]))
rsuscs_ld4x[8:11, :] = (np.abs(rsdscs_ld4x[8:11, :]) - np.abs(rsnscs_ld4x[8:11, :]))
rsuscs_ldor[8:11, :] = (np.abs(rsdscs_ldor[8:11, :]) - np.abs(rsnscs_ldor[8:11, :])) 

# saving data for later use
print('Saving zonal time mean rsuscs')
np.savez('rsuscs_zonaltimemean.npz', \
         **{'rsuscs_aqct':rsuscs_aqct, 'rsuscs_aq4x':rsuscs_aq4x, 'rsuscs_ldct':rsuscs_ldct, \
            'rsuscs_ld4x':rsuscs_ld4x, 'rsuscs_ldor':rsuscs_ldor, 'lat':lat})