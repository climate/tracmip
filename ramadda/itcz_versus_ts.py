# load modules
import numpy as np
import matplotlib.pyplot as plt
import scipy.stats as spstats

# load own modules
import sys
sys.path.append('/home/aiko/Dropbox/Projects/current/Tracmip/analysis/python3/ramadda/mymodules/')
sys.path.append('/home/aiko/Dropbox/Python3/mymodules/')
import tracmipmodels as tracmipmodels
import atmosphere as atm

# load pr and ts data
file       = np.load('pr_zonaltimemean.npz')
lat        = file['lat']
pr_aqct_tm = file['pr_aqct']
pr_aq4x_tm = file['pr_aq4x']
pr_ldct_tm = file['pr_ldct']
pr_ld4x_tm = file['pr_ld4x'] 

file       = np.load('pr_zonalmonthmean.npz')
pr_aqct_mm = file['pr_aqct']
pr_aq4x_mm = file['pr_aq4x']
pr_ldct_mm = file['pr_ldct']
pr_ld4x_mm = file['pr_ld4x'] 

file    = np.load('ts_globaltimemean.npz')
ts_aqct = file['ts_aqct']
ts_aq4x = file['ts_aq4x']
ts_ldct = file['ts_ldct']
ts_ld4x = file['ts_ld4x'] 

nmod = ts_aqct.size

# get model colors, names and numbers
modelcolors   = tracmipmodels.get_modelcolors(nmod)
modelnames    = tracmipmodels.get_modelnames()
modelnumbers  = tracmipmodels.get_modelnumbers()

# list of available models
modlist_aqct = tracmipmodels.get_availablemodels('aqct')
modlist_aq4x = tracmipmodels.get_availablemodels('aq4x')
modlist_ldct = tracmipmodels.get_availablemodels('ldct')
modlist_ld4x = tracmipmodels.get_availablemodels('ld4x')

# analysis

# calculate time-mean ITCZ position
itcz_aqct_tm = np.zeros(nmod)
itcz_aq4x_tm = np.zeros(nmod)
itcz_ldct_tm = np.zeros(nmod)
itcz_ld4x_tm = np.zeros(nmod)
for i in range(0, nmod):
    itcz_aqct_tm[i] = atm.get_itczposition(pr_aqct_tm[i, :], lat, 30, 0.1)
    itcz_aq4x_tm[i] = atm.get_itczposition(pr_aq4x_tm[i, :], lat, 30, 0.1)
    itcz_ldct_tm[i] = atm.get_itczposition(pr_ldct_tm[i, :], lat, 30, 0.1)
    itcz_ld4x_tm[i] = atm.get_itczposition(pr_ld4x_tm[i, :], lat, 30, 0.1)

# calculate monthly-mean ITCZ position
itcz_aqct_mm = np.zeros((nmod, 12))
itcz_aq4x_mm = np.zeros((nmod, 12))
itcz_ldct_mm = np.zeros((nmod, 12))
itcz_ld4x_mm = np.zeros((nmod, 12))
for i in range(0, nmod):
    for t in range(0, 12):
        itcz_aqct_mm[i, t] = atm.get_itczposition(pr_aqct_mm[i, t, :], lat, 30, 0.1)
        itcz_aq4x_mm[i, t] = atm.get_itczposition(pr_aq4x_mm[i, t, :], lat, 30, 0.1)
        itcz_ldct_mm[i, t] = atm.get_itczposition(pr_ldct_mm[i, t, :], lat, 30, 0.1)
        itcz_ld4x_mm[i, t] = atm.get_itczposition(pr_ld4x_mm[i, t, :], lat, 30, 0.1)
  
# plotting
fig = plt.figure( figsize=(12, 8), dpi=80, facecolor='w', edgecolor='k' )

ax = plt.subplot(2, 2, 1)
ax.spines['right'].set_color('none')
ax.spines['top'].set_color('none')
ax.xaxis.set_ticks_position('bottom')
ax.yaxis.set_ticks_position('left')
#ax.spines['bottom'].set_position('zero')

plt.plot([200, 400], [0, 0], 'k:')
# crosses for seasonal excursions
for i in modlist_aqct:
    plt.plot(ts_aqct[i], np.min(itcz_aqct_mm[i, :]), 'o', mec='k', mfc='none', mew=1)
    plt.plot(ts_aqct[i], np.max(itcz_aqct_mm[i, :]), 'x', mec='k', mfc='none', mew=1)
for i in modlist_aq4x:
    plt.plot(ts_aq4x[i], np.min(itcz_aq4x_mm[i, :]), 'o', mec='k', mfc='none', mew=1)
    plt.plot(ts_aq4x[i], np.max(itcz_aq4x_mm[i, :]), 'x', mec='k', mfc='none', mew=1)
# model numbers
for i in modlist_aqct:
    plt.text(ts_aqct[i], itcz_aqct_tm[i], modelnumbers[i], color=modelcolors[i], fontsize=14, \
    fontweight='normal', ha='center', va='center', backgroundcolor='none')
for i in modlist_aq4x:
    plt.text(ts_aq4x[i], itcz_aq4x_tm[i], modelnumbers[i], color=modelcolors[i], fontsize=14, \
    fontweight='normal', ha='center', va='center', backgroundcolor='none')

plt.xlim(288, 308), plt.ylim(-11, 21)
plt.xlabel('Surface temperature (K)', fontsize=12)
plt.ylabel('ITCZ (deg lat)', fontsize=12)
plt.title('Aquaplanet simulations', fontsize=14)
ax.xaxis.set_ticks([288, 290, 292, 294, 296, 298, 300, 302, 304, 306, 308])
ax.xaxis.set_ticklabels([288, '', 292, '', 296, '', 300, '', 304, '', 308], fontsize=10)
ax.yaxis.set_ticks([-10, -5, 0, 5, 10, 15, 20])
ax.yaxis.set_ticklabels([-10, '', 'Eq', '', 10, '', 20], fontsize=10)
plt.text(0.02, 0.98, 'a)', fontsize=14, ha='left', va='center', transform=ax.transAxes)

# fit through annual-mean itcz and northern and southern seasonal excursions
# with respect to surface temperature
xfit = np.linspace(280, 320, 100)
xdata = np.concatenate((ts_aqct[modlist_aqct], ts_aq4x[modlist_aq4x]))
ydata = np.concatenate((itcz_aqct_tm[modlist_aqct], itcz_aq4x_tm[modlist_aq4x]))
r, b, corr, p, _ = spstats.linregress(xdata, ydata); plt.plot(xfit, r*xfit + b, 'k', linewidth=2)
print('Aqua, annual-mean:', r, p)
xdata = np.concatenate((ts_aqct[modlist_aqct], ts_aq4x[modlist_aq4x])) 
ydata =np.nanmin(np.concatenate((itcz_aqct_mm[modlist_aqct], itcz_aq4x_mm[modlist_aq4x])), axis=1)
r, b, corr, p, _ = spstats.linregress(xdata, ydata); plt.plot(xfit, r*xfit + b, 'k--', linewidth=2)
print('Aqua, southern edge:', r, p)
xdata = np.concatenate((ts_aqct[modlist_aqct], ts_aq4x[modlist_aq4x]))
ydata =np.nanmax(np.concatenate((itcz_aqct_mm[modlist_aqct], itcz_aq4x_mm[modlist_aq4x])), axis=1)
r, b, corr, p, _ = spstats.linregress(xdata, ydata); plt.plot(xfit, r*xfit + b, 'k--', linewidth=2)
print('Aqua, northern edge:', r, p)

ax = plt.subplot(2, 2, 2)
ax.spines['right'].set_color('none')
ax.spines['top'].set_color('none')
ax.xaxis.set_ticks_position('bottom')
ax.yaxis.set_ticks_position('left')
#ax.spines['bottom'].set_position('zero')

plt.plot([200, 400], [0, 0], 'k:')
# crosses for seasonal excursions
for i in modlist_ldct: 
    plt.plot(ts_ldct[i], np.min(itcz_ldct_mm[i, :]), 'o', mec='k', mfc='none', mew=1)
    plt.plot(ts_ldct[i], np.max(itcz_ldct_mm[i, :]), 'x', mec='k', mfc='none', mew=1)
for i in modlist_ld4x:    
    plt.plot(ts_ld4x[i], np.min(itcz_ld4x_mm[i, :]), 'o', mec='k', mfc='none', mew=1)
    plt.plot(ts_ld4x[i], np.max(itcz_ld4x_mm[i, :]), 'x', mec='k', mfc='none', mew=1)
# model numbers
for i in modlist_ldct:   
    plt.text(ts_ldct[i], itcz_ldct_tm[i], modelnumbers[i], color=modelcolors[i], fontsize=14, \
    fontweight='normal', ha='center', va='center', backgroundcolor='none')
for i in modlist_ld4x:
    plt.text(ts_ld4x[i], itcz_ld4x_tm[i], modelnumbers[i], color=modelcolors[i], fontsize=14, \
    fontweight='normal', ha='center', va='center', backgroundcolor='none')

plt.xlim(288, 308), plt.ylim(-11, 21)
plt.xlabel('Surface temperature (K)', fontsize=12)
#plt.ylabel('ITCZ (deg lat)', fontsize=12)
plt.title('Land simulations', fontsize=14)
ax.xaxis.set_ticks([288, 290, 292, 294, 296, 298, 300, 302, 304, 306, 308])
ax.xaxis.set_ticklabels([288, '', 292, '', 296, '', 300, '', 304, '', 308], fontsize=10)
ax.yaxis.set_ticks([-10, -5, 0, 5, 10, 15, 20])
ax.yaxis.set_ticklabels([-10, '', 'Eq', '', 10, '', 20], fontsize=10)
plt.text(0.02, 0.98, 'b)', fontsize=14, ha='left', va='center', transform=ax.transAxes)

# fit through annual-mean itcz and northern and southern seasonal excursions
# with respect to surface temperature
xfit = np.linspace(280, 320, 100)
xdata = np.concatenate((ts_ldct[modlist_ldct], ts_ld4x[modlist_ld4x]))
ydata = np.concatenate((itcz_ldct_tm[modlist_ldct], itcz_ld4x_tm[modlist_ld4x]))
r, b, corr, p, _ = spstats.linregress(xdata, ydata); plt.plot(xfit, r*xfit + b, 'k', linewidth=2)
print('Land, annual-mean:', r, p)
xdata = np.concatenate((ts_ldct[modlist_ldct], ts_ld4x[modlist_ld4x]))
ydata =np.nanmin(np.concatenate((itcz_ldct_mm[modlist_ldct], itcz_ld4x_mm[modlist_ld4x])), axis=1)
r, b, corr, p, _ = spstats.linregress(xdata, ydata); plt.plot(xfit, r*xfit + b, 'k--', linewidth=2)
print('Land, southern edge:', r, p)
xdata = np.concatenate((ts_ldct[modlist_ldct], ts_ld4x[modlist_ld4x]))
ydata =np.nanmax(np.concatenate((itcz_ldct_mm[modlist_ldct], itcz_ld4x_mm[modlist_ld4x])), axis=1)
r, b, corr, p, _ = spstats.linregress(xdata, ydata); plt.plot(xfit, r*xfit + b, 'k--', linewidth=2)
print('Land, northern edge:', r, p)

ax = plt.subplot(2, 2, 3)
plt.xlim(0, 1), plt.ylim(0, 1)
plt.axis('off')
ystart=1.0
delta=0.0666
for m in modlist_aqct:
    if np.int(modelnumbers[m])<4:
        plt.text(0.1, ystart-delta*np.float(modelnumbers[m]), modelnumbers[m], color=modelcolors[m], fontsize=14)
        plt.text(0.15, ystart-delta*np.float(modelnumbers[m]), modelnames[m], color=modelcolors[m], fontsize=14)
    elif np.int(modelnumbers[m])<7:
        plt.text(0.5, ystart-delta*(np.float(modelnumbers[m])-3), modelnumbers[m], color=modelcolors[m], fontsize=14)
        plt.text(0.55, ystart-delta*(np.float(modelnumbers[m])-3), modelnames[m], color=modelcolors[m], fontsize=14)
    elif np.int(modelnumbers[m])<10:
        plt.text(0.9, ystart-delta*(np.float(modelnumbers[m])-6), modelnumbers[m], color=modelcolors[m], fontsize=14)
        plt.text(0.95, ystart-delta*(np.float(modelnumbers[m])-6), modelnames[m], color=modelcolors[m], fontsize=14)
    elif np.int(modelnumbers[m])<13:
        plt.text(1.3, ystart-delta*(np.float(modelnumbers[m])-9), modelnumbers[m], color=modelcolors[m], fontsize=14)
        plt.text(1.4, ystart-delta*(np.float(modelnumbers[m])-9), modelnames[m], color=modelcolors[m], fontsize=14)
    elif np.int(modelnumbers[m])<15:
        plt.text(1.7, ystart-delta*(np.float(modelnumbers[m])-12), modelnumbers[m], color=modelcolors[m], fontsize=14)
        plt.text(1.8, ystart-delta*(np.float(modelnumbers[m])-12), modelnames[m], color=modelcolors[m], fontsize=14)


plt.tight_layout

plt.savefig('figs/itcz_versus_ts_aqua_land.pdf')

plt.show()
