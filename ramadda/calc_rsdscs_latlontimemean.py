# load modules
import numpy as np

# load own modules
import sys
sys.path.append('/home/aiko/Dropbox/Projects/current/Tracmip/analysis/python3/ramadda/mymodules/')

import loaddata as loaddata

# load data from ramadda server
rsdscs_aqct, lat, lon = loaddata.get_data_latlontm('AQUA'   , 'rsdscs')
rsdscs_aq4x, lat, lon = loaddata.get_data_latlontm('AQUACO2', 'rsdscs')
rsdscs_ldct, lat, lon = loaddata.get_data_latlontm('LAND'   , 'rsdscs')
rsdscs_ld4x, lat, lon = loaddata.get_data_latlontm('LANDCO2', 'rsdscs')
rsdscs_ldor, lat, lon = loaddata.get_data_latlontm('LORBIT' , 'rsdscs')

# saving data for later use
print('Saving lat-lon time mean rsdscs')
np.savez('rsdscs_latlontimemean.npz', \
         **{'rsdscs_aqct':rsdscs_aqct, 'rsdscs_aq4x':rsdscs_aq4x, 'rsdscs_ldct':rsdscs_ldct, \
            'rsdscs_ld4x':rsdscs_ld4x, 'rsdscs_ldor':rsdscs_ldor, 'lat':lat, 'lon':lon})