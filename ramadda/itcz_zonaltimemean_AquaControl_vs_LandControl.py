# load modules
import numpy as np
import matplotlib.pyplot as plt

# load own modules
import sys
sys.path.append('/home/aiko/Dropbox/Projects/current/Tracmip/analysis/python3/ramadda/mymodules/')
sys.path.append('/home/aiko/Dropbox/Python3/mymodules/')
import tracmipmodels as tracmipmodels
import atmosphere as atm

# loading data
print('Load zonal time mean pr')
file    = np.load('pr_zonaltimemean.npz')
pr_aqct = file['pr_aqct']
pr_ldct = file['pr_ldct']
lat     = file['lat']

nmod = pr_aqct[:, 0].size
nlat = lat.size

# get model colors, names and numbers
modelcolors   = tracmipmodels.get_modelcolors(nmod)
modelnames    = tracmipmodels.get_modelnames()
modelnumbers  = tracmipmodels.get_modelnumbers()

# analysis
# calculate time-mean ITCZ position
itcz_aqct = np.zeros(nmod)
itcz_ldct = np.zeros(nmod)
for i in range(0, nmod):
    itcz_aqct[i] = atm.get_itczposition(pr_aqct[i, :], lat, 30, 0.1)
    itcz_ldct[i] = atm.get_itczposition(pr_ldct[i, :], lat, 30, 0.1)
    
# plotting
plt.figure( figsize=(12, 4), dpi=80, facecolor='w', edgecolor='k' )

ax = plt.subplot(1, 2, 1)
ax.spines['right'].set_color('none')
ax.spines['top'].set_color('none')
ax.xaxis.set_ticks_position('bottom')
ax.yaxis.set_ticks_position('left')

plt.plot([-20, 20], [-20, 20], 'k--')
for i in range(1, 11):
    plt.text(itcz_aqct[i], itcz_ldct[i], modelnumbers[i], fontweight='bold', color=modelcolors[i], fontsize=14)
plt.title('Zonal time mean ITCZ (deg lat)', fontsize=14)
plt.xlabel('AquaControl', fontsize=12)
plt.ylabel('LandControl', fontsize=12)
plt.xlim(1, 12), plt.ylim(1, 12)

ax = plt.subplot(1, 2, 2)
ax.spines['right'].set_color('none')
ax.spines['top'].set_color('none')
ax.xaxis.set_ticks_position('bottom')
ax.yaxis.set_ticks_position('left')

plt.plot([-20, 20], [0, 0], 'k--')
for i in range(1, 11):
    plt.text(itcz_aqct[i], (itcz_ldct-itcz_aqct)[i], modelnumbers[i], fontweight='bold', color=modelcolors[i], fontsize=14)
plt.title('ITCZ change (deg lat)', fontsize=14)
plt.xlabel('AquaControl', fontsize=12)
plt.ylabel('LandControl - AquaControl', fontsize=12)
plt.xlim(1, 12), plt.ylim(-5, 1)

plt.tight_layout
plt.savefig('figs/itcz_zonaltimemean_AquaControl_LandControl.pdf')
    
print(np.nanmedian(itcz_ldct-itcz_aqct))    

print(itcz_ldct-itcz_aqct)  
    
plt.show()
