# load modules
import numpy as np
import matplotlib.pyplot as plt

# load own modules
import sys
sys.path.append('/home/aiko/Dropbox/Python3/mymodules/')


# load global mean pr and ts data
file    = np.load('pr_zonaltimemean.npz')
lat     = file['lat']
pr_ldct = file['pr_ldct']
pr_ldor = file['pr_ldor'] 

nmod = pr_ldct[:, 0].size

# plotting
fig = plt.figure( figsize=(15, 10), dpi=80, facecolor='w', edgecolor='k' )

for i in range(0, nmod):
    ax = plt.subplot(3, 4, i+1)
    ax.spines['right'].set_color('none')
    ax.spines['top'].set_color('none')
    ax.xaxis.set_ticks_position('bottom')
    ax.yaxis.set_ticks_position('left')
    plt.plot(lat, pr_ldct[i, :], 'b', linewidth=2)
    plt.plot(lat, pr_ldor[i, :], 'g', linewidth=2)
    plt.plot(lat, pr_ldor[i, :] - pr_ldct[i, :], 'g--', linewidth=2)
    plt.title(models[i])
    plt.xlim(-80, 80), plt.ylim([-1, 10])
    if i==0:
        plt.text(-70, 1, 'LandControl', color='blue' , fontsize=14, fontweight='bold')
        plt.text(-70, 0, 'LandOrbit'  , color='green', fontsize=14, fontweight='bold')
  
plt.tight_layout

plt.savefig('figs/precip_landcontrol_versus_landorbit.pdf')

plt.show()
