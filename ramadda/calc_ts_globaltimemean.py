# load modules
import numpy as np

# load own modules
import sys
sys.path.append('/home/aiko/Dropbox/Projects/current/Tracmip/analysis/python3/ramadda/mymodules/')

import loaddata as loaddata

# load data from ramadda server
ts_aqct = loaddata.get_data_gmtm('AQUA'   , 'ts')
ts_aq4x = loaddata.get_data_gmtm('AQUACO2', 'ts')
ts_ldct = loaddata.get_data_gmtm('LAND'   , 'ts')
ts_ld4x = loaddata.get_data_gmtm('LANDCO2', 'ts')
ts_ldor = loaddata.get_data_gmtm('LORBIT' , 'ts')

# saving data for later use
print('Saving global time mean ts')
np.savez('ts_globaltimemean.npz', \
         **{'ts_aqct':ts_aqct, 'ts_aq4x':ts_aq4x, 'ts_ldct':ts_ldct, \
            'ts_ld4x':ts_ld4x, 'ts_ldor':ts_ldor} )
