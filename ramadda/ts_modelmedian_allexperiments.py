# load modules
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import cm
from matplotlib.patches import Rectangle

# load own modules
import sys
sys.path.append('/home/aiko/Dropbox/Projects/current/Tracmip/analysis/python3/ramadda/mymodules/')
sys.path.append('/home/aiko/Dropbox/Python3/mymodules/')
import tracmipmodels as tracmipmodels

def make_nice_plot(ax, title):
    plt.xlim(-178, 178), plt.ylim(-0.98, 0.98)
    ax.xaxis.set_ticks([-120, -60, 0, 60, 120])
    ax.xaxis.set_ticklabels([''],fontsize=10)
    ax.yaxis.set_ticks([-0.866, -0.5, 0, 0.5, 0.866])
    ax.yaxis.set_ticklabels([''], fontsize=10)
    plt.title(title, fontsize=14)

# load data
file    = np.load('ts_latlontimemean.npz')
lat     = file['lat']
lon     = file['lon']
ts_aqct = file['ts_aqct']
ts_aq4x = file['ts_aq4x']
ts_ldct = file['ts_ldct']
ts_ld4x = file['ts_ld4x']
ts_ldor = file['ts_ldor']

nmod   = ts_aqct[:, 0, 0].size
sinlat = np.sin(lat*np.pi/180)
nlon   = lon.size

# list of available models
modlist_aqct = tracmipmodels.get_availablemodels('aqct')
modlist_aq4x = tracmipmodels.get_availablemodels('aq4x')
modlist_ldct = tracmipmodels.get_availablemodels('ldct')
modlist_ld4x = tracmipmodels.get_availablemodels('ld4x')
modlist_ldor = tracmipmodels.get_availablemodels('ldor')
   
# plotting
plt.figure( figsize=(12, 12), dpi=80, facecolor='w', edgecolor='k')

clev   = np.array([275, 280, 285, 290, 295, 300, 305, 310])

ax = plt.subplot(3, 2, 1)
plt.contourf(lon, sinlat, np.nanmedian(ts_aqct[modlist_aqct], axis=0), clev, cmap=cm.YlOrRd, extend='both')
plt.plot([-200, 200], [0, 0], 'k--')
make_nice_plot(ax, 'AquaControl')#, plt.ylabel('hPa', fontsize=12)
ax.yaxis.set_ticklabels(['60S', '30S', 'Eq', '30N', '60N'], fontsize=10)
plt.text(0.02, 0.93, 'a)', fontsize=14, ha='left', va='center', transform=ax.transAxes, backgroundcolor='white')

ax = plt.subplot(3, 2, 3)
c=plt.contourf(lon, sinlat, np.nanmedian(ts_aq4x[modlist_aq4x], axis=0), clev, cmap=cm.YlOrRd, extend='both')
plt.plot([-200, 200], [0, 0], 'k--')
make_nice_plot(ax, 'Aqua4xCO2')
ax.xaxis.set_ticklabels(['120W', '60W', '0', '60E', '120E'],fontsize=10)
ax.yaxis.set_ticklabels(['60S', '30S', 'Eq', '30N', '60N'], fontsize=10)
plt.text(0.02, 0.93, 'c)', fontsize=14, ha='left', va='center', transform=ax.transAxes, backgroundcolor='white')

ax = plt.subplot(3, 2, 5)
ax.axis('off')
cbar = plt.colorbar(c, ticks=clev, orientation='horizontal', aspect=30)
cbar.ax.tick_params(labelsize=10) 
plt.text(0, -0.1, 'K', fontsize=10, ha='left', va='center', transform=ax.transAxes)

ax = plt.subplot(3, 2, 2)
plt.contourf(lon, sinlat, np.nanmedian(ts_ldct[modlist_ldct], axis=0), clev, cmap=cm.YlOrRd, extend='both')
plt.plot([-200, 200], [0, 0], 'k--')
make_nice_plot(ax, 'LandControl')
ax.add_patch(Rectangle((0, -0.5), 45, 1, alpha=1, facecolor='none', edgecolor='gray', linewidth=2))
plt.text(0.02, 0.93, 'b)', fontsize=14, ha='left', va='center', transform=ax.transAxes, backgroundcolor='white')

ax = plt.subplot(3, 2, 4)
plt.contourf(lon, sinlat, np.nanmedian(ts_ld4x[modlist_ld4x], axis=0), clev, cmap=cm.YlOrRd, extend='both')
plt.plot([-200, 200], [0, 0], 'k--')
make_nice_plot(ax, 'Land4xCO2')
from matplotlib.patches import Rectangle
ax.add_patch(Rectangle((0, -0.5), 45, 1, alpha=1, facecolor='none', edgecolor='gray', linewidth=2))
plt.text(0.02, 0.93, 'd)', fontsize=14, ha='left', va='center', transform=ax.transAxes, backgroundcolor='white')

ax = plt.subplot(3, 2, 6)
plt.contourf(lon, sinlat, np.nanmedian(ts_ldor[modlist_ldor], axis=0), clev, cmap=cm.YlOrRd, extend='both')
plt.plot([-200, 200], [0, 0], 'k--')
make_nice_plot(ax, 'LandOrbit')
from matplotlib.patches import Rectangle
ax.add_patch(Rectangle((0, -0.5), 45, 1, alpha=1, facecolor='none', edgecolor='gray', linewidth=2))
ax.xaxis.set_ticklabels(['120W', '60W', '0', '60E', '120E'],fontsize=10)
ax.yaxis.set_ticklabels(['60S', '30S', 'Eq', '30N', '60N'], fontsize=10)
plt.text(0.02, 0.93, 'e)', fontsize=14, ha='left', va='center', transform=ax.transAxes, backgroundcolor='white')

plt.tight_layout 

plt.savefig('figs/ts_modelmedian_allexperiments.pdf')                      

plt.show()
