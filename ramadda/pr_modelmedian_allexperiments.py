# load modules
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import cm
from matplotlib.patches import Rectangle

# load own modules
import sys
sys.path.append('/home/aiko/Dropbox/Projects/current/Tracmip/analysis/python3/ramadda/mymodules/')
sys.path.append('/home/aiko/Dropbox/Python3/mymodules/')
import atmosphere as atm
import tracmipmodels as tracmipmodels

def make_nice_plot(ax, title):
    plt.xlim(-178, 178), plt.ylim(-0.6, 0.6)
    ax.xaxis.set_ticks([-120, -60, 0, 60, 120])
    ax.xaxis.set_ticklabels([''],fontsize=10)
    ax.yaxis.set_ticks([-0.5, 0, 0.5])
    ax.yaxis.set_ticklabels([''], fontsize=10)
    plt.title(title, fontsize=14)

# load data
file    = np.load('pr_latlontimemean.npz')
lat     = file['lat']
lon     = file['lon']
pr_aqct = file['pr_aqct']
pr_aq4x = file['pr_aq4x']
pr_ldct = file['pr_ldct']
pr_ld4x = file['pr_ld4x']
pr_ldor = file['pr_ldor']

nmod   = pr_aqct[:, 0, 0].size
sinlat = np.sin(lat*np.pi/180)
nlon   = lon.size

# list of available models
modlist_aqct = tracmipmodels.get_availablemodels('aqct')
modlist_aq4x = tracmipmodels.get_availablemodels('aq4x')
modlist_ldct = tracmipmodels.get_availablemodels('ldct')
modlist_ld4x = tracmipmodels.get_availablemodels('ld4x')
modlist_ldor = tracmipmodels.get_availablemodels('ldor')

# model median itcz
itcz_aqct_mm = np.zeros(nlon) + np.nan
itcz_aq4x_mm = np.zeros(nlon) + np.nan
itcz_ldct_mm = np.zeros(nlon) + np.nan
itcz_ld4x_mm = np.zeros(nlon) + np.nan
itcz_ldor_mm = np.zeros(nlon) + np.nan
for i in range(0, nlon):
    itcz_aqct_mm[i] = atm.get_itczposition(np.nanmedian(pr_aqct[modlist_aqct, :, i], axis=0), lat, 30.0, 0.1)
    itcz_aq4x_mm[i] = atm.get_itczposition(np.nanmedian(pr_aq4x[modlist_aq4x, :, i], axis=0), lat, 30.0, 0.1)
    itcz_ldct_mm[i] = atm.get_itczposition(np.nanmedian(pr_ldct[modlist_ldct, :, i], axis=0), lat, 30.0, 0.1)
    itcz_ld4x_mm[i] = atm.get_itczposition(np.nanmedian(pr_ld4x[modlist_ld4x, :, i], axis=0), lat, 30.0, 0.1)
    itcz_ldor_mm[i] = atm.get_itczposition(np.nanmedian(pr_ldor[modlist_ldor, :, i], axis=0), lat, 30.0, 0.1)
        

# plotting
plt.figure( figsize=(12, 12), dpi=80, facecolor='w', edgecolor='k')

clev   = np.array([1, 3, 5, 7, 9, 11, 13])
clev   = np.array([1, 3, 5, 7, 9, 11])

ax = plt.subplot(3, 2, 1)
plt.contourf(lon, sinlat, np.nanmedian(pr_aqct[modlist_aqct], axis=0), clev, cmap=cm.Blues, extend='both')
plt.plot([-200, 200], [0, 0], 'k--')
plt.plot(lon, np.sin(itcz_aqct_mm*np.pi/180.0), 'firebrick', linewidth=2)
make_nice_plot(ax, 'AquaControl')#, plt.ylabel('hPa', fontsize=12)
ax.yaxis.set_ticklabels(['30S', 'Eq', '30N'], fontsize=10)
plt.text(0.02, 0.93, 'a)', fontsize=14, ha='left', va='center', transform=ax.transAxes, backgroundcolor='white')

ax = plt.subplot(3, 2, 3)
c=plt.contourf(lon, sinlat, np.nanmedian(pr_aq4x[modlist_aq4x], axis=0), clev, cmap=cm.Blues, extend='both')
plt.plot([-200, 200], [0, 0], 'k--')
plt.plot(lon, np.sin(itcz_aq4x_mm*np.pi/180.0), 'firebrick', linewidth=2)
make_nice_plot(ax, 'Aqua4xCO2')
ax.xaxis.set_ticklabels(['120W', '60W', '0', '60E', '120E'],fontsize=10)
ax.yaxis.set_ticklabels(['30S', 'Eq', '30N'], fontsize=10)
plt.text(0.02, 0.93, 'c)', fontsize=14, ha='left', va='center', transform=ax.transAxes, backgroundcolor='white')

ax = plt.subplot(3, 2, 5)
ax.axis('off')
cbar = plt.colorbar(c, ticks=clev, orientation='horizontal', aspect=30)
cbar.ax.tick_params(labelsize=10) 
plt.text(0, -0.1, 'mm/day', fontsize=10, ha='left', va='center', transform=ax.transAxes)

ax = plt.subplot(3, 2, 2)
plt.contourf(lon, sinlat, np.nanmedian(pr_ldct[modlist_ldct], axis=0), clev, cmap=cm.Blues, extend='both')
plt.plot([-200, 200], [0, 0], 'k--')
plt.plot(lon, np.sin(itcz_ldct_mm*np.pi/180.0), 'firebrick', linewidth=2)
make_nice_plot(ax, 'LandControl')
ax.add_patch(Rectangle((0, -0.5), 45, 1, alpha=1, facecolor='none', edgecolor='gray', linewidth=2))
plt.text(0.02, 0.93, 'b)', fontsize=14, ha='left', va='center', transform=ax.transAxes, backgroundcolor='white')

ax = plt.subplot(3, 2, 4)
plt.contourf(lon, sinlat, np.nanmedian(pr_ld4x[modlist_ld4x], axis=0), clev, cmap=cm.Blues, extend='both')
plt.plot([-200, 200], [0, 0], 'k--')
plt.plot(lon, np.sin(itcz_ld4x_mm*np.pi/180.0), 'firebrick', linewidth=2)
make_nice_plot(ax, 'Land4xCO2')
from matplotlib.patches import Rectangle
ax.add_patch(Rectangle((0, -0.5), 45, 1, alpha=1, facecolor='none', edgecolor='gray', linewidth=2))
plt.text(0.02, 0.93, 'd)', fontsize=14, ha='left', va='center', transform=ax.transAxes, backgroundcolor='white')

ax = plt.subplot(3, 2, 6)
plt.contourf(lon, sinlat, np.nanmedian(pr_ldor[modlist_ldor], axis=0), clev, cmap=cm.Blues, extend='both')
plt.plot([-200, 200], [0, 0], 'k--')
plt.plot(lon, np.sin(itcz_ldor_mm*np.pi/180.0), 'firebrick', linewidth=2)
make_nice_plot(ax, 'LandOrbit')
from matplotlib.patches import Rectangle
ax.add_patch(Rectangle((0, -0.5), 45, 1, alpha=1, facecolor='none', edgecolor='gray', linewidth=2))
ax.xaxis.set_ticklabels(['120W', '60W', '0', '60E', '120E'],fontsize=10)
ax.yaxis.set_ticklabels(['30S', 'Eq', '30N'], fontsize=10)
plt.text(0.02, 0.93, 'e)', fontsize=14, ha='left', va='center', transform=ax.transAxes, backgroundcolor='white')

plt.tight_layout 

plt.savefig('figs/pr_modelmedian_allexperiments.pdf')                      

plt.show()
