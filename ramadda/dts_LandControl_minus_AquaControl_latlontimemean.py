# load modules
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import cm
from matplotlib.patches import Rectangle

# load own modules
import sys
sys.path.append('/home/aiko/Dropbox/Projects/current/Tracmip/analysis/python3/ramadda/mymodules/')
import tracmipmodels as tracmipmodels

def make_niceplot(ax, modelname):
    ax.xaxis.set_ticks([-120, -60, 0, 60, 120])
    ax.xaxis.set_ticklabels([''], fontsize=11)
    ax.yaxis.set_ticks([-0.866, -0.5, 0, 0.5, 0.866])
    ax.yaxis.set_ticklabels([''], fontsize=11) 
    plt.text(0.03, 0.93, modelname, fontsize=15, ha='left', va='center', \
             transform=ax.transAxes, backgroundcolor='white')
    plt.xlim(-175, 175), plt.ylim(-0.98, 0.98)         

# load ts data
file    = np.load('ts_latlontimemean.npz')
lat     = file['lat']
lon     = file['lon']
ts_aqct = file['ts_aqct']
ts_ldct = file['ts_ldct']

nmod = ts_aqct[:, 0, 0].size
nlat = lat.size
nlon = lon.size

sinlat = np.sin(lat*np.pi/180)

# get model colors, names and numbers
modelcolors   = tracmipmodels.get_modelcolors(nmod)
modelnames    = tracmipmodels.get_modelnames()
modelnumbers  = tracmipmodels.get_modelnumbers()

# list of available models
modlist_aqct = tracmipmodels.get_availablemodels('aqct')
modlist_ldct = tracmipmodels.get_availablemodels('ldct')

# plotting
plt.figure(figsize=(18, 20), dpi=80, facecolor='w', edgecolor='k' )
clev = np.array([-3.5, -3.0, -2.5, -2.0, -1.5, -1.0, -0.5, 0.5, 1.0, 1.5, 2.0, 2.5, 3.0, 3.5])

ax = plt.subplot(5, 3, 1)
c = plt.contourf(lon, sinlat, np.nanmedian((ts_ldct-ts_aqct)[modlist_ldct], axis=0), clev, extend='both', cmap=cm.RdBu_r)
ax.add_patch(Rectangle((0, -0.5), 45, 1, alpha=1, facecolor='none', edgecolor='gray', linewidth=2))
plt.plot([-200, 200], [0, 0], 'k--')
make_niceplot(ax, 'Model median')
ax.yaxis.set_ticklabels(['60S', '30S', 'Eq', '30N', '60N'], fontsize=11) 
ax = plt.subplot(5, 3, 2)
ax.axis('off')
cbar = plt.colorbar(c, ticks=[-3, -2, -1, 0, 1, 2, 3], orientation='horizontal', aspect=30)
cbar.ax.tick_params(labelsize=11)
plt.text(1, -0.17, 'K', fontsize=11, ha='right')  

for m in modlist_ldct:
    if modelnames[m] == 'AM2.1'    : msubplot = 3
    if modelnames[m] == 'CAM3'     : msubplot = 4 
    if modelnames[m] == 'CAM4'     : msubplot = 5 
    if modelnames[m] == 'CAM5Nor'  : msubplot = 6
    if modelnames[m] == 'CNRM-AM5' : msubplot = 7 
    if modelnames[m] == 'ECHAM6.1' : msubplot = 8
    if modelnames[m] == 'ECHAM6.3' : msubplot = 9 
    if modelnames[m] == 'LMDZ5A'   : msubplot = 10
    if modelnames[m] == 'MetUM-CTL': msubplot = 11
    if modelnames[m] == 'MetUM-ENT': msubplot = 12
    if modelnames[m] == 'MIROC5'   : msubplot = 13  
    if modelnames[m] == 'MPAS'     : msubplot = 14
    if modelnames[m] == 'CALTECH'  : msubplot = 15
    ax = plt.subplot(5, 3, msubplot)
    c = plt.contourf(lon, sinlat, (ts_ldct-ts_aqct)[m, :, :], clev, extend='both', cmap=cm.RdBu_r)
    ax.add_patch(Rectangle((0, -0.5), 45, 1, alpha=1, facecolor='none', edgecolor='gray', linewidth=2))
    plt.plot([-200, 200], [0, 0], 'k--')
    make_niceplot(ax, modelnames[m])
    if (msubplot == 13) or (msubplot == 14) or (msubplot ==15):
        ax.xaxis.set_ticklabels(['120W', '60W', '0', '60E', '120E'], fontsize=11) 
    if msubplot in [1, 4, 7, 10, 13]:
        ax.yaxis.set_ticklabels(['60S', '30S', 'Eq', '30N', '60N'], fontsize=11) 

plt.subplots_adjust(wspace=0.04, hspace=0.05)  
plt.tight_layout

plt.savefig('figs/dts_latlontimemean_LandControl_minus_AquaControl_allmodels.pdf')

plt.show()
