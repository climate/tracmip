# load modules
import numpy as np
import matplotlib.pyplot as plt

# load own modules
import sys
sys.path.append('/home/fd8940/Dropbox/Projects/current/Tracmip/analysis/python3/ramadda/mymodules/')
sys.path.append('/home/fd8940/Dropbox/Python3/mymodules/')
import tracmipmodels as tracmipmodels

# loading data
print('Load global time mean ts')
file    = np.load('ts_globaltimemean.npz')
ts_aqct = file['ts_aqct']
ts_ldct = file['ts_ldct']

nmod = ts_aqct.size

# get model colors, names and numbers
modelcolors   = tracmipmodels.get_modelcolors(nmod)
modelnames    = tracmipmodels.get_modelnames()
modelnumbers  = tracmipmodels.get_modelnumbers()

# get available models
modlist_aqct = tracmipmodels.get_availablemodels('aqct')
modlist_ldct = tracmipmodels.get_availablemodels('ldct')

# plotting
plt.figure( figsize=(12, 4), dpi=80, facecolor='w', edgecolor='k' )

ax = plt.subplot(1, 2, 1)
ax.spines['right'].set_color('none')
ax.spines['top'].set_color('none')
ax.xaxis.set_ticks_position('bottom')
ax.yaxis.set_ticks_position('left')

plt.plot([280, 310], [280, 310], 'k--')
for i in modlist_ldct:
    plt.text(ts_aqct[i], ts_ldct[i], modelnumbers[i], fontweight='bold', color=modelcolors[i], fontsize=14)
plt.title('Global surface temperature (K)', fontsize=14)
plt.xlabel('AquaControl', fontsize=12)
plt.ylabel('LandControl', fontsize=12)
ax.xaxis.set_ticks([292, 294, 296, 298, 300, 302])
ax.xaxis.set_ticklabels([292, 294, 296, 298, 300, 302], fontsize=10)
ax.yaxis.set_ticks([292, 294, 296, 298, 300, 302])
ax.yaxis.set_ticklabels([292, 294, 296, 298, 300, 302], fontsize=10)
plt.xlim(290, 304), plt.ylim(290, 304)

ax = plt.subplot(1, 2, 2)
ax.spines['right'].set_color('none')
ax.spines['top'].set_color('none')
ax.xaxis.set_ticks_position('bottom')
ax.yaxis.set_ticks_position('left')

plt.plot([280, 310], [0, 0], 'k--')
for i in modlist_ldct:
    plt.text(ts_aqct[i], (ts_ldct-ts_aqct)[i], modelnumbers[i], fontweight='bold', color=modelcolors[i], fontsize=14)
plt.title('Global surface temperature (K)', fontsize=14)
plt.xlabel('AquaControl', fontsize=12)
plt.ylabel('LandControl - AquaControl', fontsize=12)
ax.xaxis.set_ticks([292, 294, 296, 298, 300, 302])
ax.xaxis.set_ticklabels([292, 294, 296, 298, 300, 302], fontsize=10)
ax.yaxis.set_ticks([-2, -1, 0, 1, 2])
ax.yaxis.set_ticklabels([-2, -1, 0, 1, 2], fontsize=10)
plt.xlim(290, 304), plt.ylim(-2, 2)


plt.tight_layout
plt.savefig('figs/ts_globaltimemean_AquaControl_LandControl.pdf')
    
print(np.nanmedian((ts_ldct-ts_aqct)[modlist_ldct]))    
print((ts_ldct-ts_aqct)[modlist_ldct])  
    
plt.show()
