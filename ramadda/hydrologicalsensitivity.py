# load modules
import numpy as np
import matplotlib.pyplot as plt
import scipy.stats as spstats

# load own modules
import sys
sys.path.append('/home/aiko/Dropbox/Projects/current/Tracmip/analysis/python3/ramadda/mymodules/')
import tracmipmodels as tracmipmodels

# load global mean pr and ts data
file    = np.load('pr_globaltimemean.npz')
pr_aqct = file['pr_aqct']
pr_aq4x = file['pr_aq4x']
pr_ldct = file['pr_ldct']
pr_ld4x = file['pr_ld4x'] 
pr_ldor = file['pr_ldor'] 
file    = np.load('ts_globaltimemean.npz')
ts_aqct = file['ts_aqct']
ts_aq4x = file['ts_aq4x']
ts_ldct = file['ts_ldct']
ts_ld4x = file['ts_ld4x'] 
ts_ldor = file['ts_ldor'] 

nmod = pr_aqct.size

# get model colors, names and numbers
modelcolors   = tracmipmodels.get_modelcolors(nmod)
modelnames    = tracmipmodels.get_modelnames()
modelnumbers  = tracmipmodels.get_modelnumbers()

# plotting
fig = plt.figure( figsize=(12, 4), dpi=80, facecolor='w', edgecolor='k' )

ax = plt.subplot(1, 2, 1)
ax.spines['right'].set_color('none')
ax.spines['top'].set_color('none')
ax.xaxis.set_ticks_position('bottom')
ax.yaxis.set_ticks_position('left')
plt.plot(273.15+14.3, 2.67, 'kx', ms=12, mew=3)  
xfit = np.linspace(286, 310, 100)
yfit = 2.67 + 0.02*(xfit - (273.15+14.3) )*2.7; plt.plot(xfit, yfit, 'k')
for i in range(0, nmod):
    plt.text(ts_aqct[i], pr_aqct[i], modelnumbers[i], ha='center', va='center', color='royalblue', fontsize=14, fontweight='bold')
for i in range(1, nmod-1):   
    plt.text(ts_ldct[i], pr_ldct[i], modelnumbers[i], ha='center', va='center', color='chocolate', fontsize=14, fontweight='bold')
plt.xlim(287, 302), plt.ylim(2.6, 4.5)
plt.xlabel('Surface temperature (K)')
plt.ylabel('Precipitation (mm/day)')
plt.text(1, 0.12, 'AquaControl', color='royalblue' , fontsize=12, fontweight='normal', ha='right', transform=ax.transAxes)
plt.text(1, 0.05, 'LandControl', color='chocolate', fontsize=12, fontweight='normal', ha='right', transform=ax.transAxes)
plt.text(0.02, 1.0, 'a)', fontsize=14, ha='left', va='center', transform=ax.transAxes)
plt.title('Control climate', fontsize=14)
plt.text(300, 3.3, '2%/K', fontsize=12)
plt.text(289, 2.7, 'present-day', fontsize=12, fontstyle='italic')

ax = plt.subplot(1, 2, 2)
ax.spines['right'].set_color('none')
ax.spines['top'].set_color('none')
ax.xaxis.set_ticks_position('bottom')
ax.yaxis.set_ticks_position('left')
xfit = np.linspace(0, 15, 100)
yfit = 2*xfit; plt.plot(xfit, yfit, 'k')
plt.xlim(0, 12), plt.ylim(0, 22)
for i in range(0, nmod):
    plt.text(ts_aq4x[i] - ts_aqct[i], 100*(pr_aq4x[i] - pr_aqct[i])/pr_aqct[i], \
             modelnumbers[i], color='royalblue', fontsize=14, fontweight='bold')
for i in range(1, nmod-1):   
    plt.text(ts_ld4x[i] - ts_ldct[i], 100*(pr_ld4x[i] - pr_ldct[i])/pr_ldct[i], \
             modelnumbers[i], color='chocolate', fontsize=14, fontweight='bold')
plt.xlabel('Surface temperature change (K)')
plt.ylabel('Precipitation change (%)')
plt.text(1, 0.12, 'Aqua4xCO2 - AquaControl', ha='right', va='center', color='royalblue' , fontsize=12, fontweight='normal', transform=ax.transAxes)
plt.text(1, 0.05, 'Land4xCO2 - LandControl', ha='right', va='center', color='chocolate', fontsize=12, fontweight='normal', transform=ax.transAxes)
plt.text(0.02, 1.0, 'b)', fontsize=14, ha='left', va='center', transform=ax.transAxes)
plt.title('4xCO2', fontsize=14)
plt.text(10, 20, '2%/K', fontsize=12)

plt.tight_layout

plt.savefig('figs/hydrologicalsensitivity.pdf')

print(np.nanmedian(pr_aqct))
print(pr_aqct)

r, _, corr, p, _ = spstats.linregress(ts_aq4x - ts_aqct, (pr_aq4x - pr_aqct)/pr_aqct)
print(r, corr, p)

r, _, corr, p, _ = spstats.linregress(ts_ld4x[1:11] - ts_ldct[1:11], (pr_ld4x[1:11] - pr_ldct[1:11])/pr_ldct[1:11])
print(r, corr, p)

plt.show()
