# load modules
import numpy as np

# load own modules
import sys
sys.path.append('/home/aiko/Dropbox/Projects/current/Tracmip/analysis/python3/ramadda/mymodules/')

import loaddata as loaddata

# load data from ramadda server
clt_aqct, lat, lon = loaddata.get_data_latlontm('AQUA'   , 'clt')
clt_aq4x, lat, lon = loaddata.get_data_latlontm('AQUACO2', 'clt')
clt_ldct, lat, lon = loaddata.get_data_latlontm('LAND'   , 'clt')
clt_ld4x, lat, lon = loaddata.get_data_latlontm('LANDCO2', 'clt')
clt_ldor, lat, lon = loaddata.get_data_latlontm('LORBIT' , 'clt')

# clt needs to be in units of 0-100%, so we need to convert from fraction to percent for
# CAM3, CAM4, LMDz5A, MIROC5, and MPAS
clt_aqct[[3,6,8,9,10]]=100*clt_aqct[[3,6,8,9,10]]
clt_aq4x[[3,6,8,9,10]]=100*clt_aq4x[[3,6,8,9,10]]
clt_ldct[[3,6,8,9,10]]=100*clt_ldct[[3,6,8,9,10]]
clt_ld4x[[3,6,8,9,10]]=100*clt_ld4x[[3,6,8,9,10]]
clt_ldor[[3,6,8,9,10]]=100*clt_ldor[[3,6,8,9,10]]

# saving data for later use
print('Saving lat-lon time mean total cloud cover')
np.savez('clt_latlontimemean.npz', \
         **{'clt_aqct':clt_aqct, 'clt_aq4x':clt_aq4x, 'clt_ldct':clt_ldct, \
            'clt_ld4x':clt_ld4x, 'clt_ldor':clt_ldor, 'lat':lat, 'lon':lon})
