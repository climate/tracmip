# load modules
import numpy as np
import matplotlib.pyplot as plt

# load own modules
import sys
sys.path.append('/home/aiko/Dropbox/Projects/current/Tracmip/analysis/python3/ramadda/mymodules/')
sys.path.append('/home/aiko/Dropbox/Python3/mymodules/')

models=['CNRM', 'ECHAM61', 'ECHAM63', 'IPSL', 'MetCTL', 'MetENT', \
        'MIROC5', 'AM2', 'CAM3', 'CAM4', 'MPAS']

# load global mean pr and ts data
file    = np.load('ts_zonaltimemean.npz')
lat     = file['lat']
ts_ldct = file['ts_ldct']
ts_ldor = file['ts_ldor'] 

nmod = ts_ldct[:, 0].size


# plotting
fig = plt.figure( figsize=(15, 10), dpi=80, facecolor='w', edgecolor='k' )

for i in range(0, nmod):
    ax = plt.subplot(3, 4, i+1)
    ax.spines['right'].set_color('none')
    ax.spines['top'].set_color('none')
    ax.xaxis.set_ticks_position('bottom')
    ax.yaxis.set_ticks_position('left')
    plt.plot(lat, ts_ldct[i, :] - 273.15, 'b', linewidth=2)
    plt.plot(lat, ts_ldor[i, :] - 273.15, 'g', linewidth=2)
    plt.plot(lat, 10*(ts_ldor[i, :] - ts_ldct[i, :]), 'g--', linewidth=2)
    plt.title(models[i])
    plt.xlim(-80, 80), plt.ylim(-1, 35)
    if i==10:
        plt.text(120, 8, 'LandControl', color='blue' , fontsize=14, fontweight='bold')
        plt.text(120, 0, 'LandOrbit'  , color='green', fontsize=14, fontweight='bold')
  
plt.tight_layout

plt.savefig('figs/ts_landcontrol_versus_landorbit.pdf')

plt.show()
