# load modules
import numpy as np
import matplotlib.pyplot as plt

# load own modules
import sys
sys.path.append('/home/aiko/Dropbox/Projects/current/Tracmip/analysis/python3/ramadda/mymodules/')
sys.path.append('/home/aiko/Dropbox/Python3/mymodules/')
import tracmipmodels as tracmipmodels

models=['CNRM', 'ECHAM61', 'ECHAM63', 'IPSL', 'MetCTL', 'MetENT', \
        'MIROC5', 'AM2', 'CAM3', 'CAM4', 'MPAS', 'GISS']

# load rsdscs and rsuscs data
file        = np.load('rsdscs_latlontimemean.npz')
lat         = file['lat']
lon         = file['lon']
rsdscs_aqct = file['rsdscs_aqct']
rsdscs_ldct = file['rsdscs_ldct']
file        = np.load('rsuscs_latlontimemean.npz')
rsuscs_aqct = file['rsuscs_aqct']
rsuscs_ldct = file['rsuscs_ldct']

nmod = rsdscs_aqct[:, 0, 0].size
nlat = lat.size
nlon = lon.size

# get model colors, names and numbers
modelcolors   = tracmipmodels.get_modelcolors(nmod)
modelnames    = tracmipmodels.get_modelnames()
modelnumbers  = tracmipmodels.get_modelnumbers()

plt.figure()
for m in range(0, nmod):
    plt.subplot(3, 4, m+1)
    c = plt.contourf(lon, lat, (rsuscs_ldct/rsdscs_ldct)[m, :, :])
    plt.colorbar(c)    
    plt.title(modelnames[m])


plt.figure()
for m in range(0, nmod):
    plt.subplot(3, 4, m+1)
    c = plt.contourf(lon, lat, (rsuscs_ldct/rsdscs_ldct - rsuscs_aqct/rsdscs_aqct)[m, :, :])#, \
                     #[-0.01, -0.005, 0.005, 0.01, 0.02, 0.03, 0.04, 0.05, 0.06, 0.07, 0.08, 0.09, 0.1])
    plt.colorbar(c)    
    plt.title(modelnames[m])


plt.show()
