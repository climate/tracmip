import netCDF4 as nc
import numpy as np
from scipy.interpolate import griddata

###############################################################################
# Notes: 
#  - wheneven a string is missing it is set to '???' (e.g., because
#    a variable is not available for a given model or experiment, or an
#    experiment is not available for a given model)


###############################################################################

# general setup
# number of models
nmod = 15
# number of months to be read in and averaged over
nmth = 240
# lat is the lattiudes on which data will be interpolated 
lat  = np.linspace(-89.5, 89.5, 180)     
nlat = lat.size
lon  = np.linspace(-179.0, 179.0, 180)     
nlon = lon.size
# lev are the pressure levels we want to interpolate to; we use the 17 standard
# CMIP pressure levels in hPa
lev = 100*np.array([ 10,  20,  30,  50,  70,  100, 150, 200, 250, 300, 400,
                    500, 600, 700, 850, 925, 1000])
nlev = lev.size

# grid info for AM2 is missing in umiami files and thus is stored here
latAM2 = np.array([-89.49438, -87.97753, -85.95506, -83.93259, -81.91011, -79.88764, \
    -77.86517, -75.8427, -73.82022, -71.79775, -69.77528, -67.75281,                 \
    -65.73034, -63.70787, -61.68539, -59.66292, -57.64045, -55.61798,                \
    -53.5955, -51.57303, -49.55056, -47.52809, -45.50562, -43.48315,                 \
    -41.46067, -39.4382, -37.41573, -35.39326, -33.37078, -31.34831,                 \
    -29.32584, -27.30337, -25.2809, -23.25843, -21.23595, -19.21348,                 \
    -17.19101, -15.16854, -13.14607, -11.1236, -9.101124, -7.078652,                 \
    -5.05618, -3.033708, -1.011236, 1.011236, 3.033708, 5.05618, 7.078652,           \
    9.101124, 11.1236, 13.14607, 15.16854, 17.19101, 19.21348, 21.23595,             \
    23.25843, 25.2809, 27.30337, 29.32584, 31.34831, 33.37078, 35.39326,             \
    37.41573, 39.4382, 41.46067, 43.48315, 45.50562, 47.52809, 49.55056,             \
    51.57303, 53.5955, 55.61798, 57.64045, 59.66292, 61.68539, 63.70787,             \
    65.73034, 67.75281, 69.77528, 71.79775, 73.82022, 75.8427, 77.86517,             \
    79.88764, 81.91011, 83.93259, 85.95506, 87.97753, 89.49438])
lonAM2 = np.array([1.25, 3.75, 6.25, 8.75, 11.25, 13.75, 16.25, 18.75, 21.25, 23.75, \
    26.25, 28.75, 31.25, 33.75, 36.25, 38.75, 41.25, 43.75, 46.25, 48.75,            \
    51.25, 53.75, 56.25, 58.75, 61.25, 63.75, 66.25, 68.75, 71.25, 73.75,            \
    76.25, 78.75, 81.25, 83.75, 86.25, 88.75, 91.25, 93.75, 96.25, 98.75,            \
    101.25, 103.75, 106.25, 108.75, 111.25, 113.75, 116.25, 118.75, 121.25,          \
    123.75, 126.25, 128.75, 131.25, 133.75, 136.25, 138.75, 141.25, 143.75,          \
    146.25, 148.75, 151.25, 153.75, 156.25, 158.75, 161.25, 163.75, 166.25,          \
    168.75, 171.25, 173.75, 176.25, 178.75, 181.25, 183.75, 186.25, 188.75,          \
    191.25, 193.75, 196.25, 198.75, 201.25, 203.75, 206.25, 208.75, 211.25,          \
    213.75, 216.25, 218.75, 221.25, 223.75, 226.25, 228.75, 231.25, 233.75,          \
    236.25, 238.75, 241.25, 243.75, 246.25, 248.75, 251.25, 253.75, 256.25,          \
    258.75, 261.25, 263.75, 266.25, 268.75, 271.25, 273.75, 276.25, 278.75,          \
    281.25, 283.75, 286.25, 288.75, 291.25, 293.75, 296.25, 298.75, 301.25,          \
    303.75, 306.25, 308.75, 311.25, 313.75, 316.25, 318.75, 321.25, 323.75,          \
    326.25, 328.75, 331.25, 333.75, 336.25, 338.75, 341.25, 343.75, 346.25,          \
    348.75, 351.25, 353.75, 356.25, 358.75])    
    

def cmip5var_to_modelvar(cmip5var, model):
   modelvar_array = get_modelvar_array(cmip5var)
   if model   == 'CNRM'   :
      modelvar = modelvar_array['CNRM'   ]
   elif model == 'CNRMv2' :
      modelvar = modelvar_array['CNRMv2' ]  
   elif model == 'ECHAM61':
      modelvar = modelvar_array['ECHAM61']
   elif model == 'ECHAM63':
      modelvar = modelvar_array['ECHAM61']
   elif model == 'IPSL'   :
      modelvar = modelvar_array['IPSL'   ]
   elif model == 'MetCTL' :
      modelvar = modelvar_array['MetCTL' ]
   elif model == 'MetENT' :
      modelvar = modelvar_array['MetENT' ]   
   elif model == 'MIROC5' :
      modelvar = modelvar_array['MIROC5' ]     
   elif model == 'AM2'    :
      modelvar = modelvar_array['AM2'    ]  
   elif model == 'CAM3'   :
      modelvar = modelvar_array['CAM3'   ]   
   elif model == 'CAM4'   :
      modelvar = modelvar_array['CAM4'   ]   
   elif model == 'MPAS'   :
      modelvar = modelvar_array['MPAS'   ]  
   elif model == 'GISS'   :
      modelvar = modelvar_array['GISS'   ]     
   elif model == 'NORESM'   :
      modelvar = modelvar_array['NORESM' ]     
   elif model == 'CALTECH'   :
      modelvar = modelvar_array['CALTECH' ] 
   else:
      print('ERROR in cmip5var_to_modelvar: model not known!') 
   return modelvar


def get_modelvar_array(cmip5var):

   modelvar_array = {'CNRM'  :'???', 'ECHAM61':'???', 'ECHAM63':'???', 'IPSL':'???', \
                     'MetCTL':'???', 'MetENT' :'???', 'MIROC5' :'???', 'AM2' :'???', \
                     'CAM3'  :'???', 'CAM4'   :'???', 'MPAS'   :'???', 'GISS':'???', \
                     'NORESM':'???', 'CNRMv2' :'???', 'CALTECH':'???'}
   
   if cmip5var=='pr':
      modelvar_array = {'CNRM'  :'pr' , 'ECHAM61':'pr' , 'ECHAM63':'pr' , 'IPSL':'pr' , \
                        'MetCTL':'pr' , 'MetENT' :'pr' , 'MIROC5' :'pr' , 'AM2' :'pr' , \
                        'CAM3'  :'???', 'CAM4'   :'???', 'MPAS'   :'???', 'GISS':'pr' , \
                        'NORESM':'pr' , 'CNRMv2' :'pr' , 'CALTECH':'pr'} 
   elif cmip5var=='prc':
      modelvar_array = {'CNRM'  :'???', 'ECHAM61':'???', 'ECHAM63':'???', 'IPSL':'???'  , \
                        'MetCTL':'???', 'MetENT' :'???', 'MIROC5' :'???', 'AM2' :'???'  , \
                        'CAM3'  :'PRECC', 'CAM4' :'PRECC', 'MPAS' :'PRECC', 'GISS':'???', \
                        'NORESM':'???', 'CNRMv2' :'???', 'CALTECH':'prc'}   
   elif cmip5var=='prl':
      modelvar_array = {'CNRM'  :'???', 'ECHAM61':'???', 'ECHAM63':'???', 'IPSL':'???'  , \
                        'MetCTL':'???', 'MetENT' :'???', 'MIROC5' :'???', 'AM2' :'???'  , \
                        'CAM3'  :'PRECL', 'CAM4' :'PRECL', 'MPAS' :'PRECL', 'GISS':'???', \
                        'NORESM':'???', 'CNRMv2' :'???', 'CALTECH':'???'    }  
   elif cmip5var=='rsdscs':
      modelvar_array = {'CNRM'  :'rsdscs', 'ECHAM61':'rsdscs', 'ECHAM63':'rsdscs', 'IPSL':'rsdscs', \
                        'MetCTL':'rsdscs', 'MetENT' :'rsdscs', 'MIROC5' :'rsdscs', 'AM2' :'rsdscs', \
                        'CAM3'  :'FSDSC' , 'CAM4'   :'FSDSC' , 'MPAS'   :'FSDSC' , 'GISS':'rsdscs', \
                        'NORESM':'rsdscs', 'CNRMv2' :'rsdscs', 'CALTECH':'???'}                      
   elif cmip5var=='rsdt':
      modelvar_array = {'CNRM'  :'rsdt'  , 'ECHAM61':'rsdt'  , 'ECHAM63':'rsdt', 'IPSL':'rsdt',   \
                        'MetCTL':'rsdt'  , 'MetENT' :'rsdt'  , 'MIROC5' :'rsdt', 'AM2' :'rsdt',   \
                        'CAM3'  :'SOLIN' , 'CAM4'   :'FSDTOA', 'MPAS'   :'FSDTOA', 'GISS':'rsdt', \
                        'NORESM':'rsdt'  , 'CNRMv2' :'rsdt'  , 'CALTECH':'rsdt'}
   elif cmip5var=='rsnscs':
      modelvar_array = {'CNRM'  :'rsnscs', 'ECHAM61':'rsnscs', 'ECHAM63':'rsnscs', 'IPSL':'rsnscs', \
                        'MetCTL':'rsnscs', 'MetENT' :'rsnscs', 'MIROC5' :'rsnscs', 'AM2' :'???'   , \
                        'CAM3'  :'FSNSC' , 'CAM4'   :'FSNSC' , 'MPAS'   :'FSNSC' , 'GISS':'rsnscs', \
                        'NORESM':'rsnscs', 'CNRMv2' :'rsnscs', 'CALTECH':'???'}                      
   elif cmip5var=='rsuscs':
      modelvar_array = {'CNRM'  :'rsuscs', 'ECHAM61':'rsuscs', 'ECHAM63':'rsuscs', 'IPSL':'rsuscs', \
                        'MetCTL':'rsuscs', 'MetENT' :'rsuscs', 'MIROC5' :'rsuscs', 'AM2' :'rsuscs', \
                        'CAM3'  :'???'   , 'CAM4'   :'???'   , 'MPAS'   :'???'   , 'GISS':'rsuscs', \
                        'NORESM':'rsuscs', 'CNRMv2' :'rsuscs', 'CALTECH':'???'}  
   elif cmip5var=='ts':
      modelvar_array = {'CNRM'  :'ts', 'ECHAM61':'ts', 'ECHAM63':'ts', 'IPSL':'ts', \
                        'MetCTL':'ts', 'MetENT' :'ts', 'MIROC5' :'ts', 'AM2' :'ts', \
                        'CAM3'  :'TS', 'CAM4'   :'TS', 'MPAS'   :'TS', 'GISS':'ts', \
                        'NORESM':'ts', 'CNRMv2' :'ts', 'CALTECH':'ts'} 
   elif cmip5var=='ua':
      modelvar_array = {'CNRM'  :'ua', 'ECHAM61':'ua', 'ECHAM63':'ua', 'IPSL':'ua', \
                        'MetCTL':'ua', 'MetENT' :'ua', 'MIROC5' :'ua', 'AM2' :'ua', \
                        'CAM3'  :'U' , 'CAM4'   :'U' , 'MPAS'   :'U' , 'GISS':'ua', \
                        'NORESM':'ua', 'CNRMv2' :'ua', 'CALTECH':'ua'}                     
   elif cmip5var=='va':
      modelvar_array = {'CNRM'  :'va', 'ECHAM61':'va', 'ECHAM63':'va', 'IPSL':'va', \
                        'MetCTL':'va', 'MetENT' :'va', 'MIROC5' :'va', 'AM2' :'va', \
                        'CAM3'  :'V' , 'CAM4'   :'V' , 'MPAS'   :'V' , 'GISS':'va', \
                        'NORESM':'va', 'CNRMv2' :'va', 'CALTECH':'va'}  
   elif cmip5var=='clt':
      modelvar_array = {'CNRM'  :'clt'   , 'ECHAM61':'clt'   , 'ECHAM63':'clt'   , 'IPSL':'clt', \
                        'MetCTL':'clt'   , 'MetENT' :'clt'   , 'MIROC5' :'clt'   , 'AM2' :'clt', \
                        'CAM3'  :'CLDTOT', 'CAM4'   :'CLDTOT', 'MPAS'   :'CLDTOT', 'GISS':'clt', \
                        'NORESM':'clt'   , 'CNRMv2' :'clt'   , 'CALTECH':'???'}  
   else:
      print('ERROR in get_modelvar_array: cmip5var ' + cmip5var + ' unknown')
      
   return modelvar_array


def get_data_gmtm(exp, var):
    
    data = np.zeros(nmod) + np.nan
    
    # cnrm    
    data[0 ] = retrieve_data_from_umiami_gmtm('CNRM'   , exp, var)
    # echam61
    data[1 ] = retrieve_data_from_umiami_gmtm('ECHAM61', exp, var)
    # echam63
    data[2 ] = retrieve_data_from_umiami_gmtm('ECHAM63', exp, var)
    # ipsl
    data[3 ] = retrieve_data_from_umiami_gmtm('IPSL'   , exp, var)
    # metctl
    data[4 ] = retrieve_data_from_umiami_gmtm('MetCTL' , exp, var)
    # metent
    data[5 ] = retrieve_data_from_umiami_gmtm('MetENT' , exp, var)
    # miroc5
    data[6 ] = retrieve_data_from_umiami_gmtm('MIROC5' , exp, var) 
    # am2
    data[7 ] = retrieve_data_from_umiami_gmtm('AM2'    , exp, var)  
    # cam3
    data[8 ] = retrieve_data_from_umiami_gmtm('CAM3'   , exp, var)  
    # cam4
    data[9 ] = retrieve_data_from_umiami_gmtm('CAM4'   , exp, var)      
    # mpas
    data[10] = retrieve_data_from_umiami_gmtm('MPAS'   , exp, var) 
    # giss
    data[11] = retrieve_data_from_umiami_gmtm('GISS'   , exp, var)    
    # noresm
    data[12] = retrieve_data_from_umiami_gmtm('NORESM' , exp, var) 
    # cnrmv2
    data[13] = retrieve_data_from_umiami_gmtm('CNRMv2' , exp, var)    
    # caltech
    data[14] = retrieve_data_from_umiami_gmtm('CALTECH', exp, var)    
    # output
    return data


def get_data_gmallmonths(exp, var):
    # global mean for all available months
    data = np.zeros((nmod, 50*12)) + np.nan # we assume there are at most 50 years per simulation
    # cnrm    
    data[0 , :] = retrieve_data_from_umiami_gmallmonths('CNRM'   , exp, var)
    # echam61
    data[1 , :] = retrieve_data_from_umiami_gmallmonths('ECHAM61', exp, var)
    # echam63
    data[2 , :] = retrieve_data_from_umiami_gmallmonths('ECHAM63', exp, var)
    # ipsl
    data[3 , :] = retrieve_data_from_umiami_gmallmonths('IPSL'   , exp, var)
    # metctl
    data[4 , :] = retrieve_data_from_umiami_gmallmonths('MetCTL' , exp, var)
    # metent
    data[5 , :] = retrieve_data_from_umiami_gmallmonths('MetENT' , exp, var)
    # miroc5
    data[6 , :] = retrieve_data_from_umiami_gmallmonths('MIROC5' , exp, var) 
    # am2
    data[7 , :] = retrieve_data_from_umiami_gmallmonths('AM2'    , exp, var)  
    # cam3
    data[8 , :] = retrieve_data_from_umiami_gmallmonths('CAM3'   , exp, var)  
    # cam4
    data[9 , :] = retrieve_data_from_umiami_gmallmonths('CAM4'   , exp, var)      
    # mpas
    data[10, :] = retrieve_data_from_umiami_gmallmonths('MPAS'   , exp, var) 
    # mpas
    data[11, :] = retrieve_data_from_umiami_gmallmonths('GISS'   , exp, var)    
    # noresm
    data[12, :] = retrieve_data_from_umiami_gmallmonths('NORESM' , exp, var) 
    # cnrmv2
    data[13, :] = retrieve_data_from_umiami_gmallmonths('CNRMv2' , exp, var)  
    # caltech
    data[14, :] = retrieve_data_from_umiami_gmallmonths('CALTECH', exp, var)    
    # output
    return data


def get_data_zmtm(exp, var):
    # zonal time mean data
    data = np.zeros((nmod, nlat)) + np.nan
    # cnrm    
    data[0 , :] = retrieve_data_from_umiami_zmtm('CNRM'   , exp, var)
    # echam61
    data[1 , :] = retrieve_data_from_umiami_zmtm('ECHAM61', exp, var)
    # echam63
    data[2 , :] = retrieve_data_from_umiami_zmtm('ECHAM63', exp, var)
    # ipsl
    data[3 , :] = retrieve_data_from_umiami_zmtm('IPSL'   , exp, var)
    # metctl
    data[4 , :] = retrieve_data_from_umiami_zmtm('MetCTL' , exp, var)
    # metent
    data[5 , :] = retrieve_data_from_umiami_zmtm('MetENT' , exp, var)
    # miroc5
    data[6 , :] = retrieve_data_from_umiami_zmtm('MIROC5' , exp, var) 
    # am2
    data[7 , :] = retrieve_data_from_umiami_zmtm('AM2'    , exp, var)    
    # cam3
    data[8 , :] = retrieve_data_from_umiami_zmtm('CAM3'   , exp, var)  
    # cam4
    data[9 , :] = retrieve_data_from_umiami_zmtm('CAM4'   , exp, var)      
    # mpas
    data[10, :] = retrieve_data_from_umiami_zmtm('MPAS'   , exp, var)  
    # giss
    data[11, :] = retrieve_data_from_umiami_zmtm('GISS'   , exp, var) 
    # noresm    
    data[12, :] = retrieve_data_from_umiami_zmtm('NORESM' , exp, var) 
    # cnrmv2
    data[13, :] = retrieve_data_from_umiami_zmtm('CNRMv2' , exp, var) 
    # caltech
    data[14, :] = retrieve_data_from_umiami_zmtm('CALTECH', exp, var)    
    # output
    return data, lat
    
    
def get_data_zmmm(exp, var):
    # zonal multi-month mean
    data = np.zeros((nmod, 12, nlat)) + np.nan
    # cnrm    
    data[0 , :, :] = retrieve_data_from_umiami_zmmm('CNRM'   , exp, var)
    # echam61
    data[1 , :, :] = retrieve_data_from_umiami_zmmm('ECHAM61', exp, var)
    # echam63
    data[2 , :, :] = retrieve_data_from_umiami_zmmm('ECHAM63', exp, var)
    # ipsl
    data[3 , :, :] = retrieve_data_from_umiami_zmmm('IPSL'   , exp, var)
    # metctl
    data[4 , :, :] = retrieve_data_from_umiami_zmmm('MetCTL' , exp, var)
    # metent
    data[5 , :, :] = retrieve_data_from_umiami_zmmm('MetENT' , exp, var)
    # miroc5
    data[6 , :, :] = retrieve_data_from_umiami_zmmm('MIROC5' , exp, var) 
    # am2
    data[7 , :, :] = retrieve_data_from_umiami_zmmm('AM2'    , exp, var)    
    # cam3
    data[8 , :, :] = retrieve_data_from_umiami_zmmm('CAM3'   , exp, var)  
    # cam4
    data[9 , :, :] = retrieve_data_from_umiami_zmmm('CAM4'   , exp, var)      
    # mpas
    data[10, :, :] = retrieve_data_from_umiami_zmmm('MPAS'   , exp, var)  
    # giss
    data[11, :, :] = retrieve_data_from_umiami_zmmm('GISS'   , exp, var) 
    # noresm
    data[12, :, :] = retrieve_data_from_umiami_zmmm('NORESM' , exp, var) 
    # cnrmv2
    data[13, :, :] = retrieve_data_from_umiami_zmmm('CNRMv2' , exp, var)     
    # caltech
    data[14, :, :] = retrieve_data_from_umiami_zmmm('CALTECH', exp, var)    
    # output
    return data, lat


def get_data_zmtm_plevels(exp, var):
    # zonal time mean data on 17 CMIP5 pressure levels
    data = np.zeros((nmod, nlev, nlat)) + np.nan
    # cnrm    
    data[0 , :] = retrieve_data_from_umiami_zmtm_plevels('CNRM'   , exp, var)
    # echam61
    data[1 , :] = retrieve_data_from_umiami_zmtm_plevels('ECHAM61', exp, var)
    # echam63
    data[2 , :] = retrieve_data_from_umiami_zmtm_plevels('ECHAM63', exp, var)
    # ipsl
    data[3 , :] = retrieve_data_from_umiami_zmtm_plevels('IPSL'   , exp, var)
    # metctl
    data[4 , :] = retrieve_data_from_umiami_zmtm_plevels('MetCTL' , exp, var)
    # metent
    data[5 , :] = retrieve_data_from_umiami_zmtm_plevels('MetENT' , exp, var)
    # miroc5
    data[6 , :] = retrieve_data_from_umiami_zmtm_plevels('MIROC5' , exp, var) 
    # am2
    data[7 , :] = retrieve_data_from_umiami_zmtm_plevels('AM2'    , exp, var)    
    # cam3
    data[8 , :] = retrieve_data_from_umiami_zmtm_plevels('CAM3'   , exp, var)  
    # cam4
    data[9 , :] = retrieve_data_from_umiami_zmtm_plevels('CAM4'   , exp, var)      
    # mpas
    print('get_data_zmtm_plevels, MPAS', exp, var)
    print('ATTENTION: MPAS DATA NOT ON PRESSURE LEVELS YET SO NOT RETRIEVED FROM SERVER')
    #data[10, :] = retrieve_data_from_umiami_zmtm_plevels('MPAS'   , exp, var)  
    # giss
    data[11, :] = retrieve_data_from_umiami_zmtm_plevels('GISS'   , exp, var) 
    # noresm
    data[12, :] = retrieve_data_from_umiami_zmtm_plevels('NORESM' , exp, var) 
    # cnrmv2
    data[13, :] = retrieve_data_from_umiami_zmtm_plevels('CNRMv2' , exp, var)     
    # caltech
    data[14, :] = retrieve_data_from_umiami_zmtm_plevels('CALTECH', exp, var)    
    # output    
    return data, lev, lat
    
    
def get_data_latlontm(exp, var):
    # lat-lon time mean data
    data = np.zeros((nmod, nlat, nlon)) + np.nan
    # cnrm    
    data[0 , :, :] = retrieve_data_from_umiami_latlontm('CNRM'   , exp, var)
    # echam61
    data[1 , :, :] = retrieve_data_from_umiami_latlontm('ECHAM61', exp, var)
    # echam63
    data[2 , :, :] = retrieve_data_from_umiami_latlontm('ECHAM63', exp, var)
    # ipsl
    data[3 , :, :] = retrieve_data_from_umiami_latlontm('IPSL'   , exp, var)
    # metctl
    data[4 , :, :] = retrieve_data_from_umiami_latlontm('MetCTL' , exp, var)
    # metent
    data[5 , :, :] = retrieve_data_from_umiami_latlontm('MetENT' , exp, var)
    # miroc5
    data[6 , :, :] = retrieve_data_from_umiami_latlontm('MIROC5' , exp, var) 
    # am2
    data[7 , :, :] = retrieve_data_from_umiami_latlontm('AM2'    , exp, var)    
    # cam3
    data[8 , :, :] = retrieve_data_from_umiami_latlontm('CAM3'   , exp, var)  
    # cam4
    data[9 , :, :] = retrieve_data_from_umiami_latlontm('CAM4'   , exp, var)      
    # mpas
    data[10, :, :] = retrieve_data_from_umiami_latlontm('MPAS'   , exp, var)  
    # giss
    data[11, :, :] = retrieve_data_from_umiami_latlontm('GISS'   , exp, var) 
    # noresm
    data[12, :, :] = retrieve_data_from_umiami_latlontm('NORESM' , exp, var) 
    # cnrmv2
    data[13, :, :] = retrieve_data_from_umiami_latlontm('CNRMv2' , exp, var)     
    # caltech
    data[14, :, :] = retrieve_data_from_umiami_latlontm('CALTECH', exp, var)        
    # output
    return data, lat, lon
    
    
def get_data_latlonmm(exp, var):
    # lat-lon time mean data
    data = np.zeros((nmod, 12, nlat, nlon)) + np.nan
    # cnrm    
    data[0 , :, :, :] = retrieve_data_from_umiami_latlonmm('CNRM'   , exp, var)
    # echam61
    data[1 , :, :, :] = retrieve_data_from_umiami_latlonmm('ECHAM61', exp, var)
    # echam63
    data[2 , :, :, :] = retrieve_data_from_umiami_latlonmm('ECHAM63', exp, var)
    # ipsl
    data[3 , :, :, :] = retrieve_data_from_umiami_latlonmm('IPSL'   , exp, var)
    # metctl
    data[4 , :, :, :] = retrieve_data_from_umiami_latlonmm('MetCTL' , exp, var)
    # metent
    data[5 , :, :, :] = retrieve_data_from_umiami_latlonmm('MetENT' , exp, var)
    # miroc5
    data[6 , :, :, :] = retrieve_data_from_umiami_latlonmm('MIROC5' , exp, var) 
    # am2
    data[7 , :, :, :] = retrieve_data_from_umiami_latlonmm('AM2'    , exp, var)    
    # cam3
    data[8 , :, :, :] = retrieve_data_from_umiami_latlonmm('CAM3'   , exp, var)  
    # cam4
    data[9 , :, :, :] = retrieve_data_from_umiami_latlonmm('CAM4'   , exp, var)      
    # mpas
    data[10, :, :, :] = retrieve_data_from_umiami_latlonmm('MPAS'   , exp, var)  
    # giss
    data[11, :, :, :] = retrieve_data_from_umiami_latlonmm('GISS'   , exp, var) 
    # noresm
    data[12, :, :, :] = retrieve_data_from_umiami_latlonmm('NORESM' , exp, var) 
    # cnrmv2
    data[13, :, :, :] = retrieve_data_from_umiami_latlonmm('CNRMv2' , exp, var)     
    # caltech
    data[14, :, :, :] = retrieve_data_from_umiami_latlonmm('CALTECH', exp, var)        
    # output
    return data, lat, lon    


def retrieve_data_from_umiami_gmtm(model, exp, var):
    # global time mean data
    print('retrieve_data_from_umiami_gmtm, get data for:', model, exp, var)
    # model specific variable name
    varname = '???'
    varname = cmip5var_to_modelvar(var, model)
    # file from which variable shall be read 
    fname = '???'
    fname = get_url_string(model, exp, varname) 
    print(varname, fname)
    # check if variable and file actually exists
    if (varname != '???') and (fname != '???'): 
        # file from which variable shall be read 
        file  = nc.Dataset(fname, 'r')
        dat     = np.squeeze(np.array(file.variables[varname]))
        datlat  = np.squeeze(np.array(file.variables['lat'  ]))  # latitude of data set
        if model == 'AM2': datlat = latAM2
        # average over longitude and last nmth months
        dat_zmtm = np.nanmean(np.nanmean(dat[-nmth:, :, :], 2), 0) 
        # now also average over latitude --> output out
        area = np.cos(datlat*np.pi/180.0)
        print('retrieve_data_from_umiami_gmtm, get data for', model, exp, var, 'successful')
        out  = np.nansum(dat_zmtm*area)/np.nansum(area)
    else: # either file or variable are missing
        out = np.nan
    return out


def retrieve_data_from_umiami_gmallmonths(model, exp, var):
    # global mean data for all available months
    print('retrieve_data_from_umiami_gm_allmonths, get data for:', model, exp, var)
    out = np.zeros(50*12) + np.nan# we are assuming that there are at most 50 years per simulation    
    # model specific variable name
    varname = '???'
    varname = cmip5var_to_modelvar(var, model)
    # file from which variable shall be read 
    fname = '???'
    fname = get_url_string(model, exp, varname) 
    print(varname, fname)
    # check if variable and file actually exists
    if (varname != '???') and (fname != '???'): 
        # file from which variable shall be read 
        file  = nc.Dataset(fname, 'r')
        dat     = np.squeeze(np.array(file.variables[varname]))
        datlat  = np.squeeze(np.array(file.variables['lat'  ]))  # latitude of data set
        if model == 'AM2': datlat = latAM2
        # average over longitude 
        dat_zm = np.nanmean(dat, 2) 
        # now also average over latitude --> output out
        area = np.cos(datlat*np.pi/180.0)
        for i in range(0, dat_zm[:, 0].size):  # loop over monthly timesteps
            out[i] = np.nansum(dat_zm[i,:]*area)/np.nansum(area)
        print('retrieve_data_from_umiami_gmallmonths, get data for', model, exp, var, 'successful')
    else: # either file or variable are missing
        out = np.nan
    return out





def retrieve_data_from_umiami_zmtm(model, exp, var):
    # zonal time mean data
    print('retrieve_data_from_umiami_zmtm, get data for:', model, exp, var)
    # model specific variable name
    varname = '???'
    varname = cmip5var_to_modelvar(var, model)
    # file from which variable shall be read 
    fname = '???'
    fname = get_url_string(model, exp, varname) 
    print(varname, fname)
    # check if variable and file actually exists
    if (varname != '???') and (fname != '???'): 
        # file from which variable shall be read 
        file  = nc.Dataset(fname, 'r')
        dat     = np.squeeze(np.array(file.variables[varname]))
        datlat  = np.squeeze(np.array(file.variables['lat'  ]))  # latitude of data set
        if model == 'AM2': datlat = latAM2        
        # average over longitude and last nmth months
        dat_zmtm = np.nanmean(np.nanmean(dat[-nmth:, :, :], 2), 0) 
        # interpolate to lat defined at top of file
        out = np.zeros(nlat) + np.nan
        # if lat is from NP to SP we need to flip lat, otherwise np.interp gives meaningless result
        if datlat[0] > datlat[1]: 
            dat_zmtm = dat_zmtm[::-1]
            datlat   = datlat[::-1]
        print('retrieve_data_from_umiami_zmtm, get data for', model, exp, var, 'successful')
        out = np.interp(lat, datlat, dat_zmtm) 
    else:    # file does not exist
        print('retrieve_data_from_umiami_zmtm, get data for', model, exp, var, 'failed and data set to nan')
        out = np.zeros(nlat) + np.nan
    return out
    
    
def retrieve_data_from_umiami_zmmm(model, exp, var):
    # zonal monthly mean data
    print('retrieve_data_from_umiami_zmmm, get data for:', model, exp, var)
    # model specific variable name
    varname = '???'
    varname = cmip5var_to_modelvar(var, model)
    # file from which variable shall be read 
    fname = '???'
    fname = get_url_string(model, exp, varname) 
    print(varname, fname)
    # check if variable and file actually exists
    if (varname != '???') and (fname != '???'): 
        # file from which variable shall be read 
        file  = nc.Dataset(fname, 'r')
        dat     = np.squeeze(np.array(file.variables[varname]))
        datlat  = np.squeeze(np.array(file.variables['lat']))  # latitude of data set
        if model == 'AM2': datlat = latAM2        
        # average over longitude and only take last nmth months
        dat_zm = np.nanmean(dat[-nmth:, :, :], 2)
        # average over all Januaries, Februaries, Marchs and so on
        dat_zmmm = np.zeros((12, datlat.size)) + np.nan
        for imonth in range(0, 12):
            dat_zmmm[imonth, :] = np.nanmean(dat_zm[imonth:nmth:12, :], axis=0)
        # interpolate to lat defined at top of file
        out = np.zeros((12, nlat)) + np.nan
        # if lat is from NP to SP we need to flip lat, otherwise np.interp gives meaningless result
        if datlat[0] > datlat[1]: 
            dat_zmmm = dat_zmmm[:, ::-1]
            datlat   = datlat[::-1]
        for imonth in range(0, 12):
            out[imonth, :] = np.interp(lat, datlat, dat_zmmm[imonth, :]) 
        print('retrieve_data_from_umiami_zmmm, get data for', model, exp, var, 'successful')    
    else:    # file does not exist
        out = np.zeros((12, nlat)) + np.nan
        print('retrieve_data_from_umiami_zmmm, get data for', model, exp, var, 'failed and data set to nan')
    return out    


def retrieve_data_from_umiami_zmtm_plevels(model, exp, var):
    # zonal time mean data on 17 CMIP5 pressure levels
    print('retrieve_data_from_umiami_zmtm_plevels, get data for:', model, exp, var)
    # model specific variable name
    varname = '???'
    varname = cmip5var_to_modelvar(var, model)
    # file from which variable shall be read 
    fname = '???'
    fname = get_url_string(model, exp, varname) 
    print(varname, fname)
    # check if variable and file actually exists
    if (varname != '???') and (fname != '???'): 
        # file from which variable shall be read 
        file  = nc.Dataset(fname, 'r')
        dat     = np.squeeze(np.array(file.variables[varname]))
        datlat  = np.squeeze(np.array(file.variables['lat'  ]))  # latitude of data set
        if model == 'AM2': datlat = latAM2
        # need to catch models for which levels are not nomed plev     
        if model == 'AM2':
            datlev = 100*np.squeeze(np.array(file.variables['lev']))
        elif model == 'CAM3':
            datlev = 100*np.squeeze(np.array(file.variables['lev']))
        elif model == 'CAM4': 
            datlev = 100*np.squeeze(np.array(file.variables['lev']))
        elif model == 'CALTECH':
            datlev = 100*np.squeeze(np.array(file.variables['lev_p']))
        else:
            datlev  = np.squeeze(np.array(file.variables['plev' ]))  # levels of data set 
        # Met and MIROC5 models pressure levels in on hPa and so need to be multiplied by 100
        if (model == 'MetCTL') or (model == 'MetENT') or (model == 'MIROC5'): 
            datlev = 100*datlev    
        # average over longitude and last nmth months
        dat_zmtm_plevels = np.nanmean(np.nanmean(dat[-nmth:, :, :, :], 3), 0) 
        print(dat_zmtm_plevels.shape)
        # interpolate to lat defined at top of file
        out = np.zeros((nlev, nlat)) + np.nan
        # if lev is from high to low values we need to flip lev for interp
        if datlev[0] > datlev[1]: 
            dat_zmtm_plevels = dat_zmtm_plevels[::-1, :]
            datlev           = datlev[::-1]
        # if lat is from NP to SP we need to flip lat for interp
        if datlat[0] > datlat[1]: 
            dat_zmtm_plevels = dat_zmtm_plevels[:, ::-1]
            datlat           = datlat[::-1]    
        # interpolate to pressure levels lev  
        tmp = np.zeros((nlev, datlat.size))
        for i in range(0, datlat.size):
           tmp[:, i] = np.interp(lev, datlev, dat_zmtm_plevels[:, i])
        # interpolate to latitudes lat
        for i in range(0, nlev):
           out[i, :] = np.interp(lat, datlat, tmp[i, :])
        print('retrieve_data_from_umiami_zmtm_plevels, get data for', model, exp, var, 'successful')
    else:    # file does not exist
        out = np.zeros(nlat) + np.nan
        print('retrieve_data_from_umiami_zmtm_plevels, get data for', model, exp, var, 'failed and data set to nan')
    return out
    
    
def retrieve_data_from_umiami_latlontm(model, exp, var):
    # lat-lon time mean data
    print('retrieve_data_from_umiami_latlontm, get data for:', model, exp, var)
    # model specific variable name
    varname = '???'
    varname = cmip5var_to_modelvar(var, model)
    # file from which variable shall be read 
    fname = '???'
    fname = get_url_string(model, exp, varname) 
    # check if variable and file actually exists
    if (varname != '???') and (fname != '???'): 
        # file from which variable shall be read 
        file  = nc.Dataset(fname, 'r')
        dat     = np.squeeze(np.array(file.variables[varname]))
        datlat  = np.squeeze(np.array(file.variables['lat'  ]))  # latitude of data set
        datlon  = np.squeeze(np.array(file.variables['lon'  ]))  # longitude of data set
        if model == 'AM2': 
            datlat = latAM2
            datlon = lonAM2
        # for all models except IPSL lon is from 0-360 deg, but we want to data on -180-180
        # so that continent is in the middle of lat-lon plots    
        if model != 'IPSL': 
            auxdat = np.zeros(dat.shape)   + np.nan
            auxlon = np.zeros(datlon.size) + np.nan
            auxdat[:, :, datlon.size/2:datlon.size] = dat[:, :,  0:datlon.size/2]
            auxdat[:, :,  0:datlon.size/2]          = dat[:, :, datlon.size/2:datlon.size]
            auxlon[datlon.size/2:datlon.size] = datlon[ 0:datlon.size/2]
            auxlon[0:datlon.size/2]           = datlon[datlon.size/2:datlon.size] - 360.0
            dat    = auxdat
            datlon = auxlon
        # average over last nmth months
        dat_tm = np.nanmean(dat[-nmth:, :, :], 0) 
        # interpolate to lat-lon grid defined at top of file
        out = np.zeros((nlat, nlon)) + np.nan
        # grid of model data      
        datx, daty   = np.meshgrid(datlon, datlat)
        # grid on which we interpolate
        x, y = np.meshgrid(lon, lat)
        # interpolated data
        out = griddata((datx.ravel(), daty.ravel()), dat_tm.ravel(), (x, y))
        print('retrieve_data_from_umiami_latlontm, get data for', model, exp, var, 'successful')
    else:    # file does not exist
        print('retrieve_data_from_umiami_latlontm, get data for', model, exp, var, 'failed and data set to nan')
        out = np.zeros((nlat, nlon)) + np.nan
    return out    
    
    
def retrieve_data_from_umiami_latlonmm(model, exp, var):
    # lat-lon monthly mean data
    print('retrieve_data_from_umiami_latlonmm, get data for:', model, exp, var)
    # model specific variable name
    varname = '???'
    varname = cmip5var_to_modelvar(var, model)
    # file from which variable shall be read 
    fname = '???'
    fname = get_url_string(model, exp, varname) 
    # check if variable and file actually exists
    if (varname != '???') and (fname != '???'): 
        # file from which variable shall be read 
        file  = nc.Dataset(fname, 'r')
        dat     = np.squeeze(np.array(file.variables[varname]))
        datlat  = np.squeeze(np.array(file.variables['lat'  ]))  # latitude of data set
        datlon  = np.squeeze(np.array(file.variables['lon'  ]))  # longitude of data set
        if model == 'AM2': 
            datlat = latAM2
            datlon = lonAM2
        # for all models except IPSL lon is from 0-360 deg, but we want to data on -180-180
        # so that continent is in the middle of lat-lon plots    
        if model != 'IPSL': 
            auxdat = np.zeros(dat.shape)   + np.nan
            auxlon = np.zeros(datlon.size) + np.nan
            auxdat[:, :, datlon.size/2:datlon.size] = dat[:, :,  0:datlon.size/2]
            auxdat[:, :,  0:datlon.size/2]          = dat[:, :, datlon.size/2:datlon.size]
            auxlon[datlon.size/2:datlon.size] = datlon[ 0:datlon.size/2]
            auxlon[0:datlon.size/2]           = datlon[datlon.size/2:datlon.size] - 360.0
            dat    = auxdat
            datlon = auxlon
        # average over all Januaries, Februaries, Marchs and so on
        dat_mm = np.zeros((12, datlat.size, datlon.size)) + np.nan
        for imonth in range(0, 12):
            dat_mm[imonth, :, :] = np.nanmean(dat[imonth:nmth:12, :, :], axis=0)    
        # interpolate to lat-lon grid defined at top of file
        out = np.zeros((12, nlat, nlon)) + np.nan
        # grid of model data      
        datx, daty   = np.meshgrid(datlon, datlat)
        # grid on which we interpolate
        x, y = np.meshgrid(lon, lat)
        # interpolated data
        for imonth in range(0, 12):
            out[imonth, :, :] = griddata((datx.ravel(), daty.ravel()), dat_mm[imonth, :, :].ravel(), (x, y))
        print('retrieve_data_from_umiami_latlonmm, get data for', model, exp, var, 'successful')
    else:    # file does not exist
        print('retrieve_data_from_umiami_latlonmm, get data for', model, exp, var, 'failed and data set to nan')
        out = np.zeros((12, nlat, nlon)) + np.nan
    return out        
    
    
def get_url_string(model, exp, var):
    #########################################################################
    # I/O function to go into a csv file pulled from the TRACMIP website
    # that finds the URL needed to access a particular variable's file on the 
    # TRACMIP server.
    ##########################################################################
    import os.path
    csvfile = '/home/aiko/Dropbox/Projects/current/Tracmip/analysis/python3/ramadda/mymodules/csv/' + model +'_' + exp + '.csv'
    if os.path.isfile(csvfile):   # csv file exists 
        hcsvfile   = open(csvfile )
        url_string = 'https://weather.rsmas.miami.edu/repository/opendap/'
        # if we have cmorized data for which each field is in a single file, then
        # we need to find the line in the csv file for the variable we want to read
        if (model == 'CNRM') or (model == 'ECHAM61') or (model == 'ECHAM63') or \
        (model == 'IPSL') or (model == 'MetCTL' ) or (model == 'MetENT' ) or \
        (model == 'MIROC5') or (model == 'GISS') or (model == 'NORESM') or \
        (model == 'CNRMv2'):
            again = True
            cline = 0  # number of lines that have been read
            while again == True:
                line = hcsvfile.readline()
                cline = cline + 1
                #print(line)
                aux  = (line.split("_"))
                #print(aux)
                var_check = aux[0]
                if var_check == var:
                    again = False
                if cline > 1000:
                    again = False
                    print('get_url_string, unable to find line in csv files for model', model, ', exp', exp, ', var', var)
                    url_string = '???'         
        # for models with all data in one big file we just need to read the second
        # entry of the second line
        elif (model == 'AM2') or (model == 'CAM3') or (model == 'CAM4') or (model == 'MPAS') or (model == 'CALTECH'):    
            line = hcsvfile.readline()
            line = hcsvfile.readline()
        # in case requested model is not available    
        else:
           print('ERROR in get_url_string: model', model, 'unknown') 

        # once we have the line corresponding to var we need the second entry to
        # build the complete url_string
        if url_string != '???': # only if we found csv file and line in the csv file        
            aux = (line.split(","))
            url_string = url_string + aux[1] + '/entry.das'   
        #print('get_url_string', model, ':', url_string)
        # close csv file
        hcsvfile.close()
    else:
       print('ERROR in get_url_string: no csv file for model', model, ', exp', exp, 'and var', var) 
       url_string = '???'
    return(url_string)
