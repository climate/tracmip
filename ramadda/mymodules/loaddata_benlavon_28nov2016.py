import netCDF4 as nc
import numpy as np
from scipy.interpolate import griddata

###############################################################################
# Notes: 
#  - wheneven a string is missing it is set to '???' (e.g., because
#    a variable is not available for a given model or experiment, or an
#    experiment is not available for a given model)


###############################################################################

# general setup
# number of models
nmod = 15
# number of months to be read in and averaged over
nmth = 240
# lat is the lattiudes on which data will be interpolated 
lat  = np.linspace(-89.5, 89.5, 180)     
nlat = lat.size
lon  = np.linspace(-179.0, 179.0, 180)     
nlon = lon.size
# lev are the pressure levels we want to interpolate to; we use the 17 standard
# CMIP pressure levels in hPa
lev = 100*np.array([ 10,  20,  30,  50,  70,  100, 150, 200, 250, 300, 400,
                    500, 600, 700, 850, 925, 1000])
nlev = lev.size

# grid info for AM2 is missing in umiami files and thus is stored here
latAM2 = np.array([-89.49438, -87.97753, -85.95506, -83.93259, -81.91011, -79.88764, \
    -77.86517, -75.8427, -73.82022, -71.79775, -69.77528, -67.75281,                 \
    -65.73034, -63.70787, -61.68539, -59.66292, -57.64045, -55.61798,                \
    -53.5955, -51.57303, -49.55056, -47.52809, -45.50562, -43.48315,                 \
    -41.46067, -39.4382, -37.41573, -35.39326, -33.37078, -31.34831,                 \
    -29.32584, -27.30337, -25.2809, -23.25843, -21.23595, -19.21348,                 \
    -17.19101, -15.16854, -13.14607, -11.1236, -9.101124, -7.078652,                 \
    -5.05618, -3.033708, -1.011236, 1.011236, 3.033708, 5.05618, 7.078652,           \
    9.101124, 11.1236, 13.14607, 15.16854, 17.19101, 19.21348, 21.23595,             \
    23.25843, 25.2809, 27.30337, 29.32584, 31.34831, 33.37078, 35.39326,             \
    37.41573, 39.4382, 41.46067, 43.48315, 45.50562, 47.52809, 49.55056,             \
    51.57303, 53.5955, 55.61798, 57.64045, 59.66292, 61.68539, 63.70787,             \
    65.73034, 67.75281, 69.77528, 71.79775, 73.82022, 75.8427, 77.86517,             \
    79.88764, 81.91011, 83.93259, 85.95506, 87.97753, 89.49438])
lonAM2 = np.array([1.25, 3.75, 6.25, 8.75, 11.25, 13.75, 16.25, 18.75, 21.25, 23.75, \
    26.25, 28.75, 31.25, 33.75, 36.25, 38.75, 41.25, 43.75, 46.25, 48.75,            \
    51.25, 53.75, 56.25, 58.75, 61.25, 63.75, 66.25, 68.75, 71.25, 73.75,            \
    76.25, 78.75, 81.25, 83.75, 86.25, 88.75, 91.25, 93.75, 96.25, 98.75,            \
    101.25, 103.75, 106.25, 108.75, 111.25, 113.75, 116.25, 118.75, 121.25,          \
    123.75, 126.25, 128.75, 131.25, 133.75, 136.25, 138.75, 141.25, 143.75,          \
    146.25, 148.75, 151.25, 153.75, 156.25, 158.75, 161.25, 163.75, 166.25,          \
    168.75, 171.25, 173.75, 176.25, 178.75, 181.25, 183.75, 186.25, 188.75,          \
    191.25, 193.75, 196.25, 198.75, 201.25, 203.75, 206.25, 208.75, 211.25,          \
    213.75, 216.25, 218.75, 221.25, 223.75, 226.25, 228.75, 231.25, 233.75,          \
    236.25, 238.75, 241.25, 243.75, 246.25, 248.75, 251.25, 253.75, 256.25,          \
    258.75, 261.25, 263.75, 266.25, 268.75, 271.25, 273.75, 276.25, 278.75,          \
    281.25, 283.75, 286.25, 288.75, 291.25, 293.75, 296.25, 298.75, 301.25,          \
    303.75, 306.25, 308.75, 311.25, 313.75, 316.25, 318.75, 321.25, 323.75,          \
    326.25, 328.75, 331.25, 333.75, 336.25, 338.75, 341.25, 343.75, 346.25,          \
    348.75, 351.25, 353.75, 356.25, 358.75])    
    

def cmip5var_to_modelvar(cmip5var, model):
   modelvar_array = get_modelvar_array(cmip5var)
   if model   == 'CNRM'   :
      modelvar = modelvar_array['CNRM']
   elif model == 'ECHAM61':
      modelvar = modelvar_array['ECHAM61']
   elif model == 'ECHAM63':
      modelvar = modelvar_array['ECHAM61']
   elif model == 'IPSL'   :
      modelvar = modelvar_array['IPSL'   ]
   elif model == 'MetCTL' :
      modelvar = modelvar_array['MetCTL' ]
   elif model == 'MetENT' :
      modelvar = modelvar_array['MetENT' ]   
   elif model == 'MIROC5' :
      modelvar = modelvar_array['MIROC5' ]     
   elif model == 'AM2'    :
      modelvar = modelvar_array['AM2'    ]  
   elif model == 'CAM3'   :
      modelvar = modelvar_array['CAM3'   ]   
   elif model == 'CAM4'   :
      modelvar = modelvar_array['CAM4'   ]   
   elif model == 'MPAS'   :
      modelvar = modelvar_array['MPAS'   ]  
   elif model == 'GISS'   :
      modelvar = modelvar_array['GISS'   ]   
   elif model == 'NORESM2':
      modelvar = modelvar_array['NORESM2']
   elif model == 'CNRM-AM5':
      modelvar = modelvar_array['CNRM-AM5']
   elif model == 'CALTECH':
      modelvar = modelvar_array['CALTECH']   
   else:
      print('ERROR in cmip5var_to_modelvar: model not known!') 
   return modelvar



def get_modelvar_array(cmip5var):
#for CAM3, rsdt is set to net solar flux at top of model FSNTOA
#for calculating energy balance, it uses FSNTOA - FLUT
#to calculate rsdt for CAM3, use 'SOLIN'

#In order to calculate net surface flux, rlds and rsds are set to FLNS and FSNS, respectively
#for CAM3, CAM4, and MPAS. The upwelling values will be set to ??? so when calculating
#abs(rlds)-abs(rlus) it will already have the net value. To calculate rlds or rsds for these models
#change their entries in the appropriate directory below to FLDS and FSDS respectively.
    modelvar_array = {'CNRM'  :'???', 'ECHAM61':'???', 'ECHAM63':'???', 'IPSL':'???', \
                     'MetCTL':'???', 'MetENT' :'???', 'MIROC5' :'???', 'AM2' :'???', \
                     'CAM3'  :'???', 'CAM4'   :'???', 'MPAS'   :'???'}
   
    if cmip5var=='pr':
        modelvar_array = {'CNRM'  :'pr' , 'ECHAM61':'pr' , 'ECHAM63':'pr' , 'IPSL':'pr' , \
                        'MetCTL':'pr' , 'MetENT' :'pr' , 'MIROC5' :'pr' , 'AM2' :'pr' , \
                        'CAM3'  :'???', 'CAM4'   :'???', 'MPAS'   :'???', 'GISS':'pr'} 
    elif cmip5var=='prc':
        modelvar_array = {'CNRM'  :'???', 'ECHAM61':'???', 'ECHAM63':'???', 'IPSL':'???', \
                        'MetCTL':'???', 'MetENT' :'???', 'MIROC5' :'???', 'AM2' :'???', \
                        'CAM3'  :'PRECC', 'CAM4' :'PRECC', 'MPAS' :'PRECC', 'GISS':'???'}   
    elif cmip5var=='prl':
        modelvar_array = {'CNRM'  :'???', 'ECHAM61':'???', 'ECHAM63':'???', 'IPSL':'???', \
                        'MetCTL':'???', 'MetENT' :'???', 'MIROC5' :'???', 'AM2' :'???', \
                        'CAM3'  :'PRECL', 'CAM4' :'PRECL', 'MPAS' :'PRECL', 'GISS':'???'}  
    elif cmip5var=='rsdscs':
        modelvar_array = {'CNRM'  :'rsdscs', 'ECHAM61':'rsdscs', 'ECHAM63':'rsdscs', 'IPSL':'rsdscs', \
                        'MetCTL':'rsdscs', 'MetENT' :'rsdscs', 'MIROC5' :'rsdscs', 'AM2' :'rsdscs',
                        'CAM3'  :'FSDSC' , 'CAM4'   :'FSDSC' , 'MPAS'   :'FSDSC' , 'GISS':'rsdscs'}                      
    elif cmip5var=='rsdt':
        modelvar_array = {'CNRM'  :'rsdt'  , 'ECHAM61':'rsdt'  , 'ECHAM63':'rsdt', 'IPSL':'rsdt', \
                        'MetCTL':'rsdt'  , 'MetENT' :'rsdt'  , 'MIROC5' :'rsdt', 'AM2' :'rsdt',
                        'CAM3'  :'SOLIN'   , 'CAM4'   :'SOLIN', 'MPAS'   :'SOLIN', 'GISS':'rsdt', 
						'NORESM2': 'rsdt' , 'CNRM-AM5': 'rsdt', 'CALTECH': 'rsdt'}  
    elif cmip5var=='rsutcs':
        modelvar_array = {'CNRM'  :'rsutcs'  , 'ECHAM61':'rsutcs'  , 'ECHAM63':'rsutcs', 'IPSL':'rsutcs', \
                        'MetCTL':'rsutcs'  , 'MetENT' :'rsutcs'  , 'MIROC5' :'rsutcs', 'AM2' :'rsutcs',
                        'CAM3'  :'FSNTC'   , 'CAM4'   :'FSNTC', 'MPAS'   :'FSNTC', 'GISS':'rsutcs', 
						'NORESM2': '???' , 'CNRM-AM5': 'rsutcs', 'CALTECH': '???'} 
	
    elif cmip5var=='rlutcs':
        modelvar_array = {'CNRM'  :'rlutcs'  , 'ECHAM61':'rlutcs'  , 'ECHAM63':'rlutcs', 'IPSL':'rlutcs', \
                        'MetCTL':'rlutcs'  , 'MetENT' :'rlutcs'  , 'MIROC5' :'rlutcs', 'AM2' :'rlutcs',
                        'CAM3'  :'FLNTC'   , 'CAM4'   :'FLNTC', 'MPAS'   :'FLNTC', 'GISS':'rlutcs', 
						'NORESM2': '???' , 'CNRM-AM5': 'rlutcs', 'CALTECH': '???'} 
	
    elif cmip5var=='rsnscs':
        modelvar_array = {'CNRM'  :'rsnscs', 'ECHAM61':'rsnscs', 'ECHAM63':'rsnscs', 'IPSL':'rsnscs', \
                        'MetCTL':'rsnscs', 'MetENT' :'rsnscs', 'MIROC5' :'rsnscs', 'AM2' :'???'   ,
                        'CAM3'  :'FSNSC' , 'CAM4'   :'FSNSC' , 'MPAS'   :'FSNSC' , 'GISS':'rsnscs'}                      
    elif cmip5var=='rsuscs':
        modelvar_array = {'CNRM'  :'rsuscs', 'ECHAM61':'rsuscs', 'ECHAM63':'rsuscs', 'IPSL':'rsuscs', \
                        'MetCTL':'rsuscs', 'MetENT' :'rsuscs', 'MIROC5' :'rsuscs', 'AM2' :'rsuscs'   ,
                        'CAM3'  :'???'   , 'CAM4'   :'???'   , 'MPAS'   :'???'   , 'GISS':'rsuscs'}  
    elif cmip5var=='ts':
        modelvar_array = {'CNRM'  :'ts', 'ECHAM61':'ts', 'ECHAM63':'ts', 'IPSL':'ts', \
                        'MetCTL':'ts', 'MetENT' :'ts', 'MIROC5' :'ts', 'AM2' :'ts',
                        'CAM3'  :'TS', 'CAM4'   :'TS', 'MPAS'   :'TS', 'GISS':'ts',
						'NORESM2': 'ts' , 'CNRM-AM5': 'ts', 'CALTECH': 'ts'} 
    elif cmip5var=='ua':
        modelvar_array = {'CNRM'  :'ua', 'ECHAM61':'ua', 'ECHAM63':'ua', 'IPSL':'ua', \
                        'MetCTL':'ua', 'MetENT' :'ua', 'MIROC5' :'ua', 'AM2' :'ua',
                        'CAM3'  :'U' , 'CAM4'   :'U' , 'MPAS'   :'U' , 'GISS':'ua'}                     
    elif cmip5var=='va':
        modelvar_array = {'CNRM'  :'va', 'ECHAM61':'va', 'ECHAM63':'va', 'IPSL':'va', \
                        'MetCTL':'va', 'MetENT' :'va', 'MIROC5' :'va', 'AM2' :'va',
                        'CAM3'  :'V' , 'CAM4'   :'V' , 'MPAS'   :'V' , 'GISS':'va'}  
						
    elif cmip5var=='rsut':
	    modelvar_array = {'CNRM'  :'rsut'  , 'ECHAM61':'rsut'  , 'ECHAM63':'rsut', 'IPSL':'rsut', \
                        'MetCTL':'rsut'  , 'MetENT' :'rsut'  , 'MIROC5' :'rsut', 'AM2' :'rsut',
                        'CAM3'  :'FSNT'   , 'CAM4'   :'FSNT', 'MPAS'   :'FSNT', 'GISS':'rsut', 
						'NORESM2': 'rsut' , 'CNRM-AM5': 'rsut', 'CALTECH': 'rsut'}	
    elif cmip5var=='rlut':
	    modelvar_array = {'CNRM'  :'rlut'  , 'ECHAM61':'rlut'  , 'ECHAM63':'rlut', 'IPSL':'rlut', \
                        'MetCTL':'rlut'  , 'MetENT' :'rlut'  , 'MIROC5' :'rlut', 'AM2' :'rlut',
                        'CAM3'  :'FLNT'   , 'CAM4'   :'FLNT'  , 'MPAS'   :'FLNT', 'GISS':'rlut',
						'NORESM2': 'rlut' , 'CNRM-AM5': 'rlut', 'CALTECH': 'rlut'}
    elif cmip5var=='rlds':
	    modelvar_array = {'CNRM'  :'rlds'  , 'ECHAM61':'rlds'  , 'ECHAM63':'rlds', 'IPSL':'rlds', \
                        'MetCTL':'rlds'  , 'MetENT' :'rlds'  , 'MIROC5' :'rlds', 'AM2' :'rlds',
                        'CAM3'  :'FLNS'   , 'CAM4'   :'FLNS'  , 'MPAS'   :'FLNS', 'GISS':'rlds'}
    elif cmip5var=='rlus':
	    modelvar_array = {'CNRM'  :'rlus'  , 'ECHAM61':'rlus'  , 'ECHAM63':'rlus', 'IPSL':'rlus', \
                        'MetCTL':'rlus'  , 'MetENT' :'rlus'  , 'MIROC5' :'rlus', 'AM2' :'rlus',
                        'CAM3'  :'???'   , 'CAM4'   :'???'  , 'MPAS'   :'???', 'GISS':'rlus'}
    elif cmip5var=='rsds':
	    modelvar_array = {'CNRM'  :'rsds'  , 'ECHAM61':'rsds'  , 'ECHAM63':'rsds', 'IPSL':'rsds', \
                        'MetCTL':'rsds'  , 'MetENT' :'rsds'  , 'MIROC5' :'rsds', 'AM2' :'rsds',
                        'CAM3'  :'FSNS'   , 'CAM4'   :'FSNS'  , 'MPAS'   :'FSNS', 'GISS':'rsds'}
    elif cmip5var=='rsus':
	    modelvar_array = {'CNRM'  :'rsus'  , 'ECHAM61':'rsus'  , 'ECHAM63':'rsus', 'IPSL':'rsus', \
                        'MetCTL':'rsus'  , 'MetENT' :'rsus'  , 'MIROC5' :'rsus', 'AM2' :'rsus',
                        'CAM3'  :'???'   , 'CAM4'   :'???'  , 'MPAS'   :'???', 'GISS':'rsus'}
    elif cmip5var=='hfls':
	    modelvar_array = {'CNRM'  :'hfls'  , 'ECHAM61':'hfls'  , 'ECHAM63':'hfls', 'IPSL':'hfls', \
                        'MetCTL':'hfls'  , 'MetENT' :'hfls'  , 'MIROC5' :'hfls', 'AM2' :'???',
                        'CAM3'  :'LHFLX'   , 'CAM4'   :'LHFLX'  , 'MPAS'   :'LHFLX', 'GISS':'hfls'}
    elif cmip5var=='hfss':
	    modelvar_array = {'CNRM'  :'hfss'  , 'ECHAM61':'hfss'  , 'ECHAM63':'hfss', 'IPSL':'hfss', \
                        'MetCTL':'???'  , 'MetENT' :'???'  , 'MIROC5' :'hfss', 'AM2' :'hfss',
                        'CAM3'  :'SHFLX'   , 'CAM4'   :'SHFLX'  , 'MPAS'   :'SHFLX', 'GISS':'hfss'}
    elif cmip5var=='FLNT': #these two arrays are for calculating EB for CAM models alone
        modelvar_array = {#'CNRM'  :'rsdt'  , 'ECHAM61':'rsdt'  , 'ECHAM63':'rsdt', 'IPSL':'rsdt', \
                        #'MetCTL':'rsdt'  , 'MetENT' :'rsdt'  , 'MIROC5' :'rsdt', 'AM2' :'rsdt',
                        'CAM3'  :'FLNT'   , 'CAM4'   :'FLNT', 'MPAS'   :'FLNT'}#, 'GISS':'rsdt'}  
    elif cmip5var=='FSNT': #these two arrays are for calculating EB for CAM models alone
        modelvar_array = {#'CNRM'  :'rsdt'  , 'ECHAM61':'rsdt'  , 'ECHAM63':'rsdt', 'IPSL':'rsdt', \
                        #'MetCTL':'rsdt'  , 'MetENT' :'rsdt'  , 'MIROC5' :'rsdt', 'AM2' :'rsdt',
                        'CAM3'  :'FSNT'   , 'CAM4'   :'FSNT', 'MPAS'   :'FSNT'}#, 'GISS':'rsdt'}  
	
    else:
        print('ERROR in get_modelvar_array: cmip5var ' + cmip5var + ' unknown')
      
    return modelvar_array



def get_data_gmtm(exp, var):
    
    data = np.zeros(nmod) + np.nan
    
    # cnrm    
    data[0] = retrieve_data_from_umiami_gmtm('CNRM'   , exp, var)
    # echam61
    data[1 ] = retrieve_data_from_umiami_gmtm('ECHAM61', exp, var)
    # echam63
    data[2 ] = retrieve_data_from_umiami_gmtm('ECHAM63', exp, var)
    # ipsl
    data[3 ] = retrieve_data_from_umiami_gmtm('IPSL'   , exp, var)
    # metctl
    data[4 ] = retrieve_data_from_umiami_gmtm('MetCTL' , exp, var)
    # metent
    data[5 ] = retrieve_data_from_umiami_gmtm('MetENT' , exp, var)
    # miroc5
    data[6 ] = retrieve_data_from_umiami_gmtm('MIROC5' , exp, var) 
    # am2
    data[7 ] = retrieve_data_from_umiami_gmtm('AM2'    , exp, var)  
    # cam3
    data[8 ] = retrieve_data_from_umiami_gmtm('CAM3'   , exp, var)  
    # cam4
    data[9 ] = retrieve_data_from_umiami_gmtm('CAM4'   , exp, var)      
    # mpas
    data[10] = retrieve_data_from_umiami_gmtm('MPAS'   , exp, var) 
    # mpas
    data[11] = retrieve_data_from_umiami_gmtm('GISS'   , exp, var)    
    
    return data
	
	
def get_data_latlontm(exp, var):
    # lat-lon time mean data
    data = np.zeros((nmod, nlat, nlon)) + np.nan
    # cnrm    
    data[0 , :, :] = retrieve_data_from_umiami_latlontm('CNRM'   , exp, var)
    # echam61
    data[1 , :, :] = retrieve_data_from_umiami_latlontm('ECHAM61', exp, var)
    # echam63
    data[2 , :, :] = retrieve_data_from_umiami_latlontm('ECHAM63', exp, var)
    # ipsl
    data[3 , :, :] = retrieve_data_from_umiami_latlontm('IPSL'   , exp, var)
    # metctl
    data[4 , :, :] = retrieve_data_from_umiami_latlontm('MetCTL' , exp, var)
    # metent
    data[5 , :, :] = retrieve_data_from_umiami_latlontm('MetENT' , exp, var)
    # miroc5
    data[6 , :, :] = retrieve_data_from_umiami_latlontm('MIROC5' , exp, var) 
    # am2
    data[7 , :, :] = retrieve_data_from_umiami_latlontm('AM2'    , exp, var)    
    # cam3
    data[8 , :, :] = retrieve_data_from_umiami_latlontm('CAM3'   , exp, var)  
    # cam4
    data[9 , :, :] = retrieve_data_from_umiami_latlontm('CAM4'   , exp, var)      
    # mpas
    data[10, :, :] = retrieve_data_from_umiami_latlontm('MPAS'   , exp, var)  
    # giss
    #data[11, :, :] = retrieve_data_from_umiami_latlontm('GISS'   , exp, var) 
    return data, lat, lon
	
	
	
	
	
def get_data_zmmm(exp, var):
    # zonal multi-month mean
    data = np.zeros((nmod, 12, nlat)) + np.nan
    # cnrm    
    data[0 , :, :] = retrieve_data_from_umiami_zmmm('CNRM'   , exp, var)
    # echam61
    data[1 , :, :] = retrieve_data_from_umiami_zmmm('ECHAM61', exp, var)
    # echam63
    data[2 , :, :] = retrieve_data_from_umiami_zmmm('ECHAM63', exp, var)
    # ipsl
    data[3 , :, :] = retrieve_data_from_umiami_zmmm('IPSL'   , exp, var)
    # metctl
    data[4 , :, :] = retrieve_data_from_umiami_zmmm('MetCTL' , exp, var)
    # metent
    data[5 , :, :] = retrieve_data_from_umiami_zmmm('MetENT' , exp, var)
    # miroc5
    data[6 , :, :] = retrieve_data_from_umiami_zmmm('MIROC5' , exp, var) 
    # am2
    data[7 , :, :] = retrieve_data_from_umiami_zmmm('AM2'    , exp, var)    
    # cam3
    data[8 , :, :] = retrieve_data_from_umiami_zmmm('CAM3'   , exp, var)  
    # cam4
    data[9 , :, :] = retrieve_data_from_umiami_zmmm('CAM4'   , exp, var)      
    # mpas
    data[10, :, :] = retrieve_data_from_umiami_zmmm('MPAS'   , exp, var)  
    # giss
    data[11, :, :] = retrieve_data_from_umiami_zmmm('GISS'   , exp, var) 
	#cam5NOR
    data[12, :, :] = retrieve_data_from_umiami_zmmm('NORESM2'   , exp, var) 
	#CRNM-AM5
    data[13, :, :] = retrieve_data_from_umiami_zmmm('CNRM-AM5'   , exp, var) 
	#CALTECH
    data[14, :, :] = retrieve_data_from_umiami_zmmm('CALTECH'   , exp, var) 
    return data, lat
	
	
def get_data_gmam(exp, var):
    
    data = np.zeros((nmod, 45)) + np.nan
    
    #cnrm
    cnrm = retrieve_data_from_umiami_gm480('CNRM'   , exp, var)
    average = yearlymean_gm(cnrm)
    data[0, 0:average.size] =average
    # echam61
    echam61 = retrieve_data_from_umiami_gm480('ECHAM61', exp, var)
    average = yearlymean_gm(echam61)
    data[1, 0:average.size] = average
    # echam63
    echam63	=  retrieve_data_from_umiami_gm480('ECHAM63', exp, var)
    average = yearlymean_gm(echam63)
    data[2, 0:average.size ] =average   
    # ipsl
    ipsl = retrieve_data_from_umiami_gm480('IPSL'   , exp, var)
    average  = yearlymean_gm(ipsl)
    data[3, 0:average.size ] = average   
    # metctl
    metctl = retrieve_data_from_umiami_gm480('MetCTL' , exp, var)
    average  = yearlymean_gm(metctl)
    data[4, 0:average.size ] = average   
    # metent
    metent = retrieve_data_from_umiami_gm480('MetENT' , exp, var)
    average  = yearlymean_gm(metent)
    data[5, 0:average.size ] = average   
    # miroc5
    miroc5 = retrieve_data_from_umiami_gm480('MIROC5' , exp, var) 
    average  = yearlymean_gm(miroc5)
    data[6, 0:average.size ] = average   
    # am2
    am2 = retrieve_data_from_umiami_gm480('AM2'    , exp, var) 
    average = yearlymean_gm(am2)
    data[7, 0:average.size ] =  average   
    # cam3
    cam3 = retrieve_data_from_umiami_gm480('CAM3'   , exp, var)  
    average = yearlymean_gm(cam3)
    data[8, 0:average.size ] =average    
    # cam4
    cam4 = retrieve_data_from_umiami_gm480('CAM4'   , exp, var)
    average  = yearlymean_gm(cam4)
    data[9, 0:average.size ] = average      
    # mpas
    mpas = retrieve_data_from_umiami_gm480('MPAS'   , exp, var) 
    average  = yearlymean_gm(mpas)
    data[10, 0:average.size] = average    
    # giss
    #giss = retrieve_data_from_umiami_gm480('GISS'   , exp, var)
    #average = yearlymean_gm(giss)
    #data[11, 0:average.size] = average   
    
    return data
    


	
#old retrieve data inserted nan if the model didn't exist
#this version inserts zero so that matrix operations may be performed on 
#the output (as in energy_balance)
def retrieve_data_from_umiami_gmtm(model, exp, var):
    # global time mean data
    
    print 'retrieve_data_from_umiami_gmtm, get data for:', model, exp, var
    # model specific variable name
    varname = '???'    
    varname = cmip5var_to_modelvar(var, model)
    # file from which variable shall be read 
    fname = '???'
    fname = get_url_string(model, exp, varname) 
    print varname, fname
    # check if variable and file actually exists
    if varname != '???' and fname != '???': 
        # file from which variable shall be read 
        file  = nc.Dataset(fname, 'r')
        dat     = np.squeeze(np.array(file.variables[varname]))
        datlat  = np.squeeze(np.array(file.variables['lat'  ]))  # latitude of data set
        if model == 'AM2': datlat = latAM2
        # average over longitude and last nmth months
        dat_zmtm = np.nanmean(np.nanmean(dat[-nmth:, :, :], 2), 0) 
        # now also average over latitude --> output out
        area = np.cos(datlat*np.pi/180.0)
        print 'retrieve_data_from_umiami_gmtm, get data for', model, exp, var, 'successful'
        out  = np.nansum(dat_zmtm*area)/np.nansum(area)
    else: # either file or variable are missing
        out = 0
    return out

	
#Experiment names are called using 'AQUA', 'AQUAO2', 'LAND', 'LANDCO2', and 'LORBIT'. Don't be fooled!

def retrieve_data_from_umiami_gm(model, exp, var):
    # global time mean data
    print 'retrieve_data_from_umiami_gmtm, get data for:', model, exp, var
    #model specific variable name
    varname = '???'
    varname = cmip5var_to_modelvar(var, model)
    # file from which variable shall be read 
    fname = '???'
    fname = get_url_string(model, exp, varname) 
    print "check?"
    print varname, fname
      
# check if variable and file actually exists
    if varname != '???' and fname != '???': 
        # file from which variable shall be read 
        fill  = nc.Dataset(fname, 'r')
        
        
        dat     = np.squeeze(np.array(fill.variables[varname]))
        #print 'DAT', dat       
        
        datlat  = np.squeeze(np.array(fill.variables['lat'  ]))  # latitude of data set
        
        # average over longitude, only last 240 months
        
        
        dat_zm = np.nanmean(dat[-nmth:, :, :], 2)
                
        # now also average over latitude --> output out
        #to do this, I create an array which consists of the altered latitude array repeated 240 times
        #so it hits each row (correpsonding to each of 240 monthly time averages). Then I divide by the total area.
        #the output ("out", below) is a 240 x 1 array, with each entry being the value for that month averaged over lat and lon. 
       
        area = np.cos(datlat*np.pi/180.0)
        area240 = np.tile(area, (240, 1))
		
        print 'retrieve_data_from_umiami_gmtm, get data for', model, exp, var, 'successful'
        out  = np.nansum(dat_zm*area240, axis=1)/np.nansum(area)
		
    else: # either file or variable are missing
        #we can't perform matrix operations on entries with nan... what if we
        #set them to zero so we can add/subtract?
        out= 0.0
        #out = np.nan
    return out
	
	
	
	



def oldretrieve_data_from_umiami_gmtm(model, exp, var):
    # global time mean data
    print 'retrieve_data_from_umiami_gmtm, get data for:', model, exp, var
     
    #model specific variable name
    print "Who knows whats wrong"
    varname = '???'
    varname = cmip5var_to_modelvar(var, model)
    # file from which variable shall be read 
    fname = '???'
    fname = get_url_string(model, exp, varname) 
    print "check?"
    print varname, fname
    # check if variable and file actually exists
    if varname != '???' and fname != '???': 
        # file from which variable shall be read 
        file  = nc.Dataset(fname, 'r')
        dat     = np.squeeze(np.array(file.variables[varname]))
        datlat  = np.squeeze(np.array(file.variables['lat'  ]))  # latitude of data set
        if model == 'AM2': datlat = latAM2
        # average over longitude and last nmth months
        dat_zmtm = np.nanmean(np.nanmean(dat[-nmth:, :, :], 2), 0) 
        # now also average over latitude --> output out
        area = np.cos(datlat*np.pi/180.0)
        print 'retrieve_data_from_umiami_gmtm, get data for', model, exp, var, 'successful'
        out  = np.nansum(dat_zmtm*area)/np.nansum(area)
    else: # either file or variable are missing
        #we can't perform matrix operations on entries with nan... what if we
        #set them to zero so we can add/subtract?
        out= 0.0
        #out = np.nan
	return out
	
	
	
def get_url_string(model, exp, var):
    #########################################################################
    # I/O function to go into a csv file pulled from the TRACMIP website
    # that finds the URL needed to access a particular variable's file on the 
    # TRACMIP server.
    ##########################################################################
    import os.path
    csvfile = 'csv/' + model +'_' + exp + '.csv'
    if (var == 'rlut') and (model == 'NORESM2'): # because NORESM2 saved their TOA rlut in a separate directory, we ignore the normal CSV for this variable
	    csvfile = 'csv/' + model +'_' + exp + '_' + var + '.csv'
    if os.path.isfile(csvfile):   # csv file exists 
        hcsvfile   = open(csvfile )
        url_string = 'https://weather.rsmas.miami.edu/repository/opendap/'
        # if we have cmorized data for which each field is in a single file, then
        # we need to find the line in the csv file for the variable we want to read
        if (model == 'CNRM') or (model == 'ECHAM61') or (model == 'ECHAM63') or\
        (model == 'IPSL') or (model == 'MetCTL' ) or (model == 'MetENT' ) or\
        (model == 'MIROC5') or (model == 'GISS') or (model =='NORESM2') or (model =='CNRM-AM5'):
            again = True
            cline = 0  # number of lines that have been read
            while again == True:
                line = hcsvfile.readline()
                cline = cline + 1
                #print(line)
                aux  = (line.split("_"))
                #print(aux)
                var_check = aux[0]
                if var_check == var:
                    again = False
                if cline > 1000:
                    again = False
                    print('get_url_string, unable to find line in csv files for model', model, ', exp', exp, ', var', var)
                    url_string = '???'         
        # for models with all data in one big file we just need to read the second
        # entry of the second line
        elif (model == 'AM2') or (model == 'CAM3') or (model == 'CAM4') or (model == 'MPAS') or (model == 'CALTECH'):    
            line = hcsvfile.readline()
            line = hcsvfile.readline()
        # in case requested model is not available    
        else:
           print('ERROR in get_url_string: model', model, 'unknown') 

        # once we have the line corresponding to var we need the second entry to
        # build the complete url_string
        if url_string != '???': # only if we found csv file and line in the csv file        
            aux = (line.split(","))
            url_string = url_string + aux[1] + '/entry.das'   
        #print('get_url_string', model, ':', url_string)
        # close csv file
        hcsvfile.close()
    else:
       print('ERROR in get_url_string: no csv file for model', model, ', exp', exp, 'and var', var) 
       url_string = '???'
    return(url_string)
	


	
	
	
def retrieve_data_from_umiami_latlontm(model, exp, var):
    # lat-lon time mean data
    print('retrieve_data_from_umiami_latlontm, get data for:', model, exp, var)
    # model specific variable name
    varname = '???'
    varname = cmip5var_to_modelvar(var, model)
    # file from which variable shall be read 
    fname = '???'
    fname = get_url_string(model, exp, varname) 
    # check if variable and file actually exists
    if (varname != '???') and (fname != '???'): 
        # file from which variable shall be read 
        file  = nc.Dataset(fname, 'r')
        dat     = np.squeeze(np.array(file.variables[varname]))
        datlat  = np.squeeze(np.array(file.variables['lat'  ]))  # latitude of data set
        datlon  = np.squeeze(np.array(file.variables['lon'  ]))  # longitude of data set
        if model == 'AM2': 
            datlat = latAM2
            datlon = lonAM2
        # for all models except IPSL lon is from 0-360 deg, but we want to data on -180-180
        # so that continent is in the middle of lat-lon plots    
        if model != 'IPSL': 
            auxdat = np.zeros(dat.shape)   + np.nan
            auxlon = np.zeros(datlon.size) + np.nan
            auxdat[:, :, datlon.size/2:datlon.size] = dat[:, :,  0:datlon.size/2]
            auxdat[:, :,  0:datlon.size/2]          = dat[:, :, datlon.size/2:datlon.size]
            auxlon[datlon.size/2:datlon.size] = datlon[ 0:datlon.size/2]
            auxlon[0:datlon.size/2]           = datlon[datlon.size/2:datlon.size] - 360.0
            dat    = auxdat
            datlon = auxlon
        # average over last nmth months
        dat_tm = np.nanmean(dat[-nmth:, :, :], 0) 
        # interpolate to lat-lon grid defined at top of file
        out = np.zeros((nlat, nlon)) + np.nan
        # grid of model data      
        datx, daty   = np.meshgrid(datlon, datlat)
        # grid on which we interpolate
        x, y = np.meshgrid(lon, lat)
        # interpolated data
        out = griddata((datx.ravel(), daty.ravel()), dat_tm.ravel(), (x, y))
        print('retrieve_data_from_umiami_latlontm, get data for', model, exp, var, 'successful')
    else:    # file does not exist
        print('retrieve_data_from_umiami_latlontm, get data for', model, exp, var, 'failed and data set to nan')
        out = np.zeros((nlat, nlon)) + np.nan
    return out    
    
    
def retrieve_data_from_umiami_latlonmm(model, exp, var):
    # lat-lon monthly mean data
    print('retrieve_data_from_umiami_latlonmm, get data for:', model, exp, var)
    # model specific variable name
    varname = '???'
    varname = cmip5var_to_modelvar(var, model)
    # file from which variable shall be read 
    fname = '???'
    fname = get_url_string(model, exp, varname) 
    # check if variable and file actually exists
    if (varname != '???') and (fname != '???'): 
        # file from which variable shall be read 
        file  = nc.Dataset(fname, 'r')
        dat     = np.squeeze(np.array(file.variables[varname]))
        datlat  = np.squeeze(np.array(file.variables['lat'  ]))  # latitude of data set
        datlon  = np.squeeze(np.array(file.variables['lon'  ]))  # longitude of data set
        if model == 'AM2': 
            datlat = latAM2
            datlon = lonAM2
        # for all models except IPSL lon is from 0-360 deg, but we want to data on -180-180
        # so that continent is in the middle of lat-lon plots    
        if model != 'IPSL': 
            auxdat = np.zeros(dat.shape)   + np.nan
            auxlon = np.zeros(datlon.size) + np.nan
            auxdat[:, :, datlon.size/2:datlon.size] = dat[:, :,  0:datlon.size/2]
            auxdat[:, :,  0:datlon.size/2]          = dat[:, :, datlon.size/2:datlon.size]
            auxlon[datlon.size/2:datlon.size] = datlon[ 0:datlon.size/2]
            auxlon[0:datlon.size/2]           = datlon[datlon.size/2:datlon.size] - 360.0
            dat    = auxdat
            datlon = auxlon
        # average over all Januaries, Februaries, Marchs and so on
        dat_mm = np.zeros((12, datlat.size, datlon.size)) + np.nan
        for imonth in range(0, 12):
            dat_mm[imonth, :, :] = np.nanmean(dat[imonth:nmth:12, :, :], axis=0)    
        # interpolate to lat-lon grid defined at top of file
        out = np.zeros((12, nlat, nlon)) + np.nan
        # grid of model data      
        datx, daty   = np.meshgrid(datlon, datlat)
        # grid on which we interpolate
        x, y = np.meshgrid(lon, lat)
        # interpolated data
        for imonth in range(0, 12):
            out[imonth, :, :] = griddata((datx.ravel(), daty.ravel()), dat_mm[imonth, :, :].ravel(), (x, y))
        print('retrieve_data_from_umiami_latlonmm, get data for', model, exp, var, 'successful')
    else:    # file does not exist
        print('retrieve_data_from_umiami_latlonmm, get data for', model, exp, var, 'failed and data set to nan')
        out = np.zeros((12, nlat, nlon)) + np.nan
    return out        
    
	
def retrieve_data_from_umiami_zmtm(model, exp, var):
    # zonal time mean data
    print('retrieve_data_from_umiami_zmtm, get data for:', model, exp, var)
    # model specific variable name
    varname = '???'
    varname = cmip5var_to_modelvar(var, model)
    # file from which variable shall be read 
    fname = '???'
    fname = get_url_string(model, exp, varname) 
    print(varname, fname)
    # check if variable and file actually exists
    if (varname != '???') and (fname != '???'): 
        # file from which variable shall be read 
        file  = nc.Dataset(fname, 'r')
        dat     = np.squeeze(np.array(file.variables[varname]))
        datlat  = np.squeeze(np.array(file.variables['lat'  ]))  # latitude of data set
        if model == 'AM2': datlat = latAM2        
        # average over longitude and last nmth months
        dat_zmtm = np.nanmean(np.nanmean(dat[-nmth:, :, :], 2), 0) 
        # interpolate to lat defined at top of file
        out = np.zeros(nlat) + np.nan
        # if lat is from NP to SP we need to flip lat, otherwise np.interp gives meaningless result
        if datlat[0] > datlat[1]: 
            dat_zmtm = dat_zmtm[::-1]
            datlat   = datlat[::-1]
        print('retrieve_data_from_umiami_zmtm, get data for', model, exp, var, 'successful')
        out = np.interp(lat, datlat, dat_zmtm) 
    else:    # file does not exist
        print('retrieve_data_from_umiami_zmtm, get data for', model, exp, var, 'failed and data set to nan')
        out = np.zeros(nlat) + np.nan
    return out
	
	
def retrieve_data_from_umiami_zmmm(model, exp, var):
    # zonal monthly mean data
    print('retrieve_data_from_umiami_zmmm, get data for:', model, exp, var)
    # model specific variable name
    varname = '???'
    varname = cmip5var_to_modelvar(var, model)
    # file from which variable shall be read 
    fname = '???'
    fname = get_url_string(model, exp, varname) 
    print(varname, fname)
    # check if variable and file actually exists
    if (varname != '???') and (fname != '???'): 
        # file from which variable shall be read 
        file  = nc.Dataset(fname, 'r')
        dat     = np.squeeze(np.array(file.variables[varname]))
        datlat  = np.squeeze(np.array(file.variables['lat']))  # latitude of data set
        if model == 'AM2': datlat = latAM2        
        # average over longitude and only take last nmth months
        dat_zm = np.nanmean(dat[-(nmth+3):, :, :], 2)
        # average over all Januaries, Februaries, Marchs and so on
        dat_zmmm = np.zeros((12, datlat.size)) + np.nan
        if model != 'CALTECH' :
		    for imonth in range(0, 12):
			    dat_zmmm[imonth, :] = np.nanmean(dat_zm[(imonth+3):(nmth+3):12, :], axis=0)
        elif model == 'CALTECH': #caltech starts model in April. Shift count by 3
		    for imonth in range(0, 12):
			    dat_zmmm[imonth, :] = np.nanmean(dat_zm[imonth:nmth:12, :], axis=0)
            		
		 
        # interpolate to lat defined at top of file
        out = np.zeros((12, nlat)) + np.nan
        # if lat is from NP to SP we need to flip lat, otherwise np.interp gives meaningless result
        if datlat[0] > datlat[1]: 
            dat_zmmm = dat_zmmm[:, ::-1]
            datlat   = datlat[::-1]
        for imonth in range(0, 12):
            out[imonth, :] = np.interp(lat, datlat, dat_zmmm[imonth, :]) 
        print('retrieve_data_from_umiami_zmmm, get data for', model, exp, var, 'successful')    
    else:    # file does not exist
        out = np.zeros((12, nlat)) + np.nan
        print('retrieve_data_from_umiami_zmmm, get data for', model, exp, var, 'failed and data set to nan')
    return out    

	
	
	
	
def yearlymean_gm(gm_data):
    #data = gm_data
    #average = np.zeros(gm_data.size/12)  
    #for i in range (0,20):
     #   average[i]= data[i*12:i*12+12].sum()/12
    #print average
    #return average 
	shape = np.reshape(gm_data, (gm_data.size/12, 12))
	shape.shape
	shape_average = np.nanmean(shape, axis =1 )
	return shape_average
	
	
def retrieve_data_from_umiami_gm480(model, exp, var):
    # global time mean data
    print 'retrieve_data_from_umiami_gmtm, get data for:', model, exp, var
    #model specific variable name
    varname = '???'
    varname = cmip5var_to_modelvar(var, model)
    # file from which variable shall be read 
    fname = '???'
    fname = get_url_string(model, exp, varname) 
    print "check?"
    print varname, fname
      
# check if variable and file actually exists
    if varname != '???' and fname != '???': 
        # file from which variable shall be read 
        fill  = nc.Dataset(fname, 'r')
        
        
        dat     = np.squeeze(np.array(fill.variables[varname]))
        #print 'DAT', dat       
        
        datlat  = np.squeeze(np.array(fill.variables['lat'  ]))  # latitude of data set
        
        # average over longitude, only last 240 months
        
        
        dat_zm = np.nanmean(dat, 2)
                
        # now also average over latitude --> output out
        #to do this, I create an array which consists of the altered latitude array repeated 240 times
        #so it hits each row (correpsonding to each of 240 monthly time averages). Then I divide by the total area.
        #the output ("out", below) is a 240 x 1 array, with each entry being the value for that month averaged over lat and lon. 
       
        area = np.cos(datlat*np.pi/180.0)
        area360 = np.tile(area, (dat_zm[:, 0].size, 1))
		
        print 'retrieve_data_from_umiami_gmtm, get data for', model, exp, var, 'successful'
        out  = np.nansum(dat_zm*area360, axis=1)/np.nansum(area)
		
    else: # either file or variable are missing
        #we can't perform matrix operations on entries with nan... what if we
        #set them to zero so we can add/subtract?
        out= 0.0
        #out = np.nan
    return out
	


def retrieve_data_from_umiami_hemi(model, exp, var):
    # global time mean data
    print 'retrieve_data_from_umiami_gmtm, get data for:', model, exp, var
    #model specific variable name
    varname = '???'
    varname = alt.cmip5var_to_modelvar(var, model)
    # file from which variable shall be read 
    fname = '???'
    fname = alt.get_url_string(model, exp, varname) 
    print "check?"
    print varname, fname
      
# check if variable and file actually exists
    if varname != '???' and fname != '???': 
        # file from which variable shall be read 
        fill  = nc.Dataset(fname, 'r')
        
        
        dat     = np.squeeze(np.array(fill.variables[varname]))
        #print 'DAT', dat       
        
        datlat  = np.squeeze(np.array(fill.variables['lat'  ]))  # latitude of data set
        
        #average over longitude, only last 240 months
        
        #latitude slice needs to be adjusted based on the model and the hemisphere desired.
        dat_zm = np.nanmean(dat[-240:, -72:, :], 2)
                
        # now also average over latitude --> output out
        #to do this, I create an array which consists of the altered latitude array repeated 240 times
        #so it hits each row (correpsonding to each of 240 monthly time averages). Then I divide by the total area.
        #the output ("out", below) is a 240 x 1 array, with each entry being the value for that month averaged over lat and lon. 
       
        area = np.cos(datlat[-72:]*np.pi/180.0)
        area240 = np.tile(area, (240, 1))
		
        print 'retrieve_data_from_umiami_gmtm, get data for', model, exp, var, 'successful'
        out  = np.nansum(dat_zm*area240, axis=1)/np.nansum(area)
		
    else: # either file or variable are missing
        #we can't perform matrix operations on entries with nan... what if we
        #set them to zero so we can add/subtract?
        #out= 0.0
        out = np.nan
    return out#, dat, datlat, dat_zm, area, area240
	
	
