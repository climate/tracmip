# load modules
import numpy as np

def get_modelnames():
    modnames = ['CNRM-AM5 old', # 0 --> ??
                'ECHAM6.1'    , # 1     6
                'ECHAM6.3'    , # 2     7
                'LMDZ5A'      , # 3     9
                'MetUM-CTL'   , # 4    10
                'MetUM-ENT'   , # 5    11
                'MIROC5'      , # 6    12
                'AM2.1'       , # 7     1
                'CAM3'        , # 8     2
                'CAM4'        , # 9     3
                'MPAS'        , # 10   13
                'GISS ModelE2', # 11    8
                'CAM5Nor'     , # 12    4
                'CNRM-AM5'    , # 13    5
                'CALTECH']      # 14   14
    return modnames

def get_modelnumbers():
    modnumbers = ['??', '6', '7', '9', '10', '11', \
                  '12', '1', '2', '3', '13', '8', '4', '5', '14']                
    return modnumbers        

def get_availablemodels(exp):
    if (exp == 'aqct'):
        modlist = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14]     
    if (exp == 'aq4x'):
        modlist = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14]  
    if (exp == 'ldct'):
        modlist = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10,     12, 13, 14]     
    if (exp == 'ld4x'):
        modlist = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10,     12, 13, 14] 
    if (exp == 'ldor'):
        modlist = [1, 2, 3, 4, 5, 6,    8, 9, 10,         13    ] 
    return modlist

def get_modelcolors12():
    # from color brewer: http://colorbrewer2.org/
    modcolors12 = np.array([(166,206,227), (31,120,180), (178,223,138), (51,160,44), \
              (251,154,153), (227,26,28) , (253,191,111), (255,127,0), \
              (202,178,214), (106,61,154), (255,255,153), (177,89,40)])/255
    return modcolors12
    
def get_modelcolors(nmod):
    # from http://graphicdesign.stackexchange.com/questions/3682/where-can-i-find-a-large-palette-set-of-contrasting-colors-for-coloring-many-d
    colors = np.array([(240,163,255),(0,117,220),(153,63,0),(76,0,92),(25,25,25),    \
                   (0,92,49),(43,206,72),(255,204,153),(128,128,128),(148,255,181), \
                   (143,124,0),(157,204,0),(194,0,136),(0,51,128),(255,164,5),      \
                   (255,168,187),(66,102,0),(255,0,16),(94,241,242),(0,153,143),    \
                   (224,255,102),(116,10,255),(153,0,0),(255,255,128),(255,255,0),  \
                   (255,80,5)])/255
    return colors[0:nmod]                   
