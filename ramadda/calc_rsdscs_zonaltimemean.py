# load modules
import numpy as np

# load own modules
import sys
sys.path.append('/home/aiko/Dropbox/Projects/current/Tracmip/analysis/python3/ramadda/mymodules/')

import loaddata as loaddata

# load data from ramadda server
rsdscs_aqct, lat = loaddata.get_data_zmtm('AQUA'   , 'rsdscs')
rsdscs_aq4x, lat = loaddata.get_data_zmtm('AQUACO2', 'rsdscs')
rsdscs_ldct, lat = loaddata.get_data_zmtm('LAND'   , 'rsdscs')
rsdscs_ld4x, lat = loaddata.get_data_zmtm('LANDCO2', 'rsdscs')
rsdscs_ldor, lat = loaddata.get_data_zmtm('LORBIT' , 'rsdscs')

# saving data for later use
print('Saving zonal time mean rsdscs')
np.savez('rsdscs_zonaltimemean.npz', \
         **{'rsdscs_aqct':rsdscs_aqct, 'rsdscs_aq4x':rsdscs_aq4x, 'rsdscs_ldct':rsdscs_ldct, \
            'rsdscs_ld4x':rsdscs_ld4x, 'rsdscs_ldor':rsdscs_ldor, 'lat':lat})