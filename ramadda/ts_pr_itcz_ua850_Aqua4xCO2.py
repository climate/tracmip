# load modules
import numpy as np
import matplotlib.pyplot as plt

# load own modules
import sys
sys.path.append('/home/aiko/Dropbox/Projects/current/Tracmip/analysis/python3/ramadda/mymodules/')
sys.path.append('/home/aiko/Dropbox/Python3/mymodules/')
import atmosphere as atm
import tracmipmodels as tracmipmodels

# load pr and ts data
file  = np.load('ts_zonaltimemean.npz')
lat   = file['lat']
ts_tm = file['ts_aqct']
file  = np.load('pr_zonaltimemean.npz')
pr_tm = file['pr_aq4x']
file  = np.load('pr_zonalmonthmean.npz')
pr_mm = file['pr_aq4x']
file  = np.load('ua_zonaltimemean_plevels.npz')
lev   = file['lev']
ua    = file['ua_aq4x']

nmod   = ts_tm[:, 0].size
month  = np.array([1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12])
nmonth = month.size
nlat   = lat.size

sinlat = np.sin(lat*np.pi/180)

# get model colors, names and numbers
modelcolors   = tracmipmodels.get_modelcolors(nmod)
modelnames    = tracmipmodels.get_modelnames()
modelnumbers  = tracmipmodels.get_modelnumbers()

# list of available models
modlist_aq4x = tracmipmodels.get_availablemodels('aq4x')

# analysis
# calculate time-mean ITCZ position
itcz_tm = np.zeros(nmod)
for i in modlist_aq4x:
    itcz_tm[i] = atm.get_itczposition(pr_tm[i, :], lat, 30, 0.1)

# calculate monthly-mean ITCZ position
itcz_mm = np.zeros((nmod, 12)) + np.nan
for i in modlist_aq4x:
    for t in range(0, nmonth):
        itcz_mm[i, t] = atm.get_itczposition(pr_mm[i, t, :], lat, 30, 0.1)

# caltech seasonal cycle shifted by 3 months
tmp = np.copy(itcz_mm[14,:])
itcz_mm[14,3:12] = tmp[0:9]
itcz_mm[14,0:3 ] = tmp[9:12]

# plotting
fig = plt.figure( figsize=(12, 8), dpi=80, facecolor='w', edgecolor='k' )

ax = plt.subplot(2, 2, 1)
ax.spines['right'].set_color('none')
ax.spines['top'].set_color('none')
ax.xaxis.set_ticks_position('bottom')
ax.yaxis.set_ticks_position('left')

for m in modlist_aq4x:
    plt.plot(sinlat, ts_tm[m, :], color=modelcolors[m])
plt.plot(sinlat, np.nanmedian(ts_tm[modlist_aq4x], axis=0), 'k', linewidth=3)
plt.xlim(-0.98, 0.98), plt.ylim(270, 312)
ax.xaxis.set_ticks([-0.866, -0.5, 0, 0.5, 0.866])
ax.xaxis.set_ticklabels(['60S', '30S', 'Eq', '30N', '60N'], fontsize=10)
ax.yaxis.set_ticks([270, 280, 290, 300, 310])
ax.yaxis.set_ticklabels([270, 280, 290, 300, 310], fontsize=10) 
plt.title('Surface temperature', fontsize=14)
plt.ylabel('K', fontsize=12)
plt.text(0.02, 0.95, 'a)', fontsize=14, ha='left', va='center', transform=ax.transAxes)
   
ax = plt.subplot(2, 2, 2)
ax.spines['right'].set_color('none')
ax.spines['top'].set_color('none')
ax.xaxis.set_ticks_position('bottom')
ax.yaxis.set_ticks_position('left')

for m in modlist_aq4x:
    plt.plot(sinlat, pr_tm[m, :], color=modelcolors[m])
    plt.plot(np.sin(itcz_tm[m]*np.pi/180.0), 1, '|', mew=1, ms=15, color=modelcolors[m])
plt.plot(sinlat, np.nanmedian(pr_tm[modlist_aq4x,:], axis=0), 'k', linewidth=3) 
plt.plot(np.sin(np.nanmedian(itcz_tm[modlist_aq4x])*np.pi/180.0), 1, '|', mew=2, ms=15, color='k')
plt.xlim(-0.98, 0.98), plt.ylim(0, 18)
ax.xaxis.set_ticks([-0.866, -0.5, 0, 0.5, 0.866])
ax.xaxis.set_ticklabels(['60S', '30S', 'Eq', '30N', '60N'], fontsize=10)
ax.yaxis.set_ticks([0, 6, 12, 18])
ax.yaxis.set_ticklabels([0, 6, 12, 18], fontsize=10) 
plt.title('Precipitation', fontsize=14)
plt.ylabel('mm/day', fontsize=12)
plt.text(0.02, 0.95, 'b)', fontsize=14, ha='left', va='center', transform=ax.transAxes)

ax = plt.subplot(2, 2, 3)
ax.spines['right'].set_color('none')
ax.spines['top'].set_color('none')
ax.xaxis.set_ticks_position('bottom')
ax.yaxis.set_ticks_position('left')

plt.plot([-20, 20], [0, 0], 'k--')
for m in modlist_aq4x:
    plt.plot(month, itcz_mm[m, :], color=modelcolors[m])
plt.plot(month, np.nanmedian(itcz_mm[modlist_aq4x, :], axis=0), 'k', linewidth=3)
plt.xlim(1, 12), plt.ylim(-15, 15)
ax.xaxis.set_ticks(month)
ax.xaxis.set_ticklabels(['Jan', '', '', 'Apr', '', '', 'Jul', '', '' ,'Oct', '', ''], fontsize=10)
ax.yaxis.set_ticks([-15, -10, -5, 0, 5, 10, 15])
ax.yaxis.set_ticklabels(['15S', '10S', '5S', 'Eq', '5N', '10N', '15N'], fontsize=10) 
plt.title('ITCZ position', fontsize=14)
plt.xlabel('month', fontsize=12)
plt.ylabel('deg lat', fontsize=12)
plt.text(0.02, 0.95, 'c)', fontsize=14, ha='left', va='center', transform=ax.transAxes)
   
ax = plt.subplot(2, 2, 4)
ax.spines['right'].set_color('none')
ax.spines['top'].set_color('none')
ax.xaxis.set_ticks_position('bottom')
ax.yaxis.set_ticks_position('left')

plt.plot([-10, 10], [0, 0], 'k--')
# lev=14 is 850 hPa
for m in modlist_aq4x:
    plt.plot(sinlat, ua[m, 14, :], color=modelcolors[m])
plt.plot(sinlat, np.nanmedian(ua[modlist_aq4x, 14, :], axis=0), 'k', linewidth=3)

plt.xlim(-0.98, 0.98), plt.ylim(-10, 15)
ax.xaxis.set_ticks([-0.866, -0.5, 0, 0.5, 0.866])
ax.xaxis.set_ticklabels(['60S', '30S', 'Eq', '30N', '60N'], fontsize=10)
ax.yaxis.set_ticks([-10, -5, 0, 5, 10, 15])
ax.yaxis.set_ticklabels([-10, -5, 0, 5, 10, 15], fontsize=10)
plt.title('Zonal wind at 850 hPa', fontsize=14)
plt.ylabel('m/s', fontsize=12)
plt.text(0.02, 0.95, 'd)', fontsize=14, ha='left', va='center', transform=ax.transAxes, backgroundcolor='white')
  
plt.tight_layout
plt.savefig('figs/ts_pr_itcz_ua850_Aqua4xCO2.pdf')


plt.show()
