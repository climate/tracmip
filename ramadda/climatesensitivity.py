# load modules
import numpy as np
import matplotlib.pyplot as plt
import scipy.stats as spstats

# load own modules
import sys
sys.path.append('/home/aiko/Dropbox/Projects/current/Tracmip/analysis/python3/ramadda/mymodules/')
sys.path.append('/home/aiko/Dropbox/Python3/mymodules/')
import tracmipmodels as tracmipmodels

# loading data
print('Load global time mean ts')
file    = np.load('ts_globaltimemean.npz')
ts_aqct = file['ts_aqct']
ts_aq4x = file['ts_aq4x']
ts_ldct = file['ts_ldct']
ts_ld4x = file['ts_ld4x'] 
ts_ldor = file['ts_ldor'] 

print(ts_aqct)

nmod = ts_aqct.size

# get model colors, names and numbers
modelcolors   = tracmipmodels.get_modelcolors(nmod)
modelnames    = tracmipmodels.get_modelnames()
modelnumbers  = tracmipmodels.get_modelnumbers()

# plotting
plt.figure( figsize=(12, 4), dpi=80, facecolor='w', edgecolor='k' )

ax = plt.subplot(1, 2, 1)
ax.spines['right'].set_color('none')
ax.spines['top'].set_color('none')
ax.xaxis.set_ticks_position('bottom')
ax.yaxis.set_ticks_position('left')

for i in range(0, nmod):
    plt.text(ts_aqct[i], 0.5*(ts_aq4x - ts_aqct)[i], modelnumbers[i], fontweight='bold', color=modelcolors[i], fontsize=14)
plt.xlim(291, 302), plt.ylim(0.9, 5.7)
corr, r = spstats.spearmanr(ts_aqct, 0.5*(ts_aq4x - ts_aqct))
print(corr, r)
plt.text(298, 1.1, ' corr=0.27, p=0.40')
plt.title('Aquaplanet', fontsize=14)
plt.xlabel('Global surface temperature (K)', fontsize=12)
plt.ylabel('Climate sensitivity (K)', fontsize=12)
ax.xaxis.set_ticks([292, 294, 296, 298, 300, 302])
ax.xaxis.set_ticklabels([292, 294, 296, 298, 300, 302], fontsize=10)
ax.yaxis.set_ticks([2, 3, 4, 5])
ax.yaxis.set_ticklabels([2, 3, 4, 5], fontsize=10)


ax = plt.subplot(1, 2, 2)
ax.spines['right'].set_color('none')
ax.spines['top'].set_color('none')
ax.xaxis.set_ticks_position('bottom')
ax.yaxis.set_ticks_position('left')

for i in range (1, nmod-1):
    plt.text(ts_ldct[i], 0.5*(ts_ld4x - ts_ldct)[i], modelnumbers[i], fontweight='bold', color=modelcolors[i], fontsize=14)
plt.xlim(291, 302), plt.ylim(0.9, 5.7)
corr, r = spstats.spearmanr(ts_ldct, 0.5*(ts_ld4x - ts_ldct))
print(corr, r)
plt.text(298, 1.1, ' corr=0.54, p=0.07')
plt.title('With continent', fontsize=14)
plt.xlabel('Global surface temperature (K)', fontsize=12)
ax.xaxis.set_ticks([292, 294, 296, 298, 300, 302])
ax.xaxis.set_ticklabels([292, 294, 296, 298, 300, 302], fontsize=10)
ax.yaxis.set_ticks([2, 3, 4, 5])
ax.yaxis.set_ticklabels([2, 3, 4, 5], fontsize=10)

plt.tight_layout
plt.savefig('figs/climatesensitivity.pdf')
    
plt.figure( figsize=(12, 4), dpi=80, facecolor='w', edgecolor='k' )

plt.subplot(1, 2, 1)
plt.plot([1, 6], [1, 6], 'k--')
for i in range (1, nmod-1):
    plt.text(0.5*(ts_aq4x-ts_aqct)[i], 0.5*(ts_ld4x - ts_ldct)[i], 
             modelnumbers[i], fontweight='bold', color=modelcolors[i], fontsize=14)  
plt.xlabel('Aquaplanet')
plt.ylabel('With land')    
plt.title('Climate Sensitivity')

plt.subplot(1, 2, 2)
plt.plot([1, 6], [0, 0], 'k--'), plt.ylim(-1, 1)
for i in range (1, nmod-1):
    plt.text(0.5*(ts_aq4x-ts_aqct)[i], 0.5*(ts_ld4x - ts_ldct)[i] - 0.5*(ts_aq4x-ts_aqct)[i], 
             modelnumbers[i], fontweight='bold', color=modelcolors[i], fontsize=14)  
plt.xlabel('Aquaplanet')
plt.ylabel('Land-Aquaplanet')    
plt.title('Climate Sensitivity')    
    
print(0.5*np.nanmedian(ts_aq4x - ts_aqct))
print(0.5*np.nanmin(ts_aq4x - ts_aqct))
print(0.5*np.nanmax(ts_aq4x - ts_aqct))

print(0.5*np.nanmedian(ts_ld4x - ts_ldct))
print(0.5*np.nanmin(ts_ld4x - ts_ldct))
print(0.5*np.nanmax(ts_ld4x - ts_ldct))
  

  
plt.show()

#load_data('MetENT','AQUA', 'pr')
