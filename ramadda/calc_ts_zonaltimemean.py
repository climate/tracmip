# load modules
import numpy as np

# load own modules
import sys
sys.path.append('/home/aiko/Dropbox/Projects/current/Tracmip/analysis/python3/ramadda/mymodules/')
#sys.path.append('/home/aiko/Dropbox/Python3/mymodules/')

import loaddata as loaddata

# load data from ramadda server
ts_aqct, lat = loaddata.get_data_zmtm('AQUA'   , 'ts')
ts_aq4x, _   = loaddata.get_data_zmtm('AQUACO2', 'ts')
ts_ldct, _   = loaddata.get_data_zmtm('LAND'   , 'ts')
ts_ld4x, _   = loaddata.get_data_zmtm('LANDCO2', 'ts')
ts_ldor, _   = loaddata.get_data_zmtm('LORBIT' , 'ts')

# saving data for later use
print('Saving zonal time mean ts')
np.savez('ts_zonaltimemean.npz', \
         **{'ts_aqct':ts_aqct, 'ts_aq4x':ts_aq4x, 'ts_ldct':ts_ldct, \
            'ts_ld4x':ts_ld4x, 'ts_ldor':ts_ldor, 'lat':lat} )
