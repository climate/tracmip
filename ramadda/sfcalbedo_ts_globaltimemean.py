# load modules
import numpy as np
import matplotlib.pyplot as plt

# load own modules
import sys
sys.path.append('/home/aiko/Dropbox/Projects/current/Tracmip/analysis/python3/ramadda/mymodules/')
sys.path.append('/home/aiko/Dropbox/Python3/mymodules/')
import tracmipmodels as tracmipmodels

# load data
file        = np.load('rsuscs_zonaltimemean.npz')
lat         = file['lat']
rsuscs_aqct = file['rsuscs_aqct']
file        = np.load('rsdscs_zonaltimemean.npz')
rsdscs_aqct = file['rsdscs_aqct']
file        = np.load('ts_globaltimemean.npz')
ts_aqct     = file['ts_aqct']

nmod = rsuscs_aqct[:, 0].size
print('Number of models:', nmod)
nlat = lat.size

# get model colors, names and numbers
modelcolors   = tracmipmodels.get_modelcolors(nmod)
modelnames    = tracmipmodels.get_modelnames()
modelnumbers  = tracmipmodels.get_modelnumbers()

# list of available models
modlist_aqct  = tracmipmodels.get_availablemodels('aqct')

# analysis
area = np.cos(lat*np.pi/180)

# global mean surface albedo
sfcalb_aqct = np.zeros(nmod) + np.nan
for m in range(0, nmod):
    sfcalb_aqct[m] = np.abs(np.sum(rsuscs_aqct[m, :]*area) / np.sum(rsdscs_aqct[m, :]*area))

sfcalb_aqct[14]=0.06 #0.38 # caltech surface albedo

# plotting
plt.figure()
for m in modlist_aqct:
    plt.subplot(4, 4, m+1)
    plt.plot(lat, rsdscs_aqct[m, :])
    
    plt.title(modelnames[m])

plt.figure( figsize=(12, 4), dpi=80, facecolor='w', edgecolor='k' )  

ax = plt.subplot(1, 2, 1) 
ax.spines['right'].set_color('none')
ax.spines['top'].set_color('none')
ax.xaxis.set_ticks_position('bottom')
ax.yaxis.set_ticks_position('left') 
for m in modlist_aqct:
    plt.text(sfcalb_aqct[m], ts_aqct[m], modelnumbers[m], color=modelcolors[m], fontsize=14, \
    fontweight='normal', ha='center', va='center', backgroundcolor='none')
#plt.text(sfcalb_aqct[14], ts_aqct[14], '*'color=modelcolors[14], fontsize=14, \
#fontweight='bold', ha='center', va='center', backgroundcolor='none')
plt.xlim(0.05, 0.105), plt.ylim(290, 302)
plt.title('AquaControl', fontsize=14)
plt.xlabel('Global surface albedo', fontsize=12)
plt.ylabel('Global surface temperature (K)', fontsize=12)
ax.xaxis.set_ticks([0.06, 0.08, 0.1])
ax.xaxis.set_ticklabels([0.06, 0.08, 0.10], fontsize=10)
ax.yaxis.set_ticks([290, 294, 298, 302])
ax.yaxis.set_ticklabels([290, 294, 298, 302], fontsize=10)

ax = plt.subplot(1, 2, 2)
plt.xlim(0, 1), plt.ylim(0, 1)
plt.axis('off')
for m in modlist_aqct:
    plt.text(0.0, 1.06-0.08*np.float(modelnumbers[m]), modelnumbers[m], color=modelcolors[m], fontsize=14)
    plt.text(0.1, 1.06-0.08*np.float(modelnumbers[m]), modelnames[m], color=modelcolors[m], fontsize=14)

plt.tight_layout
plt.savefig('figs/sfcalbedo_ts_globaltimemean_aquacontrol.pdf')

print(np.nanmedian(ts_aqct[modlist_aqct]))
print(np.nanmax(ts_aqct[modlist_aqct]))
print(np.nanmin(ts_aqct[modlist_aqct]))

print(np.abs(ts_aqct[modlist_aqct]-np.nanmedian(ts_aqct[modlist_aqct])))

plt.show()
