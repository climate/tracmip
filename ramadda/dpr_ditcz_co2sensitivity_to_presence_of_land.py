# load modules
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import cm
from matplotlib.patches import Rectangle

# load own modules
import sys
sys.path.append('/home/aiko/Dropbox/Projects/current/Tracmip/analysis/python3/ramadda/mymodules/')
sys.path.append('/home/aiko/Dropbox/Python3/mymodules/')
import tracmipmodels as tracmipmodels
import atmosphere as atm

def make_niceplot1(ax):
    ax.spines['right'].set_color('none')
    ax.spines['top'].set_color('none')
    ax.xaxis.set_ticks_position('bottom')
    ax.yaxis.set_ticks_position('left')
    ax.xaxis.set_ticks([-8, -6, -4, -2, 0, 2, 4, 6, 8])
    ax.xaxis.set_ticklabels(['-8', '', '-4', '', '0', '', '4', '', '8'], fontsize=10)
    ax.yaxis.set_ticks([-0.5, 0, 0.5])
    ax.yaxis.set_ticklabels(['30S', 'Eq', '30N'], fontsize=10) 
    plt.xlim(-8, 8), plt.ylim(-0.6, 0.6)  


def make_niceplot2(ax):
    ax.xaxis.set_ticks([-120, -60, 0, 60, 120])
    ax.xaxis.set_ticklabels(['120W', '60W', '0', '60E', '120E'], fontsize=10)
    ax.yaxis.set_ticks([-0.5, 0, 0.5])
    ax.yaxis.set_ticklabels([''], fontsize=10) 
    plt.xlim(-175, 175), plt.ylim(-0.6, 0.6)         

# load pr data
file    = np.load('pr_latlontimemean.npz')
lat     = file['lat']
lon     = file['lon']
pr_aqct = file['pr_aqct']
pr_aq4x = file['pr_aq4x']
pr_ldct = file['pr_ldct']
pr_ld4x = file['pr_ld4x']

nmod = pr_aqct[:, 0, 0].size
nlat = lat.size
nlon = lon.size

sinlat = np.sin(lat*np.pi/180)

# get model colors, names and numbers
modelcolors   = tracmipmodels.get_modelcolors(nmod)
modelnames    = tracmipmodels.get_modelnames()
modelnumbers  = tracmipmodels.get_modelnumbers()

# list of available models
modlist_aqct = tracmipmodels.get_availablemodels('aqct')
modlist_aq4x = tracmipmodels.get_availablemodels('aq4x')
modlist_ldct = tracmipmodels.get_availablemodels('ldct')
modlist_ld4x = tracmipmodels.get_availablemodels('ld4x')

# itcz position for precip averaged over land and ocean
itcz_ldct = np.zeros((nmod, nlon)) + np.nan
#itcz_ld4x = np.zeros((nmod, nlon)) + np.nan
for m in modlist_ldct:
    for i in range(0, nlon):
        itcz_ldct[m, i] = atm.get_itczposition(pr_ldct[m, :, i], lat, 30.0, 0.1)
#        itcz_ld4x[m, i] = atm.get_itczposition(pr_ld4x[m, :, i], lat, 30.0, 0.1)
 
# plotting
plt.figure(figsize=(12, 8), dpi=80, facecolor='w', edgecolor='k')
#clev = np.array([-4.5, -3.5, -2.5, -1.5, -0.5, 0.5, 1.5, 2.5, 3.5, 4.5])
clev = np.array([-3.25, -2.75, -2.25, -1.75, -1.25, -0.75, -0.25, 0.25, 0.75, 1.25, 1.75, 2.25, 2.75, 3.25])

ax = plt.subplot2grid((2, 6), (0, 0))
for m in modlist_aq4x:
    plt.plot(np.nanmean((pr_aq4x-pr_aqct)[m, :, :], axis=1), sinlat, color=modelcolors[m])
plt.plot(np.nanmedian(np.nanmean(((pr_aq4x-pr_aqct)[modlist_aq4x]), axis=2), axis=0), sinlat, color='k', linewidth=3)
make_niceplot1(ax)
plt.text(0.05, 0.94, 'a)', fontsize=14, ha='left', va='center', transform=ax.transAxes)
plt.xlabel('mm/day', fontsize=12)
plt.title('Aqua', fontsize=14)

ax = plt.subplot2grid((2, 6), (0, 1), colspan=3)
c = plt.contourf(lon, sinlat, np.nanmedian((pr_ld4x-pr_ldct)[modlist_ldct],axis=0), clev, extend='both', cmap=cm.BrBG)
ax.add_patch(Rectangle((0, -0.5), 45, 1, alpha=1, facecolor='none', edgecolor='gray', linewidth=2))
plt.plot([-200, 200], [0, 0], 'k--')
plt.plot(lon, np.sin(np.nanmedian(itcz_ldct[modlist_ldct], axis=0)*np.pi/180.0), 'k', linewidth=2)
make_niceplot2(ax)
plt.text(0.02, 0.94, 'b)', fontsize=14, ha='left', va='center', transform=ax.transAxes, backgroundcolor='white')
plt.title('Land', fontsize=14)


ax = plt.subplot2grid((2, 6), (1, 1), colspan=3)
ax.axis('off')
cbar = plt.colorbar(c, ticks=[-3, -2, -1, 0, 1, 2, 3], orientation='horizontal', aspect=40)
cbar.ax.tick_params(labelsize=10)
plt.text(1, -0.17, 'mm/day', fontsize=10, ha='right')  

ax = plt.subplot2grid((2, 6), (0, 4))
for m in modlist_ld4x:
    plt.plot(np.nanmean((pr_ld4x-pr_ldct)[m, :, :], axis=1), sinlat, color=modelcolors[m])
plt.plot(np.nanmedian(np.nanmean(((pr_ld4x-pr_ldct)[modlist_ld4x]), axis=2), axis=0), sinlat, color='k', linewidth=3)
make_niceplot1(ax)
ax.spines['left'].set_color('none')
ax.yaxis.set_ticks_position('none')
ax.yaxis.set_ticklabels([''], fontsize=10) 
plt.text(0.05, 0.94, 'c)', fontsize=14, ha='left', va='center', transform=ax.transAxes, backgroundcolor='white')
plt.xlabel('mm/day', fontsize=12)
plt.title('Land', fontsize=14)

ax = plt.subplot2grid((2, 6), (0, 5))
for m in modlist_ld4x:
    plt.plot(np.nanmean((pr_ld4x-pr_ldct)[m, :, :], axis=1) - np.nanmean((pr_aq4x-pr_aqct)[m, :, :], axis=1), sinlat, color=modelcolors[m])
plt.plot(np.nanmedian(np.nanmean((pr_ld4x-pr_ldct)[modlist_ld4x], axis=2)-np.nanmean((pr_aq4x-pr_aqct)[modlist_ld4x], axis=2), axis=0), sinlat, color='k', linewidth=3)
make_niceplot1(ax)
ax.yaxis.set_ticks_position('right')
ax.spines['right'].set_color('k')
ax.spines['left'].set_color('none')
plt.text(0.05, 0.94, 'd)', fontsize=14, ha='left', va='center', transform=ax.transAxes, backgroundcolor='none')
plt.xlabel('mm/day', fontsize=12)
plt.title('Land-Aqua', fontsize=14)
ax.xaxis.set_ticks([-4, -3, -2, -1, 0, 1, 2, 3, 4])
ax.xaxis.set_ticklabels(['-4', '', '-2', '', '0', '', '2', '', '4'], fontsize=10)
plt.xlim(-4, 4)

#plt.subplots_adjust(wspace=0.04, hspace=0.05)  
plt.tight_layout

plt.savefig('figs/dpr_ditcz_co2sensitivity_to_presence_of_land.pdf')

plt.show()
