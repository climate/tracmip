# load modules
import numpy as np

# load own modules
import sys
sys.path.append('/home/aiko/Dropbox/Projects/current/Tracmip/analysis/python3/ramadda/mymodules/')

import loaddata as loaddata

# load data from ramadda server
ts_aqct = loaddata.get_data_gmallmonths('AQUA'   , 'ts')
ts_aq4x = loaddata.get_data_gmallmonths('AQUACO2', 'ts')
ts_ldct = loaddata.get_data_gmallmonths('LAND'   , 'ts')
ts_ld4x = loaddata.get_data_gmallmonths('LANDCO2', 'ts')
ts_ldor = loaddata.get_data_gmallmonths('LORBIT' , 'ts')

# saving data for later use
print('Saving global mean ts for all available months')
np.savez('ts_globalmean_allmonths.npz', \
         **{'ts_aqct':ts_aqct, 'ts_aq4x':ts_aq4x, 'ts_ldct':ts_ldct, \
            'ts_ld4x':ts_ld4x, 'ts_ldor':ts_ldor} )
