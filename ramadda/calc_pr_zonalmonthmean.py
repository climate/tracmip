# load modules
import numpy as np

# load own modules
import sys
sys.path.append('/home/aiko/Dropbox/Projects/current/Tracmip/analysis/python3/ramadda/mymodules/')
#sys.path.append('/home/aiko/Dropbox/Python3/mymodules/')

import loaddata as loaddata

# load data from ramadda server
pr_aqct, lat = loaddata.get_data_zmmm('AQUA'   , 'pr')
pr_aq4x, _   = loaddata.get_data_zmmm('AQUACO2', 'pr')
pr_ldct, _   = loaddata.get_data_zmmm('LAND'   , 'pr')
pr_ld4x, _   = loaddata.get_data_zmmm('LANDCO2', 'pr')
pr_ldor, _   = loaddata.get_data_zmmm('LORBIT' , 'pr')

# for cam3 and cam4 precip must be reconstructed from convective and large-scale components
# convective precip
prc_aqct, _   = loaddata.get_data_zmmm('AQUA'   , 'prc')
prc_aq4x, _   = loaddata.get_data_zmmm('AQUACO2', 'prc')
prc_ldct, _   = loaddata.get_data_zmmm('LAND'   , 'prc')
prc_ld4x, _   = loaddata.get_data_zmmm('LANDCO2', 'prc')
prc_ldor, _   = loaddata.get_data_zmmm('LORBIT' , 'prc')
# large-scale precip
prl_aqct, _   = loaddata.get_data_zmmm('AQUA'   , 'prl')
prl_aq4x, _   = loaddata.get_data_zmmm('AQUACO2', 'prl')
prl_ldct, _   = loaddata.get_data_zmmm('LAND'   , 'prl')
prl_ld4x, _   = loaddata.get_data_zmmm('LANDCO2', 'prl')
prl_ldor, _   = loaddata.get_data_zmmm('LORBIT' , 'prl')
# cam3, cam4 and mpas have model indices 8, 9, 10
pr_aqct[8:11] = prc_aqct[8:11] + prl_aqct[8:11]
pr_aq4x[8:11] = prc_aq4x[8:11] + prl_aq4x[8:11]
pr_ldct[8:11] = prc_ldct[8:11] + prl_ldct[8:11]
pr_ld4x[8:11] = prc_ld4x[8:11] + prl_ld4x[8:11]
pr_ldor[8:11] = prc_ldor[8:11] + prl_ldor[8:11]

# convert precip to mm/day
pr_aqct = 86400 * pr_aqct  
pr_aq4x = 86400 * pr_aq4x
pr_ldct = 86400 * pr_ldct
pr_ld4x = 86400 * pr_ld4x
pr_ldor = 86400 * pr_ldor
# for cam3, cam4 and mpas we also need to multiply by 1000
pr_aqct[8:11] = 1000*pr_aqct[8:11]
pr_aq4x[8:11] = 1000*pr_aq4x[8:11]
pr_ldct[8:11] = 1000*pr_ldct[8:11]
pr_ld4x[8:11] = 1000*pr_ld4x[8:11]
pr_ldor[8:11] = 1000*pr_ldor[8:11]

# saving data for later use
print('Saving zonal month mean pr')
np.savez('pr_zonalmonthmean.npz', \
         **{'pr_aqct':pr_aqct, 'pr_aq4x':pr_aq4x, 'pr_ldct':pr_ldct, \
            'pr_ld4x':pr_ld4x, 'pr_ldor':pr_ldor, 'lat':lat} )
