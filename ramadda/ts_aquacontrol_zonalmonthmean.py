# load modules
import numpy as np
import matplotlib.pyplot as plt

# load own modules
import sys
sys.path.append('/home/aiko/Dropbox/Projects/current/Tracmip/analysis/python3/ramadda/mymodules/')
sys.path.append('/home/aiko/Dropbox/Python3/mymodules/')

models=['CNRM', 'ECHAM61', 'ECHAM63', 'IPSL', 'MetCTL', 'MetENT', \
        'MIROC5', 'AM2', 'CAM3', 'CAM4', 'MPAS', 'GISS']

# load global mean pr and ts data
file    = np.load('ts_zonalmonthmean.npz')
lat     = file['lat']
ts_aqct = file['ts_aqct']

nmod = ts_aqct[:, 0, 0].size
nmth = ts_aqct[0, :, 0].size

plt.contourf(ts_aqct[4, :, :])

print(ts_aqct.shape)

plt.figure()

for m in range(0, nmth):
    plt.plot(lat, ts_aqct[5, m, :])

plt.show()
