# load modules

# load own modules
#import sys
#sys.path.append('/home/aiko/Dropbox/Projects/current/Tracmip/analysis/python3/ramadda/')
#sys.path.append('/home/aiko/Dropbox/Python3/mymodules/')

import calc_pr_globaltimemean
import calc_pr_zonaltimemean
import calc_pr_zonalmonthmean
import calc_pr_latlontimemean
import calc_pr_latlonmonthmean
import calc_ts_globaltimemean
import calc_ts_zonaltimemean
import calc_ts_zonalmonthmean
import calc_ts_latlontimemean
import calc_rsdt_zonalmonthmean
import calc_rsdscs_zonaltimemean
import calc_rsdscs_latlontimemean
import calc_rsuscs_zonaltimemean
import calc_rsuscs_latlontimemean
import calc_ua_zonaltimemean_plevels
import calc_va_zonaltimemean_plevels

