# load modules
import numpy as np
import matplotlib.pyplot as plt

# load own modules
import sys
sys.path.append('/home/aiko/Dropbox/Projects/current/Tracmip/analysis/python3/ramadda/mymodules/')
sys.path.append('/home/aiko/Dropbox/Python3/mymodules/')
import atmosphere as atm

# load pr and ts data
file       = np.load('pr_zonaltimemean.npz')
lat        = file['lat']
pr_aqct_tm = file['pr_aqct']
pr_ldct_tm = file['pr_ldct']
pr_ldor_tm = file['pr_ldor'] 

file       = np.load('pr_zonalmonthmean.npz')
pr_aqct_mm = file['pr_aqct']
pr_ldct_mm = file['pr_ldct']
pr_ldor_mm = file['pr_ldor'] 

nmod   = pr_aqct_tm[:, 0].size
month  = np.array([1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12])
nmonth = month.size

# calculate time-mean ITCZ position
itcz_aqct_tm = np.zeros(nmod)
itcz_ldct_tm = np.zeros(nmod)
itcz_ldor_tm = np.zeros(nmod)
for i in range(0, nmod):
    itcz_aqct_tm[i] = atm.get_itczposition(pr_aqct_tm[i, :], lat, 30, 0.1)
    itcz_ldct_tm[i] = atm.get_itczposition(pr_ldct_tm[i, :], lat, 30, 0.1)
    itcz_ldor_tm[i] = atm.get_itczposition(pr_ldor_tm[i, :], lat, 30, 0.1)

# calculate monthly-mean ITCZ position
itcz_aqct_mm = np.zeros((nmod, 12))
itcz_ldct_mm = np.zeros((nmod, 12))
itcz_ldor_mm = np.zeros((nmod, 12))
for i in range(0, nmod):
    for t in range(0, nmonth):
        itcz_aqct_mm[i, t] = atm.get_itczposition(pr_aqct_mm[i, t, :], lat, 30, 0.1)
        itcz_ldct_mm[i, t] = atm.get_itczposition(pr_ldct_mm[i, t, :], lat, 30, 0.1)
        itcz_ldor_mm[i, t] = atm.get_itczposition(pr_ldor_mm[i, t, :], lat, 30, 0.1)
  

# plotting
fig = plt.figure( figsize=(13, 7), dpi=80, facecolor='w', edgecolor='k' )

ax = plt.subplot(2, 3, 1)
ax.spines['right'].set_color('none')
ax.spines['top'].set_color('none')
ax.xaxis.set_ticks_position('bottom')
ax.yaxis.set_ticks_position('left')

for m in range(0, nmod):
    plt.plot(lat, pr_aqct_tm[m, :], 'gray')
plt.plot(lat, np.nanmedian(pr_aqct_tm, axis=0), 'k', linewidth=2)
plt.xlim(-40, 40), plt.ylim(0, 18)
ax.xaxis.set_ticks([-30, -15, 0, 15, 30])
ax.xaxis.set_ticklabels([r'30$^o$S', r'15$^o$S', 'Eq', r'15$^o$S', r'30$^o$S'], fontsize=10)
ax.yaxis.set_ticks([0, 4, 8, 12, 16])
ax.yaxis.set_ticklabels([0, 4, 8, 12, 16], fontsize=10)
plt.title('AquaControl', fontsize=14)
plt.xlabel('latitude', fontsize=12)
plt.ylabel('annual precipitation (mm/day)', fontsize=12)

ax = plt.subplot(2, 3, 2)
ax.spines['right'].set_color('none')
ax.spines['top'].set_color('none')
ax.xaxis.set_ticks_position('bottom')
ax.yaxis.set_ticks_position('left')

for m in range(0, nmod):
    plt.plot(lat, (pr_ldct_tm-pr_aqct_tm)[m, :], 'gray')
plt.plot(lat, np.nanmedian(pr_ldct_tm-pr_aqct_tm, axis=0), 'k', linewidth=2)
plt.xlim(-40, 40), plt.ylim(-2.5, 2.5)
ax.xaxis.set_ticks([-30, -15, 0, 15, 30])
ax.xaxis.set_ticklabels([r'30$^o$S', r'15$^o$S', 'Eq', r'15$^o$S', r'30$^o$S'], fontsize=10)
ax.yaxis.set_ticks([-2, -1, 0, 1, 2])
ax.yaxis.set_ticklabels([-2, -1, 0, 1, 2], fontsize=10) 
plt.title('LandControl - AquaControl')
plt.xlabel('latitude', fontsize=12)
   
ax = plt.subplot(2, 3, 3)
ax.spines['right'].set_color('none')
ax.spines['top'].set_color('none')
ax.xaxis.set_ticks_position('bottom')
ax.yaxis.set_ticks_position('left')

for m in range(0, nmod):
    plt.plot(lat, (pr_ldor_tm-pr_ldct_tm)[m, :], 'gray')
plt.plot(lat, np.nanmedian(pr_ldor_tm-pr_ldct_tm, axis=0), 'k', linewidth=2)   
plt.xlim(-40, 40), plt.ylim(-0.8, 0.8)
ax.xaxis.set_ticks([-30, -15, 0, 15, 30])
ax.xaxis.set_ticklabels([r'30$^o$S', r'15$^o$S', 'Eq', r'15$^o$S', r'30$^o$S'], fontsize=10)
ax.yaxis.set_ticks([-0.6, -0.3, 0, 0.3, 0.6])
ax.yaxis.set_ticklabels([-0.6, -0.3, 0, 0.3, 0.6], fontsize=10) 
plt.title('LandOrbit- LandControl', fontsize=14)
plt.xlabel('latitude', fontsize=12)

ax = plt.subplot(2, 3, 4)
ax.spines['right'].set_color('none')
ax.spines['top'].set_color('none')
ax.xaxis.set_ticks_position('bottom')
ax.yaxis.set_ticks_position('left')

for m in range(0, nmod):
    plt.plot(month, itcz_aqct_mm[m, :], 'gray')
plt.plot(month, np.nanmedian(itcz_aqct_mm, axis=0), 'k', linewidth=2)
plt.xlim(1, 12), plt.ylim(-15, 15)
ax.xaxis.set_ticks(month)
ax.xaxis.set_ticklabels(['Jan', '', '', 'Apr', '', '', 'Jul', '', '' ,'Oct', '', ''], fontsize=10)
ax.yaxis.set_ticks([-10, -5, 0, 5, 10])
ax.yaxis.set_ticklabels([r'10$^o$S', r'5$^o$S', 'Eq', r'5$^o$S', r'10$^o$S'], fontsize=10) 
plt.xlabel('month')
plt.ylabel('ITCZ position (deg lat)')

ax = plt.subplot(2, 3, 5)
ax.spines['right'].set_color('none')
ax.spines['top'].set_color('none')
ax.xaxis.set_ticks_position('bottom')
ax.yaxis.set_ticks_position('left')

for m in range(0, nmod):
    plt.plot(month, (itcz_ldct_mm-itcz_aqct_mm)[m, :], 'gray')
plt.plot(month, np.nanmedian(itcz_ldct_mm-itcz_aqct_mm, axis=0), 'k', linewidth=2)
plt.xlim(1, 12), plt.ylim(-10, 10)
ax.xaxis.set_ticks(month)
ax.xaxis.set_ticklabels(['Jan', '', '', 'Apr', '', '', 'Jul', '', '' ,'Oct', '', ''], fontsize=10)
ax.yaxis.set_ticks([-10, -5, 0, 5, 10])
ax.yaxis.set_ticklabels([r'10$^o$S', r'5$^o$S', 'Eq', r'5$^o$S', r'10$^o$S'], fontsize=10) 
plt.xlabel('month')

ax = plt.subplot(2, 3, 6)
ax.spines['right'].set_color('none')
ax.spines['top'].set_color('none')
ax.xaxis.set_ticks_position('bottom')
ax.yaxis.set_ticks_position('left')

for m in range(0, nmod):
    plt.plot(month, (itcz_ldor_mm-itcz_ldct_mm)[m, :], 'gray')
plt.plot(month, np.nanmedian(itcz_ldor_mm-itcz_ldct_mm, axis=0), 'k', linewidth=2)
plt.xlim(1, 12), plt.ylim(-2, 2)
ax.xaxis.set_ticks(month)
ax.xaxis.set_ticklabels(['Jan', '', '', 'Apr', '', '', 'Jul', '', '' ,'Oct', '', ''], fontsize=10)
ax.yaxis.set_ticks([-2, -1, 0, 1, 2])
ax.yaxis.set_ticklabels([r'2$^o$S', r'1$^o$S', 'Eq', r'1$^o$S', r'2$^o$S'], fontsize=10) 
plt.xlabel('month')
   
plt.tight_layout
plt.savefig('figs/itcz_AquaControl_vs_LandControl_vs_LandOrbit.pdf')
   
print(np.nanmedian(itcz_aqct_tm-itcz_ldct_tm))   
print(itcz_aqct_tm-itcz_ldct_tm)   
   
   
plt.show()
