# load modules
import numpy as np
import matplotlib.pyplot as plt

# load own modules
import sys
sys.path.append('/home/aiko/Dropbox/Projects/current/Tracmip/analysis/python3/ramadda/mymodules/')
sys.path.append('/home/aiko/Dropbox/Python3/mymodules/')
import atmosphere as atm
import tracmipmodels as tracmipmodels

# load data
file       = np.load('ts_zonaltimemean.npz')
lat        = file['lat']
ts_aqct_tm = file['ts_aqct']
ts_aq4x_tm = file['ts_aq4x']
file       = np.load('pr_zonaltimemean.npz')
pr_aqct_tm = file['pr_aqct']
pr_aq4x_tm = file['pr_aq4x']
file       = np.load('pr_zonalmonthmean.npz')
pr_aqct_mm = file['pr_aqct']
pr_aq4x_mm = file['pr_aq4x']
file       = np.load('ua_zonaltimemean_plevels.npz')
lev        = file['lev']
ua_aqct    = file['ua_aqct']
ua_aq4x    = file['ua_aq4x']

nmod   = ts_aqct_tm[:, 0].size
month  = np.array([1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12])
nmonth = month.size
nlat   = lat.size

sinlat = np.sin(lat*np.pi/180)

# get model colors, names and numbers
modelcolors   = tracmipmodels.get_modelcolors(nmod)
modelnames    = tracmipmodels.get_modelnames()
modelnumbers  = tracmipmodels.get_modelnumbers()

# list of available models
modlist_aqct = tracmipmodels.get_availablemodels('aqct')
modlist_aq4x = tracmipmodels.get_availablemodels('aq4x')

# analysis
# calculate time-mean ITCZ position
itcz_aqct_tm = np.zeros(nmod)
itcz_aq4x_tm = np.zeros(nmod)
for i in modlist_aqct:
    itcz_aqct_tm[i] = atm.get_itczposition(pr_aqct_tm[i, :], lat, 30, 0.1)
for i in modlist_aq4x:
    itcz_aq4x_tm[i] = atm.get_itczposition(pr_aq4x_tm[i, :], lat, 30, 0.1)
    
# calculate monthly-mean ITCZ position
itcz_aqct_mm = np.zeros((nmod, 12))
itcz_aq4x_mm = np.zeros((nmod, 12))
for i in modlist_aq4x:
    for t in range(0, nmonth):
        itcz_aqct_mm[i, t] = atm.get_itczposition(pr_aqct_mm[i, t, :], lat, 30, 0.1)
        itcz_aq4x_mm[i, t] = atm.get_itczposition(pr_aq4x_mm[i, t, :], lat, 30, 0.1)

# caltech seasonal cycle shifted by 3 months
tmp = np.copy(itcz_aqct_mm[14,:])
itcz_aqct_mm[14,3:12] = tmp[0:9]
itcz_aqct_mm[14,0:3 ] = tmp[9:12]
tmp = np.copy(itcz_aq4x_mm[14,:])
itcz_aq4x_mm[14,3:12] = tmp[0:9]
itcz_aq4x_mm[14,0:3 ] = tmp[9:12]

# plotting
fig = plt.figure( figsize=(12, 8), dpi=80, facecolor='w', edgecolor='k' )

ax = plt.subplot(2, 2, 1)
ax.spines['right'].set_color('none')
ax.spines['top'].set_color('none')
ax.xaxis.set_ticks_position('bottom')
ax.yaxis.set_ticks_position('left')

for m in modlist_aq4x:
    plt.plot(sinlat, (ts_aq4x_tm-ts_aqct_tm)[m, :], color=modelcolors[m])
plt.plot(sinlat, np.nanmedian((ts_aq4x_tm-ts_aqct_tm)[modlist_aq4x], axis=0), 'k', linewidth=3)
plt.xlim(-0.98, 0.98), plt.ylim(0, 16)
ax.xaxis.set_ticks([-0.866, -0.5, 0, 0.5, 0.866])
ax.xaxis.set_ticklabels(['60S', '30S', 'Eq', '30N', '60N'], fontsize=10)
ax.yaxis.set_ticks([0, 4, 8, 12, 16])
ax.yaxis.set_ticklabels([0, 4, 8, 12, 16], fontsize=10) 
plt.title('Surface temperature', fontsize=14)
plt.ylabel('K', fontsize=12)
plt.text(0.02, 0.95, 'a)', fontsize=14, ha='left', va='center', transform=ax.transAxes)
   
ax = plt.subplot(2, 2, 2)
ax.spines['right'].set_color('none')
ax.spines['top'].set_color('none')
ax.xaxis.set_ticks_position('bottom')
ax.yaxis.set_ticks_position('left')

for m in modlist_aq4x:
    plt.plot(sinlat, (pr_aq4x_tm-pr_aqct_tm)[m, :], color=modelcolors[m])
plt.plot(sinlat, np.nanmedian((pr_aq4x_tm-pr_aqct_tm)[modlist_aq4x], axis=0), 'k', linewidth=3)   
plt.xlim(-0.98, 0.98), plt.ylim(-8, 8)
ax.xaxis.set_ticks([-0.866, -0.5, 0, 0.5, 0.866])
ax.xaxis.set_ticklabels(['60S', '30S', 'Eq', '30N', '60N'], fontsize=10)
ax.yaxis.set_ticks([-8, -4, 0, 4, 8])
ax.yaxis.set_ticklabels([-8, -4, 0, 4, 8], fontsize=10) 
plt.title('Precipitation', fontsize=14)
plt.ylabel('mm/day', fontsize=12)
plt.text(0.02, 0.95, 'b)', fontsize=14, ha='left', va='center', transform=ax.transAxes)

ax = plt.subplot(2, 2, 3)
ax.spines['right'].set_color('none')
ax.spines['top'].set_color('none')
ax.xaxis.set_ticks_position('bottom')
ax.yaxis.set_ticks_position('left')

plt.plot([-20, 20], [0, 0], 'k--')
for m in modlist_aq4x:
    plt.plot(month, (itcz_aq4x_mm-itcz_aqct_mm)[m, :], color=modelcolors[m])
plt.plot(month, np.nanmedian((itcz_aq4x_mm-itcz_aqct_mm)[modlist_aq4x], axis=0), 'k', linewidth=3)
plt.xlim(1, 12), plt.ylim(-2, 16)
ax.xaxis.set_ticks(month)
ax.xaxis.set_ticklabels(['Jan', '', '', 'Apr', '', '', 'Jul', '', '' ,'Oct', '', ''], fontsize=10)
ax.yaxis.set_ticks([-2, 0, 5, 10, 15])
ax.yaxis.set_ticklabels([-2, 0, 5, 10, 15], fontsize=10) 
plt.title('ITCZ position', fontsize=14)
plt.xlabel('month', fontsize=12)
plt.ylabel('deg lat', fontsize=12)
plt.text(0.02, 0.92, 'c)', fontsize=14, ha='left', va='center', transform=ax.transAxes)
   
ax = plt.subplot(2, 2, 4)
ax.spines['right'].set_color('none')
ax.spines['top'].set_color('none')
ax.xaxis.set_ticks_position('bottom')
ax.yaxis.set_ticks_position('left')

plt.plot([-10, 10], [0, 0], 'k--')
# lev=14 is 850 hPa
for m in modlist_aq4x:
    plt.plot(sinlat, (ua_aq4x-ua_aqct)[m, 14, :], color=modelcolors[m])
plt.plot(sinlat, np.nanmedian((ua_aq4x-ua_aqct)[modlist_aq4x, 14, :], axis=0), 'k', linewidth=3)

plt.xlim(-0.98, 0.98), plt.ylim(-8, 9)
ax.xaxis.set_ticks([-0.866, -0.5, 0, 0.5, 0.866])
ax.xaxis.set_ticklabels(['60S', '30S', 'Eq', '30N', '60N'], fontsize=10)
ax.yaxis.set_ticks([-8, -4, 0, 4, 8])
ax.yaxis.set_ticklabels([-8, -4, 0, 4, 8], fontsize=10)
plt.title('Zonal wind at 850 hPa', fontsize=14)
plt.ylabel('m/s', fontsize=12)
plt.text(0.02, 0.95, 'd)', fontsize=14, ha='left', va='center', transform=ax.transAxes, backgroundcolor='white')
       
plt.tight_layout
plt.savefig('figs/ts_pr_itcz_ua850_Aqua4xCO2_minus_AquaControl.pdf')


plt.figure()
plt.plot(lat, pr_aqct_tm[10, :])

# itcz
print(np.nanmedian((itcz_aq4x_tm-itcz_aqct_tm)[modlist_aq4x]))
print(np.nanmax((itcz_aq4x_tm-itcz_aqct_tm)[modlist_aq4x]))
print(np.nanmin((itcz_aq4x_tm-itcz_aqct_tm)[modlist_aq4x]))

# annual-mean jet position
# 850 hPa is lev = 14
jetlat_nh_aqct = np.zeros(nmod) + np.nan
jetlat_sh_aqct = np.zeros(nmod) + np.nan
jetlat_nh_aq4x = np.zeros(nmod) + np.nan
jetlat_sh_aq4x = np.zeros(nmod) + np.nan
for m in modlist_aq4x:
    jetlat_nh_aqct[m], jetlat_sh_aqct[m] = atm.get_eddyjetlat(ua_aqct[m, 14, :], lat)
    jetlat_nh_aq4x[m], jetlat_sh_aq4x[m] = atm.get_eddyjetlat(ua_aq4x[m, 14, :], lat)
print(np.nanmax((jetlat_nh_aq4x-jetlat_nh_aqct)[modlist_aq4x]), 
      np.nanmin((jetlat_nh_aq4x-jetlat_nh_aqct)[modlist_aq4x]), 
      np.nanmedian((jetlat_nh_aq4x-jetlat_nh_aqct)[modlist_aq4x]))
print(np.nanmax((jetlat_sh_aq4x-jetlat_sh_aqct)[modlist_aq4x]), 
      np.nanmin((jetlat_sh_aq4x-jetlat_sh_aqct)[modlist_aq4x]), 
      np.nanmedian((jetlat_sh_aq4x-jetlat_sh_aqct)[modlist_aq4x]))

# all models but caltech
# remove caltech index
index=np.where(np.array(modlist_aq4x)==14); modlist_aq4x_nocaltech=np.delete(modlist_aq4x, index)
print(index, modlist_aq4x_nocaltech)
print('All models but caltech')
print(np.nanmax((jetlat_nh_aq4x-jetlat_nh_aqct)[modlist_aq4x_nocaltech]), 
      np.nanmin((jetlat_nh_aq4x-jetlat_nh_aqct)[modlist_aq4x_nocaltech]), 
      np.nanmedian((jetlat_nh_aq4x-jetlat_nh_aqct)[modlist_aq4x_nocaltech]))
print(np.nanmax((jetlat_sh_aq4x-jetlat_sh_aqct)[modlist_aq4x_nocaltech]), 
      np.nanmin((jetlat_sh_aq4x-jetlat_sh_aqct)[modlist_aq4x_nocaltech]), 
      np.nanmedian((jetlat_sh_aq4x-jetlat_sh_aqct)[modlist_aq4x_nocaltech]))

# caltech only
print('Caltech only')
print((jetlat_nh_aq4x-jetlat_nh_aqct)[14],(jetlat_sh_aq4x-jetlat_sh_aqct)[14])



      
plt.show()
