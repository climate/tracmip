# load modules
import numpy as np
import matplotlib.pyplot as plt

# load own modules
import sys
sys.path.append('/home/aiko/Dropbox/Projects/current/Tracmip/analysis/python3/ramadda/mymodules/')
sys.path.append('/home/aiko/Dropbox/Python3/mymodules/')
import tracmipmodels as tracmipmodels

models=['CNRM', 'ECHAM61', 'ECHAM63', 'IPSL', 'MetCTL', 'MetENT', \
        'MIROC5', 'AM2', 'CAM3', 'CAM4', 'MPAS', 'GISS']

# load global mean pr and ts data
file    = np.load('ts_latlontimemean.npz')
lat     = file['lat']
lon     = file['lon']
ts_aqct = file['ts_aqct']
ts_ldct = file['ts_ldct']

nmod = ts_aqct[:, 0, 0].size
nlat = lat.size
nlon = lon.size

# get model colors, names and numbers
modelcolors   = tracmipmodels.get_modelcolors(nmod)
modelnames    = tracmipmodels.get_modelnames()
modelnumbers  = tracmipmodels.get_modelnumbers()

plt.figure()
for m in range(0, nmod):
    plt.subplot(3, 4, m+1)
    c = plt.contourf(lon, lat, (ts_ldct-ts_aqct)[m, :, :])
    plt.colorbar(c)    
    plt.title(modelnames[m])

print(ts_aqct[3, 10, :])

plt.show()
