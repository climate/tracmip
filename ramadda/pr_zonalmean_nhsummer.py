# load modules
import numpy as np
import matplotlib.pyplot as plt

# load own modules
import sys
sys.path.append('/home/aiko/Dropbox/Projects/current/Tracmip/analysis/python3/ramadda/mymodules/')
sys.path.append('/home/aiko/Dropbox/Python3/mymodules/')

models=['CNRM', 'ECHAM61', 'ECHAM63', 'IPSL', 'MetCTL', 'MetENT', \
        'MIROC5', 'AM2', 'CAM3', 'CAM4', 'MPAS', 'GISS']

# load global mean pr and ts data
file    = np.load('pr_zonalmonthmean.npz')
lat     = file['lat']
pr_aqct = file['pr_aqct']
pr_ldct = file['pr_ldct']
pr_ldor = file['pr_ldor']

nmod  = pr_aqct[:, 0, 0].size
nmth  = pr_aqct[0, :, 0].size
month = np.arange(1, 13)

plt.contourf(lat, month, pr_aqct[0, :, :])

plt.figure()

plt.subplot(2, 3, 1)
for m in range(0, nmod):
    plt.plot(lat, np.nanmean(pr_aqct[m, 8:11, :], axis=0))

plt.subplot(2, 3, 2)
for m in range(0, nmod):
    plt.plot(lat, np.nanmean(pr_ldct[m, 8:11, :], axis=0))   
    
plt.subplot(2, 3, 5)
for m in range(0, nmod):
    plt.plot(lat, np.nanmean(pr_ldct[m, 8:11, :] - pr_aqct[m, 8:11, :], axis=0))     

plt.subplot(2, 3, 3)
for m in range(0, nmod):
    plt.plot(lat, np.nanmean(pr_ldor[m, 8:11, :], axis=0))  
    
plt.subplot(2, 3, 6)
for m in range(0, nmod):
    plt.plot(lat, np.nanmean(pr_ldor[m, 8:11, :] - pr_ldct[m, 8:11, :], axis=0))     
    
plt.show()
