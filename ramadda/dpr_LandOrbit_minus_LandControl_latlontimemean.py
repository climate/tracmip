# load modules
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import cm
from matplotlib.patches import Rectangle

# load own modules
import sys
sys.path.append('/home/aiko/Dropbox/Projects/current/Tracmip/analysis/python3/ramadda/mymodules/')
sys.path.append('/home/aiko/Dropbox/Python3/mymodules/')
import tracmipmodels as tracmipmodels
import atmosphere as atm

def make_niceplot(ax, modelname):
    ax.xaxis.set_ticks([-120, -60, 0, 60, 120])
    ax.xaxis.set_ticklabels([''], fontsize=11)
    ax.yaxis.set_ticks([-0.5, 0, 0.5])
    ax.yaxis.set_ticklabels([''], fontsize=11) 
    plt.text(0.03, 0.93, modelname, fontsize=15, ha='left', va='center', \
             transform=ax.transAxes, backgroundcolor='white')
    plt.xlim(-175, 175), plt.ylim(-0.6, 0.6)         

# load pr data
file    = np.load('pr_latlontimemean.npz')
lat     = file['lat']
lon     = file['lon']
pr_ldct = file['pr_ldct']
pr_ldor = file['pr_ldor']

nmod = pr_ldct[:, 0, 0].size
nlat = lat.size
nlon = lon.size

sinlat = np.sin(lat*np.pi/180)

# get model colors, names and numbers
modelcolors   = tracmipmodels.get_modelcolors(nmod)
modelnames    = tracmipmodels.get_modelnames()
modelnumbers  = tracmipmodels.get_modelnumbers()

# itcz position at each longitude, for aqct we use zonal-mean precip for ITCZ
itcz_ldct = np.zeros((nmod, nlon)) + np.nan
itcz_ldor = np.zeros((nmod, nlon)) + np.nan
for m in range(0, nmod):
    itcz_ldct[m, :] = atm.get_itczposition(np.nanmean(pr_ldct[m, :, :], axis=1), lat, 30.0, 1)#0.1)
    for i in range(0, nlon):
        itcz_ldor[m, i] = atm.get_itczposition(pr_ldor[m, :, i], lat, 30.0, 1) #0.1)

# plotting
plt.figure(figsize=(18, 20), dpi=80, facecolor='w', edgecolor='k')
clev = 0.5*np.array([-2.2, -1.8, -1.4, -1.0, -0.6, -0.2, 0.2, 0.6, 1.0, 1.4, 1.8, 2.2])

ax = plt.subplot(5, 3, 1)
c = plt.contourf(lon, sinlat, np.nanmedian(pr_ldor-pr_ldct, axis=0), clev, extend='both', cmap=cm.BrBG)
ax.add_patch(Rectangle((0, -0.5), 45, 1, alpha=1, facecolor='none', edgecolor='gray', linewidth=2))
plt.plot([-200, 200], [0, 0], 'k--')
plt.plot(lon, np.sin(np.nanmedian(itcz_ldct, axis=0)*np.pi/180), 'royalblue', linewidth=2)
plt.plot(lon, np.sin(np.nanmedian(itcz_ldor, axis=0)*np.pi/180), 'firebrick', linewidth=2)
make_niceplot(ax, 'Model median')
ax.yaxis.set_ticklabels(['30S', 'Eq', '30N'], fontsize=11) 
ax = plt.subplot(5, 3, 2)
ax.axis('off')
cbar = plt.colorbar(c)#, ticks=[-6, -4, -2, 0, 2, 4, 6], orientation='horizontal', aspect=30)
cbar.ax.tick_params(labelsize=10)
plt.text(1, -0.17, 'mm/day', fontsize=11, ha='right')  

for m in range(1, nmod-1):
    if modelnames[m] == 'AM2'    : msubplot = 4
    if modelnames[m] == 'CAM3'   : msubplot = 5 
    if modelnames[m] == 'CAM4'   : msubplot = 6     
    if modelnames[m] == 'ECHAM61': msubplot = 7
    if modelnames[m] == 'ECHAM63': msubplot = 8
    if modelnames[m] == 'IPSL'   : msubplot = 9
    if modelnames[m] == 'MetCTL' : msubplot = 10
    if modelnames[m] == 'MetENT' : msubplot = 11
    if modelnames[m] == 'MIROC5' : msubplot = 12  
    if modelnames[m] == 'MPAS'   : msubplot = 13   
    if (modelnames[m] != 'AM2') and (modelnames[m] != 'CAM3'):  
        ax = plt.subplot(5, 3, msubplot)
        c = plt.contourf(lon, sinlat, (pr_ldor-pr_ldct)[m, :, :], clev, extend='both', cmap=cm.BrBG)
        ax.add_patch(Rectangle((0, -0.5), 45, 1, alpha=1, facecolor='none', edgecolor='gray', linewidth=2))
        plt.plot([-200, 200], [0, 0], 'k--')
        plt.plot(lon, np.sin(itcz_ldct[m, :]*np.pi/180), 'royalblue', linewidth=2)
        plt.plot(lon, np.sin(itcz_ldor[m, :]*np.pi/180), 'firebrick', linewidth=2)
        make_niceplot(ax, modelnames[m])
        if (msubplot == 11) or (msubplot == 12) or (msubplot ==13):
            ax.xaxis.set_ticklabels(['120W', '60W', '0', '60E', '120E'], fontsize=11) 
        if msubplot in [1, 4, 7, 10, 13]:
            ax.yaxis.set_ticklabels(['30S', 'Eq', '30N'], fontsize=11) 
          

plt.subplots_adjust(wspace=0.04, hspace=0.05)  
plt.tight_layout

plt.savefig('figs/dpr_latlontimemean_LandOrbit_minus_LandControl_allmodels.pdf')

plt.show()
