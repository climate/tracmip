# load modules
import numpy as np

# load own modules
import sys
sys.path.append('/home/aiko/Dropbox/Projects/current/Tracmip/analysis/python3/ramadda/mymodules/')

import loaddata as loaddata

# load data from ramadda server
rsuscs_aqct, lat, lon = loaddata.get_data_latlontm('AQUA'   , 'rsuscs')
rsuscs_aq4x, lat, lon = loaddata.get_data_latlontm('AQUACO2', 'rsuscs')
rsuscs_ldct, lat, lon = loaddata.get_data_latlontm('LAND'   , 'rsuscs')
rsuscs_ld4x, lat, lon = loaddata.get_data_latlontm('LANDCO2', 'rsuscs')
rsuscs_ldor, lat, lon = loaddata.get_data_latlontm('LORBIT' , 'rsuscs')

# we also need rsdscs and rsnscs to reconstruct rsuscs for cam3, cam4 and mpas
rsdscs_aqct, lat, lon = loaddata.get_data_latlontm('AQUA'   , 'rsdscs')
rsdscs_aq4x, lat, lon = loaddata.get_data_latlontm('AQUACO2', 'rsdscs')
rsdscs_ldct, lat, lon = loaddata.get_data_latlontm('LAND'   , 'rsdscs')
rsdscs_ld4x, lat, lon = loaddata.get_data_latlontm('LANDCO2', 'rsdscs')
rsdscs_ldor, lat, lon = loaddata.get_data_latlontm('LORBIT' , 'rsdscs')

rsnscs_aqct, lat, lon = loaddata.get_data_latlontm('AQUA'   , 'rsnscs')
rsnscs_aq4x, lat, lon = loaddata.get_data_latlontm('AQUACO2', 'rsnscs')
rsnscs_ldct, lat, lon = loaddata.get_data_latlontm('LAND'   , 'rsnscs')
rsnscs_ld4x, lat, lon = loaddata.get_data_latlontm('LANDCO2', 'rsnscs')
rsnscs_ldor, lat, lon = loaddata.get_data_latlontm('LORBIT' , 'rsnscs')

# for cam3, cam4 and mpas, rsuscs is negative because it is upward
rsuscs_aqct[8:11, :, :] = (np.abs(rsdscs_aqct[8:11, :, :]) - np.abs(rsnscs_aqct[8:11, :, :]))
rsuscs_aq4x[8:11, :, :] = (np.abs(rsdscs_aq4x[8:11, :, :]) - np.abs(rsnscs_aq4x[8:11, :, :]))
rsuscs_ldct[8:11, :, :] = (np.abs(rsdscs_ldct[8:11, :, :]) - np.abs(rsnscs_ldct[8:11, :, :]))
rsuscs_ld4x[8:11, :, :] = (np.abs(rsdscs_ld4x[8:11, :, :]) - np.abs(rsnscs_ld4x[8:11, :, :]))
rsuscs_ldor[8:11, :, :] = (np.abs(rsdscs_ldor[8:11, :, :]) - np.abs(rsnscs_ldor[8:11, :, :])) 

# saving data for later use
print('Saving lat-lon time mean rsuscs')
np.savez('rsuscs_latlontimemean.npz', \
         **{'rsuscs_aqct':rsuscs_aqct, 'rsuscs_aq4x':rsuscs_aq4x, 'rsuscs_ldct':rsuscs_ldct, \
            'rsuscs_ld4x':rsuscs_ld4x, 'rsuscs_ldor':rsuscs_ldor, 'lat':lat, 'lon': lon})