# load modules
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import cm

# load own modules
import sys
sys.path.append('/home/aiko/Dropbox/Projects/current/Tracmip/analysis/python3/ramadda/mymodules/')
sys.path.append('/home/aiko/Dropbox/Python3/mymodules/')
import atmosphere as atm
import tracmipmodels as tracmipmodels

# load pr and ts data
file  = np.load('ts_zonaltimemean.npz')
lat   = file['lat']
ts_tm = file['ts_aqct']
file  = np.load('pr_zonaltimemean.npz')
pr_tm = file['pr_aqct']
file  = np.load('pr_zonalmonthmean.npz')
pr_mm = file['pr_aqct']

nmod   = ts_tm[:, 0].size
month  = np.array([1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12])
nmonth = month.size
nlat   = lat.size

sinlat = np.sin(lat*np.pi/180)

# get model colors, names and numbers
modelcolors   = tracmipmodels.get_modelcolors(nmod)
modelnames    = tracmipmodels.get_modelnames()
modelnumbers  = tracmipmodels.get_modelnumbers()

# analysis
# calculate time-mean ITCZ position
itcz_tm = np.zeros(nmod)
for i in range(0, nmod):
    itcz_tm[i] = atm.get_itczposition(pr_tm[i, :], lat, 30, 0.1)

# calculate monthly-mean ITCZ position
itcz_mm = np.zeros((nmod, 12))
for i in range(0, nmod):
    for t in range(0, nmonth):
        itcz_mm[i, t] = atm.get_itczposition(pr_mm[i, t, :], lat, 30, 0.1)

# plotting
fig = plt.figure( figsize=(18, 4), dpi=80, facecolor='w', edgecolor='k' )

ax = plt.subplot(1, 3, 1)
ax.spines['right'].set_color('none')
ax.spines['top'].set_color('none')
ax.xaxis.set_ticks_position('bottom')
ax.yaxis.set_ticks_position('left')

for m in range(0, nmod):
    plt.plot(sinlat, ts_tm[m, :], color=modelcolors[m])
plt.plot(sinlat, np.nanmedian(ts_tm, axis=0), 'k', linewidth=3)
plt.xlim(-0.98, 0.98), plt.ylim(270, 312)
ax.xaxis.set_ticks([-0.866, -0.5, 0, 0.5, 0.866])
ax.xaxis.set_ticklabels(['60S', '30S', 'Eq', '30N', '60N'], fontsize=10)
ax.yaxis.set_ticks([270, 280, 290, 300, 310])
ax.yaxis.set_ticklabels([270, 280, 290, 300, 310], fontsize=10) 
plt.title('Surface temperature', fontsize=14)
plt.ylabel('K', fontsize=12)
plt.text(0.02, 0.98, 'a)', fontsize=14, ha='left', va='center', transform=ax.transAxes)
   
ax = plt.subplot(1, 3, 2)
ax.spines['right'].set_color('none')
ax.spines['top'].set_color('none')
ax.xaxis.set_ticks_position('bottom')
ax.yaxis.set_ticks_position('left')

for m in range(0, nmod):
    plt.plot(sinlat, pr_tm[m, :], color=modelcolors[m])
plt.plot(sinlat, np.nanmedian(pr_tm, axis=0), 'k', linewidth=3)   
plt.xlim(-0.98, 0.98), plt.ylim(0, 18)
ax.xaxis.set_ticks([-0.866, -0.5, 0, 0.5, 0.866])
ax.xaxis.set_ticklabels(['60S', '30S', 'Eq', '30N', '60N'], fontsize=10)
ax.yaxis.set_ticks([0, 6, 12, 18])
ax.yaxis.set_ticklabels([0, 6, 12, 18], fontsize=10) 
plt.title('Precipitation', fontsize=14)
plt.ylabel('mm/day', fontsize=12)
plt.text(0.02, 0.98, 'b)', fontsize=14, ha='left', va='center', transform=ax.transAxes)

ax = plt.subplot(1, 3, 3)
ax.spines['right'].set_color('none')
ax.spines['top'].set_color('none')
ax.xaxis.set_ticks_position('bottom')
ax.yaxis.set_ticks_position('left')

plt.plot([-20, 20], [0, 0], 'k--')
for m in range(0, nmod):
    plt.plot(month, itcz_mm[m, :], color=modelcolors[m])
plt.plot(month, np.nanmedian(itcz_mm, axis=0), 'k', linewidth=3)
plt.xlim(1, 12), plt.ylim(-15, 15)
ax.xaxis.set_ticks(month)
ax.xaxis.set_ticklabels(['Jan', '', '', 'Apr', '', '', 'Jul', '', '' ,'Oct', '', ''], fontsize=10)
ax.yaxis.set_ticks([-15, -10, -5, 0, 5, 10, 15])
ax.yaxis.set_ticklabels(['15S', '10S', '5S', 'Eq', '5N', '10N', '15N'], fontsize=10) 
plt.title('ITCZ position', fontsize=14)
plt.xlabel('month', fontsize=12)
plt.ylabel('deg lat', fontsize=12)
plt.text(0.02, 0.98, 'c)', fontsize=14, ha='left', va='center', transform=ax.transAxes)
  
plt.tight_layout
plt.savefig('figs/ts_pr_itcz_AquaControl.pdf')

# itcz
print(np.nanmedian(itcz_tm))
print(np.nanmax(itcz_tm))
print(np.nanmin(itcz_tm))

# hemispheric temperature difference
ts_hdiff = np.zeros(nmod) + np.nan
for m in range(0, nmod):
    ts_hdiff[m] = atm.get_hemispheric_diff(ts_tm[m, :], lat)
print(ts_hdiff)
print(np.nanmedian(ts_hdiff))
print(np.nanmax(ts_hdiff))
print(np.nanmin(ts_hdiff))

# nh-sh at each latitude
ts_asym = np.zeros((nmod, nlat)) + np.nan
for m in range(0, nmod):
    _, ts_asym[m, :] = atm.get_sym_and_asym_component(ts_tm[m, :], lat)
plt.figure()
for m in range(0, nmod):
    plt.plot(sinlat, ts_asym[m, :], color=modelcolors[m])
plt.plot(sinlat, np.nanmedian(ts_asym, axis=0), 'k', linewidth=3)
plt.plot([-1, 1], [0, 0], 'k--')

# seasonal precip evolution
plt.figure()
for m in range(0, nmod):
    plt.subplot(4, 3, m+1)
    plt.contourf(lat, month, pr_mm[m, :, :])
    plt.title(modelnames[m])
    plt.xlim(-30, 30)

plt.show()
