# load modules
import numpy as np
import matplotlib.pyplot as plt

# load own modules
import sys
sys.path.append('/home/aiko/Dropbox/Projects/current/Tracmip/analysis/python3/ramadda/mymodules/')
sys.path.append('/home/aiko/Dropbox/Python3/mymodules/')
import tracmipmodels as tracmipmodels

# load global mean pr and ts data
file      = np.load('rsdt_zonalmonthmean.npz')
lat       = file['lat']
rsdt_aqct = file['rsdt_aqct']
rsdt_ldct = file['rsdt_ldct']
rsdt_ldor = file['rsdt_ldor']

nmod  = rsdt_aqct[:, 0, 0].size
nmth  = rsdt_aqct[0, :, 0].size
month = np.arange(1, 13)

# get model colors, names and numbers
modelcolors   = tracmipmodels.get_modelcolors(nmod)
modelnames    = tracmipmodels.get_modelnames()
modelnumbers  = tracmipmodels.get_modelnumbers()

plt.subplot(1, 2, 1)
plt.contourf(lat, month, rsdt_aqct[2, :, :])
plt.title(modelnames[2])

plt.subplot(1, 2, 2)
plt.contourf(lat, month, rsdt_aqct[14, :, :])
plt.title(modelnames[14])
plt.savefig('figs/rsdt_aquacontrol_echam63_caltech.pdf')

plt.figure()
plt.subplot(1, 2, 1)
plt.contourf(lat, month, rsdt_aqct[2, :, :])
plt.title(modelnames[2])

plt.subplot(1, 2, 2)
plt.contourf(lat, month, rsdt_aqct[13, :, :])
plt.title(modelnames[13])

plt.figure()
plt.subplot(1, 2, 1)
plt.contourf(lat, month, rsdt_ldor[2, :, :]-rsdt_ldct[2, :, :])
plt.title(modelnames[2])

plt.subplot(1, 2, 2)
plt.contourf(lat, month, rsdt_ldor[8, :, :]-rsdt_ldct[8, :, :])
plt.title(modelnames[8])
plt.savefig('figs/rsdt_landorbit-landcontrol_echam63_cam3.pdf')

plt.show()
stophere

plt.figure()
imonth=[5]
plt.subplot(2, 3, 1)
for m in range(1, nmod):
    plt.plot(lat, np.nanmean(rsdt_aqct[m, imonth, :], axis=0), color=modelcolors[m])

plt.subplot(2, 3, 2)
for m in range(1, nmod):
    plt.plot(lat, np.nanmean(rsdt_ldct[m, imonth, :], axis=0), color=modelcolors[m])   
    
plt.subplot(2, 3, 5)
for m in range(1, nmod):
    plt.plot(lat, np.nanmean(rsdt_ldct[m, imonth, :] - rsdt_aqct[m, imonth, :], axis=0), color=modelcolors[m])     

plt.subplot(2, 3, 3)
for m in range(1, nmod):
    plt.plot(lat, np.nanmean(rsdt_ldor[m, imonth, :], axis=0), color=modelcolors[m])  
    
plt.subplot(2, 3, 6)
for m in range(1, nmod):
    plt.plot(lat, np.nanmean(rsdt_ldor[m, imonth, :] - rsdt_ldct[m, imonth, :], axis=0), color=modelcolors[m])     
    
plt.show()
