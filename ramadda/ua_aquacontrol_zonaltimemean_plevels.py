# load modules
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import cm
# load own modules
import sys
sys.path.append('/home/aiko/Dropbox/Python3/mymodules/')
import atmosphere as atm

def make_nice_plot(ax, title):
    plt.xlim(-85, 85)    
    plt.ylim(1000, 10)
    ax.xaxis.set_ticks([-60, -30, 0, 30, 60])
    ax.xaxis.set_ticklabels([''],fontsize=10)                          
    ax.yaxis.set_ticks([10, 200, 600, 1000])
    ax.yaxis.set_ticklabels([''],fontsize=10)
    plt.title(title, fontsize=14)

models=['CNRM', 'ECHAM61', 'ECHAM63', 'IPSL', 'MetCTL', 'MetENT', \
        'MIROC5', 'AM2', 'CAM3', 'CAM4', 'MPAS', 'GISS']

# load zonal and meridional wind data
file    = np.load('ua_zonaltimemean_plevels.npz')
lev     = file['lev']
lat     = file['lat']
ua_aqct = file['ua_aqct']
ua_aq4x = file['ua_aq4x']
ua_ldct = file['ua_ldct']
ua_ld4x = file['ua_ld4x']
ua_ldor = file['ua_ldor']

file    = np.load('va_zonaltimemean_plevels.npz')
va_aqct = file['va_aqct']
va_aq4x = file['va_aq4x']
va_ldct = file['va_ldct']
va_ld4x = file['va_ld4x']
va_ldor = file['va_ldor']

nmod = ua_aqct[:, 0, 0].size

# mass stream function
msf_aqct = np.zeros(va_aqct.shape) + np.nan
msf_aq4x = np.zeros(va_aqct.shape) + np.nan
msf_ldct = np.zeros(va_aqct.shape) + np.nan
msf_ld4x = np.zeros(va_aqct.shape) + np.nan
msf_ldor = np.zeros(va_aqct.shape) + np.nan

for m in range(0, nmod):
    msf_aqct[m, :, :] = atm.get_massstreamfunction(va_aqct[m, :, :], lev, lat)
    msf_aq4x[m, :, :] = atm.get_massstreamfunction(va_aq4x[m, :, :], lev, lat)
    msf_ldct[m, :, :] = atm.get_massstreamfunction(va_ldct[m, :, :], lev, lat)
    msf_ld4x[m, :, :] = atm.get_massstreamfunction(va_ld4x[m, :, :], lev, lat)
    msf_ldor[m, :, :] = atm.get_massstreamfunction(va_ldor[m, :, :], lev, lat)
    
# ploting
for m in range(0, nmod):
    plt.subplot(4, 3, m+1)    
    plt.contourf(lat, lev, ua_aqct[m, :, :], np.linspace(-100, 100, 100))
    plt.title(models[m])

plt.figure( figsize=(10, 6), dpi=80, facecolor='w', edgecolor='k')

#cu   = np.array([-55, -45, -35, -25, -15, -5, 5, 15, 25, 35, 45, 55])
#cu   = np.array([-70, -65, -60, -55, -50, -45, -40, -35, -30, -25, -20, -15, -10, -5, 0,\
#                   5,  10,  15,  20,  25,  30,  35,  40,  45,  50,  55, 60, 65, 70])
cu   = np.array([-70, -65, -60, -55, -50, -45, -40, -35, -30, -25, -20, -15, -10, -5,\
                   5,  10,  15,  20,  25,  30,  35,  40,  45,  50,  55, 60, 65, 70])
cmsf = np.array([-130, -110, -90, -70, -50, -30, -10, 10, 30, 50, 70, 90, 110, 130])

ax = plt.subplot(3, 2, 1)
plt.contourf(lat, lev/1e2, np.nanmedian(ua_aqct, axis=0), cu, cmap=cm.RdBu_r)
plt.contour(lat, lev/1e2, np.nanmedian(msf_aqct, axis=0), cmsf, colors='k')
make_nice_plot(ax, 'AquaControl'), plt.ylabel('pressure (hPa)', fontsize=10)
ax.yaxis.set_ticklabels([10, 200, 600, 1000], fontsize=10)

ax = plt.subplot(3, 2, 3)
c=plt.contourf(lat, lev/1e2, np.nanmedian(ua_aq4x, axis=0), cu, cmap=cm.RdBu_r)
plt.contour(lat, lev/1e2, np.nanmedian(msf_aq4x, axis=0), cmsf, colors='k')
make_nice_plot(ax, 'Aqua4xCO2'), plt.ylabel('pressure (hPa)', fontsize=10)
ax.yaxis.set_ticklabels([10, 200, 600, 1000], fontsize=10)
ax.xaxis.set_ticklabels([r'60$^\mathrm{o}$S', r'30$^\mathrm{o}$S', \
                         'Eq', r'30$^\mathrm{o}$',r'60$^\mathrm{o}$'], fontsize=10)

ax = plt.subplot(3, 2, 5)
ax.axis('off')
cbar = plt.colorbar(c, ticks=[-70, -55, -40, -25, -10, 10, 25, 40, 55, 70], 
             orientation='horizontal', fraction=0.09)
cbar.ax.tick_params(labelsize=10)

ax = plt.subplot(3, 2, 2)
plt.contourf(lat, lev/1e2, np.nanmedian(ua_ldct, axis=0), cu, cmap=cm.RdBu_r)
plt.contour(lat, lev/1e2, np.nanmedian(msf_ldct, axis=0), cmsf, colors='k')
make_nice_plot(ax, 'LandControl')

ax = plt.subplot(3, 2, 4)
plt.contourf(lat, lev/1e2, np.nanmedian(ua_ld4x, axis=0), cu, cmap=cm.RdBu_r)
plt.contour(lat, lev/1e2, np.nanmedian(msf_ld4x, axis=0), cmsf, colors='k')
make_nice_plot(ax, 'Land4xCO2')

ax = plt.subplot(3, 2, 6)
plt.contourf(lat, lev/1e2, np.nanmedian(ua_ldor, axis=0), cu, cmap=cm.RdBu_r)
plt.contour(lat, lev/1e2, np.nanmedian(msf_ldor, axis=0), cmsf, colors='k')
make_nice_plot(ax, 'LandOrbit'), plt.ylabel('pressure (hPa)', fontsize=10)
ax.yaxis.set_ticklabels([10, 200, 600, 1000], fontsize=10)
ax.xaxis.set_ticklabels([r'60$^\mathrm{o}$S', r'30$^\mathrm{o}$S', \
                         'Eq', r'30$^\mathrm{o}$',r'60$^\mathrm{o}$'], fontsize=10)

plt.tight_layout 

plt.savefig('figs/ua_msf_modelmedian_allmodels_zonaltimemean.pdf')                      

plt.show()
