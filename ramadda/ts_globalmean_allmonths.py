# load modules
import numpy as np
import matplotlib.pyplot as plt
import scipy.stats as spstats

# load own modules
import sys
sys.path.append('/home/aiko/Dropbox/Projects/current/Tracmip/analysis/python3/ramadda/mymodules/')
sys.path.append('/home/aiko/Dropbox/Python3/mymodules/')
import tracmipmodels as tracmipmodels

# loading data
print('Load global mean ts for all months')
file    = np.load('ts_globalmean_allmonths.npz')
ts_aqct = file['ts_aqct']
ts_aq4x = file['ts_aq4x']
ts_ldct = file['ts_ldct']
ts_ld4x = file['ts_ld4x'] 
ts_ldor = file['ts_ldor'] 

nmod = ts_aqct[:, 0].size
print(nmod)

# get model colors, names and numbers
modelcolors   = tracmipmodels.get_modelcolors(nmod)
modelnames    = tracmipmodels.get_modelnames()
modelnumbers  = tracmipmodels.get_modelnumbers()

# list of available models
modlist_aqct = tracmipmodels.get_availablemodels('aqct')
modlist_aq4x = tracmipmodels.get_availablemodels('aq4x')
modlist_ldct = tracmipmodels.get_availablemodels('ldct')
modlist_ld4x = tracmipmodels.get_availablemodels('ld4x')
modlist_ldor = tracmipmodels.get_availablemodels('ldor')

# plotting
plt.figure( figsize=(12, 12), dpi=80, facecolor='w', edgecolor='k' )

ax = plt.subplot(3, 2, 1)
ax.spines['right'].set_color('none')
ax.spines['top'].set_color('none')
ax.xaxis.set_ticks_position('bottom')
ax.yaxis.set_ticks_position('left')
for i in modlist_aqct:
    plt.plot(ts_aqct[i, :], color=modelcolors[i])
plt.title('AquaControl')    

ax = plt.subplot(3, 2, 3)
ax.spines['right'].set_color('none')
ax.spines['top'].set_color('none')
ax.xaxis.set_ticks_position('bottom')
ax.yaxis.set_ticks_position('left')
for i in modlist_aq4x:
    plt.plot(ts_aq4x[i, :], color=modelcolors[i])
plt.title('Aqua4xCO2')  

ax = plt.subplot(3, 2, 2)
ax.spines['right'].set_color('none')
ax.spines['top'].set_color('none')
ax.xaxis.set_ticks_position('bottom')
ax.yaxis.set_ticks_position('left')
for i in modlist_ldct:
    plt.plot(ts_ldct[i, :], color=modelcolors[i])
plt.title('LandControl')    

ax = plt.subplot(3, 2, 4)
ax.spines['right'].set_color('none')
ax.spines['top'].set_color('none')
ax.xaxis.set_ticks_position('bottom')
ax.yaxis.set_ticks_position('left')
for i in modlist_ld4x:
    plt.plot(ts_ld4x[i, :], color=modelcolors[i])
plt.title('Land4xCO2') 

ax = plt.subplot(3, 2, 6)
ax.spines['right'].set_color('none')
ax.spines['top'].set_color('none')
ax.xaxis.set_ticks_position('bottom')
ax.yaxis.set_ticks_position('left')
for i in modlist_ldor:
    plt.plot(ts_ldor[i, :], color=modelcolors[i])
plt.title('LandOrbit') 

plt.tight_layout
plt.savefig('figs/ts_globalmean_allmonths.pdf')
   
plt.show()

