# load modules
import numpy as np

# load own modules
import sys
sys.path.append('/home/aiko/Dropbox/Projects/current/Tracmip/analysis/python3/ramadda/mymodules/')

import loaddata as loaddata

# load data from ramadda server
ts_aqct, lat = loaddata.get_data_zmmm('AQUA'   , 'ts')
ts_aq4x, lat = loaddata.get_data_zmmm('AQUACO2', 'ts')
ts_ldct, lat = loaddata.get_data_zmmm('LAND'   , 'ts')
ts_ld4x, lat = loaddata.get_data_zmmm('LANDCO2', 'ts')
ts_ldor, lat = loaddata.get_data_zmmm('LORBIT' , 'ts')

# saving data for later use
print('Saving zonal month mean ts')
np.savez('ts_zonalmonthmean.npz', \
         **{'ts_aqct':ts_aqct, 'ts_aq4x':ts_aq4x, 'ts_ldct':ts_ldct, \
            'ts_ld4x':ts_ld4x, 'ts_ldor':ts_ldor, 'lat':lat})