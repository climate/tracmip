\documentclass[a4paper,12pt]{scrartcl}
\usepackage{color}
\usepackage[utf8]{inputenc}
\usepackage{fullpage}
\usepackage{natbib}
\usepackage{graphicx}
%\usepackage{hyperref}
\usepackage{url}
\usepackage{pdfpages}
\usepackage{gensymb}

% adapt section titles
\usepackage{titlesec}

\usepackage{setspace}
\linespread{1.08}

% make nice captions for figures
\usepackage[margin=12pt,font=small,labelfont=bf,labelsep=endash,format=plain]{
caption}

\pagenumbering{arabic}

% adapt page setup (margins etc.)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%\oddsidemargin   -0.2in
%\evensidemargin  -0.2in
\textwidth        6.5in
%\headheight      0.0in
%\topmargin      -1.0in
\textheight       8.8in
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%\linespread{1.09}



% Title Page
\title{\normalsize\vspace*{-1.5cm} ITCZ/Monsoon Model Intercomparison Project
\\\vspace*{0.2cm} {Aiko Voigt, Michela Biasutti and Jack Scheff, \today}
\author{}
\date{}}


\begin{document}

\sloppy


\maketitle
%\thispagestyle{empty}
%\pagestyle{empty}
\vspace*{-2.4cm}

\hrule
\vspace*{0.7cm}


\titleformat{\section}{\selectfont\bfseries}{\thesection}{0.2em}{ }
\titleformat{\subsection}{\selectfont\bfseries}{\thesubsection}{0.2em}{ }

\textbf{\textit{4 August 2015: Clarified whether fields should be saved as means over the
output period or as snapshots.}}\\

\textbf{\textit{14 July 2015: The land setup is revised and has changed with respect to earlier versions. We also
now include plots for the ECHAM6.1 AquaControl and LandControl simulations for reference.}}\\

\textit{This document describes the simulation setup and the requested simulations
for the model-intercomparison project organized within the 2015 WCRP Grand Challenge workshop
on ITCZ and monsoons. Five simulations in idealized aquaplanet and idealized continent
setup are performed to study the dynamics and model robustness of how carbon dioxide, land and the orbital
configuration
affect the ITCZ and the monsoons as well as the interaction between the two. We note that this MIP is not part
of CMIP6, but we are very much interested in discussions and exchanges with CMIP6 activities related to monsoons
and the ITCZ.}

\section{Model setup}

The models are to be coupled to a slab ocean with time-mean zonal-mean ocean heat transport (``q-flux'') given in section
\ref{sec:qflux}. The boundary conditions largely follow the CMIP5 aquaplanet protocol except for interactive sea-surface
temperatures and the inclusion of a seasonal cycle in insolation. The following parameters should be specified:

\begin{itemize}
 \item CO$_\textrm{2}$: 348\,ppmv
 \item CH$_\textrm{4}$: 1650\,ppbv
 \item N$_\textrm{2}$O: 306\,ppbv
 \item No Halocarbons (CFCs)
 \item Ozone following values for the AquaPlanet Experiment (see \url{http://www.met.reading.ac.uk/~mike/APE/ape_ozone.html})
 \item Total solar irradiance: 1365\,Wm$^{-2}$
 \item No radiative effects of aerosols
 \item Diurnal cycle in insolation
 \item Calendar: a 360 days calendar (each month has 30 days) is desirable, but if such a calendar is not 
       available then a 365 days calendar without leap years should be used (second preference) or a 365 days
       calendar with leap years (third preference)
 \item Orbit: zero eccentricity, 23.5\degree obliquity, NH spring equinox on March 21; modelling groups should
       check the timing of the NH spring equinox by looking at daily TOA shortwave data because a correct timing
       is essential to meaningfully compare different models (a deviation from March 21 by 1-2 days is considered acceptable) 
 \item Slab ocean depth: 30\,m
 \item Sea-ice formation should be inhibited, and ocean temperatures should be allowed to cool below the freezing temperature
       of sea water \citep[see, e.g.,][]{kang_itcz_jclim2008,voigt_albedoitcz_jclim2014}
\end{itemize}

For the land simulations, a rectangular continent extending from 30\degree\,S to 30\degree\,N and 0\degree\,E to 45\degree\,E should
be introduced. The q-flux should be zeroed out over this region, which requires a small uniform
correction of the q-flux over ocean areas of -0.59\,Wm$^{-2}$ (see Sect.\,\ref{sec:qflux}).
Land should be represented as a very shallow ocean of 
depth 0.1\,m with increased surface albedo and with decreased evaporation. 

Regarding the land surface albedo: the land albedo should be the ocean albedo plus 0.07. So if the ocean
has an albedo of 0.06, the land should have an albedo of 0.13. For some models the ocean albedo is not a 
constant number but depends
on the zenith angle and the ratio of diffuse vs. direct radiation. In this case, the land
albedo should be the ocean albedo calculated for the specific conditions plus 0.07.

Regarding decreased evaporation over land: over land we aim
at a reduction of evaporation compared to ocean. This is achieved 
by rescaling the moisture transfer coefficient, $C_{q}$, which
affects the surface evaporation via
\begin{equation}
 E = C_{q} | \vec{V_1} | (q_1-q_s),
\end{equation}
with subscripts $1$ and $s$ denoting values at the lowest model level and at the surface.
Over land, the transfer coefficient $C_{q}$ should be halved, i.e.,
\begin{equation}
C_q \rightarrow C_{q}\cdot\frac{1}{2},
\end{equation}
which assuming no changes in surface wind
speed and boundary-layer humidity will reduce
the evaporation by a factor of 2.
Over ocean the transfer coefficient must not be decreased.

For the LandOrbit simulations, the orbital parameters should be set to
\begin{itemize}
 \item eccentricity: 0.02 %0.018682
 \item obliquity: 23.5\degree{}
 \item NH spring equinox on March 21
 \item longitude of perihelion=270\degree{}, implying that NH solstice occurs during aphelion.
\end{itemize}
This orbit roughly corresponds to Earth's comtemporary orbit \citep{joussaume_paleoorbits_jgr1997}.
The solar constant and greenhouse gases should be as in the other simulations.

\begin{table}[h]
\caption{List of requested simulations.}
\label{tab:jetshift_free_locked_aquaruns}
\centering
\begin{tabular}{lll}
\hline\hline
Simulation     & years   & specifications \\
\hline
AquaControl   & 15+30   & aquaplanet control simulation (includes 15 years of spin up)\\
Aqua4xCO2     & 40      & as AquaControl but with quadrupled CO$_\textrm{2}$,\\
              &         & to be restarted from end of AquaControl (Dec 30 of year 45)\\
LandControl   & 40      & as AquaControl but with idealized continent,\\
              &         & to be restarted from end of AquaControl (Dec 30 of year 45)\\
Land4xCO2     & 40      & as LandControl but with quadrupled CO$_\textrm{2}$,\\
              &         & to be restarted from end of LandControl (Dec 30 of year 40)\\
LandOrbit     & 40      & as LandControl but with changed orbital parameters,\\
              &         & to be restarted from end of LandControl (Dec 30 of year 40)\\
\hline\hline
\end{tabular}
\end{table}

\section{Requested simulations}

Table \ref{tab:jetshift_free_locked_aquaruns} lists the requested simulations. To quantify
the transient response and the adjusted radiative forcing, all of the simulations except 
AquaControl should be restarted from another simulation as described in the table. 


\section{Requested Output}

We request output following the standard output of CMIP5 (see \url{http://cmip-pcmdi.llnl.gov/cmip5/docs/standard_output.pdf}).
To facilitate the analysis of the simulations, the data should be ``cmorized''. By cmorizing we here mean that
the variables are named according to the CMIP5 names and have the same units
as in CMIP5, and that 3d-data is interpolated to the 17 CMIP5 pressure levels 
(1000, 925, 850, 700, 600, 500, 400, 300, 250, 200, 150, 100, 70, 50, 30, 20, 10 hPa)

\begin{itemize}
 \item Monthly-mean output: all fields of the CMOR Table Amon except fields related to
       carbon mass flux and mole fractions of ozone etc., saved for all years and simulations except
       the 15 years of initial spinup for AquaControl
 \item Daily output: same fields as for monthly-mean output, saved for the last 10 years of each simulation
 \item 3-hourly output: same fields as for monthly-mean output, saved for the last 3 years of each simulation
\end{itemize}

As for whether fields should be saved as means or snapshots, we adapt the CMIP5 conventions. 
For the monthly and daily output streams, all fields should be means over the output period. For the
3-hourly output stream, $T$ (including surface and atmosphere), $u$, $v$, $\omega$, $q$, $z$ (geopotential
height), should be
snapshots whereas all other fields should be means. 

\section{Specification of the ocean q-flux}
\label{sec:qflux}
The slab ocean allows interactive sea-surface temperatures without the large
computational burden from coupling to a dynamic three-dimensional ocean model.
The q-flux is derived from present-day observations, with details given below. It
is provided as a netcdf file that includes the q-flux for the
aquaplanet simulations and a corrected q-flux for the simulations
with land. The q-flux can also be implemented in the models by using
the polynomial representation that is given in section\,\ref{subsec:qflux_derivation}.
Fig.\,1 shows the q-flux and the corresponding ocean heat transport, and compares
the two with the present-day values. The q-flux is constant in time.
For the aquaplanet simulations, the q-flux is zonally-symmetric. For the land simulations,
the q-flux is zonally-symmetric over ocean regions and zero over land.


\begin{figure}
 \begin{center}
    \includegraphics[width=25pc]{./figs/qflux_oht.pdf} 
 \end{center}
 \caption{Q-flux and ocean heat transport from observations (blue)
          and the fit used in the MIP (green). The q-flux for
          simulations with land is given in red; note that in the case of land
          the q-flux shown is not the zonal-mean q-flux but the q-flux over ocean points
          only.}
 \label{fig:qflux_oht}
\end{figure}


\subsection{Derivation of the q-flux}
\label{subsec:qflux_derivation}

For the q-flux we make use of present-day observations of the annual-mean TOA radiation budget
and reanalysis estimates of the atmospheric energy transport ($ATM$). From the observed time-mean
estimate of the total ocean heat transport,
\begin{equation}
q_\textrm{obs}(\lambda,\varphi) = TOA(\lambda,\varphi)-ATM(\lambda,\varphi),
\end{equation}
we calculate the zonal-mean time-mean q-flux
\begin{equation}
\overline{q}_\textrm{obs}(\varphi) = \frac{1}{2\pi} \int_0^{2\pi} q_\textrm{obs} d\lambda.
\end{equation}
For the zonal average, land points are included with their q-flux being set to zero. I.e.,
the zonal average is not weighted by the sea-land mask. Then, we fit $\overline{q}_\textrm{obs}$
by a hemispherically-dependent polynomial of degree four to smooth out smaller-scale wiggles in the mid- and high-latitudes.
This is done to avoid that these wiggles aggravate model differences in the jet position,
and to avoid an imprint on these observed wiggles on the extratropical atmospheric circulation
in the idealized simulations pursued here. 
Specifically, the q-flux is given by
\begin{equation}
 q(\varphi) = p_0 + p_1 \cdot \varphi + p_2 \cdot \varphi^2 + p_3 \cdot \varphi^3 + p_4 \cdot \varphi^4.
\end{equation}
For the aquaplanet simulations, the polynomial coefficients are in the 
Northern hemisphere $(\varphi>0)$ 
\begin{eqnarray*}
 p^{NH}_0 & = &-50.1685     \\
 p^{NH}_1 & = &  4.9755     \\
 p^{NH}_2 & = & -1.4162\cdot 10^{-1}  \\
 p^{NH}_3 & = &  1.6743\cdot 10^{-3}  \\
 p^{NH}_4 & = & -6.8650\cdot 10^{-6}, \\
\end{eqnarray*}
where $\varphi$ has units of deg latitude.
In the Southern hemisphere $(\varphi<0)$,
\begin{eqnarray*}
 p^{SH}_0 & = & -56. 0193     \\     
 p^{SH}_1 & = &  -6.4824      \\
 p^{SH}_2 & = &  -2.3494\cdot 10^{-1}    \\
 p^{SH}_3 & = &  -3.4685\cdot 10^{-3}    \\
 p^{SH}_4 & = &  -1.7732\cdot 10^{-5}    \\
\end{eqnarray*}

As described above, for the land simulation the q-flux is set to zero over land.
This requires a small uniform correction of -0.59\,Wm$^{-2}$ to guarantee that the
global-mean q-flux is zero. The uniform correction is applied to all ocean points (but
not to the land points) by setting $p^{NH}_0 \rightarrow p^{NH}_0 -0.59$ and
$p^{SH}_0 \rightarrow p^{SH}_0 -0.59$.
\clearpage
\section{ECHAM6.1 reference plots}

This section provides plots for the ECHAM6.1 AquaControl and LandControl simulations
as a reference for other modelling groups. This does not mean
that other models must show similar or the exact same results, but the plots might
provide guidance to check whether the setup was implemented correctly.
We particularly encourage modelling groups
to check the top-of-atmosphere solar insolation,
the land surface albedo diagnosed from the surface shortwave fluxes,
and the q-flux over ocean, which can be diagnosed as the sum
of shortwave and longwave radiative fluxes and the latent
and sensible heat fluxes.

\subsection{TOA shortwave downward insolation}

\begin{figure}[h!]
 \begin{center}
    \includegraphics[width=40pc]{./figs/echam61_ldeo_aquacontrol_toaswdown.pdf} 
 \end{center}
 \caption{Top-of-atmosphere downward shortwave irradiance.}
\end{figure}

\subsection{AquaControl}

\begin{figure}[h!]
 \hspace{-2cm}\includegraphics[width=50pc]{./figs/echam61_ldeo_aquacontrol_qflux_heattransport.pdf} 
 \caption{ECHAM6.1 AquaControl: ocean q-flux (right) and meridional energy transport
          of the ocean (middle) and the atmosphere (right).}
\end{figure}

\begin{figure}[h!]
 \begin{center}
    \includegraphics[width=40pc]{./figs/echam61_ldeo_aquacontrol.pdf} 
 \end{center}
 \caption{ECHAM6.1 AquaControl: seasonal cycle of tropical precipitation (top left)
          and surface temperature (bottom left). Right: time-mean zonal-mean
          precipitation and surface temperature; global-mean values are given on top
          of the plots.}
\end{figure}

\clearpage
\subsection{LandControl}

\begin{figure}[h!]
 \hspace{-2cm}\includegraphics[width=50pc]{./figs/echam61_ldeo_itczmip003_qflux_heattransport.pdf} 
 \caption{ECHAM6.1 LandControl: ocean q-flux over ocean points (right), and meridional energy transport
          of the ocean (middle) and the atmosphere (right).}
\end{figure}

\begin{figure}[h!]
 \includegraphics[width=40pc]{./figs/echam61_ldeo_itczmip003_sfcalbedo.pdf} 
 \caption{ECHAM6.1 LandControl: time-mean surface albedo calculated by the surface downward and upward shortwave
          radiative flux.}
\end{figure}

\begin{figure}[h!]
 \hspace{-2cm}\includegraphics[width=50pc]{./figs/echam61_ldeo_tsurf_precip_itczmip003.pdf} 
 \caption{ECHAM6.1 LandControl: seasonal cycle of tropical precipitation (left)
          and surface temperature (bottom left). Right: time-mean zonal-mean
          precipitation and surface temperature; global-mean values are given on top
          of the plots.}
\end{figure}

\begin{figure}[h!]
 \hspace{-2cm}\includegraphics[width=50pc]{./figs/echam61_ldeo_tsurf_precip_oceanonly_itczmip003.pdf} 
 \caption{ECHAM6.1 LandControl: seasonal cycle of tropical precipitation (left)
          and surface temperature (bottom left) only over ocean points. Right: time-mean zonal-mean
          precipitation and surface temperature only taking into account ocean points.}
\end{figure}
  
\begin{figure}[h!]
 \hspace{-2cm}\includegraphics[width=50pc]{./figs/echam61_ldeo_tsurf_precip_landonly_itczmip003.pdf} 
 \caption{ECHAM6.1 LandControl: seasonal cycle of tropical precipitation (left)
          and surface temperature (bottom left) only over land points. Right: time-mean zonal-mean
          precipitation and surface temperature only taking into account land points.}
\end{figure}

\begin{figure}[h!]
 \hspace{-2cm}\includegraphics[width=50pc]{./figs/echam61_ldeo_tsurf_sfcfluxes_itczmip003.pdf} 
 \caption{ECHAM6.1 LandControl: time-mean surface temperature, precipitation and surface latent and
          sensible heat fluxes.}
\end{figure}


\clearpage
\vspace*{0.1cm}
\titleformat{\section}{\normalsize\selectfont\itshape}{\thesection}{0em}{}
\bibliographystyle{/home/aiko/Dropbox/BibTEX/ametsoc}	
\bibliography{/home/aiko/Dropbox/BibTEX/my_entire_bibliography}


\end{document}


