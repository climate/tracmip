# -*- coding: utf-8 -*-
"""
Created on Sun Nov  9 15:09:22 2014

@author: aiko
"""

import netCDF4 as nc
import scipy.stats as spstats
import scipy.optimize as spopt
import numpy as np
import matplotlib.pyplot as plt

# load my own modules
import sys
sys.path.append('/home/aiko/Dropbox/Python3/mymodules/')
import atmosphere as atm


def func_fit_qflux(x,p0,p1,p2,p3,p4):
    lat2rad=1#np.pi/180.0
    return p0+p1*(lat2rad*x)+p2*(lat2rad*x)**2+p3*(lat2rad*x)**3+p4*(lat2rad*x)**4

file = nc.Dataset('/home/aiko/Dropbox/Projects/current/Tracmip/sim_protocol/data/surfaceflux_r180x90.nc','r')
qflux=np.array(file.variables['flux_oh'])
lat = np.array(file.variables['lat'])
lon = np.array(file.variables['lon'])

# 1-dimensonal area
file  = nc.Dataset('/home/aiko/Dropbox/Projects/current/Tracmip/sim_protocol/data/surfaceflux_r180x90_gridarea.zonmean.nc','r')
area1d= np.array(file.variables['cell_area'])
area1d=area1d[:,0] # make sure that area1d has np.shape=(90,); if not vector
                   # multiplication of area1d with qflux fails

# time mean qflux
qflux_tm=np.mean(qflux,axis=0)

# construct land sea mask: ocean=1, land=NaN
slm=1.0+0*qflux_tm
for j in range(0,lat.size):
   for i in range(0,lon.size):
      if qflux_tm[j,i]==0: slm[j,i]=np.NaN

# zonal mean q-flux
# version1: treat land points as zero
qflux_tmzm_v1 = spstats.nanmean(qflux_tm,axis=1)
# version2: treat land points as NaN, this will give
# larger zonal-mean q-flux
qflux_tmzm_v2 = spstats.nanmean(qflux_tm*slm,axis=1)
# set NaN from nanmean to zero
qflux_tmzm_v2 = np.nan_to_num(qflux_tmzm_v2)

# make sure that global-mean of zonal-mean q-flux is zero, this is 
# not guaranteed for version2
qflux_tmzm_v1=qflux_tmzm_v1 - np.sum(qflux_tmzm_v1*area1d)/np.sum(area1d)
qflux_tmzm_v2=qflux_tmzm_v2 - np.sum(qflux_tmzm_v2*area1d)/np.sum(area1d)

print("global mean of qflux_tmzm_v1:")
print(np.sum(qflux_tmzm_v1*area1d)/np.sum(area1d))

# fit the qflux_tmzm
qflux_fit=0*qflux_tmzm_v1
indlat_nh=np.where(lat>=0)
p_nh,_=spopt.curve_fit(func_fit_qflux, lat[indlat_nh], qflux_tmzm_v1[indlat_nh])
indlat_sh=np.where(lat<0)
p_sh,_=spopt.curve_fit(func_fit_qflux, lat[indlat_sh], qflux_tmzm_v1[indlat_sh])
qflux_fit[indlat_nh]=func_fit_qflux(lat[indlat_nh],p_nh[0],p_nh[1],p_nh[2],p_nh[3],p_nh[4])
qflux_fit[indlat_sh]=func_fit_qflux(lat[indlat_sh],p_sh[0],p_sh[1],p_sh[2],p_sh[3],p_sh[4])

print("global mean of qflux_fit:")
print(np.sum(qflux_fit*area1d)/np.sum(area1d))

print("polynomial factors for NH:")
print(p_nh)
print("polynomial factors for SH:")
print(p_sh)

# find glabal mean imbalance if we put in continent from 30N-30S and 0-45 deg lon
# and keep qflux_fit the same (but cut out the continent and thus remove
# part of the tropical q-flux)
area1d_withland=1*area1d # 1* is important, otherwise we do not create new array but
                         # merely a new reference to qflux_fit, which will bring
                         # us into trouble
# ocean area reduced between 30N-30S to 7/8:
for i in range(0,lat.size):
    if (abs(lat[i])<=30): 
        area1d_withland[i]=(360-45)/360 * area1d[i]

imbalance_withland=np.sum(qflux_fit*area1d_withland)/np.sum(area1d_withland)
print("Global-mean imbalance due to introducing continent:")
print(imbalance_withland)

# qflux_fit_withland is qflux_fit corrected for imbalance_withland
qflux_fit_withland=1*qflux_fit - imbalance_withland

print('Global mean of qflux_fit_withland:')
print(np.sum(qflux_fit_withland*area1d_withland)/np.sum(area1d_withland))

#print(np.shape(qflux_fit*area1d))
# make sure qflux_fit has global-mean of zero
#print(np.sum(qflux_fit*area1d)/np.sum(area1d))
#qflux_fit=qflux_fit - np.sum(qflux_fit*area1d)/np.sum(area1d)
#print(np.sum(qflux_fit*area1d)/np.sum(area1d))


# calculate oht in PW
oht_tmzm_v1 = atm.get_atmenergytransport(-1*qflux_tmzm_v1,lat)
oht_tmzm_v2 = atm.get_atmenergytransport(-1*qflux_tmzm_v2,lat)
oht_fit     = atm.get_atmenergytransport(-1*qflux_fit,lat)
oht_fit_withland = atm.get_atmenergytransport(-1*(qflux_fit_withland*area1d_withland/area1d), lat)

#plotting
plt.figure(figsize=(12, 4),dpi=80,facecolor='w',edgecolor='k')

ax=plt.subplot(1, 2, 1)
ax.spines['right'].set_color('none')
ax.spines['top'].set_color('none')
ax.xaxis.set_ticks_position('bottom')
ax.yaxis.set_ticks_position('left')
ax.xaxis.set_ticks([-89.5,-60,-30,0,30,60,89.5])
ax.xaxis.set_ticklabels(['90S','60S','30S','Eq','30N','60N','90N'], fontsize=10)
ax.yaxis.set_ticks([-60, -45, -30, -15, 0, 15, 30])
ax.yaxis.set_ticklabels([-60, -45, -30, -15, 0, 15, 30], fontsize=10)
plt.xlim(-89, 89), plt.ylim(-60, 30)
plt.plot([-100, 100], [0, 0], 'k--')
plt.plot(lat,qflux_tmzm_v1, color='gray', linewidth=2)
#plt.plot(lat,qflux_tmzm_v2)
plt.plot(lat,qflux_fit, color='royalblue', linewidth=2)
plt.plot(lat,qflux_fit_withland, color='chocolate', linewidth=2)
plt.text(1, 0.15, 'present-day', color='gray', fontsize=12, ha='right', transform=ax.transAxes)
plt.text(1, 0.1 , 'Tracmip aquaplanet', color='royalblue', fontsize=12, ha='right', transform=ax.transAxes)
plt.text(1, 0.05, 'Tracmip with continent', color='chocolate', fontsize=12, ha='right', transform=ax.transAxes)
plt.ylabel(r'Wm$^{-2}$', fontsize=12)
plt.title('q-flux at ocean grid cells', fontsize=14)
plt.text(0.02, 1.0, 'a)', fontsize=14, ha='left', va='center', transform=ax.transAxes)

ax=plt.subplot(1, 2, 2)
ax.spines['right'].set_color('none')
ax.spines['top'].set_color('none')
ax.xaxis.set_ticks_position('bottom')
ax.yaxis.set_ticks_position('left')
ax.xaxis.set_ticks([-89.5,-60,-30,0,30,60,89.5])
ax.xaxis.set_ticklabels(['90S','60S','30S','Eq','30N','60N','90N'], fontsize=10)
ax.yaxis.set_ticks([-1.0, 0, 1.0, 2.0])
ax.yaxis.set_ticklabels([-1.0, 0, 1.0, 2.0], fontsize=10)
plt.xlim(-89, 89), plt.ylim(-1.5, 2.1)
plt.plot([-100, 100], [0, 0], 'k--')
plt.plot(lat,oht_tmzm_v1, color='gray', linewidth=2)
#plt.plot(lat,oht_tmzm_v2)
plt.plot(lat,oht_fit, color='royalblue', linewidth=2)
plt.plot(lat,oht_fit_withland, color='chocolate', linewidth=2)
plt.ylabel('PW', fontsize=12)
plt.title('Ocean energy transport', fontsize=14)
plt.text(0.02, 1.0, 'b)', fontsize=14, ha='left', va='center', transform=ax.transAxes)

plt.tight_layout
plt.savefig('qflux_oht.pdf')

#plt.figure()        
#plt.plot(lat,qflux_fit)
#plt.plot(lat,qflux_fit_withland)    
#plt.plot(lat,qflux_fit-qflux_fit_withland)

#plt.figure()
#plt.plot(lat,area1d)
#plt.plot(lat,area1d_withland)

print([lat[45], oht_fit[45]])
print([lat[44], oht_fit[44]])
print( 0.5*(oht_fit[45] + oht_fit[44]) )


print(p_nh-imbalance_withland)
print(p_sh-imbalance_withland)

plt.show()
    
#---------------------------------------
# save to netcdf files
qflux_fit_3d=np.zeros((12,lat.size,lon.size))
qflux_fit_withland_3d=np.zeros((12,lat.size,lon.size))
for j in range(0,lat.size):
    qflux_fit_3d[:,j,:]=qflux_fit[j]
    for i in range(0,lon.size):
        if ((abs(lat[j])<=30) & (lon[i]>=0) & (lon[i]<=45)):
            qflux_fit_withland_3d[:,j,i]=0
        else:
            qflux_fit_withland_3d[:,j,i]=qflux_fit_withland[j]
    
 
root_grp = nc.Dataset('qflux_idealized.nc', 'w',clobber=True, format='NETCDF3_CLASSIC')
root_grp.description = 'Idealized Q-flux'

# dimensions
root_grp.createDimension('time', None)
root_grp.createDimension('lat', 90)
root_grp.createDimension('lon', 180)

# variables
times      = root_grp.createVariable('time', 'f8', ('time',))
latitudes  = root_grp.createVariable('latitude', 'f4', ('lat',))
longitudes = root_grp.createVariable('longitude', 'f4', ('lon',))
qflux_fit_3d_data = root_grp.createVariable('qflux_aqua', 'f4', ('time', 'lat', 'lon',))
qflux_fit_withland_3d_data = root_grp.createVariable('qflux_withland', 'f4', ('time', 'lat', 'lon',))

# data
latitudes[:]  = lat
longitudes[:] = lon
qflux_fit_3d_data[:,:,:] = qflux_fit_3d
qflux_fit_withland_3d_data[:,:,:] = qflux_fit_withland_3d

root_grp.close()